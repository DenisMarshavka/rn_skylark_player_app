import React, { useEffect, useState } from 'react'
import { AppRegistry, AppState } from 'react-native';
import App from './src/navigation/navigation';
import { name as appName } from './app.json';
import { Provider, useDispatch, useSelector } from 'react-redux'
import store from './src/store/store'

import TrackPlayer from 'react-native-track-player';
import SplashScreen from 'react-native-splash-screen'
import { SUBSCRIPTION_SKU } from './src/constants/api_config';
import { toggleSubscribe, toggleActiveSubscription } from './src/store/appReducer';
import RNIap, { purchaseErrorListener, purchaseUpdatedListener, finishTransaction, finishTransactionIOS, acknowledgePurchaseAndroid } from 'react-native-iap';
import { instance } from './src/api/api';
import { getToken } from "./src/store/authReducer";

const AppWithSubscriptionStatus = () => {

    const dispatch = useDispatch()
    const isSubscribed = useSelector(state => state.appReducer.isSubscribed)
    const isActiveSubscription = useSelector(state => state.appReducer.isActiveSubscription)

    const isUserSubscriptionActive = async (subscriptionId) => {
        const connection = await RNIap.initConnection()
        if (connection) {
            const availablePurchases = await RNIap.getAvailablePurchases();
            if (availablePurchases !== null && availablePurchases.length > 0) {
                const subscription = availablePurchases.find((element) => {
                    return subscriptionId === element.productId;
                });
                if (subscription && subscription.autoRenewingAndroid === true) {
                    dispatch(toggleSubscribe(true))
                    dispatch(toggleActiveSubscription(true))
                    console.log('User is subscribe')
                } 
                else {
                    dispatch(toggleSubscribe(false))
                    dispatch(toggleActiveSubscription(false))
                    console.log('no subscription')
                }
            } else {
                console.log('User is unsubscribe')
                dispatch(toggleSubscribe(false))
                dispatch(toggleActiveSubscription(false))
            }
        }
    }

    // const isNewUser = async () => {
    //     try {
    //         const token = await getToken();
    //         let data = await instance.get(`user/get_info?token=${token}`).then( res => res.data )

    //         if (data.data && data.data.subscribe === 1) {
    //             console.log('subscribe === 1')
    //             dispatch(toggleSubscribe(true))
    //         }
    //     } catch (e) {
    //         console.log(e)
    //     }
    // }

    useEffect(() => {
        // async function getInfo() {
            isUserSubscriptionActive(SUBSCRIPTION_SKU)
        //     await isNewUser()
        // }
        // getInfo()
    }, [])

    useEffect(()=> {
        console.log('isSubscribed', isSubscribed)
        console.log('isActiveSubscription', isActiveSubscription)
    }, [isSubscribed, isActiveSubscription])

    return <App />
}

const AppWithStore = (props) => {

    let purchaseUpdateSubscription = null
    let purchaseErrorSubscription = null

    useEffect(() => {
        purchaseUpdateSubscription = purchaseUpdatedListener(
            async (purchase) => {
                const receipt = purchase.transactionReceipt;
                console.log('receipt', receipt)

                if (receipt) {
                    try {
                        if (Platform.OS === 'ios') {
                            finishTransactionIOS(purchase.transactionId);
                        } else if (Platform.OS === 'android') {
                            let res = await acknowledgePurchaseAndroid(purchase.purchaseToken)
                            console.log('acknowledgePurchaseAndroid', res)
                        }

                        let res = await finishTransaction(purchase);
                        console.log('finishTransaction', res)
                    } catch (ackErr) {
                        console.log('ackErr', ackErr);
                    }
                }
            },
        );
        purchaseErrorSubscription = purchaseErrorListener(
            (error) => console.log('purchaseErrorListener', error)
        );

        return (() => {
            if (purchaseUpdateSubscription) {
                purchaseUpdateSubscription.remove();
                purchaseUpdateSubscription = null;
            }
            if (purchaseErrorSubscription) {
                purchaseErrorSubscription.remove();
                purchaseErrorSubscription = null;
            }
            RNIap.endConnection();
        })
    }, []);

    useEffect(() => {
        AppState.addEventListener('change', handleAppStateChange);
        SplashScreen.hide()

        return () => AppState.removeEventListener('change', handleAppStateChange)
    }, [])

    const handleAppStateChange = (nextAppState) => {
        if (nextAppState === 'inactive') {
            console.log('the app is closed');
            TrackPlayer.stop();
            TrackPlayer.remove();
        }
    };

    return <Provider store={store}><AppWithSubscriptionStatus /></Provider>
}

AppRegistry.registerComponent(appName, () => AppWithStore);
TrackPlayer.registerPlaybackService(() => require('./src/service/playerService'));
