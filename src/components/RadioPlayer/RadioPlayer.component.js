import React, {forwardRef, useMemo} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';

import PlayBackIcon from 'icons/playBack';
import PlayForwardIcon from 'icons/playForward';
import RadioPlayIcon from 'icons/radioPlay';

import MusicCover from 'components/MusicCover/MusicCover';

const {height} = Dimensions.get('screen');

const RadioPlayer = forwardRef(({currentRadio}, ref) => {
  const widgetHeight = useMemo(() => (height > 800 ? 730 : height), [height]);

  return (
    <RBSheet
      ref={ref}
      height={widgetHeight}
      closeOnDragDown={true}
      customStyles={{
        wrapper: {
          backgroundColor: 'rgba(51, 51, 51, 0.48)',
        },
        draggableIcon: {
          width: 134,
          backgroundColor: '#E4E4E4',
        },
      }}>
      {
        currentRadio && currentRadio.name &&

        (
          <SafeAreaView style={styles.container}>
            <ImageBackground
              style={styles.img}
              resizeMode="cover"
              source={require('../../assets/edward-cisneros-KoKAXLKJwhk-unsplash.png')}
              blurRadius={1}>
              <MusicCover image="https://www.radiobells.com/stations/tntmusicradio.jpg">
                <View>
                  <Text style={styles.title}>{currentRadio.name}</Text>
                  <Text style={styles.text}>Прямой эфир</Text>
                </View>
              </MusicCover>

              <View style={styles.actions}>
                <TouchableOpacity>
                  <PlayBackIcon style={styles.icon} />
                </TouchableOpacity>

                <TouchableOpacity>
                  <RadioPlayIcon width={76} height={76} />
                </TouchableOpacity>

                <TouchableOpacity>
                  <PlayForwardIcon style={styles.icon} />
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </SafeAreaView>
        )
      }
    </RBSheet>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
  },
  img: {
    height: height - 100,
  },
  icon: {
    marginHorizontal: 12,
  },
  title: {
    fontWeight: '600',
    fontSize: 20,
    color: '#fff',
    marginBottom: 8,
    marginTop: 90,
    textAlign: 'center',
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  text: {
    fontSize: 18,
    color: '#fff',
    textAlign: 'center',
  },
  playButtonContainer: {
    backgroundColor: '#FFF',
    borderColor: 'rgba(93, 63, 106, 0.2)',
    borderWidth: 16,
    width: 128,
    height: 128,
    borderRadius: 64,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 32,
    shadowColor: '#5D3F6A',
    shadowRadius: 30,
    shadowOpacity: 0.5,
  },
});

export default RadioPlayer;
