import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  SafeAreaView,
  View,
} from 'react-native';
import ArrowLeftIcon from '../../icons/arrowLeft';

const CustomHeader = ({
  title,
  pressGoBack,
  onIconPress,
  icon,
  style,
  darkMode,
  isPlayLists,

  withoutHeaderButtonMore = true
}) => {
  // console.log('isPlayLists: ', isPlayLists);

  return (
    <View style={[style, styles.container]}>
      <View
        style={styles.headerWrapper}>
        <TouchableOpacity style={[ {...styles.arrowBackWrapper}, {marginLeft: isPlayLists ? 10 : 0} ]} onPress={() => pressGoBack()} >
          <ArrowLeftIcon style={[ {...styles.icon}, {marginLeft: isPlayLists ? 5 : 0} ]} color={darkMode ? 'white' : 'black' } />
        </TouchableOpacity>

        <Text style={[ styles.title, {color: darkMode ? 'white' : 'black'} ]}>{title}</Text>
      </View>

      {!withoutHeaderButtonMore && (
        <TouchableOpacity onPress={onIconPress} style={styles.customIcon}>
          {icon}
        </TouchableOpacity>
      )}
    </View>
  );
};

export default CustomHeader;

const styles = StyleSheet.create({
  headerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  // icon: {
  //   marginLeft: 5,
  // },
  customIcon: {
    marginRight: 10,
  },
  container: {
    marginVertical: 15,
    marginBottom: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  arrowBackWrapper: {
    // width: 25,
    // height: 26,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 22,
    fontWeight: '500',
    marginLeft: 33,
  },
});
