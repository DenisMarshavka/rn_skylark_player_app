import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import ArrowLeftIcon from '../../icons/arrowLeft';
import {COLORS} from '../../constants/colors.constants';
import { getStatusBarHeight } from "react-native-status-bar-height";


const NavigationBlank = ({title, buttonText, children, enableScroll, goBack, onPressBtn, darkMode}) => {
  return (
    <SafeAreaView style={[styles.blankContainer, {backgroundColor: darkMode ? COLORS.black : COLORS.white,}]}>
      <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'}
          backgroundColor={darkMode ? '#000' : '#F9F9F9'} />
      <TouchableOpacity style={styles.header} onPress={goBack}>
        <ArrowLeftIcon style={styles.icon} color={darkMode ? COLORS.white : COLORS.black} />
        <Text style={[styles.titleText, {color: darkMode ? COLORS.white : COLORS.black }]}>{title}</Text>
      </TouchableOpacity>

      {enableScroll ? (
        <ScrollView contentContainerStyle={styles.contentStyle}>
          {children}
          <TouchableOpacity style={styles.btnWrapper}>
            <Text style={styles.btnText}>{buttonText || 'Далее'}</Text>
          </TouchableOpacity>
        </ScrollView>
      ) : (
        <View style={styles.contentStyle}>
          {children}

          <TouchableOpacity style={styles.btnWrapper} onPress={onPressBtn}>
            <Text style={styles.btnText}>{buttonText || 'Далее'}</Text>
          </TouchableOpacity>
        </View>
      )}
    </SafeAreaView>
  );
};

export default NavigationBlank;

const styles = StyleSheet.create({
  blankContainer: {
    flex: 1,
    width: '100%',
  },
  header: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: getStatusBarHeight()
  },
  icon: {
    marginRight: 20,
  },
  titleText: {
    fontSize: 22,
    fontWeight: 'bold',
    marginVertical: 25,
    textAlign: 'left',
  },
  contentStyle: {
    width: '100%',
    alignItems: 'center',
  },
  btnWrapper: {
    backgroundColor: COLORS.purple,
    width: 173,
    height: 50,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 60,
  },
  btnText: {
    color: COLORS.white,
    fontSize: 18,
  },
});

