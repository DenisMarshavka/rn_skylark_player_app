import React from 'react';
import Toggle from 'toggle-switch-react-native';

import {palette} from 'state/palette';

export const Switch = ({ checked = false, onChange }) => (
    <Toggle
        isOn={checked}
        // useNativeDriver={true}
        onColor={palette.common.yellow}
        offColor={palette.common.lightGrey}
        onToggle={checked => onChange(checked)}
    />
);
