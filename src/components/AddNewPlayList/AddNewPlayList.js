import React from 'react';
import { Dimensions, TouchableOpacity, StyleSheet } from "react-native";

import {COLORS} from "../../constants/colors.constants";
import SvgPlaylistIcon from "../../icons/playlist";
import {Text} from "react-native";

const screenWidth = Dimensions.get('window').width;

const AddNewPlayList = ({ newStyles, setAddPlaylistModalVisible = () => {}, darkMode }) => (
    <TouchableOpacity
        style={[ styles.newPlaylistBlock, {...newStyles}, {backgroundColor: darkMode ? 'rgba(234, 234, 234, 0.2)' : COLORS.lightgrey} ]}
        onPress={setAddPlaylistModalVisible}
    >
        <SvgPlaylistIcon width={37} height={45} color={COLORS.grey} />

        <Text style={{ color: COLORS.grey, fontSize: 16, fontWeight: 'bold', position: 'absolute', bottom: 14 }}>Новый плейлист</Text>
    </TouchableOpacity>
);

export default AddNewPlayList;

const styles = StyleSheet.create({
    newPlaylistBlock: {
        width: screenWidth / 2 - 20,
        height: screenWidth / 2 - 25,
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
    },
});
