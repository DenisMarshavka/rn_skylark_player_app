import React from 'react';
import { Text, StyleSheet, ImageBackground, Dimensions } from 'react-native';

import Ripple from 'react-native-material-ripple';
import {logoImage} from '../../constants/default_images'

const screenWidth = Dimensions.get('window').width;

export const MusicCard = ({ name, image, onPress, onLongPress, resizeItem = false, style }) => {
  const heightItem = ( (screenWidth / (screenWidth > 420 ? 3.6 : 4.87)) - 20 );
  const widthItem = ( (screenWidth / (screenWidth > 420 ? 2.8 : 2.2)) - 20 );

  return (
      <Ripple
          onPress={onPress}
          onLongPress={onLongPress}
          style={[ styles.container, { height: !resizeItem ? heightItem : screenWidth / 2.5 , width: !resizeItem ? widthItem : (screenWidth / 2.34) }, { ...style } ]}>
        <ImageBackground
            imageStyle={{resizeMode: 'cover'}}
            source={ image ? {uri: image} : logoImage}
            style={styles.background}>

          <Text numberOfLines={2} style={styles.title}>{name}</Text>
        </ImageBackground>
      </Ripple>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: 155,
    marginBottom: 15,
    borderRadius: 8,
    zIndex: 999,
    overflow: 'hidden',
    position: 'relative',
  },
  title: {
    maxWidth: '88%',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    left: 10,
    bottom: 14,

    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
    backgroundColor: 'rgba(0,0,0,0.3)'
  },
  background: {
    height: '100%',
    borderRadius: 8
  },
});
