import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import {toggleBottomBar} from '../../store/appReducer'
import {connect} from 'react-redux'


const SubscriptionModal = ({ closeModal, darkMode, navigation, forDownloading, toggleBottomBar }) => {

    return (
        <View style={[styles.centeredView, { backgroundColor: darkMode ? '#272727': '#fff' }, { justifyContent: 'space-evenly' }]}>
            <Text style={[styles.subscriptionTitle, { color: darkMode ? 'white' : 'black' }]}>Для { forDownloading ? 'скачивания' : 'прослушивания'} музыки{'\n'}необходимо подписаться!</Text>

            <TouchableOpacity 
                style={[styles.subscriptionBtn, { borderColor: darkMode ? 'yellow' : 'green' }]}
                onPress={ () => {
                    closeModal()
                    toggleBottomBar(true)
                    navigation.navigate('ProfileNavigator')
                }}
            >
                <Text style={{ color: darkMode ? 'yellow' : 'green' }}>Подписаться</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.subscriptionCloseBtn} onPress={closeModal} >
                <Text style={{ color: darkMode ? 'white' : 'brown' }}>Закрыть</Text>
            </TouchableOpacity>
        </View>
    );
};

const mapStateToProps = state => ({}) 
const mapDispatchToProps = {
    toggleBottomBar
}

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionModal)

const styles = StyleSheet.create({
   
    centeredView: {
        width: 245,
        height: 210,
        borderRadius: 8,
        alignItems: 'center'
    },
    subscriptionTitle: {
        textAlign: 'center', 
        fontSize: 16
    },
    subscriptionBtn: {
        width: '60%', 
        borderColor: 'blue', 
        borderWidth: 1, 
        borderRadius: 16, 
        padding: 10, 
        alignItems: 'center'
    },
    subscriptionCloseBtn: {
        width: '60%', 
        padding: 10, 
        alignItems: 'center', 
        position: 'absolute', 
        bottom: 4
    }
});
