import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Ripple from 'react-native-material-ripple';
import ArrowRightIcon from 'icons/arrowRight';
import {palette} from '../../state/palette';
import TextInputMask from 'react-native-text-input-mask';

export function NavigationItem({title, onPress, extra, theme, isPhone}) {
  return (
    <Ripple onPress={onPress}>
      <View style={styles.wrapper}>
        <Text style={[styles.subTitle, theme === 'light' ? {color: '#000'} : {color: '#fff'} ]}>{title}</Text>
        <View style={styles.extraWrapper}>
          { isPhone ?
              extra && <Text style={styles.extraText}>{
                `+${extra.startsWith('+7') ? extra.substr(1, 1) : extra.substr(1, 2)} (***) ** ** ${extra.substr(-3, 3)}`
              }</Text>
            : extra && <Text style={styles.extraText}>{extra}</Text>
          }
          <ArrowRightIcon />
        </View>
      </View>
    </Ripple>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    paddingVertical: 16,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  extraWrapper: {
    flexDirection: 'row',
  },
  extraText: {
    fontSize: 16,
    color: '#C5C5C5',
    marginRight: 8,
  },
  subTitle: {
    fontWeight: '400',
    fontSize: 18,
    lineHeight: 24,
  },
  icon: {
    fontSize: 20,
    width: 40,
    color: '#9D9D9E',
  },
});
