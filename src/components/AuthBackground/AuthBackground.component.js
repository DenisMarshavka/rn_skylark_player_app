import React from 'react'
import { ImageBackground, StatusBar, StyleSheet, View } from 'react-native'

const AuthBackground = ({ children }) => {
	return (
		<>
			<StatusBar translucent backgroundColor="transparent" />
			<ImageBackground
				source={require('../../assets/marcela-laskoski-YrtFlrLo2DQ-unsplash.png')}
				style={{ flex: 1 }}
			>
				<View style={styles.contentWrapper}>
					{children}
				</View>
			</ImageBackground>
		</>
	);
};

export default AuthBackground

const styles = StyleSheet.create({
    contentWrapper: {
        flex: 1,
		backgroundColor: 'rgba(0, 0, 0, 0.7)',
		alignItems: 'center',
		justifyContent: 'center'
    }
})