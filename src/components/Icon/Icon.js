import React, {Component} from 'react'
import {Image, TouchableOpacity} from 'react-native'

export const Icon = ({src}) => {

    return (
        <Image
            source={`${src}`}
            style={{width : 21, height : 21}}
        />
    )

}

export default Icon