import React, { useState, useEffect, useRef, useCallback } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, Image, Platform } from 'react-native';
import { COLORS } from '../../constants/colors.constants'
import { palette } from 'state/palette';
import { connect } from 'react-redux';
import TrackPlayer, {
    STATE_NONE, // 0
    STATE_READY, // 2
    STATE_PLAYING, // 3
    STATE_PAUSED, // 2
    STATE_STOPPED, // 1
    STATE_BUFFERING, // 6
    STATE_CONNECTING, // 8
} from 'react-native-track-player';

import RadioPlayIcon from 'icons/radioPlay';
import PlayBackIcon from 'icons/playBack';
import PlayForwardIcon from 'icons/playForward';
import CloseIcon from '../../icons/close';
import { getSingersName } from '../../utils/functions';
import {defaultImage} from "../../constants/default_images";
import { MINI_PLAYER_HEIGHT } from '../../constants/styles';

import {
    miniPlayerStart,
    miniPlayerStop,
    miniPlayerSetTrackAction,
} from '../../actions/miniPlayer';
import SubscriptionModal from '../SubscriptionModal/SubscriptionModal';
import Modal from 'react-native-modal';

const MiniPlayer = props => {
    const {
        darkMode,
        isRadioPlayer,
        track,
        isActive,
        miniPlayerStart,
        miniPlayerSetTrackAction,
        miniPlayerStop,
        infinityPlaying,
        isRepeat,
    } = props;

    let [playerState, setPlayerState] = useState(null)

    let onPlaybackStateSubscribe = useRef(null);
    let onPlayerStopSubscribe = useRef(null);

    useEffect(() => {
        onPlaybackStateSubscribe.current = TrackPlayer.addEventListener('playback-state', ({ state }) => {
            setPlayerState(state)
        });

        onPlayerStopSubscribe.current = TrackPlayer.addEventListener('remote-stop', () => {
            miniPlayerStop();
        });

        return () => {
            if (onPlaybackStateSubscribe.current) {
                onPlaybackStateSubscribe.current.remove();
            }

            if (onPlayerStopSubscribe.current) {
                onPlayerStopSubscribe.current.remove();
            }
        };
    }, []);

    const start = async () => {

        if (playerState === STATE_STOPPED) { // 1
            await TrackPlayer.pause();
            TrackPlayer.seekTo(0).then(() => TrackPlayer.play());
        } if (playerState === STATE_PAUSED) { // 2
            await TrackPlayer.play()
        } else {
            await TrackPlayer.pause()
        }

    };

    const onClose = () => {
        TrackPlayer.destroy();
        miniPlayerStop();
    };

    const onSkipTo = async (type) => {
        // type: 'next' or 'previous'
        const event = type;

        const currentTrack = await TrackPlayer.getCurrentTrack();

        const skip = async (event) => {
            if (event === 'next') {
                await TrackPlayer.skipToNext().catch((err) => {});
            } else {
                await TrackPlayer.skipToPrevious().catch((err) => {});
            }
        };

        if (isRadioPlayer) {
            await skip(event);
        } else {
            if (isRepeat) {
                TrackPlayer.skip(currentTrack);
            } else {
                const queue = await TrackPlayer.getQueue();
                const firstTrackId = queue[0].id;
                const lastTrackId = queue[queue.length - 1].id;

                if (currentTrack === lastTrackId && event === 'next' && infinityPlaying) {
                    await TrackPlayer.skip(firstTrackId);
                } else if (currentTrack === firstTrackId && event === 'previous' && infinityPlaying) {
                    await TrackPlayer.skip(lastTrackId);
                } else {
                    await skip(event);
                }
            }
        }
    };

    let [subscriptionModalIsVisible, setSubscriptionModalIsVisible] = useState(false)

	const subscribe = () => {
		setSubscriptionModalIsVisible(true)
	}

    return (
        <>
		<View style={[styles.container, { backgroundColor: darkMode ? '#242424' : '#FCFCFC', }]}>
            <View style={styles.closeAction}>

                <TouchableOpacity
                    onPress={onClose}
                    hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
                >
                    <CloseIcon color={darkMode ? '#FCFCFC' : '#242424'} />
                </TouchableOpacity>

            </View>

			<Image
				source={
					track && track.photo ? {uri: track.photo} : defaultImage
				}
				style={styles.cover}
			/>

			<View style={styles.trackInfo}>
				<Text
					style={[styles.title, { color: darkMode ? COLORS.white : COLORS.black }]}
					numberOfLines={1}
				>
					{track && track.singers ? getSingersName(track.singers) : isRadioPlayer ? 'Радио' : 'Исполнитель'}
				</Text>
				<Text
					style={[styles.text, { color: darkMode ? COLORS.white : COLORS.black }]}
                    numberOfLines={1}
                >
					{track && track.name ? track.name : 'Название'}
				</Text>
			</View>

			<View style={styles.actions}>
                <TouchableOpacity
                    hitSlop={{top: 40, bottom: 40, left: 20, right: 20}}
                    onPress={() => onSkipTo('previous')}
                >
                    <PlayBackIcon
                        style={styles.icon}
                        color={palette.common.primary}
                        width={20}
                    />
                </TouchableOpacity>

                <View style={styles.playAction}>

                    <TouchableOpacity
                        onPress={(props.isSubscribed || props.isActiveSubscription) ? start : subscribe}
                    >
                        {
                            playerState !== STATE_PLAYING  ?

                                <RadioPlayIcon width={32} height={32} /> :

                                <View style={{ position: 'relative' }}>
                                    <Image source={require('../../assets/circle.png')} style={{ width: 32, height: 32, resizeMode: 'contain' }} />
                                    <View style={[styles.stopBtn, { left: '36%' }]}></View>
                                    <View style={[styles.stopBtn, { left: '56%' }]}></View>
                                </View>
                        }
                    </TouchableOpacity>

                </View>

                <TouchableOpacity
                    hitSlop={{top: 40, bottom: 40, left: 20, right: 20}}
                    onPress={() => onSkipTo('next')}
                >
                    <PlayForwardIcon
                        style={styles.icon}
                        color={palette.common.primary}
                        width={20}
                    />
                </TouchableOpacity>

            </View>
		</View>

            <Modal
                animationIn={'fadeIn'}
                animationOut={'fadeOut'}
                animationInTiming={200}
                animationOutTiming={10}
                hideModalContentWhileAnimating={true}
                backdropOpacity={0.5}
                style={{ alignItems: 'center' }}
                isVisible={subscriptionModalIsVisible}
            >
                <SubscriptionModal 
                    closeModal={() => setSubscriptionModalIsVisible(false) }
                    darkMode={darkMode}
                    navigation={props.navigation}
                    forDownloading={false}
                />
            </Modal>
        </>
    );
};


const mapStateToProps = state => ({
    isSubscribed: state.appReducer.isSubscribed,
    isActiveSubscription: state.appReducer.isActiveSubscription,
    darkMode: state.appReducer.darkMode,
    isRadioPlayer: state.appReducer.isRadioPlayer,
    track: state.miniPlayerReducer.track,
    isActive: state.miniPlayerReducer.isActive,
    infinityPlaying: state.appReducer.infinityPlaying,
    isRepeat: state.playerReducer.isRepeat,
});

const mapDispatchToProps = {
    miniPlayerStart,
    miniPlayerSetTrackAction,
    miniPlayerStop,
};

export default connect(mapStateToProps, mapDispatchToProps)(MiniPlayer);


const styles = StyleSheet.create({
    container: {
		flexDirection: 'row',
		paddingVertical: 8,
		paddingHorizontal: 16,
        height: MINI_PLAYER_HEIGHT,
        flex: 1,
        alignItems: 'center',
    },
    cover: {
    	width: 40,
		height: 40,
		borderRadius: 4,
		marginRight: 16,
	},
    trackInfo: {
    	flex: 1,
	},
    title: {

	},
	text: {

	},
    actions: {
        flexDirection: 'row',
        marginLeft: 16,
        alignItems: 'center',
    },
    playAction: {
        marginHorizontal: 16,
    },
    closeAction: {
        marginRight: 16,
    },
    stopBtn: {
        width: 3,
        height: '40%',
        backgroundColor: COLORS.purple,
        position: 'absolute',
        top: '30%',
        borderRadius: 10
    },
    icon: {},
});
