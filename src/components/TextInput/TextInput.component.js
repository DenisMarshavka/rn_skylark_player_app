import React from 'react';
import {TextInput as Input, StyleSheet} from 'react-native';

export const TextInput = ({onChange, value, darkMode, ...rest }) => (
  <Input
    style={[styles.field, {color: darkMode ? 'white' : 'black'}]}
    value={value}
    onChangeText={onChange}
    placeholderTextColor={darkMode ? 'rgba(255, 255, 255, 0.3)' : 'gray'}
    {...rest}
  />
);

const styles = StyleSheet.create({
  field: {
    backgroundColor: 'rgba(112, 112, 112, 0.15)',
    borderRadius: 24,
    paddingHorizontal: 24,
    paddingVertical: 7,
  },
});
