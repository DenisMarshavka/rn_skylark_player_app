import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import Arrow from '../../icons/arrowRight/index'

const YellowButtonAll = ({ onPress }) => (
	<TouchableOpacity style={styles.btnWrapper} onPress={onPress}>
		<Text style={styles.btnText}>Все</Text>

		<Arrow color='white' width={6} height={9} style={{ marginTop: 2 }} />
	</TouchableOpacity>
);

export default YellowButtonAll

const styles = StyleSheet.create({
	btnWrapper: {
		width: 64,
		height: 24,
		backgroundColor: '#FFC42E',
		borderRadius: 17,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row'
	},
	btnText: {
		color: 'white',
		marginRight: 4
	}
});
