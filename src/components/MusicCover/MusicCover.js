import React from 'react';
import {StyleSheet, View, Image, Dimensions} from 'react-native';

import {palette} from 'state/palette';
import { defaultImage } from '../../constants/default_images';

const {height} = Dimensions.get('screen');
const {width} = Dimensions.get('screen');


const MusicCover = ({ image, children, style, darkMode }) => {
  return (
    <View style={[ style, {alignItems: 'center'} ]}>
      <View style={[ styles.coverContainer, {borderRadius: Math.round(width + height) / 2, backgroundColor: darkMode ? '#242424' : '#FCFCFC'} ]}>
        <Image
          source={
            image ? {uri: image} : defaultImage
        }
          style={styles.cover}
        />
      </View>

      {children}
    </View>
  );
};

const styles = StyleSheet.create({
  coverContainer: {
    marginTop: 20,
    width: width * 0.5,
    height: width * 0.5,
    shadowColor: '#5D3F6A',
    shadowOffset: {height: 15},
    shadowRadius: 8,
    shadowOpacity: 0.3,
    borderWidth: 4,
    borderRadius: 150,
    position: 'relative',
    borderColor: palette.common.secondary,

    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  cover: {
    width: width * 0.495,
    height: width * 0.495,
    borderRadius: 190,
  },
});

export default MusicCover;
