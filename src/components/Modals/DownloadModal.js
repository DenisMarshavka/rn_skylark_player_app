import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';

import { COLORS } from '../../constants/colors.constants';
import { requestPermissionsAndMakeDir } from "../../utils/download";
import { downloadTrack, resetDownloadState } from '../../actions/download';
import SubscriptionModal from '../SubscriptionModal/SubscriptionModal';

const DownloadConfirm = ({ onConfirm, cancel, darkMode }) => {
   return (
       <>
           <Text style={[styles.modalText, { color: darkMode ? '#fff' : '#000' }]}>
               Вы действительно хотите скачать музыку?
           </Text>

           <TouchableOpacity
               style={styles.deleteBtn}
               onPress={onConfirm}
           >
               <Text style={styles.positiveText}>Скачать</Text>
           </TouchableOpacity>

           <TouchableOpacity
               style={styles.cancelBtn}
               onPress={cancel}
           >
               <Text style={styles.cancelText}>Отменить</Text>
           </TouchableOpacity>
       </>
   );
};

const DownloadLoading = ({ percent, darkMode }) => {
    return (
        <View style={styles.downloadingWrap}>
            <Text style={[styles.modalText, { color: darkMode ? '#fff' : '#000' }]}>
                Загрузка...
            </Text>

            <View style={styles.percentWrap}>
                <Text style={[styles.downloadingText, { color: darkMode ? '#fff' : '#000' }]}>
                    {`${percent} %`}
                </Text>
            </View>
        </View>
    );
};

const DownloadFinished = ({ darkMode, cancel }) => {
    return (
        <>
            <View style={{ flex: 1, }}>
                <Text style={[styles.modalText, { color: darkMode ? '#fff' : '#000' }]}>
                    Загрузка завершена!
                </Text>
            </View>

            <TouchableOpacity
                style={[styles.deleteBtn, { marginBottom: 23, }]}
                onPress={cancel}
            >
                <Text style={styles.positiveText}>ОК</Text>
            </TouchableOpacity>
        </>
    );
};

const DownloadError = ({ darkMode, cancel }) => {
    return (
        <>
            <View style={{ flex: 1, }}>
                <Text style={[styles.modalText, { color: darkMode ? '#fff' : '#000' }]}>
                    Произошла ошибка
                </Text>
            </View>

            <TouchableOpacity
                style={[styles.cancelBtn, { marginBottom: 23, }]}
                onPress={cancel}
            >
                <Text style={styles.cancelText}>ОК</Text>
            </TouchableOpacity>
        </>
    );
};

const DownloadModal = ({ modalVisible, closeModal, track, darkMode, ...props }) => {
    const {
        downloadTrack,
        resetDownloadState,
        isDownloaded,
        downloading,
        downloadError,
        percent,
        isSubscribed,
        isActiveSubscription
    } = props;

    useEffect(() => {
        if (modalVisible && isDownloaded) {
            resetDownloadState();
        }
        return () => {};
    }, [modalVisible]);

    const onConfirmDownload = async () => {
        try {
            await requestPermissionsAndMakeDir();

            downloadTrack(track);
        } catch (error) {
            closeModal();
        }
    };

    return (
        <Modal
            animationIn={'fadeIn'}
            animationOut={'fadeOut'}
            animationInTiming={200}
            animationOutTiming={10}
            hideModalContentWhileAnimating={true}
            backdropOpacity={0.5}
            style={styles.modalView}
            isVisible={modalVisible === 'object' ? !!modalVisible : modalVisible}
        >
            {isSubscribed ?
            <View style={[styles.centeredView, { backgroundColor: darkMode ? '#272727': '#fff' }]}>
                {
                    !downloading && !isDownloaded && !downloadError ? (
                        <DownloadConfirm onConfirm={onConfirmDownload} cancel={closeModal} darkMode={darkMode} />
                    ) : downloading ? (
                        <DownloadLoading percent={percent} darkMode={darkMode} />
                    ) : isDownloaded ? (
                        <DownloadFinished darkMode={darkMode} cancel={closeModal} />
                    ) : downloadError ? (
                        <DownloadError darkMode={darkMode} cancel={closeModal} />
                    ) : null
                }
            </View> :
            <SubscriptionModal
                closeModal={closeModal}
                darkMode={darkMode}
                navigation={props.navigation}
                forDownloading={true}
            />
        }
        </Modal>
    );
};

const mapStateToProps = state => ({
    isSubscribed: state.appReducer.isSubscribed,
    isActiveSubscription: state.appReducer.isActiveSubscription,
    isDownloaded: state.downloadReducer.isDownloaded,
    downloading: state.downloadReducer.downloading,
    downloadError: state.downloadReducer.downloadError,
    percent: state.downloadReducer.percent,
});

const mapDispatchToProps = {
    downloadTrack,
    resetDownloadState,
};

export default connect(mapStateToProps, mapDispatchToProps)(DownloadModal);

const styles = StyleSheet.create({
    modalView: {
        alignItems: 'center',
    },
    centeredView: {
        width: 245,
        height: 210,
        borderRadius: 8,
        alignItems: 'center'
    },
    modalText: {
        textAlign: 'center',
        fontSize: 16,
        marginTop: 23
    },
    deleteBtn: {
        width: 215,
        height: 50,
        backgroundColor: COLORS.yellow,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginTop: 27,
    },
    downloadingWrap: {
        flex: 1,
    },
    percentWrap: {
        flex: 1,
        justifyContent: 'center',
    },
    downloadingText: {
        textAlign: 'center',
        fontSize: 24,
        marginBottom: 23,
    },
    cancelBtn: {
        width: '100%',
        alignItems: 'center',
    },
    cancelText: {
        fontSize: 18,
        marginTop: 16,
        color: COLORS.darkgrey,
    },
    positiveText: {
        fontSize: 18,
    },
});
