import React from 'react';
import { View, StyleSheet } from 'react-native';

import {palette} from 'state/palette';

export const Divider = () => <View style={styles.divider} />;

const styles = StyleSheet.create({
  divider: {
    borderBottomColor: palette.common.lightGrey,
    borderBottomWidth: 1,
  },
});
