import React, { useRef, useState, useEffect } from 'react';
import { Alert, FlatList, ScrollView, Share, RefreshControl, View } from "react-native";

import MusicMenu from "../MusicItem/MusicMenu";
import DeleteModal from "../MusicItem/DeleteModal";
import AddTrackToPlayListMenu from "../MusicItem/AddTrackToPlayListMenu";
import DownloadModal from '../../components/Modals/DownloadModal';
import {connect} from "react-redux";
import { deleteTrackById, getTrackLikedCheck, setTrackItemLike, addTrackToPlayList, getAddTrackToUserTrackListByTrackId } from "../../actions";
import MusicItem from "../MusicItem/MusicItem";
import {checkSingersMusicItem} from "../../utils/functions";
import AddTrackSuccessModal from '../../screens/SearchMusicTabScreen/AddTrackSuccessModal/AddTrackSuccessModal';
import { removeTrackFromPlaylist } from "../../actions/player";
import { removeDownloadedTrack } from "../../actions/download";
import { miniPlayerStop } from "../../actions/miniPlayer";
import { destroyPlayer, removeTrackFromPlayerQueue } from "../../utils/player";
import { SHARE_URL } from '../../constants/api_config';

const MusicListWithSheetsMenu = props => {
    const {
        page = 1,
        isPlayListPage = false,
        navigation,
        icon,
        data,
        deleteTrackById,
        contentContainerStyle,
        showsHorizontalScrollIndicator,
        withoutDelete = true,
        numColumns,
        isAlbumScreen = false,
        flatListStyle,
        musicItemStyle,
        darkMode,
        withoutAddToPlayList,
        onDeleteTrackFromPlayList = () => {},
        onOpenPlayer,
        withDownload,
        onDownload = () => {},

        approveTracksListTrackDelete = false,
        approvePlayListTrackDelete = false,
        isDownloadedTracks = false,

        likedTrackLoading = false,
        setTrackItemLike,
        trackLikedTrackStatus,
        likedTrackError,
        getTrackLikedCheck,

        requestFailed,
        loadingAddTrack,

        addTrackToPlayList,
        getAddTrackToUserTrackListByTrackId,
        loadingAddTrackToTrackList,
        errorAddTrackToTrackList,
        isFooter,

        loadMoreSystemTracks
    } = props;

    const [deleteModalVisible, setDeleteModalVisible] = useState(false);
    const [downloadModalVisible, setDownloadModalVisible] = useState(false);
    const [musicData, setMusicData] = useState(null);
    const [visibleAddTrackModalSuccess, setVisibleAddTrackModalSuccess] = useState(false);

    const musicMenuRef = useRef(null);
    const addPlaylistRef = useRef(null);
    const audioPlayerRef = useRef(null);

    const onTrackLikedCheck = () => {
        if (musicData && musicData.id) getTrackLikedCheck(+musicData.id);
    };

    useEffect(() => {
        onTrackLikedCheck();
    }, [musicData, setTrackItemLike]);

    const handleOpenPlayListsMenu = (musicData, isFromMusicMenu = false) => {
        if (isFromMusicMenu) musicMenuRef.current.close();

        if (musicData) {
            setMusicData(musicData);

            addPlaylistRef.current.open();
        }
    };

    const handleOnMoreMenuOpen = currentTrack => {
        // console.log('handleOnMoreMenuOpen currentTrack: ', currentTrack);

        setMusicData(currentTrack);

        musicMenuRef.current.open();
    };


    const currentMusicItem = musicData;

    const deleteDownloadedTrack = async (musicData) => {
        const id = musicData.id;

        if (props.isPlayerActive && props.isRadioPlayer === false && props.track.localPath) {
            if (props.track.id === id) {
                props.miniPlayerStop();
                await destroyPlayer();
            } else {
                await removeTrackFromPlayerQueue(id);
                await props.removeTrackFromPlaylist(id);
            }
        }

        await props.removeDownloadedTrack(musicData);
    };

    const deleteTrack = async (track) => {
        const musicData = (track && track.id) ? track : currentMusicItem;

        if (musicData && musicData.id) {
            if (isDownloadedTracks) {
                await deleteDownloadedTrack(musicData);
            } else {
                if (approveTracksListTrackDelete) deleteTrackById(musicData.id);

                if (approvePlayListTrackDelete) onDeleteTrackFromPlayList(musicData.id, musicData.position_id);
            }
        }

        setDeleteModalVisible(false)
    };

    const handleTrackToPlaylistAdd = playListId => {
       
        if (musicData && musicData.id) {
            console.log('playListId', playListId)
            console.log('musicData add to playlist', musicData)

            addTrackToPlayList(playListId, musicData.id)
            getAddTrackToUserTrackListByTrackId(musicData.id)

            setVisibleAddTrackModalSuccess(true);
        } else {
            alert(`error add track, playListId ${playListId}, trackId ${musicData.id}`)
        }
    };

    const handleTrackShare = async () => {
        try {
            if (musicData) {
                const result = await Share.share({ message: SHARE_URL + 'music?music_id=' + musicData.id});
            }
        } catch (e) {
            console.log('Error a Track share: ', e);
        }
    };

    const handleDownloadTrack = () => {
        musicMenuRef.current.close();

        if (!props.downloading) {
            setDownloadModalVisible(true);
        } else {
            Alert.alert(
                'Информация',
                'Можно скачать только один файл одновременно!',
                [
                    {
                        text: 'Хорошо',
                        style: 'success',
                    },
                ],
                { cancelable: true }
            );
        }
    };

    return (
        <>
            <FlatList
                data={data}
                showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
                numColumns={numColumns}
                keyExtractor={ (item, index) => item.id + item.name + index }
                style={{...flatListStyle}}
                contentContainerStyle={{...contentContainerStyle}}
                ListFooterComponent={() => <View></View>}
                ListFooterComponentStyle={{marginBottom: isFooter ? 80 : 0 }}
                renderItem={({ item, ...props }) => {
                    // console.log('item', item);

                    return (
                        <MusicItem
                            key={item.name + props.index}
                            musicData={item}
                            isAlbumScreen={isAlbumScreen}
                            icon={icon}
                            style={{...musicItemStyle}}
                            isPlayListPage={isPlayListPage}
                            playListPageId={page}
                            setMusicData={setMusicData}
                            onMoreMenuOpen={handleOnMoreMenuOpen}
                            onPlayListsMenuOpen={handleOpenPlayListsMenu}
                            darkMode={darkMode}
                            navigation={navigation}
                            onOpenPlayer={onOpenPlayer}
                            onTrackDelete={deleteTrack}
                            withDownload={withDownload}
                            onDownload={onDownload}
                            isAllUserTracksList={approveTracksListTrackDelete}
                        />
                    )
                }}
                onEndReached={loadMoreSystemTracks ? loadMoreSystemTracks : () => {}}
                onEndReachedThreshold={0.9}
            />

            <MusicMenu
                isPlayListPage={isPlayListPage}

                likedTrackLoading={likedTrackLoading}
                likedTrackError={likedTrackError}
                trackLikedTrackStatus={trackLikedTrackStatus}
                setTrackItemLike={setTrackItemLike}

                ref={musicMenuRef}
                musicData={ musicData ? musicData : false }
                singerName={ checkSingersMusicItem(musicData && musicData.singerName ? musicData.singerName : '', musicData ).trim() }
                onShareTrack={handleTrackShare}
                darkMode={darkMode}
                setDeleteModalVisible={() => {
                    setDeleteModalVisible(true);
                    musicMenuRef.current.close();
                }}
                openAddPlaylistSheet={() => {
                    handleOpenPlayListsMenu(musicData, true)
                }}
                openMusicText={() => {
                    musicMenuRef.current.close();

                    if (audioPlayerRef && audioPlayerRef.current) audioPlayerRef.current.close();

                    if (musicData) navigation.navigate('MusicTextScreen', {musicData});
                }}
                withoutDelete={withoutDelete}
                withoutAddToPlayList={withoutAddToPlayList}
                onDownload={handleDownloadTrack}
            />

            <DeleteModal
                modalVisible={deleteModalVisible}
                closeModal={ () => setDeleteModalVisible(false)}
                onMusicItemDelete={deleteTrack}
                darkMode={darkMode}
            />

            {
                icon ?
                <AddTrackToPlayListMenu
                    ref={addPlaylistRef}
                    trackId={musicData && musicData.id ? musicData.id : false}
                    darkMode={darkMode}
                    handleTrackToPlaylistAdd={ (playlistId, trackId) => handleTrackToPlaylistAdd(playlistId, trackId) }
                />
                : null
            }

            <DownloadModal
                modalVisible={downloadModalVisible}
                closeModal={() => setDownloadModalVisible(false)}
                track={musicData}
                darkMode={darkMode}
                navigation={navigation}
            />

            <AddTrackSuccessModal
                modalVisible={visibleAddTrackModalSuccess}
                setModalVisible={setVisibleAddTrackModalSuccess}
                darkMode={darkMode}
                loading={loadingAddTrackToTrackList}
                errorRequest={errorAddTrackToTrackList}
            /> 
        </>
    );
};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,
    highQuality: state.appReducer.highQuality,
    isRadioPlayer: state.appReducer.isRadioPlayer,

    likedTrackLoading: state.appReducer.likedTrackLoading,
    trackLikedTrackStatus: state.appReducer.trackLikedTrackStatus,
    likedTrackError: state.appReducer.likedTrackError,
    downloadPendingList: state.downloadReducer.pendingList,
    downloading: state.downloadReducer.downloading,

    requestFailed: state.playListReduce.errorAddTrack,
    loadingAddTrack: state.playListReduce.loadingAddTrack,

    track: state.playerReducer.track,
    isPlayerActive: state.miniPlayerReducer.isActive,

    loadingAddTrackToTrackList: state.trackListReducer.loadingAddTrackToTrackList,
    errorAddTrackToTrackList: state.trackListReducer.errorAddTrackToTrackList,
});

const mapDispatchToProps = {
    setTrackItemLike,
    deleteTrackById,

    getTrackLikedCheck,

    addTrackToPlayList,
    getAddTrackToUserTrackListByTrackId,

    miniPlayerStop,
    removeTrackFromPlaylist,
    removeDownloadedTrack,
};

export default connect(mapStateToProps, mapDispatchToProps)(MusicListWithSheetsMenu);
