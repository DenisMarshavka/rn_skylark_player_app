import React from 'react';
import {Text, StyleSheet, TouchableOpacity, Image, View} from 'react-native';
import Ripple from 'react-native-material-ripple';
import Svg, {Circle, Path, Defs, LinearGradient, Stop} from 'react-native-svg';
import { COLORS } from '../../constants/colors.constants';

const PlayIcon = props => (
  <Svg width={41} height={41} viewBox="0 0 41 41" fill="none" {...props}>
    <Circle
      cx={20.5}
      cy={20.5}
      r={19.5}
      fill="#fff"
      stroke="url(#prefix__paint0_linear)"
      strokeWidth={2}
    />
    <Path
      d="M29.25 19.634a1 1 0 010 1.732l-12.375 7.145a1 1 0 01-1.5-.866v-14.29a1 1 0 011.5-.866l12.375 7.145z"
      fill="#7B3DFF"
    />
    <Defs>
      <LinearGradient
        id="prefix__paint0_linear"
        x1={11.104}
        y1={34.167}
        x2={46.552}
        y2={0}
        gradientUnits="userSpaceOnUse">
        <Stop stopColor="#7B3DFF" />
        <Stop offset={1} stopColor="#DED1FA" />
      </LinearGradient>
    </Defs>
  </Svg>
);

export function RadioCard({ radio, onPress, darkMode} ) {
  console.log('radio', radio)
  return (
    <TouchableOpacity style={[styles.shadow, { backgroundColor: darkMode ? COLORS.opacityBlack : COLORS.white }]}>
      <Ripple
          style={styles.container}
          onPress={onPress}
      >
        <View style={styles.imageWrapper}>
          <Image
            source={ radio.photo ? {uri: radio.photo} : require('../../assets/edward-cisneros-KoKAXLKJwhk-unsplash.png')}
            style={styles.image}
            resizeMode="cover"
          />

          <Text style={[ styles.title, { color: darkMode ? COLORS.white : COLORS.black } ]}>{radio.name}</Text>
        </View>

        <PlayIcon />
      </Ripple>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    width: '100%',
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 5,
    justifyContent: 'space-between',
  },
  shadow: {
    // backgroundColor: 'white',
    borderRadius: 8,
    elevation: 10,
    shadowColor: '#000',
    marginBottom: 15,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 5,
    width: '98%'
  },
  imageWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  image: {
    height: 50,
    width: 50,
    borderRadius: 25,
    marginRight: 15,
  },
});
