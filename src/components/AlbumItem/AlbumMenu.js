import React, { forwardRef } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Share } from 'react-native';
import Ripple from 'react-native-material-ripple';
import RBSheet from 'react-native-raw-bottom-sheet';
import { COLORS } from '../../constants/colors.constants';
import { palette } from '../../state/palette';
import ShareIcon from '../../icons/share';
// import RemoveIcon from '../../icons/remove';
// import SvgTextIcon from '../../icons/text';
// import SvgPlaylistIcon from '../../icons/playlist'
import SvgDownloadIcon from '../../icons/download';
import SvgHeartIcon from '../../icons/heart';
import { SHARE_URL } from '../../constants/api_config';

const AlbumMenu = forwardRef(({ albumData, darkMode }, ref) => {

    const onPressMenu = () => {
        Share.share({ message: SHARE_URL + `albums?album_id=${albumData.id}` })
    }

    console.log(albumData)

    return (
        <RBSheet
            ref={ref}
            height={240}
            closeOnDragDown={true}
            customStyles={{
                container: {
                    backgroundColor: darkMode ? COLORS.black : '#FCFCFC',
                    borderTopRightRadius: 16,
                    borderTopLeftRadius: 16
                },
                wrapper: {
                    backgroundColor: 'rgba(51, 51, 51, 0.48)',
                },
                draggableIcon: {
                    width: 134,
                    backgroundColor: '#E4E4E4',
                },
            }}>

            <View>
                <View style={[styles.albumData, darkMode ? {} : {}]}>
                    <Image style={styles.image} source={albumData && albumData.photo ? { uri: albumData.photo } : require('../../assets/logo.png')} />

                    <View style={{ width: '60%' }}>
                        <Text style={{ color: darkMode ? 'white' : 'black' }}>Альбом</Text>

                        <View style={styles.titleWrapper}>
                            <Text style={[styles.title, { color: darkMode ? 'white' : 'black' }]}>{albumData ? albumData.singerName : ''}</Text>
                            {/* <Text style={styles.time}>3:25</Text> */}
                        </View>
                        <Text style={styles.extra}>{albumData ? albumData.albumName : ''}</Text>
                        {/* <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between' }}>
                            <Ripple style={styles.downloadBtn}>
                                <Text style={{ color: COLORS.white, marginRight: 10 }}>Скачать</Text>
                                <SvgDownloadIcon color={COLORS.white} width={20} height={17} />
                            </Ripple>

                            <TouchableOpacity>
                                <SvgHeartIcon />
                            </TouchableOpacity>
                        </View> */}
                    </View>
                </View>
                    <Ripple onPress={onPressMenu}>
                        <View style={styles.actions}>
                            <View style={styles.actionIconWrapper}>
                                <ShareIcon />
                            </View>
                            <Text style={[styles.actionText, { color: darkMode ? 'white' : 'black' }]}>Поделиться</Text>
                        </View>
                    </Ripple>
            </View>
        </RBSheet>
    )
});

export default AlbumMenu

const styles = StyleSheet.create({
    downloadBtn: {
        borderRadius: 13,
        paddingVertical: 4,
        paddingHorizontal: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        backgroundColor: COLORS.purple,
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    albumData: {
        marginTop: 10,
        flexDirection: 'row',
        marginBottom: 15,
        paddingHorizontal: 15,
    },
    image: {
        width: '30%',
        height: 100,
        borderRadius: 8,
        marginRight: 30,
    },
    actions: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15,
    },
    titleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    time: {
        fontSize: 14,
        marginTop: 10,
        // right: '-200%',
        color: palette.common.titleGrey,
        position: 'relative',
    },
    actionIconWrapper: {
        width: 20,
        marginRight: 10,
    },
    actionText: {
        marginLeft: 10,
        fontSize: 18,
    },
    title: {
        fontSize: 14,
        marginTop: 10,
        fontWeight: 'bold',
    },
    extra: {
        fontSize: 14,
        marginTop: 3,
        color: '#8D8D8D',
        marginBottom: 12,
    },
});
