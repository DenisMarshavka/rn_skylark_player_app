import React, { forwardRef, useState, useEffect } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, Image, Platform } from 'react-native';
import { palette } from 'state/palette';

import RBSheet from 'react-native-raw-bottom-sheet';
import Slider from '@react-native-community/slider';

import MenuIcon from 'icons/menu';
import PlusIcon from 'icons/plus';
import RandomIcon from 'icons/random';
import RepeatIcon from 'icons/repeat';
import PlayBackIcon from 'icons/playBack';
import RadioPlayIcon from 'icons/radioPlay';
import PlayForwardIcon from 'icons/playForward';

import TrackPlayer from 'react-native-track-player';

import MusicCover from '../../components/MusicCover/MusicCover';
import { COLORS } from '../../constants/colors.constants';
import { secondsToMMSS } from '../../utils/functions';

const { height } = Dimensions.get('screen');
// const { width } = Dimensions.get('screen');

import { defaultImage } from '../../constants/default_images';


const AudioPlayer = forwardRef(({ onMenuPress, music, darkMode, setStop }, ref) => {
	let [progress, setProgress] = useState(0);
	let [duration, setDuration] = useState(0);

	let [repeat, setRepeat] = useState(false);
	let [random, setRandom] = useState(false);
	let [playerState, setPlayerState] = useState(null);

	useEffect(()=>{
		TrackPlayer.addEventListener('playback-state', ({state}) => {
			console.log('playback-state -----', state);
			setPlayerState(state)
		});
	}, []);

	const changeTime = async percents => {
		let time = duration * (percents / 100);
		console.log(time);

		setProgress(time);
		TrackPlayer.seekTo(time)
	};

	const start = async () => {
		let state = Platform.OS === 'android' ? 2 : 'ready'

		if (playerState === state){
			await TrackPlayer.play()
		} else {
			await TrackPlayer.pause()
		}

		TrackPlayer.addEventListener('playback-track-changed', () => {
			setStop()
		});
	};


	useEffect(() => {
		setupPlayer().then(() => start())
	}, []);


	const setupPlayer = async () => {
		await TrackPlayer.setupPlayer({
			waitForBuffer: true,
		});
		TrackPlayer.updateOptions({

			stopWithApp: true,

			capabilities: [
				TrackPlayer.CAPABILITY_PLAY,
				TrackPlayer.CAPABILITY_PAUSE,
				TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
				TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
			],

		});

		// TrackPlayer.registerEventHandler(() => {
		// 	return async (data) => {
		// 	  if (data.type == 'playback-state') {
		// 		// Update the UI with the new state
		// 	  } else if (data.type == 'remote-play') {
		// 		// The play button was pressed, we can forward this command to the player using
		// 		TrackPlayer.play();
		// 	  } else if (data.type == 'remote-stop') {
		// 		// The stop button was pressed, we can stop the player
		// 		TrackPlayer.stop();
		// 	  } else if (data.type == 'remote-pause') {
		// 		// The play button was pressed, we can forward this command to the player using
		// 		TrackPlayer.pause();
		// 	  }
		// 	};
		//   })

		// TrackPlayer.registerPlaybackService(data => {
		// 	console.log('registerPlaybackService', data)
		// })

		await TrackPlayer.add({
			id: music && music.id ? '' + music.id : 'trackId',
			url: music && music.audio ? music.audio : '',
			title: music && music.name ? music.name : 'Track Title',
			artist: music && music.performer ? music.performer : 'Track Artist',
			artwork: music && music.img ? music.img : defaultImage
		});

		let position = await TrackPlayer.getPosition();
		let buffered = await TrackPlayer.getBufferedPosition();
		let duration = await TrackPlayer.getDuration();

		setProgress(position);
		setDuration(duration)
	}

	return (
		<RBSheet
			ref={ref}
			height={height * 0.9}
			closeOnDragDown={true}
			closeOnPressBack={() => TrackPlayer.stop()}
			customStyles={{
				wrapper: {
					backgroundColor: 'rgba(51, 51, 51, 0.48)',
				},
				draggableIcon: {
					width: 134,
					backgroundColor: '#C2C2C2',
				},
				container: {
					borderTopLeftRadius: 16,
					borderTopEndRadius: 16,
					backgroundColor: darkMode ? '#242424' : '#FCFCFC'
				}
			}}>
			<View style={[styles.container, { backgroundColor: darkMode ? '#242424' : '#FCFCFC' }]}>
				<View>
					<MusicCover image={music.photo}>
						<View style={{ marginTop: '5%' }}>
							<Text style={[styles.title, { color: darkMode ? COLORS.white : COLORS.black }]}>{music && music.performer ? music.performer : 'Исполнитель'}</Text>
							<Text style={[styles.text, { color: darkMode ? COLORS.white : COLORS.black }]}>{music && music.name ? music.name : 'Название'}</Text>
						</View>
					</MusicCover>

					<View style={styles.actions}>
						<TouchableOpacity style={styles.iconWrapper}>
							<PlusIcon color={palette.common.primary} />
						</TouchableOpacity>
						<View style={styles.playAction}>
							<TouchableOpacity>
								<PlayBackIcon
									style={styles.icon}
									color={palette.common.primary}
								/>
							</TouchableOpacity>

							<TouchableOpacity
								onPress={start}
							>
								{playerState === 2
								? <RadioPlayIcon width={76} height={76} />
								:
								<View style={{ position: 'relative' }}>
									<Image source={require('../../assets/circle.png')} style={{ width: 76, height: 76, resizeMode: 'contain' }} />
									<View style={[styles.stopBtn, { left: '36%' }]}></View>
									<View style={[styles.stopBtn, { left: '56%' }]}></View>
								</View>
								}
							</TouchableOpacity>

							<TouchableOpacity

							>
								<PlayForwardIcon
									style={styles.icon}
									color={palette.common.primary}
								/>
							</TouchableOpacity>
						</View>
						<TouchableOpacity style={styles.iconWrapper} onPress={onMenuPress}>
							<MenuIcon color={palette.common.primary} />
						</TouchableOpacity>
					</View>

					<MyPlayerBar
						progress={progress}
						localeDuration={duration}
						duration={music.duration}
						darkMode={darkMode}
						changeTime={ time => changeTime(time) }
					/>

					<View style={styles.bottomActions}>
						<TouchableOpacity onPress={() => setRepeat(!repeat)}>
							<RepeatIcon color={repeat ? palette.common.primary : '#c2c2c2'} />
						</TouchableOpacity>
						<TouchableOpacity onPress={() => setRandom(!random)}>
							<RandomIcon color={random ? palette.common.primary : '#c2c2c2'} />
						</TouchableOpacity>
					</View>
				</View>
			</View>
		</RBSheet>
	);
});

export default AudioPlayer;

class MyPlayerBar extends TrackPlayer.ProgressComponent {
    render() {
        return (
			<View style={{ marginVertical: 15 }}>
				<View
					style={{
						flexDirection: 'row',
						justifyContent: 'space-between',
					}}>
					<Text style={styles.timeStamp}>{secondsToMMSS(this.state.position)}</Text>
					<Text style={styles.timeStamp}>{this.props.duration || secondsToMMSS(this.props.localeDuration)}</Text>
				</View>
				<Slider
					value={this.getProgress().toFixed(2) * 100}
					minimumValue={0}
					maximumValue={100}
					trackStyle={styles.track}
					thumbStyle={styles.thumb}
					minimumTrackTintColor={palette.common.primary}
					// onValueChange={seconds => this.props.changeTime(seconds)}
					maximumTrackTintColor={this.props.darkMode ? 'white' : 'black'}
				/>
			</View>
        )
    }
}


const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 20,
	},
	img: {
		height: height - 100,
	},
	iconWrapper: {
		width: 24,
		alignItems: 'center',
	},
	icon: {
		marginHorizontal: 12,
	},
	timeStamp: {
		color: '#C2C2C2',
	},
	actions: {
		flexDirection: 'row',
		alignItems: 'center',
		marginTop: '5%',
		justifyContent: 'space-between',
		height: height * 0.1,
	},
	bottomActions: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		marginTop: '5%',
	},
	title: {
		fontWeight: '600',
		fontSize: 20,
		color: '#000',
		marginBottom: 8,
		textAlign: 'center',
	},
	playAction: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	text: {
		fontSize: 18,
		color: '#C2C2C2',
		textAlign: 'center',
	},
	track: {
		height: 2,
		borderRadius: 1,
		backgroundColor: '#FFF',
	},
	thumb: {
		width: 8,
		height: 8,
		backgroundColor: palette.common.primary,
	},


	stopBtn: {
		width: 6,
		height: '40%',
		backgroundColor: COLORS.purple,
		position: 'absolute',
		top: '30%',
		borderRadius: 10
	}
});
