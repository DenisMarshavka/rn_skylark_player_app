import React from 'react'
import {View} from "react-native";
import {mainStyles} from "../../utils/styles";

export const MainScreenContainer = ({ darkMode, children, darkTintColor = 'black', lightTintColor = '#F9F9F9' }) => (
    <View style={{flex: 1, backgroundColor:  darkMode ? darkTintColor : lightTintColor}}>
        <View style={mainStyles.safeAreaViewContainer}>
            { children }
        </View>
    </View>
);
