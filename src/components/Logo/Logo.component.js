import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { COLORS } from '../../constants/colors.constants'

const Logo = props => (
    <View style={[styles.container, props.style]}>
        <View style={[styles.logo, {...props.size, backgroundColor: props.backgroundColor }]}>
            {/* <Text style={styles.logoText}>Logo</Text> */}
            <Image source={require('../../assets/logo.png')} style={{ width: '140%', height: '140%', resizeMode: 'cover', marginTop: -10 }} />
        </View>
    </View>
)

export default Logo

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        width: 170,
        height: 170,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoText: {
        fontSize: 30,
        color: COLORS.white,
        textTransform: 'uppercase',
        opacity: 0.4
    }
})
