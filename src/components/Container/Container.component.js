import React from 'react';
import { View, StyleSheet } from 'react-native';


export function Container({ children, classes, theme, style }) {
  return (
    <View style={[
        {
          ...styles.container,
          ...classes,
          backgroundColor: theme === 'dark' ? '#111' : '#fff',
        }, {...style}
    ]}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
  },
});
