import React, {useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput} from 'react-native';
import Modal from 'react-native-modal';
import {COLORS} from '../../constants/colors.constants';
import SvgSuccessIcon from '../../icons/successIcon';

const AddPlaylistModal = ({ modalVisible, closeModal, onCreatePlaylist, darkMode }) => {
    let [playlistName, setPlaylistName] = useState('');

    return (
        <Modal
            animationIn={'fadeIn'}
            animationOut={'fadeOut'}
            animationInTiming={200}
            animationOutTiming={10}
            onBackButtonPress={closeModal}
            hideModalContentWhileAnimating={true}
            backdropOpacity={0.5}
            onBackdropPress={closeModal}
            style={styles.modalView}
            isVisible={modalVisible === 'object' ? !!modalVisible : modalVisible}
        >
            <View style={[ styles.centeredView, {backgroundColor: darkMode ? 'rgba(37,37,37,0.8)' : '#fff', alignItems: 'center'} ]}>
                <Text style={[ styles.modalText, {color: darkMode ? '#fff' : '#000'} ]}>Новый плейлист</Text>

                <TextInput
                    style={[styles.input, {color: '#A4A4A4'}]}
                    value={playlistName}
                    onChangeText={setPlaylistName}
                    placeholder='Название'
                    placeholderTextColor={'#A4A4A4'}
                />

                <TouchableOpacity
                    disabled={!playlistName}
                    style={[ styles.createBtn, {backgroundColor: playlistName.length === 0 ? '#DFDFDF' : COLORS.purple} ]}
                    onPress={() => {
                        onCreatePlaylist(playlistName);

                        setTimeout(setPlaylistName, 1500, '');
                    }}
                >
                    <Text style={{fontSize: 18, color: COLORS.white}}>Создать</Text>
                </TouchableOpacity>
            </View>
        </Modal>
    )
};

export default AddPlaylistModal

const styles = StyleSheet.create({
    modalView: {
        alignItems: 'center',
    },
    centeredView: {
        width: 305,
        height: 210,
        borderRadius: 8,
        justifyContent: 'space-between',
        paddingHorizontal: 15
    },
    modalText: {
        textAlign: 'center',
        fontSize: 16,
        marginTop: 23,
        fontWeight: 'bold'
    },
    createBtn: {
        width: 170,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 34,
        marginBottom: 20
    },
    input: {
        width: '100%',
        borderWidth: 1,
        borderColor: COLORS.grey,
        borderRadius: 50,
        height: 40,
        paddingHorizontal: 15,
        fontSize: 16
    }
});
