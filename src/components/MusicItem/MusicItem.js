import React, { useRef, useState, useCallback, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import {COLORS} from '../../constants/colors.constants'
import SvgMenuIcon from '../../icons/menu';

import {toggleBottomBar} from "../../store/appReducer";
import {connect} from "react-redux";
import {checkSingersMusicItem} from "../../utils/functions";

const MusicItem = props => {
	const {
	    musicData,
        icon,
        darkMode,
        style,
        onTrackDelete,
        onOpenPlayer,
        onMoreMenuOpen = () => {},
        onPlayListsMenuOpen = () => {},
        isPlayListPage = false,
        isAlbumScreen = false,
        singerName,
        withDownload = false,
        onDownload,
		isAllUserTracksList = false,
		setMusicData = () => {},
    } = props

	const openPlayer = params => {
		onOpenPlayer({ track: musicData, onPlayListsMenuOpen, setMusicData, onMoreMenuOpen, onTrackDelete, isPlayListPage, isAllUserTracksList, ...params });
	};

	const onIconPress = () => {
		if (withDownload) {
			onDownload(musicData)
		} else {
            onPlayListsMenuOpen(musicData);
		}
	};

	const getListOfSingers = () => {
        if (musicData && musicData.singers){
			let listOfSingers = musicData.singers.map( (item, i) => i + 1 === musicData.singers.length ? item.name : item.name + ' & ')
			
            return listOfSingers
        }
        return 'Неизвестный исполнитель'
	}
	
	// console.log('item', musicData)

	return (
		<TouchableOpacity
			activeOpacity={0.9}
			style={[ styles.musicBlock, { paddingLeft: !isAlbumScreen ? 0 : 30, backgroundColor: darkMode ? 'rgba(255, 255, 255, 0.1)' : COLORS.white, ...style } ]}
			onPress={openPlayer}
		>
			{
				!isAlbumScreen &&

				<Image
					style={{ width: 50, height: 50, resizeMode: 'cover', marginHorizontal: 15, borderRadius: 50}}
					source={ musicData.photo ? {uri: musicData.photo } : require('../../assets/logo.png')}
				/>
			}

			<View style={{ flex: 1 }}>
				<Text numberOfLines={1} style={{ width: '90%', fontSize: 15, fontWeight: 'bold', color: darkMode ? COLORS.white : COLORS.black }}>{ checkSingersMusicItem(singerName, musicData).trim() || getListOfSingers()}</Text>

				<Text numberOfLines={1} style={{ width: '90%', fontSize: 15, opacity: 0.4, color: darkMode ? COLORS.grey : COLORS.black  }}>{musicData.name}</Text>
			</View>

			{
				icon && !isPlayListPage ?

				<TouchableOpacity style={styles.iconWrapper} onPress={() => onIconPress()}>
					{icon}
				</TouchableOpacity>

				: null
			}

			<TouchableOpacity style={styles.iconWrapper} onPress={() => onMoreMenuOpen(musicData)}>
				<SvgMenuIcon color={COLORS.purple} />
			</TouchableOpacity>

		</TouchableOpacity>
	);
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
});

const mapDispatchToProps = {
	toggleBottomBar,
};

export default connect(mapStateToProps, mapDispatchToProps)(MusicItem);


const styles = StyleSheet.create({
	iconWrapper: {
		width: 30,
		height: 30,
		justifyContent: 'center',
		alignItems: 'center',
		marginRight: 10,
	},
	musicBlock: {
		overflow: 'hidden',
		width: '98%',
		height: 60,
		shadowColor: 'grey',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.1,
		shadowRadius: 10,
		elevation: 3,
		borderRadius: 8,
		alignItems: 'center',
		flexDirection: 'row',
		marginVertical: 4,
		marginHorizontal: '2%'
	},
});
