import React, { forwardRef, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { COLORS } from '../../constants/colors.constants';
import SvgSuccessIcon from '../../icons/successIcon';

const DeleteModal = ({ modalVisible, closeModal, onMusicItemDelete, darkMode })=> (
    <Modal
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        animationInTiming={200}
        animationOutTiming={10}
        onBackButtonPress={closeModal}
        hideModalContentWhileAnimating={true}
        backdropOpacity={0.5}
        onBackdropPress={closeModal}
        style={styles.modalView}
        isVisible={modalVisible === 'object' ? !!modalVisible : modalVisible}
    >
        <View style={[styles.centeredView, { backgroundColor: darkMode ? '#272727' : '#fff', alignItems: 'center' }]}>
            <Text style={[styles.modalText, { color: darkMode ? '#fff' : '#000' }]}>Вы действительно хотите{'\n'} удалить эту песню?</Text>

            <TouchableOpacity
                style={styles.deleteBtn}
                onPress={onMusicItemDelete}
            >
                <Text style={{fontSize: 18}}>Удалить</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={{ width: '100%', alignItems: 'center' }}
                onPress={closeModal}
            >
                <Text style={{fontSize: 18, marginTop: 16, color: COLORS.darkgrey}}>Отменить</Text>
            </TouchableOpacity>
        </View>

    </Modal>
);

export default DeleteModal

const styles = StyleSheet.create({
    modalView: {
        alignItems: 'center',
    },
    centeredView: {
        width: 245,
        height: 210,
        borderRadius: 8
    },
    modalText: {
        textAlign: 'center',
        fontSize: 16,
        marginTop: 23
    },
    deleteBtn: {
        width: 215,
        height: 50,
        backgroundColor: COLORS.yellow,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginTop: 27,
    }
});
