import React, { forwardRef } from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, ActivityIndicator, Dimensions} from 'react-native';
import Ripple from 'react-native-material-ripple';
import RBSheet from 'react-native-raw-bottom-sheet';

import { COLORS } from '../../constants/colors.constants';
import { palette } from '../../state/palette';
import ShareIcon from '../../icons/share';
import RemoveIcon from '../../icons/remove';
import SvgTextIcon from '../../icons/text';
import SvgPlaylistIcon from '../../icons/playlist'
import SvgDownloadIcon from '../../icons/download';
import SvgHeartIcon from '../../icons/heart';
import { defaultImage, logoImage } from '../../constants/default_images';

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

let deviceH = Dimensions.get('screen').height;
let bottomNavBarH = deviceH - screenHeight;

const MusicMenu = forwardRef((
            {
                  withoutAddToPlayList = false,
                  setTrackItemLike,
                  singerName = '',
                  musicData,
                  darkMode,
                  setDeleteModalVisible,
                  openAddPlaylistSheet,
                  openMusicText,
                  onShareTrack,
                  withoutDelete = true,

                  likedTrackLoading = false,
                  trackLikedTrackStatus,
                  likedTrackError = false,

                  getTrackLikedCheck,
                  myForwardedRef,
                  onDownload,
            }, ref) => {
    const menu = [
        {
            icon: <ShareIcon />,
            title: 'Поделиться',
            onPressMenuItem: onShareTrack
        },
        {
            icon: <SvgTextIcon />,
            title: 'Показать текст песни',
            onPressMenuItem: openMusicText
        },
        {
            icon: <SvgPlaylistIcon />,
            title: 'Добавить в плейлист',
            onPressMenuItem: openAddPlaylistSheet
        },
        {
            icon: <RemoveIcon />,
            title: 'Удалить',
            onPressMenuItem: setDeleteModalVisible
        }
    ];

    const handleSetLike = () => {
        if (musicData && musicData.id) setTrackItemLike(+musicData.id);
    };

    const renderTrackMenu = () => {
        const menuItems = [];

        menu.map((item, i) => {
            if ( (withoutDelete && item.title === 'Удалить') || (withoutAddToPlayList && item.title) === 'Добавить в плейлист' ) return false;

            menuItems.push(
                <Ripple key={item.title + i} onPress={item.onPressMenuItem}>
                    <View style={styles.actions}>
                        <View style={styles.actionIconWrapper}>
                            {item.icon}
                        </View>

                        <Text style={[styles.actionText, {color: darkMode ? 'white' : 'black'}]}>{item.title}</Text>
                    </View>
                </Ripple>
            );
        });

        return menuItems;
    };

    console.log('musicData', musicData)

    const getListOfSingers = () => {
        if (musicData && musicData.singers){
            let listOfSingers = musicData.singers.map( (item, i) => i + 1 === musicData.singers.length ? item.name : item.name + ' & ')
            console.log('listOfSingers', listOfSingers)

            return listOfSingers
        }
        return 'Неизвестный исполнитель'
    } 

    return (
        <RBSheet
            ref={ref}
            closeOnDragDown={true}
            customStyles={{
                container: {
                    minHeight: (withoutDelete || withoutAddToPlayList ? 320 : 350),
                    backgroundColor: darkMode ? COLORS.black : '#FCFCFC',
                    borderTopRightRadius: 16,
                    borderTopLeftRadius: 16,
                },
                wrapper: {
                    backgroundColor: 'rgba(51, 51, 51, 0.48)',
                },
                draggableIcon: {
                    width: 134,
                    backgroundColor: '#E4E4E4',
                },
            }}>
            <View>
                <View style={[styles.musicData, darkMode ? {} : {}]}>
                    <Image style={[styles.image, musicData.photo ? {} : { borderWidth: 1, borderColor: 'black' }]} source={musicData.photo ? {uri: musicData.photo} : logoImage} />

                    <View style={{ width: '60%' }}>
                        <Text style={{ color: darkMode ? 'white' : 'black' }}>Аудиозапись</Text>

                        <View style={styles.titleWrapper}>
                            <Text style={[styles.title, { color: darkMode ? 'white' : 'black' }]}>{getListOfSingers()}</Text>

                            <Text style={styles.time}>{musicData.duration}</Text>
                        </View>

                        <Text style={styles.extra}>{musicData.name}</Text>

                        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between' }}>
                            <Ripple style={styles.downloadBtn} onPress={onDownload}>
                                <Text style={{ color: COLORS.white, marginRight: 10 }}>Скачать</Text>

                                <SvgDownloadIcon color={COLORS.white} width={20} height={17} />
                            </Ripple>

                            {
                                likedTrackLoading ?

                                <View style={styles.likeLoading}>
                                    <ActivityIndicator />
                                </View> : !likedTrackError && !likedTrackLoading ?

                                <TouchableOpacity onPress={handleSetLike}>
                                    <SvgHeartIcon color={trackLikedTrackStatus ? 'red' : '#b7b7b7'} />
                                </TouchableOpacity>

                                : null
                            }
                        </View>
                    </View>
                </View>

                { renderTrackMenu() }
            </View>
        </RBSheet>
    )
});

export default MusicMenu

const styles = StyleSheet.create({
    likeLoading: {
        width: 20,
        height: 17,
        alignItems: 'center',
        justifyContent: 'center'
    },
    downloadBtn: {
        borderRadius: 13,
        paddingVertical: 4,
        paddingHorizontal: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        backgroundColor: COLORS.purple,
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    musicData: {
        marginTop: 10,
        flexDirection: 'row',
        marginBottom: 15,
        paddingHorizontal: 15,
    },
    image: {
        width: '30%',
        height: 100,
        borderRadius: 8,
        marginRight: 30,
    },
    actions: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15,
    },
    titleWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    time: {
        fontSize: 14,
        marginTop: 10,
        // right: '-200%',
        color: palette.common.titleGrey,
        position: 'relative',
    },
    actionIconWrapper: {
        width: 20,
        marginRight: 10,
    },
    actionText: {
        marginLeft: 10,
        fontSize: 18,
    },
    title: {
        fontSize: 14,
        marginTop: 10,
        fontWeight: 'bold',
    },
    extra: {
        fontSize: 14,
        marginTop: 3,
        color: '#8D8D8D',
        marginBottom: 12,
    },
});
