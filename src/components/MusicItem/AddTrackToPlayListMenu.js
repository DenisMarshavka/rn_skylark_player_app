import React, { forwardRef, useState, useEffect } from 'react';
import {StyleSheet, Text, View, Image, Dimensions, ScrollView, ActivityIndicator} from 'react-native';
import Ripple from 'react-native-material-ripple';
import RBSheet from 'react-native-raw-bottom-sheet';

import { COLORS } from '../../constants/colors.constants';
import GradientSquare from '../../assets/gradient_square.png'
import AddNewPlayList from "../AddNewPlayList/AddNewPlayList";
import {connect} from "react-redux";
import { addTrackToPlayList, createPlayList, getAddTrackToUserTrackListByTrackId, getPlayLists } from "../../actions";
import {logoImage} from "../../constants/default_images";
import AddPlaylistModal from "./AddPlaylistModal";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const heightPlayListItem = ( (screenWidth / (screenWidth >= 600 ? 3.5 : 2.05)) - 20 );
const widthPlayListItem  = ( (screenWidth / (screenWidth >= 600 ? 3.5 : 2.05)) - 20 );

const AddTrackToPlayListMenu = ({
    getPlayLists, 
    createPlayList, 
    loadingPlayLists, 
    requestFailed, 
    playLists, 
    myForwardedRef, 
    darkMode, 
    handleTrackToPlaylistAdd }) => {

    const [visibleAddPlayListModal, setVisibleAddPlayListModal] = useState(false);

    useEffect(() => {
        getPlayLists();
    }, []);

    const handleNewPlaylistCreate = newPlayListName => {
        setVisibleAddPlayListModal(false);
        createPlayList(newPlayListName);
    };

    const generateContent = () => (
        !requestFailed && !loadingPlayLists ?

        <View style={{
            flexDirection: 'row',
            alignItems: 'flex-start',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
        }}>
            <AddNewPlayList
                setAddPlaylistModalVisible={ () => setVisibleAddPlayListModal(true) }
                darkMode={darkMode}
                newStyles={{
                    minHeight: 155,
                    height: heightPlayListItem,
                    width: widthPlayListItem,
                    marginRight: ( screenWidth > 420 ) ? 15 : 0,
                    marginBottom: 10,
                }}
            />

            {
                playLists.map((item, i) => (
                    <Ripple
                        key={item.name + i}
                        onPress={() => handleTrackToPlaylistAdd(item.id)}
                        style={{ marginBottom: 10, marginRight: ( (screenWidth > 420) && (i % 2 !== 0) ) ? 15 : 0 }}
                    >
                        <Image
                            source={item.photo ? { uri: item.photo } : logoImage}
                            style={styles.image}
                        />

                        <Image
                            source={GradientSquare}
                            style={[ styles.image, { position: 'absolute' } ]}
                        />

                        <Text style={styles.title}>{item.name}</Text>
                    </Ripple>
                ))
            }
        </View> : !requestFailed && loadingPlayLists ?

        <View style={styles.fillFull}>
            <ActivityIndicator />
        </View> :

        <View style={styles.fillFull}>
            <Text style={{
                width: '100%',
                textAlign: 'center',
                color: !darkMode ? '#000' : '#fff',
                marginTop: 50,
            }}>Ошибка при получении данных</Text>
        </View>
    );

    return (
        <RBSheet
            ref={myForwardedRef}
            height={screenHeight * 0.8}
            closeOnDragDown={true}
            customStyles={{
                container: {
                    backgroundColor: darkMode ? COLORS.black : '#FCFCFC',
                    borderTopRightRadius: 16,
                    borderTopLeftRadius: 16
                },
                wrapper: {
                    backgroundColor: 'rgba(51, 51, 51, 0.48)',
                },
                draggableIcon: {
                    width: 134,
                    backgroundColor: '#E4E4E4',
                },
            }}>

            <ScrollView
                contentContainerStyle={styles.contentStyle}
                showsVerticalScrollIndicator={false}
            >
                <View style={styles.contentWrapper}>
                    { generateContent() }
                </View>
            </ScrollView>

            <AddPlaylistModal
                modalVisible={visibleAddPlayListModal}
                closeModal={() => setVisibleAddPlayListModal(false)}
                darkMode={darkMode}
                onCreatePlaylist={handleNewPlaylistCreate}
            />

        </RBSheet>
    )
};

const mapStateToProps = state => ({
    loadingPlayLists: state.playListReduce.loading,
    playLists: state.playListReduce.playLists,
});

const mapDispatchToProps = {
    getPlayLists,
    createPlayList,

    addTrackToPlayList,

    getAddTrackToUserTrackListByTrackId,
};

const AddTrackToPlaylistMenuConnected = connect(
    mapStateToProps,
    mapDispatchToProps
)(AddTrackToPlayListMenu);

export default forwardRef((props, ref) =>
    <AddTrackToPlaylistMenuConnected {...props} myForwardedRef={ref} />
);

const styles = StyleSheet.create({
    fillFull: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentStyle: {
        minHeight: '100%',
        paddingTop: 10,
        paddingBottom: 20,
        alignItems: 'center'
    },
    contentWrapper: {
        flex: 1,
        flexDirection:'row',
        flexWrap: 'wrap',
        justifyContent: screenWidth > 420 ? 'flex-start' : 'space-between',
        width: '92%',
    },
    image: {
        width: widthPlayListItem,
        height: heightPlayListItem,
        // width: screenWidth / 2 - 20,
        // height: screenWidth / 2 - 25,
        borderRadius: 8,
        resizeMode: 'cover'
    },
    title: {
        fontSize: 18,
        color: 'white',
        position: 'absolute',
        bottom: 14,
        left: 16
    }
});
