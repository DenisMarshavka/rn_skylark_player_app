import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import AuthBackground from '../../components/AuthBackground/AuthBackground.component';
import {COLORS} from '../../constants/colors.constants';

const AuthBlank = ({ title, buttonText, onPressBtn, children, enableScroll, hasError = false, loading = false }) => (
  <AuthBackground>
    <View style={styles.blankContainer}>
      <Text style={styles.titleText}>{title}</Text>

      {enableScroll ? (
        <ScrollView contentContainerStyle={styles.contentStyle} 
        // showsVerticalScrollIndicator={false}
        >
          {children}
          <TouchableOpacity style={[styles.btnWrapper, {backgroundColor: COLORS.purple }]} onPress={() => loading ? console.log('loading...') : onPressBtn()}>
          {loading ? <ActivityIndicator color='white' size='small' /> : <Text style={styles.btnText}>{buttonText || 'Далее'}</Text>}
          </TouchableOpacity>
        </ScrollView>
      ) : (
        <View style={styles.contentStyle}>
          {children}

          <TouchableOpacity style={[styles.btnWrapper, {backgroundColor: COLORS.purple }]} onPress={() => loading ? console.log('loading...') : onPressBtn()}>
            {loading ? <ActivityIndicator color='black' size='small' /> : <Text style={styles.btnText}>{buttonText || 'Далее'}</Text>}
          </TouchableOpacity>
        </View>
      )}
    </View>
  </AuthBackground>
);

const styles = StyleSheet.create({
  blankContainer: {
    flex: 1,
    width: '100%',
    maxWidth: 550,
    marginTop: 100,
    backgroundColor: COLORS.white,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    // alignItems: 'center',
  },
  titleText: {
    fontSize: 22,
    fontWeight: 'bold',
    marginVertical: 25,
    textAlign: 'center',
  },
  contentStyle: {
    width: '100%',
    alignItems: 'center',
  },
  btnWrapper: {
    width: 173,
    height: 50,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 60,
  },
  btnText: {
    color: COLORS.white,
    fontSize: 18,
  },
});

export default AuthBlank;
