import React, {forwardRef} from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';

import SearchIcon from 'icons/search';

import {TextInput} from '../../components/';

const SearchBlock = ({ onEndEditing, value, onChange, onSearchStart = () => {}, style, placeholder = 'Поиск', darkMode }) => (
  <View style={[styles.searchBlock, style]}>
    <TouchableOpacity activeOpacity={.5} onPress={onSearchStart}>
      <SearchIcon />
    </TouchableOpacity>

    <View style={styles.searchInputWrapper}>
      <TextInput
        onEndEditing={onEndEditing}
        keyboardType="default"
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        darkMode={darkMode}
        onSubmitEditing={onSearchStart}
        returnKeyType='search'
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
  searchInputWrapper: {
    flexGrow: 1,
    marginLeft: 12,
  },
  searchBlock: {
    paddingVertical: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default SearchBlock;
