export const palette = {
  common: {
    primary: '#7B3DFF',
    secondary: '#FFC42E',
    lightGrey: '#c4c4c4',
    titleGrey: '#8d8d8d',
    disableSwitchButton: '#878787',
  },
  light: {
    text: '#000',
    container: '#fff',
    default: '#fff',
    grey: '#7b7b7b',
    pink: '#e89799',
    darkGrey: 'rgba(0, 0, 0, 0.12)',
    fontColor: 'rgba(0, 0, 0, 0.87)',
    lightColor: 'rgba(0, 0, 0, 0.54)',
  },
  dark: {
    text: '#fff',
    container: '#111',
    primary: '#7B3DFF',
    default: '#fff',
    grey: '#7b7b7b',
    pink: '#e89799',
    darkGrey: 'rgba(0, 0, 0, 0.12)',
    fontColor: 'rgba(0, 0, 0, 0.87)',
    lightColor: 'rgba(0, 0, 0, 0.54)',
  },
};

export const extraButtonStyle = {
  pink: {
    fontSize: 24,
    paddingVertical: 5,
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
};
