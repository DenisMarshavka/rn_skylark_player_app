import * as React from 'react';
import Svg, {Path, Circle} from 'react-native-svg';

function SvgSuccessIcon(props) {
    return (
        <Svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path d="M12 24L21 33L32.9694 13" stroke="#FFC42E" stroke-width="3" stroke-linecap="round"/>
            <Circle cx="23" cy="23" r="21.5" stroke="#FFC42E" stroke-width="3"/>
        </Svg>
    );
}

export default SvgSuccessIcon;
