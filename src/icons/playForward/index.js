import * as React from 'react';
import Svg, {
  Rect,
  Defs,
  LinearGradient,
  Stop,
  G,
  Circle,
  Path,
} from 'react-native-svg';

function SvgPlayForwardIcon({color = '#fff', ...rest}) {
  return (
    <Svg width={27} height={16} viewBox="0 0 27 16" fill="none" {...rest}>
      <Rect width="100%" height="100%" />
      <Defs>
        <LinearGradient
          id="prefix__a"
          x1={0.255}
          y1={0.857}
          x2={1.18}
          y2={-0.035}>
          <Stop stopColor="#7B3DFF" />
          <Stop offset={1} stopColor="#DED1FA" />
        </LinearGradient>
      </Defs>
      <G className="prefix__currentLayer">
        <Circle
          cx={-273.791}
          cy={-23.789}
          r={35.5}
          fill={color}
          stroke="url(#prefix__a)"
          strokeWidth={5}
        />
        <Path
          d="M-137.653-53.11a1 1 0 010 1.732l-21.75 12.557a1 1 0 01-1.5-.866V-64.8a1 1 0 011.5-.866l21.75 12.557z"
          fill="#7B3DFF"
        />
        <Path
          d="M13.077 6.888a1 1 0 010 1.732l-11.25 6.495a1 1 0 01-1.5-.866V1.26a1 1 0 011.5-.866l11.25 6.495zM-184.82 79.203a1 1 0 010-1.732l11.25-6.495a1 1 0 011.5.866v12.99a1 1 0 01-1.5.867l-11.25-6.496z"
          fill={color}
        />
        <Path
          d="M25.995 6.888a1 1 0 010 1.732l-11.25 6.495a1 1 0 01-1.5-.866V1.26a1 1 0 011.5-.866l11.25 6.495zM-197.82 79.203a1 1 0 010-1.732l11.25-6.495a1 1 0 011.5.866v12.99a1 1 0 01-1.5.867l-11.25-6.496z"
          fill={color}
        />
      </G>
    </Svg>
  );
}

export default SvgPlayForwardIcon;
