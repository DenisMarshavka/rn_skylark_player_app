import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function SvgArrowLeftIcon(props) {
  return (
    <Svg width={12} height={21} viewBox="0 0 12 21" fill="none" {...props}>
      <Path
        d="M10 19l-8-8.5L10 2"
        stroke={props.color || '#000'}
        strokeWidth={3}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default SvgArrowLeftIcon;
