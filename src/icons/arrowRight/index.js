import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function SvgArrowRightIcon({color = '#D5D5D5', ...rest}) {
  return (
    <Svg width={12} height={19} viewBox="0 0 12 19" fill="none" {...rest}>
      <Path
        d="M2 2l8 7.377L2 17"
        stroke={color}
        strokeWidth={2.5}
        strokeLinecap="round"
      />
    </Svg>
  );
}

export default SvgArrowRightIcon;
