import * as React from 'react';
import Svg, {Circle} from 'react-native-svg';

function SvgMenuIcon({color = '#fff', ...rest}) {
  return (
    <Svg width={5} height={21} viewBox="0 0 5 21" fill="none" {...rest}>
      <Circle cx={2.278} cy={10.25} r={2.278} fill={color} />
      <Circle cx={2.278} cy={18.222} r={2.278} fill={color} />
      <Circle cx={2.278} cy={2.278} r={2.278} fill={color} />
    </Svg>
  );
}

export default SvgMenuIcon;
