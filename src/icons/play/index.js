import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function SvgPlayIcon(props) {
  return (
    <Svg width={12} height={15} viewBox="0 0 12 15" fill="none" {...props}>
      <Path
        d="M11.5 6.634a1 1 0 010 1.732l-9.75 5.63a1 1 0 01-1.5-.867V1.871a1 1 0 011.5-.866l9.75 5.629z"
        fill="#fff"
      />
    </Svg>
  );
}

export default SvgPlayIcon;
