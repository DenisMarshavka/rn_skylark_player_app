import * as React from 'react';
import Svg, {Circle, Path, Defs, LinearGradient, Stop} from 'react-native-svg';

function SvgRadioPlayIcon({width = 34, height = 34, ...rest}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 34 34"
      fill="none"
      {...rest}>
      <Circle
        cx={17}
        cy={17}
        r={16}
        fill="#fff"
        stroke="url(#prefix__paint0_linear)"
        strokeWidth={2}
      />
      <Path
        d="M24 16.134a1 1 0 010 1.732l-9.75 5.63a1 1 0 01-1.5-.867V11.371a1 1 0 011.5-.866L24 16.134z"
        fill="#7B3DFF"
      />
      <Defs>
        <LinearGradient
          id="prefix__paint0_linear"
          x1={9.208}
          y1={28.333}
          x2={38.604}
          y2={0}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#7B3DFF" />
          <Stop offset={1} stopColor="#DED1FA" />
        </LinearGradient>
      </Defs>
    </Svg>
  );
}

export default SvgRadioPlayIcon;
