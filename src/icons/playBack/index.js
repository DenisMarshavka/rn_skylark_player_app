import * as React from 'react';
import Svg, {Rect, G, Path} from 'react-native-svg';

function SvgPlayBackIcon({color = '#fff', ...rest}) {
  return (
    <Svg width={26.818} height={15.455} viewBox="0 0 27 16" fill="none" {...rest}>
      <Rect width="100%" height="100%" />
      <G className="prefix__currentLayer">
        <Path
          d="M13.726 8.82a1 1 0 010-1.731L24.976.593a1 1 0 011.5.866v12.99a1 1 0 01-1.5.867L13.726 8.82z"
          fill={color}
        />
        <Path
          d="M.726 8.82a1 1 0 010-1.731L11.976.593a1 1 0 011.5.866v12.99a1 1 0 01-1.5.867L.726 8.82z"
          fill={color}
        />
      </G>
    </Svg>
  );
}

export default SvgPlayBackIcon;
