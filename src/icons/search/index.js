import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function SvgSearchIcon(props) {
  return (
    <Svg width={24} height={24} viewBox="0 0 24 24" fill="none" {...props}>
      <Path
        d="M9.801 19.417A9.863 9.863 0 0015.5 17.6l6.041 5.984c.28.278.65.417 1.033.417C23.4 24 24 23.369 24 22.56c0-.378-.128-.732-.408-1.01l-6.003-5.958a9.611 9.611 0 002.014-5.883C19.603 4.369 15.193 0 9.8 0 4.397 0 0 4.368 0 9.709c0 5.34 4.397 9.708 9.801 9.708zm0-2.096c-4.218 0-7.685-3.446-7.685-7.612 0-4.167 3.467-7.613 7.685-7.613 4.206 0 7.686 3.446 7.686 7.613 0 4.166-3.48 7.612-7.686 7.612z"
        fill="#7B3DFF"
      />
    </Svg>
  );
}

export default SvgSearchIcon;
