import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function SvgHeartIcon(props) {
  return (
    <Svg width={26} height={25} viewBox="0 0 26 25" fill="none" {...props}>
      <Path
        d="M13 25c.275 0 .668-.19.956-.368C21.326 19.73 26 14.025 26 8.224 26 3.404 22.819 0 18.708 0 16.155 0 14.191 1.47 13 3.717 11.835 1.484 9.845 0 7.292 0 3.182 0 0 3.404 0 8.224c0 5.801 4.674 11.506 12.057 16.408.275.177.668.368.943.368z"
        fill={props.color}
      />
    </Svg>
  );
}

export default SvgHeartIcon;
