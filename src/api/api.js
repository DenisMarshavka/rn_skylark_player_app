import axios from 'axios'
import {API_URL} from '../constants/api_config'
import {getToken} from "../store/authReducer";


export const instance = axios.create({
    withCredentials: true,
    baseURL: API_URL,
    headers: {
        "Content-Type": "application/json",
    }
})


export const authAPI = {

    registration: (name, phone, email, password) => {

        // +7%20(900)%20311-07-99 || +720900203110799

        return instance.post(`signup?name=${name}&phone=${phone}&email=${email}&password=${password}`)
            .then(response => response.data)
    },

    login: (phone, password, isEmail) => {

        // +7%20(900)%20311-07-99 || +720900203110799 || email

        if (isEmail){
            return instance.post(`login?email=${phone}&password=${password}`)
            .then(response => response.data)
        } else {
            return instance.post(`login?phone=${phone}&password=${password}`)
                .then(response => response.data)
        }
        
    },

    // smsCode: (email, secure_token) => {
    //     return instance.post(`services/login?email=${email}&secure_token=${secure_token}`)
    //         .then(response => response.data)
    // },

    // restorePassword: (token, currentPassword, newPassword) => {
    //     return instance.post(`me/password?token=${token}&current_password=${currentPassword}&new_password=${newPassword}`)
    //         .then(response => response.data)
    // },

}