import RNFetchBlob from 'rn-fetch-blob';

import {
    DOWNLOAD_DIR,
    DOWNLOAD_TRACKS_SESSION_KEY,
} from '../../constants/storage';

import {
    addTrackToStorage,
    getTracksFromStorage,
    removeTracksFromStorage,
    removeFiles,
    isFileExist,
    getFileNameViaPath,
    getFileNameViaTrack,
    removeDownloadedTrack as removeTrack,
} from '../../utils/download';

export const types = {
    GET_DOWNLOADED_TRACKS_STARTED: 'GET_DOWNLOADED_TRACKS_STARTED',
    DOWNLOAD_SET_DOWNLOADED_TRACKS: 'DOWNLOAD_SET_DOWNLOADED_TRACKS',
    GET_DOWNLOADED_TRACKS_FAILED: 'GET_DOWNLOADED_TRACKS_FAILED',
    DOWNLOAD_SET_TRACK_PENDING: 'DOWNLOAD_SET_TRACK_PENDING',
    DOWNLOAD_ADD_TRACK_PENDING: 'DOWNLOAD_ADD_TRACK_PENDING',
    DOWNLOAD_REMOVE_TRACK_PENDING: 'DOWNLOAD_REMOVE_TRACK_PENDING',
    DOWNLOAD_FETCH_TRACK_START: 'DOWNLOAD_FETCH_TRACK_START',
    DOWNLOAD_FETCH_TRACK_PROGRESS: 'DOWNLOAD_FETCH_TRACK_PROGRESS',
    DOWNLOAD_FETCH_TRACK_SUCCESS: 'DOWNLOAD_FETCH_TRACK_SUCCESS',
    DOWNLOAD_FETCH_TRACK_ERROR: 'DOWNLOAD_FETCH_TRACK_ERROR',
    DOWNLOAD_FETCH_TRACK_RESET: 'DOWNLOAD_FETCH_TRACK_RESET',
};

export const getDownloadedTracksStarted = () => ({
    type: types.GET_DOWNLOADED_TRACKS_STARTED
});

export const getDownloadedTracksError = () => ({
    type: types.GET_DOWNLOADED_TRACKS_FAILED
});

export const getDownloadedTracks = () => async dispatch => {
    try {
        dispatch(getDownloadedTracksStarted());

        const data = await getTracksFromStorage();

        if (data.length > 0) {

            const promises = await data.map(item => isFileExist(item.localPath));
            const existingFiles = await Promise.all(promises);

            const existingDownloadedTracks = data.filter((track, index) => {
                const exist = existingFiles[index];
                return exist;
            });

            dispatch({type: types.DOWNLOAD_SET_DOWNLOADED_TRACKS, payload: existingDownloadedTracks});
        } else {
            dispatch({type: types.DOWNLOAD_SET_DOWNLOADED_TRACKS, payload: []});
        }

    } catch (e) {
        dispatch(getDownloadedTracksError(e));
    }
};

export const removeDownloadedTracks = () => async dispatch => {
    try {
        await removeFiles();
        await removeTracksFromStorage();
        await dispatch(cleanDownloadTracksSession());
        await dispatch({type: types.DOWNLOAD_SET_DOWNLOADED_TRACKS, payload: []});
    } catch (e) {
    }
};

export const addTrackToPending = (id) => ({type: types.DOWNLOAD_ADD_TRACK_PENDING, payload: id});

export const removeTrackFromPending = (id) => ({type: types.DOWNLOAD_REMOVE_TRACK_PENDING, payload: id});

export const downloadTrack = (data) => async dispatch => {
    try {
        dispatch(addTrackToPending(data.id));
        dispatch({type: types.DOWNLOAD_FETCH_TRACK_START});

        const link = data.audio;

        // for subscribe user
        // const filename = getFileNameViaTrack(data);

        // for unsubscribe user
        // let filename = getFileNameViaPath(link);
        // const name = filename.split('.')[0];
        // const extension = '.skl';
        // filename = name + extension;

        RNFetchBlob
            .config({
                session: DOWNLOAD_TRACKS_SESSION_KEY,
                fileCache: true,
                // addAndroidDownloads : {
                //     useDownloadManager : true,
                //     notification : true,
                //     path: `${DOWNLOAD_DIR}${filename}`,
                // }
            })
            .fetch('GET', link)
            .progress({ count : 5 }, (received, total) => {
                dispatch({type: types.DOWNLOAD_FETCH_TRACK_PROGRESS, payload: ((received / total) * 100).toFixed(0)});
            })
            .then(async (res) => {
                const localTrack = {
                    ...data,
                    localPath: res.path(),
                    localDateUpdated: new Date(),
                };
                await addTrackToStorage(localTrack);
                dispatch(removeTrackFromPending(data.id));
                dispatch({type: types.DOWNLOAD_FETCH_TRACK_SUCCESS});
            });

    } catch (e) {
        dispatch(removeTrackFromPending(data.id));
        dispatch({type: types.DOWNLOAD_FETCH_TRACK_ERROR});
    }
};

export const cleanDownloadTracksSession = () => async dispatch => {
    try {
        RNFetchBlob
            .session(DOWNLOAD_TRACKS_SESSION_KEY)
            .dispose()
            .then(() => {})

    } catch (e) {
    }
};

export const cleanDownloadedPlaylist = () => async dispatch => {
    await dispatch({type: types.DOWNLOAD_SET_DOWNLOADED_TRACKS, payload: []});
};

export const resetDownloadState = () => async dispatch => {
    await dispatch({type: types.DOWNLOAD_FETCH_TRACK_RESET});
};

export const removeDownloadedTrack = (track) => async dispatch => {
    try {
        await removeTrack(track);
        await dispatch(getDownloadedTracks());
    } catch (e) {
    }
};