import AsyncStorage from "@react-native-community/async-storage";

import {
    PLAYER_CONFIG_REPEAT,
    PLAYER_CONFIG_SHUFFLE,
} from '../../constants/storage';

export const types = {
    PLAYER_INIT: 'PLAYER_INIT',
    PLAYER_SET_TRACK: 'PLAYER_SET_TRACK',
    PLAYER_SET_REPEAT: 'PLAYER_SET_REPEAT',
    PLAYER_SET_SHUFFLE: 'PLAYER_SET_SHUFFLE',
    PLAYER_SET_SHUFFLE_QUEUE: 'PLAYER_SET_SHUFFLE_QUEUE',
    PLAYER_REMOVE_TRACK_FROM_PLAYLIST: 'PLAYER_REMOVE_TRACK_FROM_PLAYLIST',
    PLAYER_CLEAN: 'PLAYER_CLEAN',
    PLAYER_RESET: 'PLAYER_RESET',
};

import { togglePlayerType } from '../../store/appReducer';

export const clearPlayer = () => ({
    type: types.PLAYER_CLEAN
});

export const playerReset = () => ({
    type: types.PLAYER_RESET
});

export const playerInit = (data) => dispatch => {
    dispatch({type: types.PLAYER_INIT, payload: data});
    dispatch(togglePlayerType(false));
};

export const playerSetTrackAction = (data) => ({type: types.PLAYER_SET_TRACK, payload: data});

export const setRepeat = data => ({
    type: types.PLAYER_SET_REPEAT,
    payload: data,
});

export const playerSetRepeat = (isRepeat) => async dispatch => {
    try {
        dispatch(setRepeat({ isRepeat: isRepeat }));
        await AsyncStorage.setItem(PLAYER_CONFIG_REPEAT, isRepeat ? '1' : '0');
        return true;
    } catch (e) {
        console.log(e)
    }
};

export const setShuffle = data => ({
    type: types.PLAYER_SET_SHUFFLE,
    payload: data,
});

export const playerSetShuffle = (isShuffle) => async dispatch => {
    try {
        dispatch(setShuffle({ isShuffle: isShuffle }));
        await AsyncStorage.setItem(PLAYER_CONFIG_SHUFFLE, isShuffle ? '1' : '0');
        return true;
    } catch (e) {
        console.log(e)
    }
};

export const playerSetShuffleQueue = (data) => dispatch => {
    dispatch({type: types.PLAYER_SET_SHUFFLE_QUEUE, payload: data});
};

export const getPlayerSettings = () => async dispatch => {
    try {
        let repeat = await AsyncStorage.getItem(PLAYER_CONFIG_REPEAT);
        let shuffle = await AsyncStorage.getItem(PLAYER_CONFIG_SHUFFLE);

        repeat === '1' ? dispatch(setRepeat({ isRepeat: true })) : dispatch(setRepeat({ isRepeat: false }));
        shuffle === '1' ? dispatch(setShuffle({ isShuffle: true })) : dispatch(setShuffle({ isShuffle: false }));

        return true;
    } catch (error) {
        console.log(error);
    }
};

export const removeTrackFromPlaylist = (id) => dispatch => {
    dispatch({type: types.PLAYER_REMOVE_TRACK_FROM_PLAYLIST, payload: id});
};