import { togglePlayerType } from '../../store/appReducer';

export const types = {
    RADIO_PLAYER_INIT: 'RADIO_PLAYER_INIT',
    RADIO_PLAYER_SET_TRACK: 'RADIO_PLAYER_SET_TRACK',
    RADIO_PLAYER_RESET: 'RADIO_PLAYER_RESET',
};

export const radioPlayerReset = () => ({
    type: types.RADIO_PLAYER_RESET
});

export const radioPlayerInit = (data) => dispatch => {
    dispatch({type: types.RADIO_PLAYER_INIT, payload: data});
    dispatch(togglePlayerType(true));
};

export const radioPlayerSetTrackAction = (data) => ({type: types.RADIO_PLAYER_SET_TRACK, payload: data});