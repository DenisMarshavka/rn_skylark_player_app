export const types = {
    MINI_PLAYER_START: 'MINI_PLAYER_START',
    MINI_PLAYER_SET_TRACK: 'MINI_PLAYER_SET_TRACK',
    MINI_PLAYER_STOP: 'MINI_PLAYER_STOP',
};

export const miniPlayerStart = (data) => dispatch => {
    dispatch({type: types.MINI_PLAYER_START, payload: data});
};

export const miniPlayerSetTrackAction = (data) => ({type: types.MINI_PLAYER_SET_TRACK, payload: data});

export const miniPlayerStop = () => ({
    type: types.MINI_PLAYER_STOP
});