import {
    CLEAR_SINGER_DATA,
    CREATE_PLAY_LIST_SUCCESS,
    DELETE_PLAY_LIST_SUCCESS,
    DELETE_PLAY_LIST_TRACK_SUCCESS,
    GET_DATA_SEARCH_SCREEN_FAILED,
    START_FETCH_DATA_SEARCH_SCREEN_TRACK_LIST,
    GET_DATA_START_TOP_TACK_LIST,
    GET_DATA_MOODS_START,
    GET_MOODS_SUCCESS,
    ERROR_MOODS_LIST_REQUEST,
    GET_PLAY_LIST_TRACKS_SUCCESS,
    GET_PLAY_LISTS_DATA_ERROR,
    GET_PLAY_LISTS_DATA_STARTED,
    GET_PLAY_LISTS_SUCCESS,
    GET_RADIO_GENRE_SUCCESS,
    GET_RADIO_STATIONS_SUCCESS,
    GET_SINGER_ALBUMS_SUCCESS,
    GET_SINGER_DATA_ERROR,
    GET_SINGER_DATA_STARTED,
    GET_SINGER_TRACK_LIST_SUCCESS,
    GET_START_SINGERS_TACK_LIST_DATA,
    GET_SINGERS_SUCCESS,
    GET_TRACK_LIST_ERROR,
    GET_TRACK_LIST_SEARCH_SCREEN_SUCCESS,
    GET_TRACK_LIST_STARTED,
    GET_TRACK_LIST_SUCCESS,
    GET_TRACK_LIST_TOP_SUCCESS,
    PLAY_LIST_TRACKS_EMPTY,
    PLAY_LISTS_EMPTY,
    SET_USER_DATA,
    START_GET_USER_DATA,
    GET_RESET_SEARCH_SCREEN_DATA,
    GET_START_FETCH_PLAY_LIST_TRACKS_DATA,
    ERROR_TRACK_LIST_TOP_REQUEST,
    ERROR_SINGERS_LIST_REQUEST,
    GET_START_RADIO_MOODS_REQUEST,
    GET_RADIO_MOODS_DATA_SUCCESS,
    GET_RADIO_MOODS_REQUEST_FAILED,
    GET_TRACK_LIST_BY_USER_TOKEN_EMPTY,
    ADD_TRACK_TO_USER_TRACK_LIST_SUCCESS,
    ADD_TRACK_TO_USER_TRACK_LIST_START,
    ADD_TRACK_TO_USER_TRACK_LIST_FAILED,
    TRACK_TO_USER_TRACK_LIST_IS_ALREADY,
    RESET_TRACK_TO_USER_TRACK_LIST_IS_ALREADY,
    GET_RADIO_GENRE_START,
    GET_RADIO_GENRE_REQUEST_FAILED,
    GET_RADIO_STATIONS_DATA_STARTED,
    GET_RADIO_STATIONS_REQUEST_FAILED,
    GET_RADIO_RECOMMENDATIONS_STATIONS_DATA_STARTED,
    GET_RADIO_RECOMMENDATIONS_STATIONS_SUCCESS,
    GET_RADIO_RECOMMENDATIONS_STATIONS_REQUEST_FAILED,
    SET_RADIO_STATIONS_LISTENING_STARTED,
    SET_RADIO_STATIONS_LISTENING_SUCCESS,
    SET_RADIO_STATIONS_LISTENING_REQUEST_FAILED,
    GET_SINGER_TRACK_LIST_DATA_START,
    GET_SINGER_TRACK_LIST_DATA_FAILED,
    GET_SEARCH_RADIO_STATIONS_START,
    GET_SEARCH_RADIO_STATIONS_SUCCESS,
    GET_SEARCH_RADIO_STATIONS_FAILED,
    RESET_SEARCH_RADIO_STATIONS,
    ADD_TRACK_TO_PLAY_LIST_FAILED,
    ADD_TRACK_TO_PLAY_LIST_START,
    ADD_TRACK_TO_PLAY_LIST_SUCCESS,
    RESET_PLAY_LIST_TRACKS_DATA,
    TRACK_LIST_CLEAN,
    GET_SINGER_ALBUMS_DATA_START,
    GET_SINGER_ALBUMS_FAILED,
    CHECK_LIKED_TRACK_START,
    CHECK_LIKED_TRACK_SUCCESS,
    CHECK_LIKED_TRACK_FAILED,
    GET_RADIO_DATA_ERROR,
    GET_RADIO_LIST_BY_MOOD_SUCCESS,
    GET_RECENT_LISTENING_START,
    GET_RECENT_LISTENING_SUCCESS,
    GET_RECENT_LISTENING_FAILED,
    SET_TRACK_LISTENING_STARTED,
    SET_TRACK_LISTENING_SUCCESS,
    SET_TRACK_LISTENING_REQUEST_FAILED,
    GET_TRACK_BY_ALBUM_ID_SUCCESS,
    GET_TRACK_BY_ALBUM_ID_FAILED,
    GET_TRACK_BY_ALBUM_ID_START,
    GET_RADIO_RECENTLY_STATIONS_REQUEST_FAILED,
    GET_RADIO_RECENTLY_STATIONS_SUCCESS,
    GET_RADIO_RECENTLY_STATIONS_DATA_STARTED,
    GET_WEEK_POPULAR_TRACKS_DATA_START,
    GET_WEEK_POPULAR_TRACKS_DATA_SUCCESS,
    GET_WEEK_POPULAR_TRACKS_DATA_FAILED,
    GET_MONTH_POPULAR_TRACKS_DATA_START,
    GET_MONTH_POPULAR_TRACKS_DATA_SUCCESS,
    GET_MONTH_POPULAR_TRACKS_DATA_FAILED, GET_RADIO_DATA_STARTED, GET_RADIO_STATIONS_BY_GENRE_ID_SUCCESS,
    GET_START_POPULAR_SINGERS_TACK_LIST_DATA, GET_POPULAR_SINGERS_SUCCESS, ERROR_POPULAR_SINGERS_LIST_REQUEST
} from "../constants/ActionsTypes";
// import AsyncStorage from "@react-native-community/async-storage";
import { getToken, getUserId } from "../store/authReducer";
import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

const BASE_URL = 'https://skylarkmusic.ru/api';

//For the Tracks screen
export const getTrackListStarted = () => ({
    type: GET_TRACK_LIST_STARTED
});

export const getAllTracksList = () => async dispatch => {
    try {
        dispatch(getTrackListStarted());

        await axios.get(`${BASE_URL}/tracklist`)
            .then(res => {
                const data = res.data;

                // console.log('Received data get All Track list: ', res, 'data', data);
                if (data.data) {
                    // console.log('all data', data.data);
                    dispatch(getTrackListSuccess(data.data));
                } else if (data.status === 400) dispatch(getTrackListByUserTokenIsEmpty());//todo
            });
    } catch (e) {
        dispatch(getTrackListError(e));

        console.log('Error the request to All Track list: ', e);
    }
};




export const getTrackList = () => dispatch => {
    dispatch(getTrackListStarted());
    fetch(`${BASE_URL}/tracklist`)
        .then(response => response.json())
        .then(res => {

            // throw new Error('NOT!');
            dispatch(getTrackListSuccess(res.data));
        })
        .catch(err => {
            dispatch(getTrackListError(err.message));
        });
};

export const cleanTrackList = () => ({
    type: TRACK_LIST_CLEAN,
});

export const getTrackListByUserToken = () => async dispatch => {
    try {
        dispatch(getTrackListStarted());

        let token = await getToken();
        let sortBy = await AsyncStorage.getItem('orderNew');

        if (token) {
            await axios.get(`${BASE_URL}/user/tracklist?token=${token}&sortBy=${+sortBy}`)
                .then(res => {
                    const data = res.data;

                    // console.log('Received data get Track list by User Token: ', res, 'data', data);

                    if (data.status === 200) {
                        // console.log('Received data get Track list by User Token: ', res, 'data.status: ', data.status, 'data.data.tracklist.data: ', data.data.tracklist.data);

                        dispatch(getTrackListSuccess(data.data.tracklist.data));
                    } else if (data.status === 400) dispatch(getTrackListByUserTokenIsEmpty());
                });
        } else console.log('Error token when created request to take a Track list by a User Token');
    } catch (e) {
        dispatch(getTrackListError(e));

        console.log('Error the request to Track list by a User Token: ', e);
    }
};

export const getTrackListSuccess = trackList => ({
    type: GET_TRACK_LIST_SUCCESS,
    payload: trackList
});

export const getTrackListByUserTokenIsEmpty = () => ({
    type: GET_TRACK_LIST_BY_USER_TOKEN_EMPTY,
});

export const getTrackListError = error => ({
    type: GET_TRACK_LIST_ERROR,
    payload: {
        error
    }
});




export const getSearchPopularTracksData = searchValue => async dispatch => {
    try {
        dispatch(getTrackListStarted());

        if (searchValue.trim()) {
            const data = await axios.get(`${BASE_URL}/search/new_releases?search=${searchValue.trim()}`)
                                        .then(res => res.data);

            //console.log('Received data when taking Search Popular Tracks Data: ', data, 'data.status: ', data.status, 'data.data.result: ', data.data.result);
            dispatch(getTrackListSuccess(data.data.result));
        } else console.log('Error because received the bad property "searchValues": ', searchValue);
    } catch (e) {
        dispatch(getTrackListError(e));

        console.log('Error the request to Search Popular Tracks Data: ', e);
    }
};




export const getSearchList = (type = 'user_songs', search) => async dispatch => {
    try {
        dispatch(getTrackListStarted());

        const userId = await getUserId()

        if (userId) {
            let tracks = await axios.get(`${BASE_URL}/search/${type}?search=${search}&user_id=${userId}`)

            if (tracks.data.status === 200){
                dispatch(getTrackListSuccess(tracks.data.data.result));
                if (tracks.data.data.result.length === 0) {
                    dispatch(getTrackListByUserTokenIsEmpty());
                }
                return true
            } else {
                dispatch(getTrackListError('Error'));
                return false
            }
        } else {
            console.log('Error because received the bad property "userId", when created the request Grt Search List to the API: ', userId);

            dispatch(getTrackListError('Error'))
        }
    } catch (e) {
        console.log('Error request getSearchList', e)
        dispatch(getTrackListError(e.message));
    }
};




export const getPopularTracksListBySinger = () => async (dispatch, getState) => {
    try {
        dispatch(getTrackListStarted());

        const singerId = getState().singerReducer.singerId;

        if (singerId) {
            const response = await axios.get(`${BASE_URL}/singer/${singerId}/tracks`)
                .then(result => result.data);

            console.log('Get Popular Tracks List By Singer result Data: ', response, 'result data: ', response.data.tracklist.data);
            dispatch(getTrackListSuccess(response.status === 200 ? response.data.tracklist.data : []));
        } else {
            console.log('Error because received the bad property "singerId", when created the request Grt Popular Tracks List By Singer to the API: ', singerId);

            dispatch(getTrackListError('Error'));
        }
    } catch (e) {
        console.log('Error Get Popular Tracks List By Singer the request to the API: ', e);

        dispatch(getTrackListError(e.message));
    }
};


//For the Music Tab screen
const getDataTopTrackListStart = () => ({
    type: GET_DATA_START_TOP_TACK_LIST
});

export const getTrackListTop = () => dispatch => {
    dispatch(getDataTopTrackListStart());

    fetch(`${BASE_URL}/tracklist_top`)
        .then(response => response.json())
        .then(res => {
            // console.log('Received Data when created the request to the Top Track List from the API: ', res, 'res.data.list: ', res.data.list);
            console.log('----- mus ------', res.data)
            dispatch(getTrackListTopSuccess(res.data));
        })
        .catch(err => {
            dispatch(getTopTrackListDataFailed(err.message));
        });
};

const getTrackListTopSuccess = trackListTop => ({
    type: GET_TRACK_LIST_TOP_SUCCESS,
    payload: trackListTop
});

const getTopTrackListDataFailed = error => ({
    type: ERROR_TRACK_LIST_TOP_REQUEST,
    payload: {error},
});




export const getStartSingersListsData = () => ({
    type: GET_START_SINGERS_TACK_LIST_DATA
});

export const getSingers = () => dispatch => {
    dispatch(getStartSingersListsData());
    fetch(`${BASE_URL}/singers`)
        .then(response => response.json())
        .then(res => {
            // console.log('Received the Data from the request to Singers List API: ', res);

            dispatch(getSingersListSuccess(res.data.list.data));
        })
        .catch(err => {
            dispatch(getSingersListDataFailed(err.message));
        });
};

const getSingersListSuccess = singers => ({
    type: GET_SINGERS_SUCCESS,
    payload: singers
});

const getSingersListDataFailed = error => ({
    type: ERROR_SINGERS_LIST_REQUEST,
    payload: {error},
});



export const getStartPopularSingersListsData = () => ({
    type: GET_START_POPULAR_SINGERS_TACK_LIST_DATA
});

export const getPopularSingers = (page = 1, perPage = 10) => dispatch => {
    dispatch(getStartPopularSingersListsData());
    fetch(`${BASE_URL}/popular-singers?perPage=${perPage}&page=${page}`)
        .then(response => response.json())
        .then(res => {
            dispatch(getPopularSingersListSuccess(res.data.list));
        })
        .catch(err => {
            dispatch(getPopularSingersListDataFailed(err.message));
        });
};

const getPopularSingersListSuccess = singers => ({
    type: GET_POPULAR_SINGERS_SUCCESS,
    payload: singers
});

const getPopularSingersListDataFailed = error => ({
    type: ERROR_POPULAR_SINGERS_LIST_REQUEST,
    payload: {error},
});




const getDataMoodsStart = () => ({
    type: GET_DATA_MOODS_START
});

export const getMoods = () => dispatch => {
    dispatch(getDataMoodsStart());

    // changed moods to system playlists !!!!!!!!

    fetch(`${BASE_URL}/playlists`)
        .then(response => response.json())
        .then(res => {
            dispatch(getMoodsSuccess(res.data));
        })
        .catch(err => {
            dispatch(getMoodsListDataFailed(err.message));
        });
};

const getMoodsSuccess = moods => ({
    type: GET_MOODS_SUCCESS,
    payload: moods
});

const getMoodsListDataFailed = error => ({
    type: ERROR_MOODS_LIST_REQUEST,
    payload: {error},
});

// const getDataFailed = error => ({
//     type: GET_DATA_FAILED,
//     payload: {
//         error
//     }
// });




//For the Album screen
const getTracksByAlbumStart = () => ({
    type: GET_TRACK_BY_ALBUM_ID_START,
});

export const getTracksByAlbum = albumId => async dispatch => {
    try {
        dispatch(getTracksByAlbumStart());

        if (albumId) {
            const response = await axios.get(`${BASE_URL}/album/${albumId}/tracklist`)
                .then(result => result.data);

            // console.log('Received data when created the request to Tracks by the Album Id: ', response, 'response.data.tracklist.data: ', response.data.tracklist.data);
            dispatch(getTracksByAlbumSuccess( response.status === 200 ? response.data.tracklist.data : []));//Todo: check after will fix the breakpoint API
        } else console.log('Error when created the request to a Tracks of the Album Id because received the bad property "albumId": ', albumId);
    } catch (e) {
        console.log('Error when created the request to a Tracks of the Album Id: ', e);

        dispatch(getTracksByAlbumFailed(e));
    }
};

const getTracksByAlbumSuccess = payload => ({
    type: GET_TRACK_BY_ALBUM_ID_SUCCESS,
    payload,
});

const getTracksByAlbumFailed = error => ({
    type: GET_TRACK_BY_ALBUM_ID_FAILED,
    payload: {error},
});




//For the Performer screen
const getSingerDataStart = () => ({
    type: GET_SINGER_DATA_STARTED,
});




const getRecentListeningStart = () => ({
    type: GET_RECENT_LISTENING_START,
});

export const getRecentListening = () => async dispatch => {
    dispatch(getRecentListeningStart());

    try {
        const token = await getToken();

        if (token) {
            const response = await axios.get(`${BASE_URL}/user/recently_listened?token=${token}`)
                            .then(result => result.data);

            if (response.status === 200) {
                dispatch(getResentListeningSuccess(response.data.tracklist.data))
                return true
            } else {
                dispatch(getResentListeningSuccess([]))
            }
        } else console.log("Error generating the User Token when created request to Recent Listening");
        return false
    } catch (e) {
        console.log('Error Get Resent Listening Start request to the API: ', e);

        dispatch(getResentListeningFailed(e));
    }
};

const getResentListeningSuccess = payload => ({
    type: GET_RECENT_LISTENING_SUCCESS,
    payload,
});

const getResentListeningFailed = error => ({
    type: GET_RECENT_LISTENING_FAILED,
    payload: {error},
});




const setTrackListeningStart = () => ({
    type: SET_TRACK_LISTENING_STARTED,
});

export const setTrackListening = (trackId) => async dispatch => {
    try {
        dispatch(setTrackListeningStart());

        const token = await getToken();

        if (token && trackId) {

            const response = await axios.get(`${BASE_URL}/track/${trackId}/listening?token=${token}`);

            dispatch(setTrackListeningSuccess());
        }
    } catch (e) {
        dispatch(setTrackListeningFailed(e));
    }
};

const setTrackListeningSuccess = () => ({
    type: SET_TRACK_LISTENING_SUCCESS,
});

const setTrackListeningFailed = () => ({
    type: SET_TRACK_LISTENING_REQUEST_FAILED,
});




const getSingerTracksDataStart = () => ({
    type: GET_SINGER_TRACK_LIST_DATA_START,
});

export const getSingerTracks = id => async dispatch => {
    try {
        dispatch(getSingerTracksDataStart());

        if (id) {
            const response = await axios.get(`${BASE_URL}/singer/${id}/tracks`)
                .then(result => result.data);

            // console.log('Get Singer Tracks List By Singer result Data: ', response, 'result data: ', response.data.tracklist.data);
            dispatch(getSingerTracksSuccess(id, response.status === 200 ? response.data.tracklist.data : []));
        } else dispatch(getSingerTracksDataFailed('Error'));
    } catch (e) {
        console.log('Error Singer Tracks List By Singer the request to the API: ', e);

        dispatch(getSingerTracksDataFailed(e.message));
    }
};

const getSingerTracksSuccess = (singerId, singerTracks) => ({
    type: GET_SINGER_TRACK_LIST_SUCCESS,
    payload: { singerId, singerTracks }
});

const getSingerTracksDataFailed = error => ({
    type: GET_SINGER_TRACK_LIST_DATA_FAILED,
    payload: {error}
});



const getSingerAlbumsDataStart = () => ({
    type: GET_SINGER_ALBUMS_DATA_START,
});

export const getSingerAlbums = singerId => async dispatch => {
    dispatch(getSingerAlbumsDataStart());

    try {
        if (singerId) {
            const res = await axios.get(`${BASE_URL}/singer/${singerId}/albums`)
                                .then(result => result.data);

            // console.log('Get Singer Albums received Data: ', res, 'res.data.albums_list: ', res.data.albums_list);
            dispatch(getSingerAlbumsSuccess(res.status === 400 ? [] : res.data.albums_list.data));
        }
    } catch (e) {
        console.log('Error Get Singer Albums request to the API: ', e);

        dispatch(getSingerAlbumsFailed(e));
    }
};

const getSingerAlbumsSuccess = singerAlbums => ({
    type: GET_SINGER_ALBUMS_SUCCESS,
    payload: singerAlbums
});

const getSingerAlbumsFailed = error => ({
    type: GET_SINGER_ALBUMS_FAILED,
    payload: {error},
});




const getSingerDataFailed = error => ({
    type: GET_SINGER_DATA_ERROR,
    payload: {
        error
    }
});

export const clearSingerData = () => ({
    type: CLEAR_SINGER_DATA
});



//For the Radio screen
const getRadioDataStart = () => ({
    type: GET_RADIO_DATA_STARTED,
});




const getRadioStationsStart = () => ({
    type: GET_RADIO_STATIONS_DATA_STARTED,
});

export const getRadioStations = () => dispatch => {
    dispatch(getRadioStationsStart());

    fetch(`${BASE_URL}/radios`)
        .then(response => response.json())
        .then(res => {
            // console.log('Received Radio Stations from the API: ', res.data.data);

            dispatch(getRadioStationsSuccess(res.data.data));
        })
        .catch(err => {
            dispatch(getRadioStationsFailed(err.message));
        });
};

const getRadioStationsSuccess = radioStations => ({
    type: GET_RADIO_STATIONS_SUCCESS,
    payload: radioStations
});

const getRadioStationsFailed = error => ({
    type: GET_RADIO_STATIONS_REQUEST_FAILED,
    payload: {error}
});




const getRadioRecommendationsStationsStart = () => ({
    type: GET_RADIO_RECOMMENDATIONS_STATIONS_DATA_STARTED,
});

export const getRadioRecommendationsStations = () => dispatch => {
    dispatch(getRadioRecommendationsStationsStart());

    fetch(`${BASE_URL}/radios/recommend`)
        .then(response => response.json())
        .then(res => {
            // console.log('Received Radio Recommendations Stations from the API: ', res.data.data);

            dispatch(getRadioRecommendationsStationsSuccess(res.data.data));
        })
        .catch(err => {
            dispatch(getRadioRecommendationsStationsFailed(err.message));
        });
};

const getRadioRecommendationsStationsSuccess = radioStations => ({
    type: GET_RADIO_RECOMMENDATIONS_STATIONS_SUCCESS,
    payload: radioStations
});

const getRadioRecommendationsStationsFailed = error => ({
    type: GET_RADIO_RECOMMENDATIONS_STATIONS_REQUEST_FAILED,
    payload: {error}
});

const getRadioDataFailed = () => ({
    type: GET_RADIO_DATA_ERROR,
    payload: {
        error
    }
});




export const getRadioStationsByGenreId = genreId => async dispatch => {
    try {
        dispatch(getRadioStationsStart());

        if (genreId) {
            const data = await axios.get(`${BASE_URL}/radios/genres?genre_id=${genreId}`)
                            .then(res => res.data);

            // console.log('Received data when tGet Radio Stations By Genre Id Data: ', data, 'data.data: ', data.data);
            dispatch(getRadioStationsByGenreIdSuccess(data.data));
        } else console.log('Error because received the bad property "genreId", when created the request Get Radio Stations By Genre Id to the API: ', genreId);
    } catch (e) {
        dispatch(getRadioStationsFailed(e));

        console.log('Error the request to Get Radio Stations By Genre Id: ', e);
    }
};

const getRadioStationsByGenreIdSuccess = payload => ({
    type: GET_RADIO_STATIONS_BY_GENRE_ID_SUCCESS,
    payload,
});




const getRadioRecentListeningStart = () => ({
    type: GET_RADIO_RECENTLY_STATIONS_DATA_STARTED,
});

export const getRadioRecentListening = () => async dispatch => {
    try {
        dispatch(getRadioRecentListeningStart());

        const token = await getToken();

        console.log(`${BASE_URL}/radios/last_listen?token=${token}`);

        if (token) {
            const response = await axios.get(`${BASE_URL}/radios/last_listen?token=${token}`)
                                        .then(result => result.data)

            // console.log("Received data Radio Recent Listening from the API: ", response);
            dispatch(getRadioRecentListeningSuccess( response && response.length ? response : [] ));
        } else {
            console.log("Error generating the User Token when created request to Radio Recent Listening");

            dispatch(getRadioRecentListeningFailed('Error Token'));
        }
    } catch (e) {
        if (e.includes('404')) {
            dispatch(getRadioRecentListeningSuccess([]));
        } else {
            console.log('Error Get Radio Recent Listening Start request to the API: ', e);

            dispatch(getRadioRecentListeningFailed(e));
        }
    }
};

const getRadioRecentListeningSuccess = payload => ({
    type: GET_RADIO_RECENTLY_STATIONS_SUCCESS,
    payload,
});

const getRadioRecentListeningFailed = error => ({
    type: GET_RADIO_RECENTLY_STATIONS_REQUEST_FAILED,
    payload: {error},
});



const setRadioStationListeningStart = () => ({
    type: SET_RADIO_STATIONS_LISTENING_STARTED,
});

export const setRadioStationListening = radioId => async dispatch => {
    try {
        dispatch(setRadioStationListeningStart());

        const token = await getToken();

        if (token && radioId) {
            const response = await axios.get(`${BASE_URL}/radios/listen?token=${token}&radio_id=${radioId}`);

            dispatch(setRadioStationListeningSuccess());
            dispatch(getRadioRecentListening());
        }
    } catch (e) {
        dispatch(setRadioStationListeningFailed(e));
    }
};

const setRadioStationListeningSuccess = () => ({
    type: SET_RADIO_STATIONS_LISTENING_SUCCESS,
});

const setRadioStationListeningFailed = () => ({
    type: SET_RADIO_STATIONS_LISTENING_REQUEST_FAILED,
});




const getStartRadiosMoodsData = () => ({
    type: GET_START_RADIO_MOODS_REQUEST,
});

export const getRadioMoods = () => async dispatch => {
    try {
        dispatch(getStartRadiosMoodsData());

        const response = await axios.get(`${BASE_URL}/moods`)
                                .then(result => result.data);

        // console.log('Received Radio moods data from the API: ', response, 'response.data.list: ', response.data.list);
        dispatch(getRadioMoodsDataSuccess(response.data.list));
    } catch (e) {
        dispatch(getRadioMoodsRequestFailed(e));

        console.log('Error request to Radio Moods: ', e);
    }
};

const getRadioMoodsDataSuccess = radioMoods => ({
    type: GET_RADIO_MOODS_DATA_SUCCESS,
    payload: radioMoods,
});

const getRadioMoodsRequestFailed = error => ({
    type: GET_RADIO_MOODS_REQUEST_FAILED,
    payload: {error},
});


export const getRadioListByMoodId = moodId => async dispatch => {
    try {
        dispatch(getRadioDataStart());

        const response = await axios.get(`${BASE_URL}/radios/moods?mood_id=${moodId}`)
            .then(result => result.data);

        // console.log('Received Radio Stations Data by a Mood Id: ', response, 'response.data ', response.data);
        dispatch(getRadioListByMoodIdSuccess(response.data));
    } catch (e) {
        dispatch(getRadioDataFailed(e));

        console.log('Error request to Radio Stations by a Mood Id: ', e);
    }
};

const getRadioListByMoodIdSuccess = stationsList => ({
    type: GET_RADIO_LIST_BY_MOOD_SUCCESS,
    payload: stationsList,
});



const getGenreListStart = () => ({
    type: GET_RADIO_GENRE_START,
});

export const getGenreList = () => dispatch => {
    dispatch(getGenreListStart());

    fetch(`${BASE_URL}/genres`)
        .then(response => response.json())
        .then(res => {
            // console.log('Received data of the request to Genre List: ', res);

            dispatch(getGenreListSuccess(res.data.genres_list.data));
        })
        .catch(err => {
            dispatch(getGenreListFailed(err.message));
        });
};

const getGenreListSuccess = genreList => ({
    type: GET_RADIO_GENRE_SUCCESS,
    payload: genreList,
});

const getGenreListFailed = error => ({
    type: GET_RADIO_GENRE_REQUEST_FAILED,
    payload: {error},
});



const getSearchRadioStart = () => ({
    type: GET_SEARCH_RADIO_STATIONS_START,
});

export const getSearchRadio = ({ page, searchValue, perPage, moodId })=> async dispatch => {
    try {
        dispatch(getSearchRadioStart());

        const bodyParams = {
            name: searchValue,
        };

        if (moodId) bodyParams.mood_id = moodId;

        const response = await axios.post(`${BASE_URL}/radios/search?page=${page}&per_page=${perPage}`, bodyParams);

        // console.log('Received Search Radio Data from the request: ', response, 'response.data.data.data: ', response.data.data.data);
        dispatch(getSearchRadioSuccess(response.data.data.data));
    } catch (e) {
        console.log('Error request to Search Radio API: ', e);

        dispatch(getSearchRadioFailed(e));
    }
};

const getSearchRadioSuccess = radioList => ({
    type: GET_SEARCH_RADIO_STATIONS_SUCCESS,
    payload: radioList,
});

const getSearchRadioFailed = error => ({
    type: GET_SEARCH_RADIO_STATIONS_FAILED,
    payload: {error}
});

export const resetSearchRadioStations = () => ({
    type: RESET_SEARCH_RADIO_STATIONS,
});



//For the screen 'Play Lists'
const getPlayListsDataStarted = () => ({
    type: GET_PLAY_LISTS_DATA_STARTED,
});

export const getPlayLists = () => async dispatch => {
    let token = await getToken();
    // console.log(`token - ${token}`);

    dispatch(getPlayListsDataStarted());
    fetch(`${BASE_URL}/user/playlists?token=${token}`)
        .then(response => response.json())
        .then(res => {
            // console.log(`getPlayLists reducer res.data :`, res);
            // throw new Error('NOT!');
            if (res.status === 200) {
                dispatch(getPlayListsSuccess(res.data));
            } else dispatch(onPlayListsEmpty());
        })
        .catch(err => {
            dispatch(getPlayListsDataFailed(err.message));
        });
};

const getPlayListsSuccess = playLists => ({
    type: GET_PLAY_LISTS_SUCCESS,
    payload: playLists
});

const onPlayListsEmpty = () => ({
    type: PLAY_LISTS_EMPTY,
});


export const createPlayList = playListName => async dispatch => {
    let token = await getToken();

    if (typeof playListName === "string" && playListName.trim() && token) {
        dispatch(getPlayListsDataStarted());

        fetch(`${BASE_URL}/create_playlist?name=${playListName}&token=${token}`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
        .then(res => {
            // console.log(`createPlayList res reducer ${JSON.stringify(res)}`);
            dispatch(getPlayListsAfterCreatedSuccess(res.data));
            dispatch(getPlayLists());
        })
        .catch(err => {
            dispatch(getPlayListsDataFailed(err.message));
        });
    } else console.log('Error create the request to Create New PLayList because received badly a property PlayList Name or bad generated the User Token, PlayList Name: ', playListName, ', the Token: ', token);
};

const getPlayListsAfterCreatedSuccess = playList => ({
    type: CREATE_PLAY_LIST_SUCCESS,
    payload: playList
});

export const deletePlayList = playListId => async dispatch => {
    let token = await getToken();

    fetch(`${BASE_URL}/delete_playlist?playlist_id=${playListId}&token=${token}`)
        .then(response => response.json())
        .then(res => {
            // console.log('Error("NOT!");', res);
            dispatch(deletePlayListSuccess(playListId));
            dispatch(getPlayLists());
        })
        .catch(err => {
            // dispatch(getPlayListsFailed(err.message));
        });
};

const deletePlayListSuccess = (playListId) => ({
    type: DELETE_PLAY_LIST_SUCCESS,
    payload: playListId
});



//For the Play List screen
const getStartPlayListTracksData = () => ({
    type: GET_START_FETCH_PLAY_LIST_TRACKS_DATA,
});


export const getPlayListTracks = (playListId) => async dispatch => {
    let token = await getToken();
    let sortBy = await AsyncStorage.getItem('orderNew');
    console.log(`getPlayListTracks playListId ${playListId}`);

    dispatch(getStartPlayListTracksData());

    fetch(`${BASE_URL}/user/playlists/${playListId}/tracklist?token=${token}&sortBy=${+sortBy}`)
        .then(response => response.json())
        .then(res => {
            // console.log('getPlayListTracks res: ', res);

            if (res.status === 200) {
                dispatch(getPlayListTracksSuccess(res.data.tracklist.data));
            } else {
                dispatch(onPlayListTrackEmpty());
            }
        })
        .catch(err => {
            console.log('getPlayListTracks request failed: ', err);

            dispatch(getPlayListsDataFailed(err.message));
        });
};

const getPlayListTracksSuccess = playListTracks => ({
    type: GET_PLAY_LIST_TRACKS_SUCCESS,
    payload: playListTracks
});


const onPlayListTrackEmpty = () => ({
    type: PLAY_LIST_TRACKS_EMPTY,
});

const getPlayListsDataFailed = (error) => ({
    type: GET_PLAY_LISTS_DATA_ERROR,
    payload: {
        error
    }
});

export const resetPlayListTracksData = () => ({
    type: RESET_PLAY_LIST_TRACKS_DATA,
});




const addTrackToPlayListStart = () => ({
    type: ADD_TRACK_TO_PLAY_LIST_START,
});

export const addTrackToPlayList = (playListId, trackId) => async dispatch => {
    try {
        let token = await getToken();

        if (token && playListId && trackId) {
            fetch(`${BASE_URL}/playlist/${playListId}/add?token=${token}&song_id=${trackId}`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                }})
                .then(response => response.json())
                .then(res => dispatch(addTrackToPlayListSuccess(res.data.song)))
        }
    } catch (e) {
        console.log('Error: Add Track to Playlist: ', e);

        dispatch(addTrackToPLayListFailed(e));
    }
};

const addTrackToPlayListSuccess = newMusic => ({
    type: ADD_TRACK_TO_PLAY_LIST_SUCCESS,
    payload: newMusic,
});

const addTrackToPLayListFailed = error => ({
    type: ADD_TRACK_TO_PLAY_LIST_FAILED,
    payload: {error},
});




export const deleteTrackInPlayList = (playListId, songId, positionId) => async dispatch => {
    let token = await getToken();

    fetch(`${BASE_URL}/playlist/${playListId}/delete?token=${token}&song_id=${songId}&position_id=${positionId}`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }})
        .then(response => response.json())
        .then(res => {
            // console.log('Delete Track In PLay list request to the API result Data: ', res);

            dispatch(deleteTrackInPlayListSuccess(songId));
            dispatch(getPlayListTracks(playListId));
        })
        .catch(err => {
            alert(err)
        });
};

const deleteTrackInPlayListSuccess = (songId) => ({
    type: DELETE_PLAY_LIST_TRACK_SUCCESS,
    payload: songId
});



//For the Search Music Screen
export const startFetchTrackListSearchScreenData = () => ({
    type: START_FETCH_DATA_SEARCH_SCREEN_TRACK_LIST,
});

export const getTrackListSearchSuccess = trackList => ({
    type: GET_TRACK_LIST_SEARCH_SCREEN_SUCCESS,
    payload: trackList,
});

export const getTrackListSearchFailed = error => ({
    type: GET_DATA_SEARCH_SCREEN_FAILED,
    payload: error,
});

export const getTrackListSearchByParams = params => async dispatch => {
    const { search, perPage, page } = params;

    try {
        dispatch(startFetchTrackListSearchScreenData());

        const response = await axios.get(`${BASE_URL}/search/?search=${search.trim()}&perPage=${perPage}&page=${page}`);

        if (response.status === 200) {
            dispatch(getTrackListSearchSuccess(response.data.data));
        } else dispatch(getTrackListSearchFailed(response.statusText));

        // console.log('getTrackListSearchByParams result: ', response, 'result data: ', response.data.data);
    } catch (e) {
        console.log('Fetch Track List Search error: ', e);

        dispatch(getTrackListSearchFailed(e.message));
    }
};

export const getResetSearchData = () => ({
    type: GET_RESET_SEARCH_SCREEN_DATA,
});




export const fetchWeekPopularTrackLisStart = () => ({
    type: GET_WEEK_POPULAR_TRACKS_DATA_START,
});

export const getWeekPopularTrackList = () => async dispatch => {
    dispatch(fetchWeekPopularTrackLisStart());

    try {
        const response = await axios.get(`${BASE_URL}/week_tracklist`);

        // console.log('Received Data when created the request Get Week Popular Track List to the API result: ', response, 'response.data.data.tracklist.data: ', response.data.data.tracklist.data);
        if (response.status === 200 ) {
            if (response.data.data.tracklist.data){
                dispatch(geWeekPopularTrackListSuccess(response.data.data.tracklist.data));
            } else {
                dispatch(geWeekPopularTrackListSuccess([]));
            }
        } else {
            dispatch(geWeekPopularTrackListSuccess([]));
        }
       
    } catch (e) {
        console.log('Error the request Get Week Popular Track List to the API: ', e);

        dispatch(getWeekPopularTrackListFailed(e));
    }
};

export const geWeekPopularTrackListSuccess = trackList => ({
    type: GET_WEEK_POPULAR_TRACKS_DATA_SUCCESS,
    payload: trackList,
});

export const getWeekPopularTrackListFailed = error => ({
    type: GET_WEEK_POPULAR_TRACKS_DATA_FAILED,
    payload: error,
});



export const fetchMonthPopularTrackLisStart = () => ({
    type: GET_MONTH_POPULAR_TRACKS_DATA_START,
});

export const getMonthPopularTrackList = () => async dispatch => {
    dispatch(fetchMonthPopularTrackLisStart());

    try {
        const response = await axios.get(`${BASE_URL}/month_tracklist`);

        // console.log('Received Data when created the request Get Month Popular Track List to the API result: ', response, 'response.data.data.tracklist.data: ', response.data.data.tracklist.data);
        if (response.status === 200){
            if (response.data.data.tracklist.data) {
                dispatch(geMonthPopularTrackListSuccess(response.data.data.tracklist.data))
            } else {
                dispatch(geMonthPopularTrackListSuccess([]))
            }
        } else {
            dispatch(geMonthPopularTrackListSuccess([]))
        }
    } catch (e) {
        console.log('Error the request Get Month Popular Track List to the API: ', e);

        dispatch(getMonthPopularTrackListFailed(e));
    }
};

export const geMonthPopularTrackListSuccess = trackList => ({
    type: GET_MONTH_POPULAR_TRACKS_DATA_SUCCESS,
    payload: trackList,
});

export const getMonthPopularTrackListFailed = error => ({
    type: GET_MONTH_POPULAR_TRACKS_DATA_FAILED,
    payload: error,
});




//For the Profile music Screen
export const startGetUserData = () => ({
    type: START_GET_USER_DATA,
});

export const setUserData = useData => ({
    type: SET_USER_DATA,
    payload: useData,
});



//For the Profile screen
export const getUserData = () => async dispatch => {
    try {
        const token = await getToken();

        dispatch(startGetUserData());

        if (token) {
            const response = await axios.get(`${BASE_URL}/user/get_info?token=${token}`).
                                        then(result => result.data.data);

            // console.log(`Fetch by url API: ${BASE_URL}/user/get_info?token=${token}`);
            // console.log('Fetch response: ', response);

            if (response && response.name && response.email) {
                dispatch(setUserData(response));
            } else console.log('Error generate user Token, for get user Data');
        } else console.log('Error generate user Token, for get user Data');
    } catch (e) {
        console.log('Error get user Data: ', e);
    }
};

// const reLoginAfterError = () => async dispatch => {
//     isUserSignIn();
//
//     await removeToken();
//
//     dispatch(getUserData());
// };




//For the component Music Item
export const deleteTrackById = songId => async dispatch => {
    try {
        const token = await getToken();

        if (token) {
            const response = await axios.get(`${BASE_URL}/user/tracklist/delete?token=${token}&song_id=${songId}`)
                                        .then(result => result.data);

            dispatch(getTrackListByUserToken());
            // console.log('Received data from the API, when created the request to delete a Track by Id: ', response);
        } else console.log('Error token when created request to delete a Track by Id');
    } catch (e) {
        console.log('Error request to delete Track By Id: ', e);
    }
};

// const deleteTrackByIdSuccess = songId => ({
//     type: DELETE_TRACK_BY_ID_SUCCESS,
//     payload: songId,
// });

const startAddTrackToUserTrackList = () => ({
    type: ADD_TRACK_TO_USER_TRACK_LIST_START,
});

export const getAddTrackToUserTrackListByTrackId = trackId => async dispatch => {
    try {
        dispatch(startAddTrackToUserTrackList());

        const token = await getToken();

        if (trackId && token) {
            await axios.post(`${BASE_URL}/user/tracklist/add?token=${token}&song_id=${trackId}`)
                .then(result => {
                    const data = result.data;

                    // console.log('Received data request add Track to a User Track List: ', result, data);

                    if (data.status === 200 && data && data.data && data.data.song) {
                        dispatch(addTrackToUserTrackListSuccess());
                    } else dispatch(getTrackInUserTrackListIsAlready());
                });
        } else console.log('Error generated the User Token or received the bad property "trackId" when created request to User Track List by Track Id, the current User Token: ', token, ", Track Id: ", trackId);
    } catch (e) {
        dispatch(addTrackToUserTrackListFailed(e));

        console.log('Error request add Track to User Track List by Track Id: ', e);
    }
};

const addTrackToUserTrackListSuccess = () => ({
    type: ADD_TRACK_TO_USER_TRACK_LIST_SUCCESS,
});

export const resetTrackInUserTrackListIsAlready = () => ({
    type: RESET_TRACK_TO_USER_TRACK_LIST_IS_ALREADY,
});

const getTrackInUserTrackListIsAlready = () => ({
    type: TRACK_TO_USER_TRACK_LIST_IS_ALREADY,
});

const addTrackToUserTrackListFailed = error => ({
    type: ADD_TRACK_TO_USER_TRACK_LIST_FAILED,
    payload: {error},
});



//For the component Track Item
const startTrackLikedCheck = () => ({
    type: CHECK_LIKED_TRACK_START
});

export const getTrackLikedCheck = trackId => async dispatch => {
    try {
        dispatch(startTrackLikedCheck());

        const token = await getToken();

        if (token && trackId) {
            const response  = await axios.get(`${BASE_URL}/track/${trackId}/is_liked?token=${token}`);
            // console.log('Received data when Checking Liked Track: ', response, 'response.data.data.liked: ', response.data.data.liked);

            dispatch(getTrackLikedCheckSuccess(response.data.data.liked));
        } else console.log('Error generating token, or Track ID when created request when Checking Liked Track by a User Token, Token: ', token, 'Track ID: ', trackId);
    } catch (e) {
        console.log('Error the request to Check Track Liked by a User Token: ', e);

        dispatch(getTrackLikedCheckFailed(e));
    }
};

const getTrackLikedCheckSuccess = payload => ({
    type: CHECK_LIKED_TRACK_SUCCESS,
    payload,
});

const getTrackLikedCheckFailed = error => ({
    type: CHECK_LIKED_TRACK_FAILED,
    payload: {error},
});




export const setTrackItemLike = trackId => async dispatch => {
    try {
        const token = await getToken();

        // console.log(`Track Item like. Will generated the url to the request API: ${BASE_URL}/like?track_id=${trackId}&token=${token}`);
        if (token) {
            await axios.get(`${BASE_URL}/like?song_id=${trackId}&token=${token}`);

            // console.log('Received data from the API, when liked a Track by Id: ', response);
            dispatch(getTrackLikedCheck(trackId));
        } else console.log('Error token when created request to a Track Like');
    } catch (e) {
        console.log('Error request to a Set Track like: ', e);

        dispatch(getTrackLikedCheckFailed(e));
    }
};

// const setTrackItemLikeSuccess = trackId => ({
//     type: GET_TRACK_ITEM_LIKE_SUCCESS,
//     payload: trackId,
// });



export const getSearchListOfLikedTracks = search => async dispatch => {
    try {
        const userId = await getUserId()

        if (userId) {
            let tracks = await axios.get(`${BASE_URL}/search/likes?search=${search}&user_id=${userId}`)

            if (tracks.data.status === 200){
                dispatch(getTrackListSuccess(tracks.data.data.result));
                return true
            } else dispatch(getTrackListByUserTokenIsEmpty());

            return false
        } else console.log('userId is null');
    } catch (e) {
        console.log('Error request getSearchListOfLikedTracks', e)
        dispatch(getTrackListError(e));
    }
};


export const getListOfLikedTracks = () => async dispatch => {
    try {
        let token = await getToken();

        if (token) {
            let tracks = await axios.get(`${BASE_URL}/user/likes?token=` + token)
            console.log('Liked tracks: ', tracks.data.data)
            if (tracks.data.status === 200){
                dispatch(getTrackListSuccess(tracks.data.data.tracklist.data));
                return true
            } else dispatch(getTrackListByUserTokenIsEmpty());

            return false
        } else console.log('token is null');
    } catch (e) {
        console.log('Error request getListOfLikedTracks', e)
        dispatch(getTrackListError(e));
    }
};

export const getGenreTrackList = (id, page = 1, perPage = 10) => async dispatch => {
    try {
        dispatch(getTrackListStarted());
        
        // system playlist's tracks
        let tracks = await axios.get(`${BASE_URL}/user/playlists/${id}/tracklist?page=${page}&perPage=${perPage}`)
        // let tracks = await axios.get(`${BASE_URL}/genres/${id}/list`);

        if (tracks.data.status === 200){
            dispatch(getTrackListSuccess(tracks.data.data.tracklist.data));
            console.log('system playlist tracks: ', tracks.data.data.tracklist.data);

            return true
        } else dispatch(getTrackListByUserTokenIsEmpty());
        return false
    } catch (e) {
        console.log('Error request getListOfLikedTracks', e)
        dispatch(getTrackListError(e));
    }
};

export const getSearchGenreTrackList = (id, search) => async dispatch => {
    try {
        dispatch(getTrackListStarted());

        let tracks = await axios.get(`${BASE_URL}/search/song?genre_id=${id}&search=${search}`)
        console.log('genre tracks search: ', tracks.data.data)
        if (tracks.data.status === 200){
            dispatch(getTrackListSuccess(tracks.data.data.result));
            return true
        } else dispatch(getTrackListByUserTokenIsEmpty());
        return false
    } catch (e) {
        console.log('Error request getListOfLikedTracks', e)
        dispatch(getTrackListError(e));
    }
};
