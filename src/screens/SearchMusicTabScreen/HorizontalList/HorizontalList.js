import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, Dimensions } from 'react-native';
import RadioPlayIcon from '../../../icons/radioPlay';
import Ripple from 'react-native-material-ripple';
import Gradient from '../../../assets/gradient.png'
import { logoImage } from '../../../constants/default_images';

// const screenWidth = Dimensions.get('window').width;
// const screenHeight = Dimensions.get('window').height;

const HorizontalList = ({ data, darkMode, navigation, onPlayListsMenuOpen = () => {} }) => (
	<FlatList
		data={data}
		horizontal={true}
		showsHorizontalScrollIndicator={false}
		renderItem={({ item, ...props }) => {

			return (
				<TouchableOpacity style={styles.radioBlock} key={item.name + props.index}>
					<Ripple onPress={() => navigation.navigate('PerformerScreen', { id: item.id, singerName: item.name, photo: item.photo, onPlayListsMenuOpen })}>

						<Image
							style={styles.image}
							source={item.photo ? {uri: item.photo} : logoImage}
						/>

						<Image
							style={[styles.image, styles.gradient]}
							source={Gradient}
						/>

						<View style={styles.content}>
							<RadioPlayIcon style={styles.icon} />

							<Text numberOfLines={2} style={styles.name}>{item.name}</Text>

							<Text numberOfLines={1} style={styles.musicTitle}>{item.title}</Text>
						</View>
					</Ripple>
				</TouchableOpacity>
			)
		}}
		keyExtractor={item => item.id.toString()}
		style={{ paddingTop: 20 }}
	/>
);

export default HorizontalList


const styles = StyleSheet.create({
	radioBlock: {
		position: 'relative',
		borderRadius: 8,
		marginRight: 15,
		overflow: 'hidden',
	},
	radioBlockEmpty: {
		position: 'relative',
		borderRadius: 8,
		marginRight: 0,
		overflow: 'hidden',
	},
	content: {
		position: 'absolute',
		width: '85%',
		left: 10,
		bottom: 10,
		height: '100%',
		maxHeight: 70,
		zIndex: 20
	},
	icon: {
		marginBottom: 5,
	},
	name: {
		color: '#fff',
		fontSize: 12,
		fontWeight: '700',
	},
	musicTitle: {
		color: '#fff',
		fontSize: 12,
		fontWeight: '500',
	},
	image: {
		width: 100,
		height: 140,
		resizeMode: 'cover'
	},
	gradient: {
		position: 'absolute',
		top: 0, left: 0, right: 0, bottom: 0,
		zIndex: 10
	},
	list: {
		flexDirection: 'row',
		alignItems: 'center',
		flexWrap: 'wrap',
		justifyContent: 'space-between',
		marginBottom: 20,
	},
});
