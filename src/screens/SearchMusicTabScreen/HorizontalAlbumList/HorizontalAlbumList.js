 import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, Dimensions, ActivityIndicator } from 'react-native';
import RadioPlayIcon from '../../../icons/radioPlay';
import Ripple from 'react-native-material-ripple';
import Gradient from '../../../assets/gradient.png'

// const screenWidth = Dimensions.get('window').width;
// const screenHeight = Dimensions.get('window').height;

const HorizontalAlbumList = ({ requestFailed, loading, data, darkMode, navigation, singerName }) => (
    <>
        {
            !loading && !requestFailed && data.length ?

            <FlatList
                data={data}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, ...props }) => (
                    <TouchableOpacity style={styles.radioBlock} key={item.name + props.index}>
                        <Ripple
                            onPress={() => navigation.navigate('AlbumScreen', {
                                id: item.id,
                                albumName: item.name,
                                photo: item.photo,
                                singerName,
                                genre: item.genre
                            })}
                        >
                            <Image
                                style={styles.image}
                                source={ item.photo ? {uri: item.photo} : require('../../../assets/edward-cisneros-KoKAXLKJwhk-unsplash.png') }
                            />

                            <Image
                                style={[styles.image, styles.gradient]}
                                source={Gradient}
                            />

                            <View style={styles.wrapperContent}>
                                <View style={styles.content}>
                                    <RadioPlayIcon style={styles.icon} />

                                    <Text numberOfLines={1} style={styles.name}>{item.name}</Text>

                                    {/*<Text numberOfLines={2} style={styles.musicTitle}>{item.title}</Text>*/}
                                </View>
                            </View>
                        </Ripple>
                    </TouchableOpacity>
                )}
                keyExtractor={item => item.id.toString()}
                contentContainerStyle={{ minWidth: '100%', paddingTop: 20, paddingBottom: 40 }}
            /> : loading && !requestFailed ?

            <View style={styles.fillFull}>
                <ActivityIndicator />
            </View> : !loading && !requestFailed && !data.length ?

            <View style={styles.fillFull}>
                <Text style={{
                    width: '100%',
                    textAlign: 'center',
                    color: !darkMode ? '#000' : '#fff',
                }}>Список альбомов пуст</Text>
            </View> :

            <View style={styles.fillFull}>
                <Text style={{
                    width: '100%',
                    textAlign: 'center',
                    color: !darkMode ? '#000' : '#fff',
                }}>Ошибка получения данных</Text>
            </View>
        }
    </>
);

export default HorizontalAlbumList


const styles = StyleSheet.create({
    fillFull: {
         flex: 1,
         minHeight: 175,
         alignItems: 'center',
         justifyContent: 'center',
    },
    radioBlock: {
        position: 'relative',
        borderRadius: 8,
        marginRight: 15,
        overflow: 'hidden',
    },
    radioBlockEmpty: {
        position: 'relative',
        borderRadius: 8,
        marginRight: 0,
        overflow: 'hidden',
    },
    wrapperContent: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        height: '65%',
        width: '90%',
        justifyContent: 'flex-end',
        zIndex: 15,
    },
    content: {
        position: 'relative',
        left: 10,
        bottom: 10,
        width: '100%',
        zIndex: 20
    },
    icon: {
        marginBottom: 5,
    },
    name: {
        color: '#fff',
        fontSize: 12,
        width: '100%',
        fontWeight: '700',
    },
    musicTitle: {
        color: '#fff',
        fontSize: 12,
        fontWeight: '500',
    },
    image: {
        width: Dimensions.get('window').width / 3.25,
        height: Dimensions.get('window').height / 4.75,
        resizeMode: 'cover'
    },
    gradient: {
        position: 'absolute',
        top: 0, left: 0, right: 0, bottom: 0,
        zIndex: 10
    },
    list: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        marginBottom: 20,
    },
});
