import React from 'react';
import {StyleSheet, Dimensions, View, Text, ActivityIndicator} from 'react-native';
import SvgSuccessIcon from '../../../icons/successIcon';
import Modal from 'react-native-modal';
const screenWidth = Dimensions.get('window').width;
// const screenHeight = Dimensions.get('window').height;

const AddTrackSuccessModal = ({ modalVisible, setModalVisible, darkMode, errorRequest = false, trackIsAlready = false, loading = false }) => (
    <Modal
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        animationInTiming={200}
        animationOutTiming={10}
        onBackButtonPress={() => {
            setModalVisible(!modalVisible);
        }}
        hideModalContentWhileAnimating={true}
        backdropOpacity={0.5}
        onBackdropPress={() => {
            setModalVisible(!modalVisible);
        }}
        style={styles.modalView}
        isVisible={modalVisible === 'object' ? !!modalVisible : modalVisible}
    >
        <View style={[styles.centeredView, {paddingTop: errorRequest || trackIsAlready ? 25 : 0, backgroundColor: darkMode ? '#000' : '#fff', alignItems: 'center' }]}>
            {
                !loading ?

                <>
                    {
                        !errorRequest && !trackIsAlready &&

                        <View style={{marginVertical: 14}}>
                            <SvgSuccessIcon />
                        </View>
                    }

                    <Text style={[styles.modalText, { color: darkMode ? '#fff' : '#000' }]}>
                        {
                            !errorRequest && !trackIsAlready ?
                            "Трек успешно добавлен \n в Вашу музыку!" : trackIsAlready && !errorRequest ?
                            "Этот трек \n уже добавлен Вами раньше" :
                            "Уупс \n что-то пошло не так \n сообщите пожалуйста в тех поддержку \n В розделе \"Профиль\" :("
                        }
                    </Text>
                </> :

                <View style={{paddingTop: 30}}>
                    <ActivityIndicator />
                </View>
            }
        </View>

    </Modal>
);


export default AddTrackSuccessModal

const styles = StyleSheet.create({
    loader: {
        height: 150,
        width: screenWidth,
        alignItems: 'center',
        justifyContent: 'center',
    },
    centeredView: {
        paddingHorizontal: 40,
        paddingBottom: 30,
        borderRadius: 8
    },
    modalView: {
        margin: 0,
        width: screenWidth,
        backgroundColor: "rgba(51, 51, 51, 0.48)",
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flex: 1
    },
    modalText: {
        textAlign: "center",
        fontSize: 18,
        fontWeight: 'bold',
    }
});
