import React, { useState, useEffect } from 'react';
import { StyleSheet, Dimensions, View, Text, ScrollView, TouchableOpacity, SafeAreaView, StatusBar, Image, Alert } from 'react-native';
import { connect } from 'react-redux';
import { COLORS } from "../../constants/colors.constants";
import CustomHeader from '../../components/CustomHeader/CustomHeader'
import Ripple from 'react-native-material-ripple';
import SvgDownloadIcon from '../../icons/download';
import {clearSingerData, getSingerAlbums, getSingerTracks} from "../../actions";
import {toggleBottomBar} from "../../store/appReducer";
import { defaultImage } from '../../constants/default_images';
import { getStatusBarHeight } from "react-native-status-bar-height";
import DownloadModal from '../../components/Modals/DownloadModal';

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const MusicTextScreen = props => {
    let darkMode = props.darkMode;

    const [downloadModalVisible, setDownloadModalVisible] = useState(false);
    let [musicData, setMusicData] = useState(props.navigation.state.params ? props.navigation.state.params.musicData : null);

    useEffect(() => {
        props.toggleBottomBar(false);
    }, []);

    const onDownload = () => {
        if (!props.downloading) {
            setDownloadModalVisible(true);
        } else {
            Alert.alert(
                'Информация',
                'Можно скачать только один файл одновременно!',
                [
                    {
                        text: 'Хорошо',
                        style: 'success',
                    },
                ],
                { cancelable: true }
            );
        }
    };

    const getListOfSingers = () => {
        if (musicData && musicData.singers){
            let listOfSingers = musicData.singers.map( (item, i) => i + 1 === musicData.singers.length ? item.name : item.name + ' & ')
            // console.log('listOfSingers', listOfSingers)

            return listOfSingers
        }
        return 'Неизвестный исполнитель'
    }

    return (
        <SafeAreaView style={{ flex: 1, marginTop: getStatusBarHeight() }}>
            <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} backgroundColor={darkMode ? '#000' : '#F9F9F9'} />

            <ScrollView
                style={{ backgroundColor: darkMode ? COLORS.black : COLORS.white }}
                showsVerticalScrollIndicator={false}
            >
                <View style={{ marginHorizontal: 15 }}>
                    <CustomHeader
                        title="Музыка"
                        pressGoBack={() => props.navigation.goBack()}
                        darkMode={darkMode}
                    />

                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                        <View style={{ width: '30%' }}>
                            <Image
                                style={{ width: 100, height: 100, resizeMode: 'cover', borderRadius: 8 }}
                                source={( musicData && musicData.photo ? {uri: musicData.photo} : defaultImage )}
                            />
                        </View>
                        
                        <View style={{ marginLeft: 20, width: '70%' }}>
                            <Text style={{ marginBottom: 8, color: darkMode ? COLORS.white : COLORS.black }}>Текст песни</Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
                                <Text style={{ fontWeight: 'bold', color: darkMode ? COLORS.white : COLORS.black, width: '100%', maxWidth: '78%' }}>{getListOfSingers()}</Text>

                                <Text style={{ color: COLORS.darkgrey, position: 'absolute', right: 20 }}>{musicData.duration}</Text>
                            </View>

                            <Text style={{ color: COLORS.darkgrey }}>{musicData.name}</Text>

                            <Ripple style={styles.downloadBtn} onPress={() => onDownload()}>
                                <Text style={{ color: COLORS.white, marginRight: 10 }}>Скачать</Text>
                                <SvgDownloadIcon color={COLORS.white} width={20} height={17} />
                            </Ripple>
                        </View>
                    </View>

                    <View style={{ alignItems: 'center', marginTop: 45, marginBottom: 60 }}>
                        <Text style={{ textAlign: 'center', color: darkMode ? COLORS.white : COLORS.black, width: '100%', maxWidth: '75%' }}>
                            { musicData.text ? musicData.text : 'Текст песни отсутствует!' }
                        </Text>
                    </View>
                </View>
            </ScrollView>

            <DownloadModal
                modalVisible={downloadModalVisible}
                closeModal={() => setDownloadModalVisible(false)}
                track={musicData}
                darkMode={darkMode}
                navigation={props.navigation}
            />
        </SafeAreaView>
    );
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
    downloadPendingList: state.downloadReducer.pendingList,
    downloading: state.downloadReducer.downloading,
});

const mapDispatchToProps = {
    toggleBottomBar,
};

export default connect(mapStateToProps, mapDispatchToProps)(MusicTextScreen)

const styles = StyleSheet.create({
    downloadBtn: {
        width: 105,
        borderRadius: 13,
        paddingVertical: 4,
        paddingHorizontal: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        backgroundColor: COLORS.purple,
        overflow: 'hidden',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    }
});
