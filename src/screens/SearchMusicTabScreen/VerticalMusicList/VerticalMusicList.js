import React from 'react';
import { ScrollView, FlatList, TouchableOpacity } from 'react-native';

import {COLORS} from "../../../constants/colors.constants";
import MusicItem from "../../../components/MusicItem/MusicItem";
import SvgPlusIcon from "../../../icons/plus";
import {connect} from "react-redux";

// const screenWidth = Dimensions.get('window').width;
// const screenHeight = Dimensions.get('window').height;


const VerticalMusicList = ({ isPlayListPage = false, data, darkMode, navigation, playListPageId}) => {
    const handleOpenPlayer = ({ track }) => {
        navigation.navigate('PlayerScreen', { track, playlist: data });
    };

    return (
        <ScrollView style={{ marginBottom: 75, paddingLeft: '2.5%', paddingRight: '2.5%' }}>
            {data && data.length > 0 &&
            <FlatList
                data={data}
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id + item.name}
                style={{ paddingTop: 20, flex: 1 }}
                contentContainerStyle={{alignItems: 'center'}}
                renderItem={({ item }) => (
                    <MusicItem
                        isPlayListPage={isPlayListPage}
                        playListPageId={playListPageId}
                        musicData={item}
                        icon={<SvgPlusIcon width={18} height={18} color={COLORS.purple} />}
                        darkMode={darkMode}
                        navigation={navigation}
                        onOpenPlayer={handleOpenPlayer}
                    />
                )}
            />}
        </ScrollView>
    );
};


export default VerticalMusicList

// const styles = StyleSheet.create({
//
// });
