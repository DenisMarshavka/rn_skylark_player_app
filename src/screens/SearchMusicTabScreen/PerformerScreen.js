import React, {useEffect} from 'react';
import {
    StyleSheet,
    Dimensions,
    View,
    Image,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    StatusBar, SafeAreaView
} from 'react-native';
import {connect} from 'react-redux';

import RadioPlayIcon from "../../icons/radioPlay";
import {COLORS} from "../../constants/colors.constants";
import YellowButtonAll from "../../components/YellowButtonAll/YellowButtonAll.component";
import ArrowLeftIcon from '../../icons/arrowLeft';
import HorizontalAlbumList from "./HorizontalAlbumList/HorizontalAlbumList";
import { clearSingerData, getSingerAlbums, getSingerTracks } from "../../actions";
import {toggleBottomBar} from "../../store/appReducer";
import MusicListWithSheetsMenu from "../../components/MusicListWithSheetsMenu/MusicListWithSheetsMenu";
import SvgPlusIcon from "../../icons/plus";
import { getStatusBarHeight } from "react-native-status-bar-height";
import {MainScreenContainer} from "../../components/MainScreenContainer/MainScreenContainer";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const PerformerScreen = props => {
    const {
        getSingerTracks,

        getSingerAlbums,
        clearSingerData,
        singerAlbums,
        toggleBottomBar,

        isLoadingSingerPopularTracks,
        singerPopularTracks,
        isErrorSingerPopularTracks,

        loadingSingerAlbums,
        errorSingerAlbums,

        onPlayListsMenuOpen = () => {},
    } = props;

    const {params = {}} = props.navigation.state;
    let { id, singerName, photo, fromSingersScreen = false } = params;

    if (singerPopularTracks.length) {
        photo = singerPopularTracks[0]?.photo
    }

    useEffect(() => {
        clearSingerData();
        getSingerTracks(id);
        getSingerAlbums(id);
        toggleBottomBar(false);

        return () => {
            if (!fromSingersScreen) toggleBottomBar(true);
            clearSingerData();
        };
    }, []);

    let darkMode = props.darkMode;

    // console.log(`singerList ${JSON.stringify(singerList)}`);
    // console.log(`Perform data navigation state: `, params);

    const handleOpenPlayer = params => {
        props.navigation.navigate('PlayerScreen', { playlist: singerPopularTracks, onPlayListsMenuOpen, fromSingersScreen: true, ...params });
    };

    return (
        <ScrollView style={{ backgroundColor: darkMode ? '#000000' : '#fff', paddingTop: getStatusBarHeight() }}>
            <StatusBar translucent backgroundColor='transparent' />

            <MainScreenContainer darkMode={darkMode}>
                <TouchableOpacity onPress={() => props.navigation.goBack()} style={[styles.arrowBackWrapper, ]}>
                    <ArrowLeftIcon color={darkMode ? 'white' : 'black'}/>
                </TouchableOpacity>

                <Image
                    source={photo ? {uri: photo} : require('../../assets/logo.png')}
                    style={styles.headerImage}
                    blurRadius={1}
                />

                <Image
                    source={darkMode ? require('../../assets/black_gradient.png') : require('../../assets/white_gradient.png')}
                    style={styles.headerImage}/>

                <View style={{alignItems: 'center', marginBottom: 6, paddingHorizontal: 15, paddingTop: getStatusBarHeight()}}>
                    <View style={{alignItems: 'center'}}>
                        <Image
                            resizeMode={'cover'}
                            source={photo ? {uri: photo} : require('../../assets/logo.png')}
                            style={styles.image}/>

                        <RadioPlayIcon style={styles.icon}/>
                    </View>

                    <Text  numberOfLines={2} style={{width: '100%', maxWidth: '75%', textAlign: 'center', fontSize: 22, color: darkMode ? COLORS.white : COLORS.black, marginTop: 10}}>{singerName}</Text>

                    {/* <Text style={{fontSize: 11, color: darkMode ? COLORS.white : COLORS.black, marginTop: 7}}>Альтернативный рок</Text> */}

                    <View style={styles.titleWrapper}>
                        <Text style={[styles.blockTitle, {color: darkMode ? COLORS.white : COLORS.black}]}>Популярное</Text>

                        {
                            singerPopularTracks.length ?

                            <YellowButtonAll onPress={() => props.navigation.navigate('TracksScreen', { isSingerPopularTracksList: true, singerName, fromSingersScreen: true })} />

                            : null
                        }
                    </View>

                    {
                        !isLoadingSingerPopularTracks && !isErrorSingerPopularTracks && singerPopularTracks.length ?

                        (
                            <>
                                {/*<HorizontalListWithMultipleRows*/}
                                {/*    singerName={singerName}*/}
                                {/*    data={singerPopularTracks}*/}
                                {/*    darkMode={darkMode}*/}
                                {/*    navigation={props.navigation}*/}
                                {/*/>*/}

                                <ScrollView
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                >
                                    <MusicListWithSheetsMenu
                                        showsHorizontalScrollIndicator={false}
                                        singerName={singerName}
                                        data={singerPopularTracks}
                                        darkMode={darkMode}
                                        numColumns={Math.ceil(singerPopularTracks.length / 3)}
                                        navigation={props.navigation}
                                        page={1}
                                        flatListStyle={{paddingTop: 20, height: 240}}
                                        icon={<SvgPlusIcon width={18} height={18} color={COLORS.purple} />}
                                        musicItemStyle={{width: 305, marginRight: 10, marginLeft: 4}}
                                        isPlayListPage={false}
                                        onOpenPlayer={handleOpenPlayer}
                                    />
                                </ScrollView>
                            </>
                        ) : isLoadingSingerPopularTracks && !isErrorSingerPopularTracks ?

                        <View style={styles.loader}>
                            <ActivityIndicator />
                        </View> : !isLoadingSingerPopularTracks && !isErrorSingerPopularTracks && !singerPopularTracks.length ?

                        <View style={styles.loader}>
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                color: !darkMode ? '#000' : '#fff',
                            }}>Список треков пуст</Text>
                        </View> :

                        <View style={styles.loader}>
                            <Text style={{
                                width: '100%',
                                textAlign: 'center',
                                color: !darkMode ? '#000' : '#fff',
                            }}>Ошибка получения данных</Text>
                        </View>
                    }

                    <View style={styles.titleWrapper}>
                        <Text style={[ styles.blockTitle, {color: darkMode ? COLORS.white : COLORS.black} ]}>Альбомы</Text>

                        <View/>
                    </View>

                    <HorizontalAlbumList requestFailed={errorSingerAlbums} loading={loadingSingerAlbums} props={props} data={singerAlbums} darkMode={darkMode} navigation={props.navigation} singerName={singerName} />
                </View>
            </MainScreenContainer>
        </ScrollView>
    );
};

PerformerScreen.navigationOptions = () => ({
    tabBarVisible: false
});

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,

    singerAlbums: state.singerReducer.singerAlbums,
    loadingSingerAlbums: state.singerReducer.loadingSingerAlbums,
    errorSingerAlbums: state.singerReducer.errorSingerAlbums,

    isLoadingSingerPopularTracks: state.singerReducer.loadingSingerPopularTracks,
    singerPopularTracks: state.singerReducer.singerPopularTracks,
    isErrorSingerPopularTracks: state.singerReducer.errorSingerPopularTracks,
});

const mapDispatchToProps = {
    getSingerTracks,
    getSingerAlbums,
    clearSingerData,

    toggleBottomBar,
};

export default connect(mapStateToProps, mapDispatchToProps)(PerformerScreen);


const styles = StyleSheet.create({
    icon: {
        marginBottom: 'auto',
        position: 'absolute',
        bottom: 20
    },
    name: {
        color: '#000',
        fontSize: 12,
        fontWeight: '700',
    },
    headerImage: {
        width: screenWidth,
        height: screenHeight / 2.2,
        position: 'absolute',
        resizeMode: 'cover',
    },
    image: {
        width: 130,
        height: 122,
        marginTop: screenHeight / 25,
        borderRadius: 8,
    },
    titleWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 24,
        paddingHorizontal: 15,
        width: '100%',

    },
    blockTitle: {
        fontSize: 22,
        fontWeight: '700'
    },
    arrowBackWrapper: {
        left: 20,
        top: 30,
        position: 'absolute',
        zIndex: 110
    },
    loader: {
        flex: 1,
        height: 240,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
