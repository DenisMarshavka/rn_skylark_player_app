import React from 'react';
import {
	StyleSheet,
	Text,
	Image,
	Dimensions,
	ImageBackground,
	TouchableOpacity,
} from 'react-native';

import RadioPlayIcon from '../../../icons/radioPlay';
import { logoImage } from '../../../constants/default_images';


const screenWidth = Dimensions.get('window').width;
// const screenHeight = Dimensions.get('window').height;

const MonthTracks = ({ listIcons, darkMode, handler, navigation, listDataName = '' }) => {
	// console.log('Week Tracks listIcon Data: ', listIcons);

	const generateImagesTracks = () => {
		let styleZIndex = 4;
		let stylePositionRight = 0.35;

		return listIcons.map((item, i) => {
			styleZIndex -= 1;
			stylePositionRight -= 0.08;

			if (i < 4){
				return (
					<Image
						key={item.name + i}
						source={ item.photo ? {uri: item.photo} : logoImage}
						style={[styles.popularImage, { right: stylePositionRight * screenWidth, zIndex: styleZIndex }]}
					/>
				);
			} else {
				return false
			}
			
		});
	};

	return (
		<TouchableOpacity onPress={ () => {if (listDataName) navigation.navigate('TracksScreen', { [listDataName]: true })}}>
			<ImageBackground
				source={require('../../../assets/purple_gradient.png')}
				style={{ width: '100%', height: 120, marginVertical: 20, position: 'relative' }}
				imageStyle={{ borderRadius: 8 }}
			>
				<Text style={styles.title}>Треки недели</Text>

				<RadioPlayIcon width={40} height={40} style={{ position: 'absolute', bottom: 20, left: 16 }} />

				{ generateImagesTracks() }
			</ImageBackground>
		</TouchableOpacity>
	);
};

export default MonthTracks

const styles = StyleSheet.create({
	title: {
		position: 'absolute',
		top: 20,
		left: 16,
		color: 'white',
		fontWeight: 'bold',
		fontSize: 18
	},
	popularImage: {
		width: 80,
		height: 80,
		resizeMode: 'cover',
		position: 'absolute',
		top: 20,
		borderRadius:  80,
		opacity: 0.9
	}
});
