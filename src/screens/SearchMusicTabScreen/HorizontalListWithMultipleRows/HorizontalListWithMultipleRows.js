import React,  {useState} from 'react';
import {
    StyleSheet,
    ScrollView,
    FlatList,
    Dimensions,
    TouchableOpacity,
    View,
    Text,
    ActivityIndicator
} from 'react-native';
import {COLORS} from '../../../constants/colors.constants'
import SvgPlusIcon from '../../../icons/plus';
import SvgSuccessIcon from '../../../icons/successIcon';
import MusicItem from '../../../components/MusicItem/MusicItem';
import Modal from 'react-native-modal';
const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height
import AddTrackSuccessModal from '../AddTrackSuccessModal/AddTrackSuccessModal'
import {connect} from "react-redux";
import {getAddTrackToUserTrackListByTrackId, resetTrackInUserTrackListIsAlready} from "../../../actions";
import trackListReducer from "../../../store/TrackListReducer";

const HorizontalList = ({ isAlbumScreen = false, singerName, data, darkMode, navigation, withoutDelete, getAddTrackToUserTrackListByTrackId, loadingAddTrackToTrackList, errorAddTrackToTrackList, trackIsAlready, resetTrackInUserTrackListIsAlready }) => {
    const [modalVisible, setModalVisible] = useState(false);

    const onAddTrackToUserTracksList = trackId => {
        console.log('add', trackId);

        getAddTrackToUserTrackListByTrackId(trackId);
        setModalVisible(!modalVisible);
    };

    // console.log(`DATA HorizontalList ${data}`);

    const handleOpenPlayer = ({ track }) => {
        navigation.navigate('PlayerScreen', { track, playlist: data });
    };

    return (
        <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
        >
            {/*{*/}
            {/*    <Modal*/}
            {/*        animationIn={'fadeIn'}*/}
            {/*        animationOut={'fadeOut'}*/}
            {/*        animationInTiming={200}*/}
            {/*        animationOutTiming={10}*/}
            {/*        onBackButtonPress={() => {*/}
            {/*            setModalVisible(!modalVisible);*/}
            {/*        }}*/}
            {/*        hideModalContentWhileAnimating={true}*/}
            {/*        backdropOpacity={0.5}*/}
            {/*        onBackdropPress={() => {*/}
            {/*            setModalVisible(!modalVisible);*/}
            {/*        }}*/}
            {/*        style={styles.modalView}*/}
            {/*        isVisible={modalVisible === 'object' ? !!modalVisible : modalVisible}*/}
            {/*    >*/}
            {/*        {*/}
            {/*            loadingAddTrackToTrackList ?*/}

            {/*            <View style={styles.loader}>*/}
            {/*                <ActivityIndicator />*/}
            {/*            </View> :*/}

            {/*            <View style={[styles.centeredView, {backgroundColor: darkMode ? '#000' : '#fff', alignItems: 'center'}]}>*/}
            {/*                <View style={{marginVertical: 14}}>*/}
            {/*                    <SvgSuccessIcon />*/}
            {/*                </View>*/}

            {/*                <Text style={[styles.modalText, {color: darkMode ? '#fff' : '#000'}]}>Трек успешно добавлен {"\n"} в Вашу музыку!</Text>*/}
            {/*            </View>*/}
            {/*        }*/}
            {/*    </Modal>*/}
            {/*}*/}

            {/*<AddTrackSuccessModal*/}
            {/*    modalVisible={modalVisible}*/}
            {/*    setModalVisible={setModalVisible}*/}
            {/*    darkMode={darkMode}*/}
            {/*    errorRequest={errorAddTrackToTrackList}*/}
            {/*    trackIsAlready={trackIsAlready}*/}
            {/*    loading={loadingAddTrackToTrackList}*/}
            {/*    resetTrackIsAlready={resetTrackInUserTrackListIsAlready}*/}
            {/*/>*/}

            <FlatList
                data={data}
                showsHorizontalScrollIndicator={false}
                numColumns={Math.ceil(data.length / 3)}
                keyExtractor={item => item.id.toString()}
                style={{paddingTop: 20, height: 240}}
                renderItem={({item, ...props}) => (
                    <MusicItem
                        key={item.name + props.index}
                        musicData={item}
                        isAlbumScreen={isAlbumScreen}
                        singerName={singerName}
                        icon={
                            <TouchableOpacity
                                onPress={() => {
                                    onAddTrackToUserTracksList(item.id);
                                }}
                            >
                                <SvgPlusIcon fill={COLORS.purple}/>
                            </TouchableOpacity>}
                        darkMode={darkMode}
                        style={{width: 305, marginRight: 10, marginLeft: 4}}
                        navigation={navigation}
                        onOpenPlayer={handleOpenPlayer}
                    />
                )}
            />
        </ScrollView>
    );
};

const mapStateToProps = state => ({
    loadingAddTrackToTrackList: state.trackListReducer.loadingAddTrackToTrackList,
    errorAddTrackToTrackList: state.trackListReducer.errorAddTrackToTrackList,
    trackIsAlready: state.trackListReducer.trackInTrackListIsAlready,
});

const mapDispatchToProps = {
    resetTrackInUserTrackListIsAlready,
    getAddTrackToUserTrackListByTrackId,
};

export default connect(mapStateToProps, mapDispatchToProps)(HorizontalList);

const styles = StyleSheet.create({
    centeredView: {
        paddingHorizontal: 40,
        paddingBottom: 30,
        borderRadius: 8
    },
    modalView: {
        margin: 0,
        width: screenWidth,
        backgroundColor: "rgba(51, 51, 51, 0.48)",
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flex: 1
    },
    modalText: {
        textAlign: "center",
        fontSize: 18,
        fontWeight: 'bold',
    }
});
