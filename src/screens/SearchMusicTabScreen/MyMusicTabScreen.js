import React, { useEffect, useState } from 'react';
import {
	StyleSheet,
	SafeAreaView,
	Text,
	View,
	ScrollView,
	StatusBar,
	TouchableOpacity,
	ActivityIndicator,
	Dimensions,
} from 'react-native';
// import AsyncStorage from "@react-native-community/async-storage";
import {connect} from 'react-redux';

import {COLORS} from '../../constants/colors.constants'
import { MINI_PLAYER_HEIGHT } from '../../constants/styles';
import SvgStarIcon from '../../icons/star';
import SvgArrowRightIcon from '../../icons/arrowRight';
import SvgDownloadIcon from '../../icons/download';
import SvgAllTracksIcon from '../../icons/alltracks';
import SvgNoteIcon from '../../icons/note';
import SvgPlusIcon from '../../icons/plus';
import MusicListWithSheetsMenu from "../../components/MusicListWithSheetsMenu/MusicListWithSheetsMenu";
import {getStatusBarHeight} from "react-native-status-bar-height";
import {getRecentListening} from "../../actions";
import {MainScreenContainer} from "../../components/MainScreenContainer/MainScreenContainer";

const screenHeight = Dimensions.get('window').height;

const SearchMusicTabScreen = ({ navigation, darkMode, getRecentListening, recentListeningData = false, loading = true, recentListeningError, isPlayerActive }) => {
	let musicCollection = [];
	const lines = [
		{
			icon: <SvgAllTracksIcon />,
			title: 'Все треки',
			onPress: (item) => navigation.navigate('TracksScreen', { item, withoutDelete: false, approveToTrackDownload: true })
		},
		{
			icon: <SvgDownloadIcon />,
			title: 'Загрузки',
			onPress: (item) => navigation.navigate('TracksScreenForDownloads', { item, withoutDelete: false, })
		},
		{
			icon: <SvgStarIcon />,
			title: 'Мне нравится',
			onPress: (item) => navigation.navigate('TracksScreen', { item, is_I_Like_It: true, withoutDelete: true, approveToTrackDownload: true })
		},
		{
			icon: <SvgNoteIcon />,
			title: 'Мои плейлисты',
			onPress: (item) => navigation.navigate('PlayListScreen', { item, isPlayLists: true, withoutDelete: false})
		}
	];

	let [refreshing, setRefreshing] = useState(false)

	useEffect(() => {
		getRecentListening()
	}, []);

    const handleOpenPlayer = ({ track }) => {
        navigation.navigate('PlayerScreen', { track, playlist: recentListeningData });
    };

	let generateMusicCollectionNavigate = () => {
		 lines.map((item, i) => {
			musicCollection.push(
				<TouchableOpacity
					key={item.title + i}
					style={{flexDirection: 'row', width: '100%', alignItems: 'center', marginTop: 22}}
					// onPress={() => item.title === 'Мои плейлисты' ?
					// 					item.onPress() : item.title === 'Все треки' ?
					// 					navigation.navigate('TracksScreen', { item, withoutDelete: false }) :
					// 					item.title === 'Загрузки' ?
					// 					navigation.navigate('TracksScreenForDownloads', { item, withoutDelete: false, isDownloadedTracks: true }) :
					// 					navigation.navigate('TracksScreen', { item })
					// }
					onPress={() => item.onPress(item)}
				>
					<View style={{width: 30, alignItems: 'center'}}>
						{item.icon}
					</View>

					<Text style={{
						flex: 1,
						fontSize: 18,
						marginLeft: 14,
						color: darkMode ? COLORS.white : COLORS.black
					}}>{item.title}</Text>

					<SvgArrowRightIcon color='lightgrey' style={{opacity: darkMode ? 0.6 : 1}} />
				</TouchableOpacity>
			)
		});

		return musicCollection;
	};

    return (
		<SafeAreaView style={{ flex: 1, backgroundColor: darkMode ? '#232323' : '#F9F9F9', marginTop: getStatusBarHeight(), paddingBottom: isPlayerActive ? MINI_PLAYER_HEIGHT : 0  }}>
			<StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} backgroundColor={ darkMode ? '#232323' : '#F9F9F9'} />

			<MainScreenContainer darkMode={darkMode}>
				<View style={[styles.header, { backgroundColor: darkMode ? '#232323' : '#F9F9F9' }]}>
					<TouchableOpacity style={styles.tabHeader} onPress={() => navigation.navigate('SearchMusicTabScreen')}>
						<Text style={[styles.tabHeaderText, { color: darkMode ? COLORS.grey : COLORS.black }]}>Поиск музыки</Text>
					</TouchableOpacity>

					<TouchableOpacity style={styles.tabHeader}>
						<Text style={[styles.tabHeaderText, styles.activeTabHeader, { color: darkMode ? COLORS.white : COLORS.black }]}>Моя музыка</Text>
					</TouchableOpacity>
				</View>


				<ScrollView style={{ paddingHorizontal: 14, backgroundColor: darkMode ? COLORS.black : COLORS.white }} showsVerticalScrollIndicator={false}>
					{ generateMusicCollectionNavigate() }

					{
						recentListeningError || (recentListeningData && recentListeningData.length) ?

						<View style={{flex: 1, minHeight: 350, width: '100%', justifyContent: 'flex-start'}}>
							<View style={styles.titleWrapper}>
								<Text style={[styles.blockTitle, { color: darkMode ? COLORS.white : COLORS.black }]}>Вы недавно слушали</Text>
							</View>

							{
								loading ?

								<View style={styles.loading}>
									<ActivityIndicator color='black' size='small' />
								</View> 
								
								: recentListeningData.length && !recentListeningError ?

                                <ScrollView contentContainerStyle={{ flex: 1, marginBottom: 100 }}>
                                    <MusicListWithSheetsMenu
                                        data={recentListeningData}
                                        showsHorizontalScrollIndicator={false}
                                        flatListStyle={{ paddingTop: 20, flex: 1 }}
                                        contentContainerStyle={{ alignItems: 'center' }}
                                        icon={<SvgPlusIcon width={18} height={18} fill={COLORS.purple}/>}
                                        onOpenPlayer={handleOpenPlayer}
										navigation={navigation}
                                    />
                                </ScrollView> :

								<View style={styles.loading}>
									<Text style={{
										width: '100%',
										textAlign: 'center',
										color: !darkMode ? '#000' : '#fff'
									}}>Ошибка получения данных</Text>
								</View>
                            }
                        </View> :

						null
					}
				</ScrollView>
			</MainScreenContainer>
		</SafeAreaView>
	);
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,

	loading: state.appReducer.recentListeningLoading,
	recentListeningData: state.appReducer.recentListeningData,
	recentListeningError: state.appReducer.recentListeningError,

    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
	getRecentListening,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchMusicTabScreen);


const styles = StyleSheet.create({
	loading: {
		position: 'absolute',
		top: 0, left: 0, right: 0, bottom: 0,
		justifyContent: 'center',
		alignItems: 'center',
	},
	header: {
		height: 50,
		width: '100%',
		flexDirection: 'row',
		backgroundColor: '#F9F9F9'
	},
	tabHeader: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'flex-end'
	},
	tabHeaderText: {
		fontSize: 15,
		fontWeight: 'bold',
		paddingBottom: 14,
		width: '100%',
		textAlign: 'center',
	},
	activeTabHeader: {
		borderBottomColor: '#7B3DFF',
		borderBottomWidth: 4,
		paddingBottom: 10,
	},
	titleWrapper: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginTop: 24
	},
	blockTitle: {
		fontSize: 22,
		fontWeight: '700'
	},


	musicBlock: {
		width: '100%',
		height: 60,
		shadowColor: 'grey',
		shadowOffset: { width: 2, height: 2 },
		shadowOpacity: 0.1,
		shadowRadius: 10,
		elevation: 3,
		borderRadius: 8,
		alignItems: 'center',
		flexDirection: 'row',
		marginVertical: 4,
		marginRight: 10
	}

});
