import React from 'react'
import {createStackNavigator} from 'react-navigation-stack';

import MyMusicTabScreen from './MyMusicTabScreen';
import PerformerScreen from './PerformerScreen';
import AlbumScreen from "./AlbumScreen";
import PlayListPage from "../MusicScreen/PlayListPage/PlayListPage";
import MusicTextScreen from "../SearchMusicTabScreen/MusicTextScreen";
import SearchMusicTabScreen from "./SearchMusicTabScreen";
import TracksScreen from "./../TracksScreen/TracksScreen";
import TracksScreenForDownloads from '../TracksScreen/TracksScreenForDownloads'
import ProfileScreen from "./../ProfileScreen/ProfileScreen/ProfileScreen.component";
import PlayerScreen from "./../PlayerScreen/PlayerScreen";
import SingersScreen from "./../SingersScreen/SingersScreen";

import PlayListScreen from '../MusicScreen/PlayListScreen/PlayListScreen.component'
// import CurrentPlayList from '../MusicScreen/PlayListScreen/CurrentPlayList.component'
// import AudioPlayer from '../../components/AudioPlayer/AudioPlayer';

import { withNavigationFocus } from "react-navigation";

const SearchMusicTabNavigator = createStackNavigator(
    {
        SearchMusicTabScreen: {
            screen: withNavigationFocus(SearchMusicTabScreen),
            navigationOptions: {
                gestureEnabled: false 
            },
        },
        TracksScreen,
        TracksScreenForDownloads,
        MyMusicTabScreen,
        ProfileScreen,
        PerformerScreen,
        SingersScreen,
        // AudioPlayer,
        // PlayListScreen,
        PlayListScreen: {
            screen: () => <PlayListScreen />
        },
        AlbumScreen,
        PlayListPage,
        MusicTextScreen,
        PlayerScreen,
    },
    {
        headerMode: 'none',
    },
);


export default SearchMusicTabNavigator;
