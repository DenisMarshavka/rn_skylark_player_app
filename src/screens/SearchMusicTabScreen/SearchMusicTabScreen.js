import React, { useEffect, useState, useMemo, useCallback } from 'react';
import { StyleSheet, SafeAreaView, Text, View, ScrollView, StatusBar, TouchableOpacity, Image, Dimensions, ActivityIndicator, Linking, RefreshControl, BackHandler, Platform } from 'react-native';
import { connect } from 'react-redux';

import { COLORS } from '../../constants/colors.constants'
import { MINI_PLAYER_HEIGHT } from '../../constants/styles';
import YellowButtonAll from '../../components/YellowButtonAll/YellowButtonAll.component';
import Ripple from 'react-native-material-ripple';
import GradientSquare from '../../assets/gradient_square.png'
import HorizontalList from './HorizontalList/HorizontalList';
import WeekTracks from './WeekTracks/WeekTracks'
import MonthTracks from './MonthTracks/MonthTracks'
import {
    getMonthPopularTrackList,
    getMoods,
    getPlayLists,
    getSingers,
    getPopularSingers,
    getTrackListTop,
    getWeekPopularTrackList
} from "../../actions";
import { logoImage } from '../../constants/default_images';
import { getAllUrlParams } from '../../utils/getUrlParams'
import { instance } from '../../api/api';
import { palette } from "../../state/palette";
import { wait } from "../../utils/promises";
import MusicListWithSheetsMenu from "../../components/MusicListWithSheetsMenu/MusicListWithSheetsMenu";
import SvgPlusIcon from "../../icons/plus";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { mainStyles } from "../../utils/styles";
import { MainScreenContainer } from "../../components/MainScreenContainer/MainScreenContainer";
import ArrowRightIcon from '../../icons/arrowRight';

const screenWidth = Dimensions.get('window').width;

const sizeMoodItem = ((screenWidth / (screenWidth > 420 ? 2.5 : 2)) - 20);

const SearchMusicTabScreen = props => {
    const {
        isLoadingTrackListTop,
        getTrackListTop,
        trackListTop,
        errorTopTrackListRequest,

        getSingers,
        isSingersTackListLoading,
        singersList,
        errorSingersListRequest,

        getPopularSingers,
        isPopularSingersTackListLoading,
        popularSingersList,
        errorPopularSingersListRequest,

        getMoods,
        moodsList,
        errorMoodsListRequest,

        moodsLoading,

        getWeekPopularTrackList,
        getMonthPopularTrackList,

        weekListLoading = false,
        weekList = [],
        errorWeekList = false,

        monthListLoading = false,
        monthList = [],
        errorMonthList = false,
    } = props;

    const [refreshing, setRefresh] = useState(false);
    let darkMode = props.darkMode;

    const onDataListsLoad = () => {
        getTrackListTop();
        getSingers();
        getPopularSingers();
        getMoods();
        getWeekPopularTrackList();
        getMonthPopularTrackList();
        // getPlayLists()
    };

    useEffect(() => {
        onDataListsLoad();

        // console.log('search music tab screen data list top music: ', singersList, 'singersList.length', singersList.length);
    }, []);

    const onDataRefresh = useCallback(() => {
        setRefresh(true);

        onDataListsLoad();

        wait(500)
            .then(() => setRefresh(false));
    }, [refreshing]);


    // deep linking
    useEffect(() => {
        Linking.addEventListener('url', handleOpenURL);

        Linking.getInitialURL().then(initialUrl => {
            console.log('initialUrl', initialUrl);

            if (initialUrl) {
                _splitUrl(initialUrl)
            }
        });

        return () => Linking.removeEventListener('url', handleOpenURL);
    }, []);

    const handleOpenURL = event => {
        if (event.url) {
            _splitUrl(event.url)
        }
    };

    const _splitUrl = url => {
        let data = getAllUrlParams(url);
        console.log('url', url);

        if (data.music_id) {
            getMusicData(data.music_id)
        }
        if (data.playlist_id) {
            getPlaylistData(data.playlist_id)
        }
        if (data.album_id) {
            getAlbumData(data.album_id)
        }
    };

    const getMusicData = async (track_id) => {
        let track_data = await instance.get(`get_track?track_id=` + track_id)
            .then(response => response.data.data);

        console.log('track_data', track_data)

        if (track_data) {
            props.navigation.push('PlayerScreen', { currentTrack: {...track_data} })
        }
    };

    const getPlaylistData = async (playlist_id) => {
        let playlist_tracks_list = await instance.get(`user/playlists/${playlist_id}/tracklist`)
            .then(response => response.data);

            console.log('playlist_tracks_list', playlist_tracks_list)
            console.log('-------', playlist_tracks_list.data)

        if (playlist_tracks_list.data) {
            props.navigation.push('PlayListPage', { item: playlist_tracks_list.data, deepLink: true })
        }
    };

    const getAlbumData = async album_id => {
        let album_data = await instance.get(`album/${album_id}/tracklist`)
            .then(response => response.data);

        if (album_data.data) {
            console.log('album_data', album_data.data)

            props.navigation.push('AlbumScreen', {
                id: album_data.data.album_id,
                albumName: album_data.data.name,
                photo: album_data.data.photo,
                singerName: album_data.data.singer,
                genre: album_data.data.genre,
            })
        }
    };

    const handleOpenPlayer = params => {
        props.navigation.navigate('PlayerScreen', { playlist: trackListTop, ...params });
    };

    // const trackListTopSliced = [...trackListTop].splice(0, 20);

    // console.log('singersList data trackListTopSliced.length:', trackListTopSliced.length, 'trackListTopSliced.length / 3: ', Math.ceil(trackListTopSliced.length / 4));
    const generateHorizontalListWithNewReleases = useMemo(() => (
        <>
            {/*<HorizontalListWithMultipleRows*/}
            {/*    data={trackListTop} darkMode={darkMode}*/}
            {/*    navigation={props.navigation} withoutDelete={true}*/}
            {/*/>*/}

            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
                <MusicListWithSheetsMenu
                    showsHorizontalScrollIndicator={false}
                    data={trackListTop}
                    darkMode={darkMode}
                    numColumns={Math.ceil(trackListTop.length / 4)}
                    navigation={props.navigation}
                    page={1}
                    flatListStyle={{ paddingTop: 20, minHeight: 240 }}
                    icon={<SvgPlusIcon width={18} height={18} color={COLORS.purple} />}
                    musicItemStyle={{ width: 305, marginRight: 10, marginLeft: 4 }}
                    onOpenPlayer={handleOpenPlayer}
                    approveTracksListTrackDelete={false}
                />
            </ScrollView>
        </>
    ), [trackListTop]);

    const handleAllTracksListOpen = () => props.navigation.navigate('TracksScreen', { isAllTracksList: true });

    // console.log('Result loading data: 1 - ', !singersList.length, 'isSingersTackListLoading: ', isSingersTackListLoading, 'errorSingersListRequest: ', errorSingersListRequest, 'singersList.length: ', singersList.length/*, ' 2 - ', !isLoadingTrackListTop && !requestFailed && trackListTop.length, ' 3 - ', (!trackListTop || !trackListTop.length) && !requestFailed, ' 4 - error'*/);




    // BackHandler
    useEffect(() => {
        if (Platform.OS === "android") {
            BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        } else {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButton)
        }

        return () => BackHandler.removeEventListener('hardwareBackPress', handleBackButton)
    }, [props.isFocused])

    const handleBackButton = () => {
        if (props.isFocused) {
            BackHandler.exitApp()
            return true
        } else {
            return false
        }
    }

    // console.log('singersList', trackListTop)

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: darkMode ? '#232323' : '#F9F9F9' }}>
            <StatusBar
                barStyle={darkMode ? 'light-content' : 'dark-content'}
                // backgroundColor={darkMode ? '#232323' : '#F9F9F9'}
                translucent
                backgroundColor='transparent'
            />

            <MainScreenContainer darkMode={darkMode}>
                <View style={[styles.header, { backgroundColor: darkMode ? '#232323' : '#F9F9F9' }]}>
                    <TouchableOpacity style={styles.tabHeader}>
                        <Text style={[styles.tabHeaderText, styles.activeTabHeader, { color: darkMode ? COLORS.white : COLORS.black }]}>Поиск музыки</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.tabHeader}
                        onPress={() => props.navigation.navigate('MyMusicTabScreen')}
                    >
                        <Text style={[styles.tabHeaderText, { color: darkMode ? COLORS.grey : COLORS.black }]}>Моя музыка</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView
                    style={{ paddingHorizontal: 14, backgroundColor: darkMode ? COLORS.black : COLORS.white }}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onDataRefresh}
                            progressViewOffset={50}
                            progressBackgroundColor={'transparent'}
                            colors={['#fff', palette.common.secondary, palette.common.primary]}
                            tintColor={palette.common.primary}
                        />
                    }
                >
                    <View style={styles.titleWrapper}>
                        <Text style={[styles.blockTitle, { color: darkMode ? COLORS.white : COLORS.black }]}>Новые релизы</Text>

                        <YellowButtonAll onPress={handleAllTracksListOpen} />
                    </View>
                    {
                        isLoadingTrackListTop ?

                            <View style={styles.loader}>
                                <ActivityIndicator />
                            </View> : !isLoadingTrackListTop && !errorTopTrackListRequest && trackListTop.length ?

                                generateHorizontalListWithNewReleases : (!trackListTop || !trackListTop.length) && !errorTopTrackListRequest ?

                                    <View style={styles.loader}>
                                        <Text style={[styles.emptyText, { color: !darkMode ? '#000' : '#fff'}]}>Список треков пуст</Text>
                                    </View> :

                                    <View style={styles.loader}>
                                        <Text style={[styles.emptyText, { color: !darkMode ? '#000' : '#fff'}]}>Ошибка получения данных</Text>
                                    </View>
                    }

                    <View style={styles.titleWrapper}>
                        <Text style={[styles.blockTitle, { color: darkMode ? COLORS.white : COLORS.black }]}>Новые исполнители</Text>

                        <YellowButtonAll onPress={() => props.navigation.navigate('SingersScreen')} />
                    </View>
                    {
                        isSingersTackListLoading ?

                            <View style={styles.loader}>
                                <ActivityIndicator />
                            </View> : singersList.length && !errorSingersListRequest && !isSingersTackListLoading ?

                                <HorizontalList onPlayListsMenuOpen={handleOpenPlayer} props={props} data={singersList} navigation={props.navigation} /> : !isSingersTackListLoading && !singersList.length ?

                                    <View style={styles.loader}>
                                        <Text style={[styles.emptyText, { color: !darkMode ? '#000' : '#fff'}]}>Список исполнителей пуст</Text>
                                    </View> :

                                    <View style={styles.loader}>
                                        <Text style={[styles.emptyText, { color: !darkMode ? '#000' : '#fff'}]}>Ошибка получения данных</Text>
                                    </View>
                    }

                    <View style={styles.titleWrapper}>
                        <Text style={[styles.blockTitle, { color: darkMode ? COLORS.white : COLORS.black }]}>Популярные исполнители</Text>

                        <YellowButtonAll onPress={() => props.navigation.navigate('SingersScreen', { isPopularSingers: true })} />
                    </View>
                    {
                        isPopularSingersTackListLoading ?

                        <View style={styles.loader}>
                            <ActivityIndicator />
                        </View> 
                            
                        : popularSingersList.length && !errorPopularSingersListRequest && !isPopularSingersTackListLoading ?
                            <HorizontalList 
                                onPlayListsMenuOpen={handleOpenPlayer} 
                                props={props} 
                                data={popularSingersList} 
                                navigation={props.navigation} 
                                isPopularSingers={true}
                            /> 
                        : !isPopularSingersTackListLoading && !popularSingersList.length ?

                        <View style={styles.loader}>
                            <Text style={[styles.emptyText, { color: !darkMode ? '#000' : '#fff'}]}>Список исполнителей пуст</Text>
                        </View> 
                        :
                        <View style={styles.loader}>
                            <Text style={[styles.emptyText, { color: !darkMode ? '#000' : '#fff'}]}>Ошибка получения данных</Text>
                        </View>
                    }

                    
                    {/* Systems playlists */}
                    <View style={styles.titleWrapper}>
                        <Text style={[styles.blockTitle, { color: darkMode ? COLORS.white : COLORS.black }]}>Плейлисты</Text>
                    </View>

                    <View style={[styles.systemPlaylistContainer, {
                        justifyContent: moodsList.length && !errorMoodsListRequest ? 'space-between' : 'center',
                        alignItems: moodsList.length && !errorMoodsListRequest ? 'flex-start' : 'center',
                    }]}>
                        {
                            !moodsLoading && moodsList.length && !errorMoodsListRequest ?

                            moodsList.map((item, index) => (
                                <Ripple 
                                    key={item.name + index} 
                                    onPress={() => props.navigation.navigate('TracksScreen', { 
                                        genreId: item.id, 
                                        iconPlus: true, 
                                        isGenre: true 
                                    })} 
                                    style={{ marginTop: 10 }}
                                >
                                    <Image
                                        imageStyle={{ resizeMode: 'cover' }}
                                        source={item.photo ? { uri: item.photo } : logoImage}
                                        style={{ width: sizeMoodItem, height: sizeMoodItem, borderRadius: 8,  }}
                                    />
                                    <Image source={GradientSquare} style={styles.moodGradient} />
                                    <Text style={styles.moodText}>{item.name}</Text>
                                </Ripple>
                            )) : moodsLoading ?

                            <View style={styles.loader}>
                                <ActivityIndicator />
                            </View> :

                            <View style={styles.loader}>
                                <Text style={[styles.emptyText, { color: !darkMode ? '#000' : '#fff'}]}>Ошибка получения данных</Text>
                            </View>
                        }
                    </View>

                    {/* GENRES */}

                    {/* <View style={styles.titleWrapper}>
                        <Text style={[styles.blockTitle, { color: darkMode ? COLORS.white : COLORS.black }]}>Жанры</Text>
                    </View>


                    <View style={{
                        minHeight: 175,
                        width: '100%',
                        flexDirection: 'column',
                        flexWrap: 'wrap',
                        // justifyContent: moodsList.length && !errorMoodsListRequest ? 'space-between' : 'center',
                        alignItems: moodsList.length && !errorMoodsListRequest ? 'flex-start' : 'center',
                    }}>
                        {
                            !moodsLoading && moodsList.length && !errorMoodsListRequest ?

                                moodsList.map((item, index) => (
                                    <TouchableOpacity
                                        onPress={() => props.navigation.navigate('TracksScreen', { genreId: item.id, iconPlus: true, isGenre: true })}
                                        key={item.name + index}>
                                        <View style={styles.wrapper}>
                                            <Text style={styles.title}>{item.name}</Text>
                                            <ArrowRightIcon />
                                        </View>
                                    </TouchableOpacity>

                                )) : moodsLoading ?

                                    <View style={styles.loader}>
                                        <ActivityIndicator />
                                    </View> :

                                    <View style={styles.loader}>
                                        <Text style={{
                                            width: '100%',
                                            textAlign: 'center',
                                            color: !darkMode ? '#000' : '#fff'
                                        }}>Ошибка получения данных</Text>
                                    </View>
                        }
                    </View> */}


                    <View style={{ paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT + 35 : 35 }}>
                        <View style={styles.titleWrapper}>
                            <Text style={[styles.blockTitle, { color: darkMode ? COLORS.white : COLORS.black }]}>Популярное</Text>
                        </View>

                        {
                            weekListLoading || monthListLoading ?

                                <View style={styles.loader}>
                                    <ActivityIndicator />
                                </View>
                                :
                                (!errorWeekList && weekList && weekList) && (!errorMonthList && monthList && monthList)
                                ?
                                <>
                                    <WeekTracks listIcons={weekList.length > 3 ? weekList : trackListTop} navigation={props.navigation} listDataName={'isPopularTracksWeek'} />

                                    <MonthTracks listIcons={monthList.length > 3 ? monthList : trackListTop } navigation={props.navigation} listDataName={'isPopularTracksMonth'} />
                                </>
                                :
                                <View style={styles.loader}>
                                    <Text style={[styles.emptyText, { color: !darkMode ? '#000' : '#fff'}]}>Ошибка получения данных</Text>
                                </View>
                        }
                    </View>
                </ScrollView>
            </MainScreenContainer>
        </SafeAreaView>
    );

};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,

    isLoadingTrackListTop: state.searchMusicReducer.topTrackListLoading,
    trackListTop: state.searchMusicReducer.trackListTop,
    errorTopTrackListRequest: state.searchMusicReducer.errorTopTrackList,

    isSingersTackListLoading: state.searchMusicReducer.singersTackListLoading,
    singersList: state.searchMusicReducer.singersList,
    errorSingersListRequest: state.searchMusicReducer.errorSingersList,

    isPopularSingersTackListLoading: state.searchMusicReducer.isPopularSingersTackListLoading,
    popularSingersList: state.searchMusicReducer.popularSingersList,
    errorPopularSingersListRequest: state.searchMusicReducer.errorPopularSingersListRequest,

    moodsLoading: state.searchMusicReducer.moodsListLoading,
    moodsList: state.searchMusicReducer.moodsList,
    errorMoodsListRequest: state.searchMusicReducer.errorMoodsList,

    weekListLoading: state.searchMusicReducer.weekListLoading,
    weekList: state.searchMusicReducer.weekList,
    errorWeekList: state.searchMusicReducer.errorWeekList,

    monthListLoading: state.searchMusicReducer.monthListLoading,
    monthList: state.searchMusicReducer.monthList,
    errorMonthList: state.searchMusicReducer.errorMonthList,

    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
    getTrackListTop,
    getSingers,
    getPopularSingers,
    getMoods,
    getPlayLists,

    getWeekPopularTrackList,
    getMonthPopularTrackList,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchMusicTabScreen);


const styles = StyleSheet.create({
    header: {
        height: 50,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: '#F9F9F9',
        marginTop: getStatusBarHeight()
    },
    tabHeader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    tabHeaderText: {
        fontSize: 15,
        fontWeight: 'bold',
        paddingBottom: 14,
        width: '100%',
        textAlign: 'center',
    },
    activeTabHeader: {
        borderBottomColor: '#7B3DFF',
        borderBottomWidth: 4,
        paddingBottom: 10,
    },
    titleWrapper: {
        marginTop: 24,

        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    blockTitle: {
        fontSize: 20,
        fontWeight: '700'
    },

    popularImage: {
        width: 80,
        height: 80,
        resizeMode: 'cover',
        position: 'absolute',
        top: 20,
    },
    loader: {
        flex: 1,
        minHeight: 240,
        alignItems: 'center',
        justifyContent: 'center',
    },


    title: {
        fontSize: 18,
        color: palette.common.primary,
        paddingVertical: 15,
        width: '100%'
    },
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingRight: 40,
    },

    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.28,
        shadowRadius: 4,
        elevation: 8,
    },

    emptyText: {
        width: '100%',
        textAlign: 'center',
    },


    systemPlaylistContainer: {
        minHeight: 175,
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    moodGradient: {
        width: sizeMoodItem,
        height: sizeMoodItem,
        borderRadius: 8,
        resizeMode: 'cover',
        position: 'absolute'
    },
    moodText: {
        fontSize: 18,
        color: 'white',
        position: 'absolute',
        bottom: 14,
        left: 16,
        width: '80%',
    }

});
