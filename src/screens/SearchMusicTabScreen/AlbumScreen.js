import React, { useRef, useEffect } from 'react';
import {
    StyleSheet,
    Dimensions,
    View,
    Image,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';

import {COLORS} from "../../constants/colors.constants";
import { MINI_PLAYER_HEIGHT } from '../../constants/styles';
import ArrowLeftIcon from '../../icons/arrowLeft';
import SvgMenuIcon from "../../icons/menu";
import SvgMusicalNoteIcon from "../../icons/musicalNote";
// import SvgPlusIcon from "../../icons/plus";
import VerticalMusicList from "./VerticalMusicList/VerticalMusicList";
import AlbumMenu from "../../components/AlbumItem/AlbumMenu";
import {getTracksByAlbum} from "../../actions";
import SvgPlusIcon from "../../icons/plus";
import MusicListWithSheetsMenu from "../../components/MusicListWithSheetsMenu/MusicListWithSheetsMenu";
import {MainScreenContainer} from "../../components/MainScreenContainer/MainScreenContainer";
import {getStatusBarHeight} from "react-native-status-bar-height";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const AlbumScreen = ({ getTracksByAlbum, loading = true, requestFailed = false, tracksList, navigation, darkMode, ...props }) => {
    const { id, albumName, photo, singerName, genre } = navigation.state.params;
    const refRBSheet = useRef(null);

    useEffect(() => {
        getTracksByAlbum(id);
    }, []);

    const onMenuPress = () => {
        refRBSheet.current.open();
    };

    const handleOpenPlayer = () => {
        navigation.navigate('PlayerScreen', { playlist: tracksList });
    };

    console.log(`Album screen data navigation:`, navigation.state.params);

    return (
        <MainScreenContainer darkMode={darkMode}>
            <ScrollView contentContainerStyle={{ backgroundColor: darkMode ? '#000000' : '#fff', alignItems: 'center',
             marginTop: getStatusBarHeight(),
             paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT + 35 : 35
        }}>
                <TouchableOpacity onPress={() => navigation.goBack()} style={[styles.arrowBackWrapper, {marginTop: getStatusBarHeight()}]}>
                    <ArrowLeftIcon color={darkMode ? 'white' : 'black'}/>
                </TouchableOpacity>

                <TouchableOpacity
                    hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
                    onPress={onMenuPress}
                    style={[styles.svgMenuIcon, {marginTop: getStatusBarHeight()}]}>
                    <SvgMenuIcon color={darkMode ? 'white' : 'black'} />
                </TouchableOpacity>

                <Image
                    source={ photo ? {uri: photo} : require('../../assets/edward-cisneros-KoKAXLKJwhk-unsplash.png') }
                    style={styles.headerImage}
                    blurRadius={2}
                />

                <Image
                    source={ darkMode ? require('../../assets/black_gradient.png') : require('../../assets/white_gradient.png') }
                    style={styles.headerImage}/>

                <View style={{alignItems: 'center', marginBottom: 6, paddingHorizontal: 15}}>
                    <Image
                        resizeMode={'cover'}
                        source={ photo ? {uri: photo} : require('../../assets/edward-cisneros-KoKAXLKJwhk-unsplash.png') }
                        style={styles.image}/>

                    <Text style={{marginTop: 10, fontSize: 14, color: darkMode ? COLORS.white : COLORS.black}}>Альбом</Text>

                    <Text style={{marginTop: 3, fontSize: 18, color: darkMode ? COLORS.white : COLORS.black}}>{albumName || 'Название альбома'}</Text>

                    <Text style={{fontSize: 18, color: '#8d8d8d'}}>{singerName}</Text>

                    <Text style={{ fontSize: 11, color: darkMode ? COLORS.white : COLORS.black, marginTop: 7 }}>{genre}</Text>

                    <View style={{flexDirection: 'row', marginTop: 24}}>
                        <TouchableOpacity style={styles.btnWrapper} onPress={handleOpenPlayer}>
                            <Text style={styles.btnText}>Слушать</Text>

                            <SvgMusicalNoteIcon color='white' style={{marginTop: 2}}/>
                        </TouchableOpacity>

                        {/* <TouchableOpacity style={[styles.btnWrapper, {backgroundColor: '#7B3DFF'}]} onPress={() => {
                        }}>
                            <Text style={styles.btnText}>Добавить</Text>
                            <SvgPlusIcon color='white' width={16} height={16} style={{marginTop: 2}}/>
                        </TouchableOpacity> */}

                    </View>
                </View>

                {
                    loading && !requestFailed ?

                    <View style={styles.wrapperContent}>
                        <ActivityIndicator />
                    </View> : !loading && !requestFailed && tracksList.length ?

                    <>
                        {/*<VerticalMusicList data={{}} darkMode={darkMode} navigation={props.navigation} />*/}

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <MusicListWithSheetsMenu
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{ width: '95%', paddingTop: 20, paddingBottom: 40, justifyContent: 'center', alignItems: 'center' }}
                                data={tracksList}
                                darkMode={darkMode}
                                navigation={navigation}
                                page={1}
                                flatListStyle={{ paddingTop: 20, width: '100%' }}
                                icon={<SvgPlusIcon width={18} height={18} color={COLORS.purple} />}
                                isPlayListPage={false}
                                onOpenPlayer={handleOpenPlayer}
                            />
                        </View>
                    </> : !requestFailed && !loading && !tracksList.length ?

                    <View style={styles.wrapperContent}>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            color: !darkMode ? '#000' : '#fff'
                        }}>Список треков пуст</Text>
                    </View> :

                    <View style={styles.wrapperContent}>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            color: !darkMode ? '#000' : '#fff'
                        }}>Ошибка получения данных</Text>
                    </View>
                }

                <AlbumMenu ref={refRBSheet} albumData={{ id, albumName, photo, singerName, genre }} darkMode={darkMode} />
            </ScrollView>
        </MainScreenContainer>
    );
};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,

    loading: state.singerReducer.albumTracksLoading,
    requestFailed: state.singerReducer.albumTracksError,
    tracksList: state.singerReducer.albumTracks,

    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
    getTracksByAlbum,
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumScreen);


const styles = StyleSheet.create({
    wrapperContent: {
        flex: 1,
        height: '100%',
        minHeight: screenHeight / 1.61,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        marginBottom: 'auto',
        position: 'absolute',
        bottom: 20
    },
    name: {
        color: '#000',
        fontSize: 11,
        fontWeight: '700',
    },
    headerImage: {
        width: screenWidth,
        height: screenHeight / 2.4,
        position: 'absolute'
    },
    image: {
        width: 130,
        height: 122,
        marginTop: 20,
        borderRadius: 8,
    },
    arrowBackWrapper: {
        left: 20,
        top: 10,
        position: 'absolute',
        flexDirection: 'row',
        zIndex: 100
    },
    svgMenuIcon: {
        right: 20,
        top: 10,
        position: 'absolute',
        flexDirection: 'row',
        zIndex: 100
    },
    btnWrapper: {
        width: 120,
        height: 30,
        backgroundColor: '#FFC42E',
        borderRadius: 17,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: 7,
    },
    btnText: {
        color: 'white',
        marginRight: 4
    }
});
