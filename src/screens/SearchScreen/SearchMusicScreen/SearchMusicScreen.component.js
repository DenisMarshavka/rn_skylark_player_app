import React, { useState, useMemo, useEffect } from 'react';
import {
	StyleSheet,
	SafeAreaView,
	Text,
	View,
	ScrollView,
	ActivityIndicator,
	StatusBar
} from 'react-native';
import {connect} from 'react-redux';

import SearchBlock from '../../../components/SearchBlock/SearchBlock';
import {COLORS} from '../../../constants/colors.constants';
import { MINI_PLAYER_HEIGHT } from '../../../constants/styles';
import {deleteTrackById, getResetSearchData, getTrackListSearchByParams, setTrackItemLike} from "../../../actions";
import SvgPlusIcon from "../../../icons/plus";
import MusicListWithSheetsMenu from "../../../components/MusicListWithSheetsMenu/MusicListWithSheetsMenu";
import { getStatusBarHeight } from "react-native-status-bar-height";
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";

// const searchItems = [
// 	'Zivert',
// 	'Twenty one pilots',
// 	'Shadow',
// 	'Zivert2',
// 	'Twenty one pilots2',
// 	'Shadow2',
// ];

const SearchMusicScreen = props => {
	const perPage = 20;

	// const [list, setList] = useState([]);
	const [searchValue, setSearchValue] = useState('');
	const [wasSearchActivated, setWasSearchActivated] = useState(false);
	const [page, setPage] = useState(1);

	const { navigation, darkMode, loading, trackListSearchMusicPageId, trackList, getTrackListSearchByParams, getResetSearchData  } = props;

	useEffect(() => () => getResetSearchData(), []);

	const handleFetchSearch = () => {
		// console.log('handleFetchSearch', searchValue.toLowerCase());

        getTrackListSearchByParams({
            page,
            search: searchValue.toLowerCase(),
            perPage
        });

        setWasSearchActivated(true);
	};

    const handleOpenPlayer = ({ track, onPlayListsMenuOpen = () => {} }) => {
        navigation.navigate('PlayerScreen', { track, playlist: trackList, onPlayListsMenuOpen });
    };

	// const renderRecommendationsSearchValues = () => list.map(item => (
	// 	<Text key={item} style={[styles.searchItem, { color: props.darkMode ? COLORS.white : COLORS.black }]}>
	// 		{item}
	// 	</Text>
	// ));

	const handleSearchValue = value => setSearchValue(value);

	const renderSearchedTrackList = useMemo(() => (
		<View style={{marginBottom: 40}}>
			<MusicListWithSheetsMenu
				navigation={navigation}
				data={trackList}
				flatListStyle={{paddingTop: 20, flex: 1}}
				isPlayListPage={false}
				contentContainerStyle={{alignItems: 'center'}}
				icon={<SvgPlusIcon width={18} height={18} color={COLORS.purple} />}
				page={trackListSearchMusicPageId}
				darkMode={darkMode}
				onOpenPlayer={handleOpenPlayer}
			/>
		</View>
	), [trackList]);

	const emptySearchInput = (
		<Text style={{
			width: '100%',
			textAlign: 'center',
			color: !darkMode ? '#000' : '#fff',
			marginTop: 25,
		}}>Введите название трека, исполнителя{"\n"} или альбома</Text>
	);

	// console.log('handleFetchSearch result DATA: ', trackList/*, 'list', list*/);
	return (
		<SafeAreaView style={{flex: 1, marginTop: getStatusBarHeight(), paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT : 0}}>
			<StatusBar
                barStyle={darkMode ? 'light-content' : 'dark-content'}
                backgroundColor={darkMode ? '#232323' : '#F9F9F9'}
            />

			<MainScreenContainer darkMode={darkMode}>
				<ScrollView style={[styles.container, { backgroundColor: props.darkMode ? COLORS.black : COLORS.white }]}>
					<SearchBlock value={searchValue} onSearchStart={handleFetchSearch} onChange={handleSearchValue} darkMode={props.darkMode} />

					<View style={{width: '100%'}}>
						{/*<Text style={[styles.title, { color: props.darkMode ? COLORS.white : COLORS.black }]}>Рекомендуемые</Text>*/}
						{
							!loading && !searchValue.trim() && !wasSearchActivated ?

							emptySearchInput :

							!loading && trackList.length && wasSearchActivated ?

							renderSearchedTrackList :

							loading && wasSearchActivated ?

							<View style={{marginTop: 50}}>
								<ActivityIndicator />
							</View> : !trackList.length && wasSearchActivated && !loading ?

							<View style={styles.empty}>
								<Text style={{
									width: '100%',
									textAlign: 'center',
									color: !darkMode ? '#000' : '#fff',
									marginTop: 50,
									lineHeight: 20,
								}}>Извините, по вашему запросу ничего{"\n"} не найдено :(</Text>
							</View>  :

							null
						}
					</View>
				</ScrollView>
			</MainScreenContainer>
		</SafeAreaView>
	);
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
	loading: state.SearchMusicScreenReducer.loading,
	trackList: state.SearchMusicScreenReducer.trackList,
	trackListSearchMusicPageId: state.SearchMusicScreenReducer.trackListSearchMusicPageId,
    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
	getTrackListSearchByParams,
	getResetSearchData,

	setTrackItemLike,

	deleteTrackById,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchMusicScreen);


const styles = StyleSheet.create({
	searchItem: {
		fontSize: 18,
		lineHeight: 21,
		marginBottom: 10,
	},
	searchInputWrapper: {
		flexGrow: 1,
		marginLeft: 12,
	},
	title: {
		fontWeight: 'bold',
		fontSize: 22,
		marginBottom: 24,
	},
	container: {
		flex: 1,
		paddingHorizontal: 15,
	},
	searchBlock: {
		paddingVertical: 24,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	empty: {
		flex: 1,
		height: '100%',
		alignItems: 'center',
		justifyContent: 'center',
	}
});
