import {createStackNavigator} from 'react-navigation-stack';

import SearchMusicScreen from './SearchMusicScreen/SearchMusicScreen.component';

const SearchNavigator = createStackNavigator(
  {
    SearchMusicScreen: {
      screen: SearchMusicScreen,
    },
  },
  {
    headerMode: 'none',
  },
);

export default SearchNavigator;
