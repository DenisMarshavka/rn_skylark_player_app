import {createStackNavigator} from 'react-navigation-stack';

import PlayListScreen from './PlayListScreen/PlayListScreen.component';
import PlayListPage from "./PlayListPage/PlayListPage";

const SearchNavigator = createStackNavigator(
  {
    PlayListScreen: {
      screen: PlayListScreen,
    },
    PlayListPage : {
      screen : PlayListPage
    }
  },
  {
    headerMode: 'none',
  },
);

export default SearchNavigator;
