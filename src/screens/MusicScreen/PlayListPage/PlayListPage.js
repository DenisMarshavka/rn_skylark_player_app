import React, { useEffect, useMemo } from 'react';
import {
    StyleSheet,
    Dimensions,
    View,
    Image,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    StatusBar, SafeAreaView
} from 'react-native';
import {connect} from 'react-redux';

import ArrowLeftIcon from '../../../icons/arrowLeft';
import SvgMusicalNoteIcon from "../../../icons/musicalNote";
import {COLORS} from "../../../constants/colors.constants";
import { MINI_PLAYER_HEIGHT } from '../../../constants/styles';
import {deleteTrackInPlayList, getPlayListTracks, resetPlayListTracksData} from "../../../actions";
import MusicListWithSheetsMenu from "../../../components/MusicListWithSheetsMenu/MusicListWithSheetsMenu";
import { getStatusBarHeight } from "react-native-status-bar-height";
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const PlayListPage = props => {
    const { loading, playListTracks, isPlayListTracksEmpty, getPlayListTracks, deleteTrackInPlayList, resetPlayListTracksData, requestFailed } = props;
    const { item, deepLink } = props.navigation.state.params;

    // console.log('onOpenPlayList item: ', item, 'playListTracks: ', playListTracks);

    useEffect(() => {
        console.log('PlayListPage item: ', item);

        deepLink ? getPlayListTracks(item.playlist_id) : getPlayListTracks(item.id);

        return () => resetPlayListTracksData();
    }, []);

    const handleTrackFromPlayListDelete = (trackId, trackPositionId) => {
        if (trackId && item && item.id) {
            deleteTrackInPlayList(item.id, trackId, trackPositionId);
        }
    };

    const handleOpenPlayer = ({ track, onPlayListsMenuOpen, onTrackDelete, onMoreMenuOpen, setMusicData }) => {
        props.navigation.navigate('PlayerScreen', { track, playlist: playListTracks, withoutDelete: false, isPlayListPage: true, onPlayListsMenuOpen, onTrackDelete, onMoreMenuOpen, setMusicData });
    };

    let darkMode = props.darkMode;

    const renderPlayListTracks = useMemo(() => (
        <>
            {/*<VerticalMusicList isPlayListPage={isPlayListPage} playListPageId={item.id} data={playListTracks} darkMode={darkMode} navigation={props.navigation} />*/}

            <ScrollView style={{ marginBottom: 75, paddingHorizontal: 10 }}>
                <MusicListWithSheetsMenu
                    showsHorizontalScrollIndicator={false}
                    data={playListTracks}
                    navigation={props.navigation}
                    page={1}
                    withoutDelete={false}
                    withoutAddToPlayList={true}
                    flatListStyle={{ paddingTop: 20, flex: 1 }}
                    icon={false}
                    musicItemStyle={{width: '96%'}}
                    isPlayListPage={true}
                    onDeleteTrackFromPlayList={handleTrackFromPlayListDelete}
                    onOpenPlayer={handleOpenPlayer}
                    approvePlayListTrackDelete={true}
                />
            </ScrollView>
        </>
    ), [playListTracks, props.navigation, handleTrackFromPlayListDelete]);

    return (
        <ScrollView style={{ backgroundColor: darkMode ? '#000000' : '#fff', paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT : 0 }}>
            <StatusBar translucent backgroundColor={darkMode ? '#000' : '#fff'} />

            <MainScreenContainer darkMode={darkMode}>
                <TouchableOpacity onPress={() => props.navigation.goBack()} style={styles.arrowBackWrapper}>
                    <ArrowLeftIcon color={darkMode ? 'white' : 'black'}/>
                </TouchableOpacity>

                <Image
                    source={item && item.photo ? {uri: item.photo} : require('../../../assets/white_party.png')}
                    style={styles.headerImage}
                />

                <Image
                    source={darkMode ? require('../../../assets/black_gradient.png') : require('../../../assets/white_gradient.png')}
                    style={styles.headerImage}
                />

                <View style={{alignItems: 'center', marginBottom: 6, paddingHorizontal: 15, paddingTop: 75}}>
                    <Image
                        resizeMode={'cover'}
                        source={item && item.photo ? {uri: item.photo} : require('../../../assets/logo.png')}
                        style={styles.image}/>

                    <Text style={{fontSize: 14, color: darkMode ? COLORS.white : COLORS.black, marginTop: 7}}>Плейлист</Text>

                    <Text style={{
                        fontSize: 18,
                        color: darkMode ? COLORS.white : COLORS.black
                    }}>{deepLink ? item.playlist_title : item && item.name ? item.name : 'Имя плейлиста'}</Text>


                    <View style={{flexDirection: 'row', marginTop: 24}}>
                        <TouchableOpacity style={styles.btnWrapper} onPress={handleOpenPlayer}>
                            <Text style={styles.btnText}>Слушать</Text>

                            <SvgMusicalNoteIcon color='white' style={{marginTop: 2}}/>
                        </TouchableOpacity>

                        {/* <TouchableOpacity style={[styles.btnWrapper, {backgroundColor: '#7B3DFF'}]} onPress={() => {
                        }}>
                            <Text style={styles.btnText}>Добавить</Text>
                            <SvgPlusIcon color='white' width={16} height={16} style={{marginTop: 2}}/>
                        </TouchableOpacity> */}
                    </View>
                </View>

                {
                    !loading && !isPlayListTracksEmpty ?

                    renderPlayListTracks : loading && !requestFailed ?

                    <View style={styles.loader}>
                        <ActivityIndicator />
                    </View> : isPlayListTracksEmpty && !requestFailed ?

                    <View style={styles.empty}>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            color: !darkMode ? '#000' : '#fff',
                            marginTop: 50,
                        }}>Нет добавленных треков</Text>
                    </View> :

                    <View style={styles.empty}>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            color: !darkMode ? '#000' : '#fff',
                            marginTop: 50,
                        }}>Ошибка получения данных</Text>
                    </View>
                }
            </MainScreenContainer>
        </ScrollView>
    );
};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,

    loading: state.playListReduce.loading,
    playListTracks: state.playListReduce.playListTracks,
    isPlayListTracksEmpty: state.playListReduce.isPlayListTracksEmpty,
    requestFailed:  state.playListReduce.error,

    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
    deleteTrackInPlayList,
    getPlayListTracks,

    resetPlayListTracksData,
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayListPage);


const styles = StyleSheet.create({
    loader: {
        flex: 1,
        height: 240,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        marginBottom: 'auto',
        position: 'absolute',
        bottom: 20
    },
    name: {
        color: '#000',
        fontSize: 12,
        fontWeight: '700',
    },
    headerImage: {
        width: '100%',
        height: screenHeight / 4,
        position: 'absolute'
    },
    image: {
        width: 130,
        height: 122,
        marginTop: getStatusBarHeight(),
        borderRadius: 8,
    },
    arrowBackWrapper: {
        left: 15,
        top: getStatusBarHeight() + 15,
        position: 'absolute',
        flexDirection: 'row',
        zIndex: 100
    },
    svgMenuIcon: {
        right: 20,
        top: 30,
        position: 'absolute',
        flexDirection: 'row',
        zIndex: 100
    },
    btnWrapper: {
        width: 120,
        height: 30,
        backgroundColor: '#FFC42E',
        borderRadius: 17,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginHorizontal: 7,
    },
    btnText: {
        color: 'white',
        marginRight: 4
    },
    empty: {
        flex: 1,
        height: 240,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
