import React, {forwardRef} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import Ripple from 'react-native-material-ripple';
import RBSheet from 'react-native-raw-bottom-sheet';

import {COLORS} from '../../../constants/colors.constants';
import {palette} from 'state/palette';
import ShareIcon from 'icons/share';
import RemoveIcon from 'icons/remove';
import {logoImage} from '../../../constants/default_images'

const CurrentPlayList = forwardRef(({ currentCard, darkMode, onDeletePlaylist, sharePlaylist }, ref) => {
  const {photo, id, name} = currentCard;

 return (
  <RBSheet
    ref={ref}
    height={300}
    closeOnDragDown={true}
    customStyles={{
      container:{
        backgroundColor: darkMode ? COLORS.black : '#FCFCFC',
        borderTopRightRadius: 16,
        borderTopLeftRadius: 16
      },
      wrapper: {
        backgroundColor: 'rgba(51, 51, 51, 0.48)',
      },
      draggableIcon: {
        width: 134,
        backgroundColor: '#E4E4E4',
      },
    }}>

    <View>
      <View style={[styles.currentCard, darkMode ? {} : {}]}>
        <Image
          style={styles.image}
          source={photo ? {uri: photo} : logoImage}
        />

        <View style={{ marginLeft: 30 }}>
          <Text style={{color: darkMode ? 'white' : 'black'}}>Плейлист</Text>

          <View style={styles.titleWrapper}>
            <Text style={[styles.title, {color: darkMode ? 'white' : 'black'}]}>{name || 'Название альюлма'}</Text>

            {/* <Text style={styles.time}>31:25</Text> */}
          </View>

          <Text style={styles.extra}></Text>

          <Ripple>
            <Text style={[styles.downloadBtn, {color: palette[darkMode ? 'dark' : 'light'].default}]}>Скачать</Text>
          </Ripple>
        </View>
      </View>

      <Ripple onPress={sharePlaylist}>
        <View style={styles.actions}>
          <ShareIcon style={styles.actionIcon} />

          <Text style={[styles.actionText, {color: darkMode ? 'white' : 'black'}]}>Поделиться</Text>
        </View>
      </Ripple>

      <Ripple onPress={() => onDeletePlaylist(id)}>
        <View style={styles.actions}>
          <RemoveIcon style={styles.actionIcon} />

          <Text style={[styles.actionText, {color: darkMode ? 'white' : 'black'}]}>Удалить</Text>
        </View>
      </Ripple>
    </View>
  </RBSheet>
  )
});

export default CurrentPlayList

const styles = StyleSheet.create({
  downloadBtn: {
    borderRadius: 13,
    paddingVertical: 4,
    paddingHorizontal: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: COLORS.purple,
    overflow: 'hidden',
  },
  currentCard: {
    marginTop: 26,
    flexDirection: 'row',
    marginBottom: 15,
    paddingHorizontal: 15,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 8,
    marginRight: 30,
  },
  actions: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 16,
    paddingHorizontal: 15,
  },
  titleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  time: {
    fontSize: 14,
    marginTop: 10,
    right: '-200%',
    color: palette.common.titleGrey,
    position: 'relative',
  },
  actionIcon: {
    width: 50,
    marginRight: 10,
  },
  actionText: {
    marginLeft: 10,
    fontSize: 18,
  },
  title: {
    fontSize: 14,
    marginTop: 10,
    fontWeight: 'bold',
  },
  extra: {
    fontSize: 14,
    marginTop: 3,
    color: '#8D8D8D',
    // marginBottom: 12,
  },
});


