import React, { useState, useEffect, useRef, useCallback } from 'react';
import {
    StyleSheet,
    SafeAreaView,
    View,
    Dimensions,
    Text,
    ScrollView,
    ActivityIndicator,
    Share
} from 'react-native';
import {connect} from 'react-redux';
import {withNavigation} from 'react-navigation';

import {palette} from '../../../state/palette';
import CurrentPlayList from './CurrentPlayList.component';
import SearchBlock from '../../../components/SearchBlock/SearchBlock';
import CustomHeader from '../../../components/CustomHeader/CustomHeader';
import { Container, MusicCard } from '../../../components/';
import AddPlaylistModal from "../../../components/MusicItem/AddPlaylistModal";
import { createPlayList, deletePlayList, getPlayLists } from "../../../actions";
// import {instance} from '../../../api/api';
import AddNewPlayList from "../../../components/AddNewPlayList/AddNewPlayList";
import { getStatusBarHeight } from "react-native-status-bar-height";
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";
import { SHARE_URL } from '../../../constants/api_config';
import { MINI_PLAYER_HEIGHT } from '../../../constants/styles';

const screenWidth = Dimensions.get('window').width;

const PlayListScreen = props => {
    const { error, createPlayList, playLists, navigation, isLoading, getPlayLists, isPlayListsEmpty, deletePlayList, darkMode } = props;

    const [modalVisible, onModalVisible] = useState(false);
    const [list, setList] = useState(playLists);
    const [currentCard, changeCurrentCard] = useState({});
    const [searchValue, setSearchValue] = useState('');
    const refRBSheet = useRef(null);
    const params = navigation.state.params;

    useEffect(() => {
        getPlayLists();

        return () => setList([]);
    }, []);

    useEffect(() => {
        setList(playLists);
    }, [playLists]);

    useEffect(() => {
        if (searchValue.trim()) {
            const result = playLists.filter(item =>
                item.name.toLowerCase().includes(searchValue.toLowerCase()),
            );

            setList(result);
        } else setList(playLists);
    }, [searchValue]);

    // useEffect(() => {
    //     if (!modalVisible) getPlayLists();
    // }, [modalVisible, changeCurrentCard]);

    const openPlaylist = card => {
        changeCurrentCard(card);
        refRBSheet.current.open();
    }
       
    const handleDeletePlaylist = id => {
        refRBSheet.current.close();
        deletePlayList(id);
    };

    const handleCreatePlayList = playListName => {
        createPlayList(playListName);
        onModalVisible(false);
    };

    const heightAddNewPlayListItem = ( (screenWidth / (screenWidth >= 600 ? 3.6 : 2.5)));
    const widthAddNewPlayListItem = ( (screenWidth / (screenWidth >= 600 ? 2.8 : 2.2)) - 10 );

    const createNewPlayListItem = (
        <AddNewPlayList
            newStyles={{
                minHeight: 155,
                height: heightAddNewPlayListItem,
                width: widthAddNewPlayListItem,
            }}

            darkMode={darkMode}
            setAddPlaylistModalVisible={() => onModalVisible(true)}
        />
    );

    const generateContent = () => {
        return !isLoading && playLists.length ?

        (
            <View style={styles.playlist}>
                <>
                    { createNewPlayListItem }

                    {
                        list.map((item, index) => {
                            // console.log('Play List Screen item: ', item);

                            return (
                                <MusicCard
                                    onPress={() => onOpenPlayList(item)}
                                    onLongPress={() => openPlaylist(item)}
                                    key={index + item.name}
                                    resizeItem={true}
                                    name={item.name}
                                    image={item.photo}
                                    extra={item.extra}
                                />
                            )
                        })
                    }

                </>
            </View>
        ) : isLoading && !error ?

        (
            <View style={styles.empty}>
                <ActivityIndicator />
            </View>
        ) : isPlayListsEmpty ?

        (
            <View style={styles.playlist}>
                { createNewPlayListItem }
            </View>
        ) :

        (
            <View style={styles.empty}>
                <Text style={{
                    width: '100%',
                    textAlign: 'center',
                    color: !darkMode ? '#000' : '#fff',
                    marginTop: 50,
                }}>Ошибка при получении данных</Text>
            </View>
        );
    };

    const onOpenPlayList = item => {
        // console.log('onOpenPlayList item: ', item);

        navigation.navigate('PlayListPage', {item, isPlayListPage: true});
    };

    // console.log(`playLists before start render:`, playLists);

    const sharePlaylist = async () => {
        try {
            Share.share({ message: SHARE_URL + `playlists?playlist_id=${currentCard.id}` })
        } catch (e) {
            console.log('Error Sharing Playlist: ', e);
        }
    };

    return (
        <SafeAreaView
            style={[styles.container, { backgroundColor: palette[props.darkMode ? 'dark' : 'light'].container, paddingTop: getStatusBarHeight(), paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT : 0 }]}>

            <MainScreenContainer darkMode={darkMode}>
                <AddPlaylistModal
                    modalVisible={modalVisible}
                    closeModal={() => onModalVisible(false)}
                    darkMode={props.darkMode}
                    onCreatePlaylist={handleCreatePlayList}
                />

                <ScrollView style={{flex: 1, height: '100%', backgroundColor: palette[props.darkMode ? 'dark' : 'light'].container}}>
                    <CustomHeader
                        title="Музыка"
                        pressGoBack={() => navigation.navigate('MyMusicTabScreen')}
                        darkMode={props.darkMode}
                        isPlayLists={params && params.isPlayLists}
                    />

                    <Container theme={props.darkMode ? 'dark' : 'light'} style={{flex: 1, height: '100%'}}>
                        <SearchBlock value={searchValue} onChange={setSearchValue} darkMode={props.darkMode} />

                        { generateContent() }

                        <CurrentPlayList
                            onDeletePlaylist={handleDeletePlaylist}
                            ref={refRBSheet}
                            currentCard={currentCard}
                            darkMode={props.darkMode}
                            sharePlaylist={sharePlaylist}
                        />

                        {/*<AudioPlayer ref={audioPlayerRef} onMenuPress={openPlayerMenu} />*/}
                    </Container>
                </ScrollView>
            </MainScreenContainer>
        </SafeAreaView>
    );
};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,
    error: state.playListReduce.error,
    playLists: state.playListReduce.playLists,
    isLoading: state.playListReduce.loading,
    isPlayListsEmpty: state.playListReduce.isPlayListsEmpty,
    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
    getPlayLists,
    createPlayList,
    deletePlayList,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(
    withNavigation(PlayListScreen)
);

// const {height} = Dimensions.get('screen');

const styles = StyleSheet.create({
    searchInputWrapper: {
        flexGrow: 1,
        marginLeft: 12,
    },
    btnIcon: {
        marginHorizontal: 10,
    },
    playlist: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        marginBottom: 75,
    },
    btnText: {
        color: '#fff',
        fontSize: 13,
    },
    playButton: {
        width: '48%',
        paddingHorizontal: 25,
        paddingVertical: 6,
        borderRadius: 17,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: palette.common.secondary,
    },
    createButton: {
        width: '48%',
        paddingHorizontal: 15,
        paddingVertical: 6,
        borderRadius: 17,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: palette.common.primary,
    },
    container: {
        flex: 1,
        // minHeight: height,
        height: '100%',
    },
    actions: {
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    searchBlock: {
        paddingVertical: 24,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    // loader: {
    //     flex: 1,
    //     height: 240,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    // },
    empty: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
