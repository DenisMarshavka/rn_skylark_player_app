import React, {useCallback, useEffect, useState} from 'react';
import {
    StyleSheet,
    SafeAreaView,
    Text,
    View,
    ScrollView,
    StatusBar,
    TouchableOpacity,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';

import {COLORS} from '../../constants/colors.constants'
import { MINI_PLAYER_HEIGHT } from '../../constants/styles';
import CustomHeader from '../../components/CustomHeader/CustomHeader';
import SearchBlock from '../../components/SearchBlock/SearchBlock';
import MenuIcon from '../../icons/menu';
import PlusIcon from '../../icons/plus';
import PlayIcon from '../../icons/play';
import {palette} from '../../state/palette';
import {
    createPlayList
} from "../../actions";
import MusicListWithSheetsMenu from "../../components/MusicListWithSheetsMenu/MusicListWithSheetsMenu";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { getDownloadedTracks, cleanDownloadedPlaylist } from '../../actions/download';
import AddPlaylistModal from "../../components/MusicItem/AddPlaylistModal";
import {MainScreenContainer} from "../../components/MainScreenContainer/MainScreenContainer";

// const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const TracksScreenForDownloads = props => {
    const { createPlayList, getDownloadedTracks, downloadedPlaylist, tracksLoading } = props;

    const { singerName = false, withoutDelete = true, iconPlus = false, isDownloadedTracks = true } = (props.navigation.state && props.navigation.state.params ? props.navigation.state.params : {});

    const [searchValue, setSearchValue] = useState('');
    const [downloadedTracks, setDownloadedTracks] = useState([]);
    const [createPlayListModalVisible, setCreatePlayListModalVisible] = useState(false);


    useEffect(() => {
        getDownloadedTracks();

        return () => {
            props.cleanDownloadedPlaylist();
        };
    }, []);

    const handleOpenPlayer = ({ track }) => {
        const list = downloadedTracks;
        if (list.length > 0) {
            props.navigation.navigate('PlayerScreen', { track, playlist: list, withoutDelete, isDownloadedTracks });
        }
    };

    useEffect(() => {
        if (isDownloadedTracks) {
            const search = getSearchDownloadedTracks(searchValue);
            setDownloadedTracks(search);
        }
    }, [downloadedPlaylist]);

    const getSearchDownloadedTracks = (value) => {
        if (value.trim()) {
            return downloadedPlaylist.filter(item =>
                item.name.toLowerCase().includes(value.toLowerCase()),
            );
        } else return downloadedPlaylist;
    };

    const changeSearchValue = useCallback(value => {
            const search = getSearchDownloadedTracks(value);
            setDownloadedTracks(search);
            setSearchValue(value);
        },
        [setSearchValue, downloadedPlaylist],
    );

    const handleCreatePlayList = playListName => {
        createPlayList(playListName);
        setCreatePlayListModalVisible(false);
    };

    let darkMode = props.darkMode;

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: darkMode ? '#000' : '#F9F9F9', marginTop: getStatusBarHeight() }}>
            <StatusBar
               barStyle={darkMode ? 'light-content' : 'dark-content'}
               backgroundColor={darkMode ? '#000' : '#F9F9F9'}
            />

            <MainScreenContainer darkMode={darkMode}>
                <ScrollView
                    style={{backgroundColor: darkMode ? COLORS.black : COLORS.white}}
                    contentContainerStyle={{paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT : 0}}
                    showsVerticalScrollIndicator={false}
                >
                    <View style={{marginHorizontal: 15}}>
                        <CustomHeader
                            title="Музыка"
                            icon={<MenuIcon color={palette[props.darkMode ? 'dark' : 'light'].text}/>}
                            onIconPress={() => {}}
                            pressGoBack={() => props.navigation.goBack()}
                            darkMode={props.darkMode}
                        />

                       <SearchBlock darkMode={props.darkMode} value={searchValue} onChange={changeSearchValue} />

                        <View style={styles.actions}>
                            <TouchableOpacity style={[ styles.eventButtons, styles.playButton ]} onPress={handleOpenPlayer}>
                                <Text style={styles.btnText}>Слушать все</Text>

                                <PlayIcon style={styles.btnIcon}/>
                            </TouchableOpacity>

                            <TouchableOpacity style={[ styles.eventButtons, styles.createButton ]} onPress={() => setCreatePlayListModalVisible(true)}>
                                <Text style={styles.btnText}>Создать плейлист</Text>

                                <PlusIcon color="#fff" style={styles.btnIcon} height={16} width={16}/>
                            </TouchableOpacity>
                        </View>
                        {
                            !tracksLoading && !!downloadedTracks.length ?

                            (

                                <>
                                    <MusicListWithSheetsMenu
                                        singerName={singerName}
                                        data={ downloadedTracks}
                                        showsHorizontalScrollIndicator={false}
                                        darkMode={darkMode}
                                        navigation={props.navigation}
                                        flatListStyle={{ paddingTop: 20, flex: 1 }}
                                        contentContainerStyle={{ alignItems: 'center' }}
                                        icon={

                                                null }

                                        onOpenPlayer={handleOpenPlayer}
                                        withoutDelete={withoutDelete}
                                        isDownloadedTracks={isDownloadedTracks}
                                        withoutAddToPlayList={true}
                                        />
                                    </>
                                ) : tracksLoading ?

                            <View style={styles.loader}>
                                <ActivityIndicator/>
                            </View> : !tracksLoading && !downloadedTracks.length ?

                            <View style={styles.loader}>
                                <Text style={{
                                    width: '100%',
                                    textAlign: 'center',
                                    color: !darkMode ? '#000' : '#fff',
                                }}>Список треков пока пуст</Text>
                            </View> :

                            <View style={styles.loader}>
                                <Text style={{
                                    width: '100%',
                                    textAlign: 'center',
                                    color: !darkMode ? '#000' : '#fff',
                                }}>Ошибка получения данных</Text>
                            </View>
                        }

                        <AddPlaylistModal
                            modalVisible={createPlayListModalVisible}
                            closeModal={() => setCreatePlayListModalVisible(false)}
                            onCreatePlaylist={handleCreatePlayList}
                            darkMode={darkMode}
                        />
                    </View>
                </ScrollView>
            </MainScreenContainer>
        </SafeAreaView>
    );
};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,

    tracksLoading: state.downloadReducer.loading,
    downloadedPlaylist: state.downloadReducer.playlist,

    listOfLikedTracks: state.trackListReducer.listOfLikedTracks,

    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
    getDownloadedTracks,
    cleanDownloadedPlaylist,
    createPlayList,
};

export default connect(mapStateToProps, mapDispatchToProps)(TracksScreenForDownloads);


const styles = StyleSheet.create({
    btnIcon: {
        marginHorizontal: 10,
    },
    btnText: {
        color: '#fff',
        fontSize: 13,
    },
    eventButtons: {
        width: '48%',
        maxWidth: 250,
        paddingVertical: 6,
        borderRadius: 17,

        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    playButton: {
        paddingHorizontal: 25,
        backgroundColor: palette.common.secondary,
    },
    createButton: {
        paddingHorizontal: 15,
        backgroundColor: palette.common.primary,
    },
    actions: {
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
	 loader: {
    	flex: 1,
        height: screenHeight / 1.75,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
