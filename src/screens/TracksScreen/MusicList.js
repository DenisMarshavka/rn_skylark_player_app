import React from 'react';
import { View, FlatList, } from 'react-native';
import { COLORS } from '../../constants/colors.constants'
import SvgDownloadIcon from '../../icons/download';
import MusicItem from '../../components/MusicItem/MusicItem';

// const screenWidth = Dimensions.get('window').width;
// const screenHeight = Dimensions.get('window').height;

const MusicList = ({data, darkMode, navigation}) => {
    const handleOpenPlayer = ({ track }) => {
        navigation.navigate('PlayerScreen', { track, playlist: data });
    };

	return (
        <View style={{ marginBottom: 40 }}>
            <FlatList
                data={data}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => item.id.toString() + index}
                style={{ paddingTop: 20, flex: 1 }}
                contentContainerStyle={{alignItems: 'center'}}
                renderItem={({ item, ...props }) => (
                    <MusicItem
                        key={item.name + props.index}
                        musicData={item}
                        icon={<SvgDownloadIcon color={COLORS.purple} />}
                        darkMode={darkMode}
                        navigation={navigation}
                        onOpenPlayer={handleOpenPlayer}
                    />
                )
                }
            />
        </View>
    );
};

export default MusicList

// const styles = StyleSheet.create({
//
// });
