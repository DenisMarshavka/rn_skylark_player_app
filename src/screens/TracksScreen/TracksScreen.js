import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    SafeAreaView,
    Text,
    View,
    ScrollView,
    StatusBar,
    TouchableOpacity,
    Dimensions,
    ActivityIndicator,
    Alert,
} from 'react-native';
import { connect } from 'react-redux';
import axios from "axios";

import { COLORS } from '../../constants/colors.constants'
import { MINI_PLAYER_HEIGHT } from '../../constants/styles';
import CustomHeader from '../../components/CustomHeader/CustomHeader';
import SearchBlock from '../../components/SearchBlock/SearchBlock';
import MenuIcon from '../../icons/menu';
import PlusIcon from '../../icons/plus';
import PlayIcon from '../../icons/play';
import { palette } from '../../state/palette';
import {
    getAllTracksList,
    getPopularTracksListBySinger,
    getTrackListByUserToken,
    cleanTrackList,
    getSearchListOfLikedTracks,
    getListOfLikedTracks,
    createPlayList,
    getSearchList,
    getWeekPopularTrackList,
    getMonthPopularTrackList,
    getGenreTrackList,
    getSearchGenreTrackList,
    cleanSearchMusicListsData,
    getSearchPopularTracksData,
} from "../../actions";
import SvgDownloadIcon from "../../icons/download";
import SvgPlusIcon from "../../icons/plus";
import MusicListWithSheetsMenu from "../../components/MusicListWithSheetsMenu/MusicListWithSheetsMenu";
import { getStatusBarHeight } from "react-native-status-bar-height";
// import MusicList from './MusicList';
import DownloadModal from '../../components/Modals/DownloadModal';
import AddPlaylistModal from "../../components/MusicItem/AddPlaylistModal";
import { mainStyles } from "../../utils/styles";
import { MainScreenContainer } from "../../components/MainScreenContainer/MainScreenContainer";
import { API_URL } from '../../constants/api_config';

// const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const TracksScreen = props => {
    const {
        weekListLoading = false,
        weekList = [],
        errorWeekList = false,

        monthListLoading = false,
        monthList = [],
        errorMonthList = false,

        createPlayList,
        getTrackListByUserToken,
        trackListIsEmpty,
        trackList = [],
        isLoadingTrackList = false,
        requestTrackListFailed = false,
        getPopularTracksListBySinger,
        getAllTracksList,
        downloadPendingList,
        getListOfLikedTracks,
        getGenreTrackList,
        getSearchGenreTrackList,

        getWeekPopularTrackList,
        getMonthPopularTrackList,

        getSearchPopularTracksData,
    } = props;

    const {
        isPlyListTracks = false,
        isSingerPopularTracksList = false,
        isAllUserTracksList = false,
        isAllTracksList = false,
        singerName = false,
        withoutDelete = true,
        iconPlus = false,
        isDownloadedTracks = false,
        is_I_Like_It = false,
        isPopularTracksWeek = false,
        isPopularTracksMonth = false,

        approveToTrackDownload = false,

        genreId,
        isGenre = false
    } = (props.navigation.state && props.navigation.state.params ? props.navigation.state.params : {});

    const [contentList, setContentList] = useState(isPopularTracksMonth ? monthList : isPopularTracksWeek ? weekList : trackList)
    const contentListIsEmpty = (contentList && !contentList.length) || trackListIsEmpty;

    const [loading, setLoading] = useState((isPopularTracksWeek && weekListLoading) || (isPopularTracksMonth && monthListLoading) || isLoadingTrackList)
    const requestFailed = (isPopularTracksWeek && errorWeekList) || (isPopularTracksMonth && errorMonthList) || requestTrackListFailed;

    // useEffect(()=>{
    //     if (isAllTracksList){
    //         setContentList(trackList)
    //     }
    // }, [trackList])

    const [searchValue, setSearchValue] = useState('');
    const [createPlayListModalVisible, setCreatePlayListModalVisible] = useState(false);
    const [downloadModalVisible, setDownloadModalVisible] = useState(false);
    const [musicData, setMusicData] = useState(null);
    const [systemPage, setSystemPage] = useState(1);
    const [nextPage, setNextPage] = useState(false);

    const handleSearchValue = value => setSearchValue(value);

    const handleFetchSearch = () => {
        if (is_I_Like_It) {
            if (searchValue.trim()) {
                props.getSearchListOfLikedTracks(searchValue.toLowerCase())
            }
        }
        // searching for genres
        // else if (isGenre) {
        //     getSearchGenreTrackList(genreId, searchValue.toLowerCase())
        // } 
        else if (!isAllTracksList) {
            if (searchValue.trim()) {
                props.getSearchList('user_songs', searchValue.toLowerCase());
            } else getTrackListByUserToken();
        } else onTracksListDataGenerate();
    };

    const onTracksListDataGenerate = () => {
        if (!isSingerPopularTracksList) {
            if (is_I_Like_It) {
                getListOfLikedTracks()
            } else if (isGenre) {
                // getGenreTrackList(genreId, 1, 10)
                getSystemTracks()
            } else if (isAllTracksList) {
                !searchValue.trim() 
                ? getAllReleasesTracks() //getAllTracksList() 
                : getSearchPopularTracksData(searchValue);
            } else if (isPopularTracksWeek) {
                getWeekPopularTrackList();
            } else if (isPopularTracksMonth) {
                getMonthPopularTrackList();
            } else getTrackListByUserToken();
        } else getPopularTracksListBySinger()
    };

    useEffect(() => {
        onTracksListDataGenerate();

        return () => {
            cleanTrackList();
        };
    }, []);

    const handleOpenPlayer = ({ track, onPlayListsMenuOpen, onTrackDelete, onMoreMenuOpen, setMusicData }) => {
        const list = trackList;

        // if (list.length) {
            props.navigation.navigate('PlayerScreen', { track, playlist: list, withoutDelete, isDownloadedTracks, setMusicData, isAllUserTracksList, onPlayListsMenuOpen, onTrackDelete, onMoreMenuOpen });
        // }
    };

    const onDownload = (track) => {
        if (!props.downloading) {
            setMusicData(track);
            setDownloadModalVisible(true);
        } else {
            Alert.alert(
                'Информация',
                'Можно скачать только один файл одновременно!',
                [
                    {
                        text: 'Хорошо',
                        style: 'success',
                    },
                ],
                { cancelable: true }
            );
        }
    };

    let darkMode = props.darkMode;

    const handleCreatePlayList = playListName => {
        createPlayList(playListName);
        setCreatePlayListModalVisible(false);
    };

    const getSystemTracks = async (page = 1, perPage = 20) => {
        try {
            if (contentList.length < 1) setLoading(true)

            let tracks = await axios.get(`${API_URL}user/playlists/${genreId}/tracklist?page=${page}&perPage=${perPage}`)

            if (tracks.data.status === 200) {

                setContentList([...contentList, ...tracks.data.data.tracklist.data])

                if (tracks.data.data.tracklist.next_page_url) {
                    console.log('next page true')
                    setNextPage(true)
                    setSystemPage(tracks.data.data.tracklist.current_page + 1)
                } else {
                    setNextPage(false)
                    console.log('next page false')
                }
                setLoading(false)
            } else {
                setContentList([])
                setLoading(false)
            }
        } catch (e) {
            console.log('err', e)
            setLoading(false)
        }
    }

    const getAllReleasesTracks = async (page = 1) => {
        try {
            if (contentList.length < 1) setLoading(true)

            let tracks = await axios.get(`${API_URL}tracklist?page=${page}`).then( res => res.data )

            if (tracks.data) {

                console.log('tracks.data', tracks.data)

                setContentList([...contentList, ...tracks.data])

                if (tracks.next_page_url) {
                    console.log('next page true')
                    setNextPage(true)
                    setSystemPage(+tracks.current_page + 1)
                } else {
                    setNextPage(false)
                    console.log('next page false')
                }
                setLoading(false)
            } else {
                setContentList([])
                setLoading(false)
            }
        } catch (e) {
            console.log('err', e)
            setLoading(false)
        }
    }

    const getNextSystemPage = () => {
        console.log('more...')

        if (nextPage) {
            isAllTracksList ? getAllReleasesTracks(systemPage) : getSystemTracks(systemPage)
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: darkMode ? '#000' : '#F9F9F9', marginTop: getStatusBarHeight() }}>
            <MainScreenContainer darkMode={darkMode}>
                <StatusBar
                    barStyle={darkMode ? 'light-content' : 'dark-content'}
                    backgroundColor={darkMode ? '#000' : '#F9F9F9'}
                />

                <View style={{ marginHorizontal: 15 }}>
                    <CustomHeader
                        title="Музыка"
                        icon={<MenuIcon color={palette[props.darkMode ? 'dark' : 'light'].text} />}
                        onIconPress={() => { }}
                        pressGoBack={() => props.navigation.goBack()}
                        darkMode={props.darkMode}
                    />
                </View>


                <ScrollView
                    style={{ backgroundColor: darkMode ? COLORS.black : COLORS.white }}
                    contentContainerStyle={{ paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT : 0 }}
                    showsVerticalScrollIndicator={false}
                >
                    <View style={{ marginHorizontal: 15 }}>

                        {isGenre ? null : <SearchBlock value={searchValue} onSearchStart={handleFetchSearch} onChange={handleSearchValue} darkMode={props.darkMode} />}

                        <View style={[styles.actions, isGenre ? { marginTop: 20 } : {}]}>
                            <TouchableOpacity style={[styles.eventButtons, styles.playButton]} onPress={handleOpenPlayer}>
                                <Text style={styles.btnText}>Слушать все</Text>

                                <PlayIcon style={styles.btnIcon} />
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.eventButtons, styles.createButton]} onPress={() => setCreatePlayListModalVisible(true)}>
                                <Text style={styles.btnText}>Создать плейлист</Text>

                                <PlusIcon color="#fff" style={styles.btnIcon} height={16} width={16} />
                            </TouchableOpacity>
                        </View>
                        {
                            !loading && !requestFailed && !contentListIsEmpty ?

                                (
                                    <>
                                        <MusicListWithSheetsMenu
                                            singerName={singerName}
                                            data={contentList}
                                            showsHorizontalScrollIndicator={false}
                                            darkMode={darkMode}
                                            navigation={props.navigation}
                                            flatListStyle={{ paddingTop: 20, flex: 1, paddingBottom: 75 }}
                                            contentContainerStyle={{ alignItems: 'center' }}
                                            icon={
                                                !iconPlus ?
                                                    !isDownloadedTracks ? <SvgDownloadIcon color={COLORS.purple} /> :
                                                        null :
                                                    <SvgPlusIcon width={18} height={18} color={COLORS.purple} />
                                            }
                                            onOpenPlayer={handleOpenPlayer}
                                            withoutDelete={withoutDelete}
                                            withDownload={approveToTrackDownload}
                                            onDownload={onDownload}
                                            approvePlayListTrackDelete={!isSingerPopularTracksList && !isAllTracksList}
                                            approveTracksListTrackDelete={!isSingerPopularTracksList && !isAllTracksList}
                                            isFooter
                                            loadMoreSystemTracks={isGenre || isAllTracksList ? getNextSystemPage : null}
                                        />
                                    </>
                                ) : loading ?

                                    <View style={styles.loader}>
                                        <ActivityIndicator />
                                    </View> : !requestFailed && !loading && contentListIsEmpty ?

                                        <View style={styles.loader}>
                                            <Text style={{
                                                width: '100%',
                                                textAlign: 'center',
                                                color: !darkMode ? '#000' : '#fff',
                                            }}>Список треков пока пуст</Text>
                                        </View> :

                                        <View style={styles.loader}>
                                            <Text style={{
                                                width: '100%',
                                                textAlign: 'center',
                                                color: !darkMode ? '#000' : '#fff',
                                            }}>Ошибка получения данных</Text>
                                        </View>
                        }

                        <DownloadModal
                            modalVisible={downloadModalVisible}
                            closeModal={() => setDownloadModalVisible(false)}
                            track={musicData}
                            darkMode={darkMode}
                            navigation={props.navigation}
                        />

                        <AddPlaylistModal
                            modalVisible={createPlayListModalVisible}
                            closeModal={() => setCreatePlayListModalVisible(false)}
                            onCreatePlaylist={handleCreatePlayList}
                            darkMode={darkMode}
                        />
                    </View>
                </ScrollView>
            </MainScreenContainer>
        </SafeAreaView>
    );
};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,
    highQuality: state.appReducer.highQuality,
    trackList: state.trackListReducer.trackList,
    trackListIsEmpty: state.trackListReducer.trackListIsEmpty,
    isLoadingTrackList: state.trackListReducer.loading,

    downloadedPlaylist: state.downloadReducer.playlist,
    downloadPendingList: state.downloadReducer.pendingList,
    downloading: state.downloadReducer.downloading,

    requestTrackListFailed: state.trackListReducer.error,

    listOfLikedTracks: state.trackListReducer.listOfLikedTracks,

    weekListLoading: state.searchMusicReducer.weekListLoading,
    weekList: state.searchMusicReducer.weekList,
    errorWeekList: state.searchMusicReducer.errorWeekList,

    monthListLoading: state.searchMusicReducer.monthListLoading,
    monthList: state.searchMusicReducer.monthList,
    errorMonthList: state.searchMusicReducer.errorMonthList,

    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
    getTrackListByUserToken,
    getPopularTracksListBySinger,
    cleanTrackList,
    getAllTracksList,
    getSearchList,

    getSearchListOfLikedTracks,
    getListOfLikedTracks,
    createPlayList,

    getWeekPopularTrackList,
    getMonthPopularTrackList,
    getGenreTrackList,
    getSearchGenreTrackList,

    getSearchPopularTracksData,
};

export default connect(mapStateToProps, mapDispatchToProps)(TracksScreen);


const styles = StyleSheet.create({
    btnIcon: {
        marginHorizontal: 10,
    },
    btnText: {
        color: '#fff',
        fontSize: 13,
    },
    eventButtons: {
        width: '48%',
        maxWidth: 250,
        paddingVertical: 6,
        borderRadius: 17,

        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    playButton: {
        paddingHorizontal: 25,
        backgroundColor: palette.common.secondary,
    },
    createButton: {
        paddingHorizontal: 15,
        backgroundColor: palette.common.primary,
    },
    actions: {
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    loader: {
        flex: 1,
        height: screenHeight / 1.75,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
