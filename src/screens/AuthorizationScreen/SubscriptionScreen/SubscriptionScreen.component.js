import React from 'react'
import {StyleSheet, TouchableOpacity, Text, View, StatusBar, ImageBackground, SafeAreaView} from 'react-native'
import { COLORS } from '../../../constants/colors.constants';
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";
import {connect} from "react-redux";

const SubscriptionScreen = props => {

	const goToApp = () => {
		props.navigation.navigate('SearchMusicTabNavigator')
	}

	return (
		<MainScreenContainer darkMode={props.darkMode}>
			<StatusBar translucent backgroundColor="transparent" />
				<ImageBackground
					source={require('../../../assets/edward-cisneros-KoKAXLKJwhk-unsplash.png')}
					style={{ flex: 1 }}
				>
					<View style={styles.contentWrapper}>

						<View style={styles.titleWrapper}>
							<Text style={styles.titleText}>Слушайте музыку без{'\n'} ограничений!</Text>

						</View>

						<View>
							<Text style={styles.subtitleText}>Подключите премиум возможности</Text>
						</View>

						<View style={styles.listWrapper}>
							<Text style={styles.listItem}>• Скачивайте музыку</Text>
							<Text style={styles.listItem}>• Слушайте без ограничений</Text>
							<Text style={styles.listItem}>• Собирайте свои плейлисты</Text>
						</View>

						<View style={styles.downBlock}>
							<Text style={styles.textAboveBtn}>Дарим 7 дней бесплатного пользования{'\n'} при первом подключении</Text>

							<TouchableOpacity style={styles.btnWrapper} onPress={goToApp}>
								<Text style={styles.btnText}>Войти</Text>
							</TouchableOpacity>

							<Text style={styles.textUnderBtn}>
								Оформляя подписку, я подтверждаю, что{'\n'} ознакомился(-ась) и согласен(-на) с условиями{'\n'}
								<Text style={styles.privacyPolicyLink}>Пользовательского соглашения.</Text>
							</Text>
						</View>

					</View>
				</ImageBackground>
		</MainScreenContainer>
	)
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
})

export default connect(mapStateToProps)(SubscriptionScreen)

const styles = StyleSheet.create({
	contentWrapper: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: 'rgba(0, 0, 0, 0.7)'
	},
	titleWrapper: {
		flex: 1,
		paddingHorizontal: 20,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleText: {
		marginTop: 30,
		fontSize: 26,
		color: COLORS.white,
		textAlign: 'center'
	},
	subtitleText: {
		fontSize: 18,
		color: COLORS.white,
		textAlign: 'center',
		fontWeight: 'bold'
	},

	listWrapper: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	listItem: {
		fontSize: 20,
		color: COLORS.white,
		lineHeight: 48
	},

	downBlock: {
		flex: 1,
		width: '100%',
		alignItems: 'center',
		justifyContent: 'space-evenly'
	},
	textAboveBtn: {
		fontSize: 14,
		color: COLORS.white,
		textAlign: 'center'
	},
	btnWrapper: {
		width: '90%',
		height: 60,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 8,
		backgroundColor: COLORS.white,
	},
	btnText: {
		fontSize: 18
	},
	textUnderBtn: {
		fontSize: 13,
		color: COLORS.white,
		opacity: 0.5,
		textAlign: 'center'
	},
	privacyPolicyLink: {
		textDecorationLine: 'underline'
	}

})
