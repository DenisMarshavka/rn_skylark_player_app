export const validateName = name => {
    return name.length > 1 ? true : false
}

export const validateEmail = mail => {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) ? true : false
}

export const validatePassword = (password) => {
    return password.length >= 6 ? true : false
}

export const validatePhoneNumber = (number) => {
    return number.length >= 10 && number.length <= 15 ? true : false
}

export const validateConfirmPassword = (password, confirmPassword) => {
    return password.length >= 1 && password === confirmPassword ? true : false
}