import React, { useState } from 'react'
import {StyleSheet, Text, View, TextInput, SafeAreaView} from 'react-native'
import { COLORS } from '../../../constants/colors.constants';
import AuthBlank from '../../../components/AuthBlank/AuthBlank'
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";
import {connect} from "react-redux";

const SMSScreen = props => {

	let [code, setCode] = useState('')

	const changeCode = text => {
		setCode(text)
	}

	return (
		<MainScreenContainer darkMode={props.darkMode}>
			<AuthBlank
				title='Введите код из СМС'
				buttonText='Готово'
				enableScroll={false}
				onPressBtn={() => props.navigation.navigate('Login')}
			>
				<View style={styles.contentWrapper}>
					<View style={styles.textInputWrapper}>
						<TextInput
							style={styles.textInput}
							onChangeText={changeCode}
							value={code}
							placeholder='__ __ __ __'
						/>

					</View>
				</View>
			</AuthBlank>
		</MainScreenContainer>
	);
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
})

export default connect(mapStateToProps)(SMSScreen)

const styles = StyleSheet.create({
	contentWrapper: {
		width: '100%',
		marginTop: 20
	},
	textInputWrapper: {
		paddingHorizontal: 50,
	},
	textInput: {
		width: '100%',
		height: 40,
		borderColor: COLORS.grey,
		borderWidth: 1,
		borderRadius: 50,
		fontSize: 16,
		paddingHorizontal: 14,
		marginTop: 50,
		textAlign: 'center',
		marginBottom: 240
	}

});
