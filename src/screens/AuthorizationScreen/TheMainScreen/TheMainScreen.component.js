import React, {useEffect, useState} from 'react'
import {StyleSheet, TouchableOpacity, Text, View, ActivityIndicator, SafeAreaView, Platform} from 'react-native'
import AuthBackground from '../../../components/AuthBackground/AuthBackground.component'
import Logo from '../../../components/Logo/Logo.component'
import { COLORS } from '../../../constants/colors.constants';
import { connect } from 'react-redux';
import { isUserSignIn } from '../../../store/authReducer'
import { getSettings } from '../../../store/appReducer'
import { getPlayerSettings } from '../../../actions/player'
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";

const TheMainScreen = props => {

	let [isFetching, setIsFetching] = useState(false)

	useEffect(() => {
		setIsFetching(true)

		props.isUserSignIn()
			.then(() => setIsFetching(false))

		props.getSettings()
		props.getPlayerSettings()
	}, [])

	useEffect(() => {
		if (props.isSignIn) {
			// if (props.isSubscribed){
				props.navigation.navigate('SearchMusicTabNavigator')
			// } else {
			// 	props.navigation.navigate('Subscription')
			// }
		}
	}, [props.isSignIn, props.isSubscribed])

	return (
		isFetching ?
			<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
				<ActivityIndicator size="large" color="black" />
			</View> :

			<MainScreenContainer darkMode={props.darkMode}>
				<AuthBackground>
					<Logo 
						style={{ paddingTop: 40 }} 
						size={{width: 170, height: 170}} 
						backgroundColor='rgba(255, 255, 255, 0.6)'
					/>

					<View style={styles.titleWrapper}>
						<Text style={styles.titleText}>Создайте аккаунт и слушайте{'\n'} вашу любимую музыку!</Text>
						<Text style={styles.subtitleText}>Дарим 7 дней бесплатного пользования{'\n'} при первом подключении</Text>
					</View>

					<View style={styles.btnContainer}>
						<TouchableOpacity
							style={[styles.btnWrapper, styles.loginBtn]}
							onPress={() => props.navigation.navigate('Login')}
						>
							<Text style={styles.btnText}>
								Войти
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={[styles.btnWrapper, styles.registrationBtn]}
							onPress={() => props.navigation.navigate('Registration')}
						>
							<Text style={styles.btnText}>
								Зарегистрироваться
							</Text>
						</TouchableOpacity>
					</View>
				</AuthBackground>
			</MainScreenContainer>
	)
};

const mapStateToProps = state => ({
	isSubscribed: state.appReducer.isSubscribed,
	darkMode: state.appReducer.darkMode,
	isSignIn: state.authReducer.isSignIn
})

const mapDispatchToProps = { isUserSignIn, getSettings, getPlayerSettings }

export default connect(mapStateToProps, mapDispatchToProps)(TheMainScreen);

const styles = StyleSheet.create({
	titleWrapper: {
		flex: 1,
		paddingHorizontal: 20,
		justifyContent: 'center',
		alignItems: 'center'
	},
	titleText: {
		fontSize: 22,
		fontWeight: 'bold',
		color: COLORS.white,
		textAlign: 'center'
	},
	subtitleText: {
		fontSize: 14,
		color: COLORS.white,
		textAlign: 'center',
		marginTop: 20
	},
	btnContainer: {
		flex: 1,
		width: '100%',
		alignItems: 'center',
	},
	btnWrapper: {
		width: '90%',
		maxWidth:  400,
		height: 60,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 8
	},
	btnText: {
		fontSize: 18
	},
	loginBtn: {
		backgroundColor: COLORS.white,
	},
	registrationBtn: {
		marginTop: 16,
		backgroundColor: COLORS.yellow
	}

});

