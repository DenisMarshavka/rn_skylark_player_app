import React, { useState, useRef, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, ActivityIndicator, TouchableOpacity, Dimensions } from 'react-native'
import Modal from 'react-native-modal';

import {COLORS} from '../../../constants/colors.constants';
import AuthBlank from '../../../components/AuthBlank/AuthBlank'
import {connect} from 'react-redux';
import { validatePassword, validatePhoneNumber, validateName, validateEmail, validateConfirmPassword } from '../validation';
import {registration} from '../../../store/authReducer'
import TextInputMask from 'react-native-text-input-mask';
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const RegistrationScreen = props => {
	let [name, setName] = useState('')
	let [phoneNumber, setPhoneNumber] = useState('')
	let [email, setEmail] = useState('')
	let [password, setPassword] = useState('')
	let [confirmPassword, setConfirmPassword] = useState('')
	let [confirmPrivacy, setConfirmPrivacy] = useState(false)

	let ref = useRef()

	let [error, setError] = useState('');
	let [loading, setLoading] = useState(false);
	let [modalIsVisible, setModalIsVisible] = useState(false);

	useEffect(() => {
		const emailError = props.errorMessage.trim() === "Email Exists";
		const phoneError = props.errorMessage.trim() === "Phone Exists";

		const currentError = emailError || phoneError ? `Пользователь с таким ${phoneError ? 'номером телефона' : 'Email`лом'}${"\n"} уже существует` : props.errorMessage;

		setError(currentError);
		return () => {
			setError('');
		}
	}, [props.errorMessage, loading]);

	const checkFormErrorExist = () => {
		if (error.trim()) setError('');
	};

	const changeName = text => {
		checkFormErrorExist();
		setName(text)
	};

	const changePhone = text => {
		checkFormErrorExist();
		setPhoneNumber(text)
	};

	const changeEmail = text => {
		checkFormErrorExist();
		setEmail(text)
	};

	const changePassword = text => {
		checkFormErrorExist();
		setPassword(text)
	};

	const changeConfirmPassword = text => {
		checkFormErrorExist();
		setConfirmPassword(text)
	};

	const handleStatusErrorSet = (errorMessage = '') => {
		setError(errorMessage);
		setLoading(false);
	};

	const registration = async () => {
		setError('');
		setLoading(true);

		if (!confirmPrivacy){
			handleStatusErrorSet(`Пользовательское соглашение не принято!`)
		} else if (!validateName(name.trim())){
			handleStatusErrorSet(`Неверная длина имени!`)
		} else if (!validatePhoneNumber(phoneNumber.trim())){
			handleStatusErrorSet(`Неверная длина номера телефона!`)
		} else if (!validateEmail(email.trim())) {
			handleStatusErrorSet(`Неверный email!`)
		} else if (!validatePassword(password.trim())) {
			handleStatusErrorSet(`Длина пароля должна быть не менее 6 символов!`)
		}else if (!validateConfirmPassword(password.trim(), confirmPassword.trim())){
			handleStatusErrorSet(`Пароли не совпадают!`)
		} else {
			let response = await props.registration(name.trim(), phoneNumber.trim(), email.trim(), password.trim());

			if (response) {
				setModalIsVisible(true)
			}
			setLoading(false);
		}
	};

	const goToLogin = () => {
		setModalIsVisible(false)
		props.navigation.navigate('Login')
	}

	return (
		<>
		<MainScreenContainer darkMode={props.darkMode}>
			<AuthBlank
				title='Регистрация'
				buttonText='Далее'
				enableScroll={true}
				onPressBtn={registration}
				hasError={error.trim() || loading}
			>
				{
					!loading ?

					<View style={styles.contentWrapper}>
						<View style={styles.textInputWrapper}>
							<TextInput
								style={styles.textInput}
								onChangeText={changeName}
								value={name}
								placeholder='Имя'
							/>

							<TextInputMask
								value={phoneNumber}
								style={styles.textInput}
								onChangeText={(formatted, extracted) => {
									changePhone(extracted)
								}}
								mask={phoneNumber.startsWith(7) ? "+[0] ([000]) [000] [00] [00]" : "+[00] ([000]) [000] [00] [00]"}
								placeholder='+7 (___) ___ __ __'
							/>

							<TextInput
								style={styles.textInput}
								onChangeText={changeEmail}
								value={email}
								placeholder='Почта'
								autoCapitalize='none'
							/>

							<TextInput
								style={styles.textInput}
								onChangeText={changePassword}
								value={password}
								placeholder='Придумайте пароль'
								secureTextEntry={true}
							/>

							<TextInput
								style={styles.textInput}
								onChangeText={changeConfirmPassword}
								value={confirmPassword}
								placeholder='Повторите пароль'
								secureTextEntry={true}
							/>
						</View>

						{ error.trim() ? <Text style={[ styles.errorText, {bottom: error.trim().length >= 30 ? 78 : 84 } ]}>{error}</Text> : null }

						<View style={styles.privacyBlock}>
							<TouchableOpacity 
								style={styles.checkbox} 
								onPress={() => setConfirmPrivacy(!confirmPrivacy)}
							>
								{confirmPrivacy && <View style={styles.point}></View>}
							</TouchableOpacity>
							<TouchableOpacity style={styles.confirmWrapper} onPress={() => props.navigation.navigate('PrivacyPolicy')}>
								<Text style={styles.confirmText}>
									Я подтверждаю, что ознакомился(-ась) и согласен(-на) с условиями
									<Text style={{textDecorationLine: 'underline'}}>{'\n'}Пользовательского соглашения.</Text>
								</Text>
							</TouchableOpacity>
						</View>
					</View>	:

					<View style={styles.fillFull}>
						<ActivityIndicator />
					</View>
				}
			</AuthBlank>
		</MainScreenContainer>

		<Modal isVisible={modalIsVisible} style={styles.modalContainer} >
			<View style={styles.modal}>
				<Text style={{ fontSize: 20, textAlign: 'center', color: 'green' }}>Регистрация{'\n'}прошла успешно!</Text>
				<TouchableOpacity onPress={goToLogin} style={styles.confirmBtn} >
					<Text style={{ color: 'green', fontSize: 16 }}>Продолжить</Text>
				</TouchableOpacity>
			</View>
		</Modal>
		</>
	);
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
	errorMessage: state.authReducer.errorMessage
});

const mapDispatchToProps = { registration };

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationScreen);

const styles = StyleSheet.create({
	fillFull: {
		flex: 1,
		minHeight: '50%',
		alignItems: 'center',
		justifyContent: 'center',
	},
	contentWrapper: {
		width: '100%',
		marginTop: 20,
		position: 'relative'
	},
	textInputWrapper: {
		paddingHorizontal: 50,
	},
	textInput: {
		width: '100%',
		height: 40,
		borderColor: COLORS.grey,
		borderWidth: 1,
		borderRadius: 50,
		fontSize: 16,
		paddingHorizontal: 14,
		marginTop: 10
	},
	confirmWrapper: {
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 20
	},
	confirmText: {
		fontSize: 13
	},

	errorText: {
		color: 'red',
		position: 'absolute',
		width: '100%',
		textAlign: 'center',
		fontSize: 12,
	},

	modalContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	modal: {
		borderRadius: 20,
		backgroundColor: 'white',
		width: screenWidth * 0.8,
		height: screenWidth * 0.6,

		justifyContent: 'space-evenly',
		alignItems: 'center'
	},
	confirmBtn: {
		borderWidth: 1,
		borderColor: 'green',
		paddingVertical: 10,
		paddingHorizontal: 20,
		borderRadius: 20,
	},
	privacyBlock: {
		marginVertical: 30,
		marginHorizontal: 20,
		flexDirection: 'row',
		alignItems: 'center',
	},
	checkbox:{
		width: 24,
		height: 24,
		borderWidth: 2,
		borderColor: COLORS.purple,
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center'
	},
	point: {
		width: 14,
		height: 14,
		borderRadius: 50,
		backgroundColor: COLORS.purple,
	}
});

