import React from 'react'
import Logo from '../../../components/Logo/Logo.component'
import AuthBackground from '../../../components/AuthBackground/AuthBackground.component'
import { View, TouchableOpacity, Dimensions } from 'react-native';

const width = Dimensions.get('window').width

const Splash = props => {
	return (
		<AuthBackground>
			<TouchableOpacity onPress={()=> props.navigation.navigate('TheMainScreen')}>
				<Logo 
					size={{ width: width / 2, height: width / 2 }} 
					backgroundColor='rgba(0, 0, 0, 0.2)'
				/>
			</TouchableOpacity>
		</AuthBackground>
	);
};

export default Splash
