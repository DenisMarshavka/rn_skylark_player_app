import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, SafeAreaView, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux';
import { login } from '../../../store/authReducer'
import { COLORS } from '../../../constants/colors.constants';
import AuthBlank from '../../../components/AuthBlank/AuthBlank'
import { validatePassword, validatePhoneNumber, validateEmail } from '../validation';
import { MainScreenContainer } from "../../../components/MainScreenContainer/MainScreenContainer";
import Logo from '../../../components/Logo/Logo.component';
import { NavigationActions, StackActions } from 'react-navigation'

const Login = props => {

	let [phone, setPhone] = useState('')
	let [password, setPassword] = useState('')
	let [error, setError] = useState('');

	let [loading, setLoading] = useState(false);

	useEffect(() => () => { setError('') }, []);

	useEffect(()=> {
		if (props.errorLoginMessage){
			setLoading(false);
		}
	}, [props.errorLoginMessage])

	const changeEmailOrPhone = text => {
		setPhone(text)
	}

	const changePassword = text => {
		setPassword(text)
	}

	const login = async () => {
		setError('')
		setLoading(true);

		try {
			if (validateEmail(phone)) {
				if (validatePassword(password)) {
					let response = await props.login(phone, password, 'isEmail')
					if (response) {
						props.isSubscribed ? goToApp() : goToSubscribtion()
						setLoading(false);
					}
				} else {
					setError(`Длина пароля должна быть не менее 6 символов!`)
				}
			} else if (validatePhoneNumber(phone)) {
				if (validatePassword(password)) {
					let response = await props.login(phone, password)
					if (response) {
						props.isSubscribed ? goToApp() : goToSubscribtion()
						setLoading(false);
					}
				} else {
					setError(`Длина пароля должна быть не менее 6 символов!`)
				}
			} else {
				setError(`Неверный email или номер телефона`)
			}
		} catch (e) {
			console.log(e)
			setLoading(false);
		}
	}

	const goToApp = () => {
		// const resetAction = StackActions.reset({
		// 	index: 0,
		// 	actions: [
		// 	  NavigationActions.navigate({ routeName: 'Profile' })
		// 	]
		// });

		// props.navigation.dispatch(resetAction);

		setLoading(false);
		props.navigation.navigate('SearchMusicTabNavigator')
	}

	const goToSubscribtion = () => {
		setLoading(false);
		props.navigation.navigate('Subscription')
	}

	return (
		<MainScreenContainer darkMode={props.darkMode}>
			<AuthBlank
				enableScroll={true}
				title='Вход'
				buttonText='Войти'
				enableScroll={true}
				onPressBtn={login}
				loading={loading}
			>
				{/* {!loading ? */}
					{/* <> */}
						<Logo
							size={{ width: 100, height: 100 }}
							backgroundColor='rgba(0, 0, 0, 0.2)'
						/>

						<View style={styles.contentWrapper}>
							<View style={styles.textInputWrapper}>
								<TextInput
									style={styles.textInput}
									onChangeText={changeEmailOrPhone}
									value={phone}
									placeholder='Почта или телефон'
									autoCapitalize='none'
								/>
								<TextInput
									style={styles.textInput}
									onChangeText={changePassword}
									value={password}
									placeholder='Пароль'
									secureTextEntry={true}
								/>
							</View>

							{(error !== '' || props.errorLoginMessage !== '') && <Text style={styles.errorText}>{error || props.errorLoginMessage}</Text>}

							<TouchableOpacity onPress={() => props.navigation.navigate('ResporePassword')}>
								<Text style={styles.forgotPassword}>Не помню пароль</Text>
							</TouchableOpacity>
						</View>
					{/* </> */}
				{/* :
				 	<View style={styles.fillFull}>
				 		<ActivityIndicator color='black' size='small' />
				 	</View>
				 } */}

			</AuthBlank>
		</MainScreenContainer>
	);
};

const mapStateToProps = state => ({
	isSubscribed: state.appReducer.isSubscribed,
	darkMode: state.appReducer.darkMode,
	errorLoginMessage: state.authReducer.errorLoginMessage,
})

const mapDispatchToProps = { login }

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
	contentWrapper: {
		width: '100%',
		marginTop: 20,
		position: 'relative'
	},
	logo: {
		width: 100,
		height: 100,
		borderRadius: 100,
		backgroundColor: 'rgba(94, 94, 94, 0.15)',
		alignItems: 'center',
		justifyContent: 'center',
		marginVertical: 20
	},
	logoText: {
		color: COLORS.darkgrey,
		opacity: 0.4,
		fontSize: 25,
		textTransform: 'uppercase'
	},

	textInputWrapper: {
		paddingHorizontal: 50,
	},
	textInput: {
		width: '100%',
		height: 40,
		borderColor: COLORS.grey,
		borderWidth: 1,
		borderRadius: 50,
		fontSize: 16,
		paddingHorizontal: 14,
		marginBottom: 10
	},

	forgotPassword: {
		textDecorationLine: 'underline',
		textAlign: 'center',
		fontSize: 16,
		marginTop: 30,
		marginBottom: '20%'
	},

	errorText: {
		color: 'red',
		position: 'absolute',
		top: 100,
		width: '100%',
		textAlign: 'center',
		fontSize: 12
	},

	fillFull: {
		flex: 1,
		width: '100%',
		height: 350,
		alignItems: 'center',
		justifyContent: 'center',
		
		borderColor: 'black',
		borderWidth: 1
	},

});
