import React, { useState } from 'react'
import { StyleSheet, TouchableOpacity, Text, View, TextInput, StatusBar, SafeAreaView } from 'react-native'
import CustomHeader from '../../../components/CustomHeader/CustomHeader'
import { COLORS } from '../../../constants/colors.constants'
import { validateEmail } from '../validation'
import { instance } from '../../../api/api'
import { MainScreenContainer } from "../../../components/MainScreenContainer/MainScreenContainer";
import { getStatusBarHeight } from "react-native-status-bar-height";

const RestorePasswordScreen = props => {

	let [email, setEmail] = useState('')
	let [error, setError] = useState('')

	const changeEmailOrPhone = text => {
		setEmail(text)
	}

	const pressDone = async () => {
		setError('')

		try{
			if (validateEmail(email)) {
				let data = await instance.post(`password/forgot?email=` + email)

				if (data.status === 200) {
					props.navigation.navigate('EnterCode', { email })
				} else {
					setError('Что-то пошло не так :(')
				}
			} else {
				setError('Неверный email!')
			}
		} catch (e) {
			setError('Что-то пошло не так :(')
			console.log('err', e)
		}
	}

	return (
		<MainScreenContainer darkMode={false}>
			<StatusBar barStyle={'dark-content'} backgroundColor={'#F9F9F9'} />
			<View style={{ flex: 1, paddingHorizontal: 20, paddingTop: getStatusBarHeight() + 10 }}>
				<CustomHeader
					title='Вход'
					pressGoBack={() => props.navigation.goBack()}
				/>

				<View style={{
					flex: 1,
					alignItems: 'center',
					maxWidth: 500,
					maxHeight: '65%',
					margin: 0,
					marginLeft: 'auto',
					marginRight: 'auto',
				}}>
					<View style={styles.container}>
						<Text style={styles.text}>Укажите почту, которую Вы использовали {"\n"} при регистрации.</Text>

						<TextInput
							style={[styles.textInput, { borderColor: error ? 'red' : COLORS.grey }]}
							onChangeText={changeEmailOrPhone}
							value={email}
							placeholder='Почта'
							autoCapitalize='none'
						/>
						{
							error !== '' && (
								<Text
									style={[styles.text, { color: 'red', marginTop: 20, textAlign: 'center' }]}>{error}</Text>
							)
						}

					</View>

					<View style={styles.btnContainer}>
						<TouchableOpacity style={styles.btnWrapper} onPress={pressDone}>
							<Text style={styles.btnText}>Готово</Text>
						</TouchableOpacity>
					</View>
					
				</View>
			</View>
		</MainScreenContainer>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 20,
		alignItems: 'center',
		marginTop: 40,
	},
	titleText: {
		fontSize: 30
	},
	textInput: {
		width: '100%',
		minWidth: '80%',
		height: 40,
		borderColor: COLORS.grey,
		borderWidth: 1,
		borderRadius: 50,
		fontSize: 16,
		paddingHorizontal: 14,
		marginTop: 20,
	},

	btnContainer: {
		alignItems: 'center',
		// marginBottom: 20
	},
	btnWrapper: {
		width: 173,
		height: 50,
		backgroundColor: COLORS.purple,
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center',
	},
	btnText: {
		color: COLORS.white,
		fontSize: 18
	},
	text: {
		// alignSelf: 'flex-start',
		textAlign: 'left'
	}

});

export default RestorePasswordScreen
