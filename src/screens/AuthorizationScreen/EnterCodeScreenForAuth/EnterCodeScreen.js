import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Dimensions, SafeAreaView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { COLORS } from '../../../constants/colors.constants';
import NavigationBlank from '../../../components/NaviagtionBlank/NavigationBlank';
import { validateConfirmPassword, validatePassword } from '../validation';
import ArrowLeftIcon from '../../../icons/arrowLeft';
import { instance } from '../../../api/api';
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";

const EnterCodeScreen = ({ navigation, darkMode }) => {

	let [newPassword, setNewPassword] = useState('');
	let [confirmPassword, setConfirmPassword] = useState('');
	let [code, setCode] = useState('');
	let [error, setError] = useState('');

	const changeNewPassword = text => {
		setNewPassword(text)
	}

	const changeConfirmPassword = text => {
		setConfirmPassword(text)
	}

	const changeCode = text => {
		setCode(text)
	}

	const pressDone = async () => {
		setError('')

		if (!validateConfirmPassword(newPassword, confirmPassword)) {
			setError('Пароли не совпадают!')
		} else if (code && validateConfirmPassword(newPassword, confirmPassword)) {
			try {
				let email = navigation.state.params.email

				let status = await instance.post(`password/reset?email=${email}&code=${code}&password=${newPassword}&password_confirm=${confirmPassword}`)

				if (status.data.status === 200) {
					navigation.navigate('Login')
				} else {
					setError('Вы ввели неверный код!')
				}
			} catch (e) {
				console.log('error send code to email for restore password')
			}
		} else {
			setError('Пароль должен быть не менее 6 символов!')
		}
	}


	return (
        <SafeAreaView style={[styles.blankContainer, { backgroundColor: COLORS.white, }]}>
            <MainScreenContainer>
                <TouchableOpacity style={styles.header} onPress={() => navigation.goBack()}>
                  <ArrowLeftIcon style={styles.icon} color={COLORS.black} />
                  <Text style={[styles.titleText, { color: COLORS.black }]}>Вход</Text>
                </TouchableOpacity>

                <View style={styles.contentWrapper}>
                  <View style={styles.textInputWrapper}>
                    <Text style={[styles.label, { color: COLORS.black }]}>Введите код и ваш новый пароль</Text>

                    <TextInput
                      style={styles.textInput}
                      onChangeText={changeCode}
                      value={code}
                      placeholder="Код из почты"
                      textContentType='password'
                      secureTextEntry={true}
                    />

                    <TextInput
                      style={[styles.textInput]}
                      onChangeText={changeNewPassword}
                      value={newPassword}
                      placeholder="Новый пароль"
                      textContentType='password'
                      secureTextEntry={true}
                    />

                    <TextInput
                      style={[styles.textInput]}
                      onChangeText={changeConfirmPassword}
                      value={confirmPassword}
                      placeholder="Повторите пароль"
                      textContentType='password'
                      secureTextEntry={true}
                    />

                    {error ?
                      <Text style={[{ color: 'red', marginTop: 20, textAlign: 'center' }]}>{error}</Text>
                      : null}

                  </View>

                  <View style={styles.contentStyle}>
                  <TouchableOpacity style={styles.btnWrapper} onPress={pressDone}>
                    <Text style={styles.btnText}>Готово</Text>
                  </TouchableOpacity>
                </View>
            </View>
          </MainScreenContainer>
        </SafeAreaView>
    )
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(EnterCodeScreen);

const { height } = Dimensions.get('screen');
const styles = StyleSheet.create({
	blankContainer: {
		flex: 1,
		width: '100%',
	},
	header: {
		marginTop: 20,
		paddingHorizontal: 15,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-start',
	},
	icon: {
		marginRight: 20,
	},
	titleText: {
		fontSize: 22,
		fontWeight: 'bold',
		marginVertical: 25,
		textAlign: 'left',
	},

	contentWrapper: {
		width: '100%',
		marginTop: 20,
		flex: 1,
		justifyContent: 'space-between'
	},
	label: {
		fontSize: 14,
		color: COLORS.black,
		textAlign: 'center'
	},
	textInputWrapper: {
		paddingHorizontal: 50,
	},
	textInput: {
		width: '100%',
		height: 40,
		borderColor: COLORS.grey,
		borderWidth: 1,
		borderRadius: 50,
		fontSize: 16,
		paddingHorizontal: 14,
		marginTop: 20,
		color: 'black'
	},
	subtext: {
		marginTop: 20,
		marginBottom: height / 2 - 20,
		textAlign: 'center',
		color: COLORS.grey
	},


	contentStyle: {
		width: '100%',
		alignItems: 'center',
	},
	btnWrapper: {
		backgroundColor: COLORS.purple,
		width: 173,
		height: 50,
		borderRadius: 100,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 40,
	},
	btnText: {
		color: COLORS.white,
		fontSize: 18,
	},
});
