import {createStackNavigator} from 'react-navigation-stack';

// import Splash from './Spash/Splash';
import TheMainScreen from './TheMainScreen/TheMainScreen.component';
import Registration from './RegistrationScreen/RegistrationScreen.component';
import PrivacyPolicy from './PrivacyPolicyScreen/PrivacyPolicyScreen.component';
// import SMS from './SMSScreen/SMSScreen.component';
import Subscription from './SubscriptionScreen/SubscriptionScreen.component';
import Login from './LoginScreen/LoginScreen.component';
import ResporePassword from './RestorePasswordScreen/RestorePasswordScreen.component';

import EnterCodeScreen from './EnterCodeScreenForAuth/EnterCodeScreen';

const AuthorizationNavigator = createStackNavigator(
  {
    
    // Splash: {
    //   screen: Splash,
    // },
    TheMainScreen: {
      screen: TheMainScreen,
    },
    Registration: {
      screen: Registration,
    },
    PrivacyPolicy: {
      screen: PrivacyPolicy,
    },
    // SMS: {
    //   screen: SMS,
    // },
    Subscription: {
      screen: Subscription,
    },
    Login: {
      screen: Login,
    },
    ResporePassword: {
      screen: ResporePassword,
    },
    EnterCode: {
      screen: EnterCodeScreen,
    },
  },
  {
    headerMode: 'none',
  },
);

export default AuthorizationNavigator;
