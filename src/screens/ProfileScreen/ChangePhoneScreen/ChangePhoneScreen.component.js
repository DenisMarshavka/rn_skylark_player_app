import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { COLORS } from '../../../constants/colors.constants';
import NavigationBlank from '../../../components/NaviagtionBlank/NavigationBlank';
import { validateEmail } from '../../AuthorizationScreen/validation'
import { instance } from '../../../api/api';
import {toggleBottomBar} from "../../../store/appReducer";
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";

const ChangePhoneScreen = ({ navigation, darkMode, toggleBottomBar }) => {
	let [email, setEmail] = useState('')
	let [error, setError] = useState('')

	useEffect(() => {
		toggleBottomBar(false);

		return () => {
			toggleBottomBar(true);
		};
	}, []);


	const changePhone = text => {
		setEmail(text)
	}

	const pressDone = async () => {
		setError('')
		if (validateEmail(email)) {
			let data = await instance.get(`phone/change?email=` + email)

			if (data.status === 200) {
				navigation.navigate('EnterCodeScreen', { email })
			} else {
				setError('Что-то пошло не так :(')
			}
		} else {
			setError('Неправильный email!')
		}
	}

	return (
        <MainScreenContainer darkMode={darkMode}>
            <NavigationBlank
            goBack={() => navigation.goBack()}
            title="Профиль"
            buttonText="Готово"
            enableScroll={false}
            onPressBtn={pressDone}
            darkMode={darkMode}
          >
            <View style={styles.contentWrapper}>
              <View style={styles.textInputWrapper}>
                <Text style={[styles.label, { color: darkMode ? COLORS.white : COLORS.black }]}>Введите Ваш email</Text>
                <TextInput
                  style={[styles.textInput, { borderColor: error ? 'red' : COLORS.grey }]}
                  onChangeText={changePhone}
                  value={email}
                  placeholder="example@mail.com"
                  textContentType='emailAddress'
                  autoCapitalize='none'
                />
                <Text style={[styles.subtext]}>На этот email придет код подтверждения</Text>{error !== '' && <Text style={[styles.subtext, {color: 'red'}]}>{error}</Text>}
              </View>
            </View>
          </NavigationBlank>
      </MainScreenContainer>
  );
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
});

const mapDispatchToProps = {
	toggleBottomBar,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePhoneScreen);

const { height } = Dimensions.get('screen');
const styles = StyleSheet.create({
	contentWrapper: {
		width: '100%',
		marginTop: 20,
	},
	label: {
		fontSize: 14,
		marginBottom: 20,
		color: COLORS.black,
		textAlign: 'center'
	},
	textInputWrapper: {
		paddingHorizontal: 50,
	},
	textInput: {
		width: '100%',
		height: 40,
		borderColor: COLORS.grey,
		borderWidth: 1,
		borderRadius: 50,
		fontSize: 16,
		paddingHorizontal: 14,
	},
	subtext: {
		marginTop: 20,
		marginBottom: height / 2 - 20,
		textAlign: 'center',
		color: COLORS.grey
	}
});
