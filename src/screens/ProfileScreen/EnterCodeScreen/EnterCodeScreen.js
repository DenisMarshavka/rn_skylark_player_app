import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Dimensions, SafeAreaView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { COLORS } from '../../../constants/colors.constants';
import NavigationBlank from '../../../components/NaviagtionBlank/NavigationBlank';
import { validatePhoneNumber } from '../../AuthorizationScreen/validation';
import TextInputMask from 'react-native-text-input-mask';
import ArrowLeftIcon from '../../../icons/arrowLeft';
import { getStatusBarHeight } from "react-native-status-bar-height";
import { instance } from '../../../api/api';

const EnterCodeScreen = ({ navigation, darkMode }) => {

	let [phoneNumber, setPhoneNumber] = useState('');
	let [code, setCode] = useState('');
	let [error, setError] = useState('');


	const changePhone = text => {
		setPhoneNumber(text)
	}

	const changeCode = text => {
		setCode(text)
	}

	const pressDone = async () => {
		setError('')
		if (code && validatePhoneNumber(phoneNumber)) {
			const email = navigation.state.params.email
			let data = await instance.get(`phone/reset?code=${code}&email=${email}&phone=${phoneNumber}`)
			console.log(data.data)
			if (data.data.status === 200) {
				navigation.navigate('ProfileScreen')
			} else if (data.data.message === 'That phone number already in use') {
				setError('Этот номер уже используется!')
			}else {
				setError('Что-то пошло не так :(')
			}
		} else {
			setError('Вы ввели неверные данные!')
		}
	}


	return (
		<SafeAreaView style={[styles.blankContainer, { backgroundColor: darkMode ? COLORS.black : COLORS.white, }]}>
			<TouchableOpacity style={styles.header} onPress={() => navigation.goBack()}>
				<ArrowLeftIcon style={styles.icon} color={darkMode ? COLORS.white : COLORS.black} />
				<Text style={[styles.titleText, { color: darkMode ? COLORS.white : COLORS.black }]}>Профиль</Text>
			</TouchableOpacity>

			<View style={styles.contentWrapper}>
				<View style={styles.textInputWrapper}>
					{/* <Text style={[styles.label, { color: darkMode ? COLORS.white : COLORS.black }]}>Введите код, который пришел{'\n'}на ваш email</Text> */}

					<TextInput
						style={styles.textInput}
						onChangeText={changeCode}
						value={code}
						placeholder="Введите код"
						textContentType='password'
						secureTextEntry={true}
					/>

					{/* <Text style={[styles.label, { color: darkMode ? COLORS.white : COLORS.black, marginTop: 20 }]}></Text> */}

					<TextInputMask
						value={phoneNumber}
						style={[styles.textInput, { marginTop: 20 }]}
						onChangeText={(formatted, extracted) => {
							changePhone(extracted)
						}}
						mask={phoneNumber.startsWith(7) ? "+[0] ([000]) [000] [00] [00]" : "+[00] ([000]) [000] [00] [00]"}
						placeholder='Ваш новый номер'
					/>

					{error ?
						<Text style={[{ color: 'red', marginTop: 20, textAlign: 'center' }]}>{error}</Text>
						: null}


				</View>

				<View style={styles.contentStyle}>
					<TouchableOpacity style={styles.btnWrapper} onPress={pressDone}>
						<Text style={styles.btnText}>Готово</Text>
					</TouchableOpacity>
				</View>
			</View>

		</SafeAreaView>
	);
};

const mapStateToProps = state => ({
	darkMode: state.appReducer.darkMode,
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(EnterCodeScreen);

const { height } = Dimensions.get('screen');
const styles = StyleSheet.create({

	blankContainer: {
		flex: 1,
		width: '100%',
	},
	header: {
		paddingHorizontal: 15,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'flex-start',
		marginTop: getStatusBarHeight()
	},
	icon: {
		marginRight: 20,
	},
	titleText: {
		fontSize: 22,
		fontWeight: 'bold',
		marginVertical: 25,
		textAlign: 'left',
	},


	contentWrapper: {
		width: '100%',
		marginTop: 20,
		flex: 1,
	},
	label: {
		fontSize: 14,
		marginBottom: 20,
		color: COLORS.black,
		// textAlign: 'center'
	},
	textInputWrapper: {
		paddingHorizontal: 50,
		flex: 1,
	},
	textInput: {
		width: '100%',
		height: 40,
		borderColor: COLORS.grey,
		borderWidth: 1,
		borderRadius: 50,
		fontSize: 16,
		paddingHorizontal: 14,
	},
	subtext: {
		marginTop: 20,
		marginBottom: height / 2 - 20,
		textAlign: 'center',
		color: COLORS.grey
	},


	contentStyle: {
		width: '100%',
		alignItems: 'center',
	},
	btnWrapper: {
		backgroundColor: COLORS.purple,
		width: 173,
		height: 50,
		borderRadius: 100,
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 40,
	},
	btnText: {
		color: COLORS.white,
		fontSize: 18,
	},
});
