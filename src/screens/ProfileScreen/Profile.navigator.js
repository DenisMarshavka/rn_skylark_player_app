import React from 'react'
import {createStackNavigator} from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import ProfileScreen from './ProfileScreen/ProfileScreen.component';
import ContactUsScreen from './ContactUsScreen/ContactUsScreen.component';
import ChangePhoneScreen from './ChangePhoneScreen/ChangePhoneScreen.component';
import EnterCodeScreen from './EnterCodeScreen/EnterCodeScreen';

class ProfileRootNavigator extends React.Component {
  constructor(props)  {
      super(props);
  }

  render() {
      return (
          <MainProfileNavigator screenProps={{ rootNavigation: this.props.navigation }} />
      )
  }
}

const ProfileNavigator = createStackNavigator(
  {
    ProfileScreen: {
      screen: ProfileScreen,
    },
    ChangePhoneScreen: {
      screen: ChangePhoneScreen,
    },
    ContactUsScreen: {
      screen: ContactUsScreen,
    },
    EnterCodeScreen: {
      screen: EnterCodeScreen
    }
  },
  {
    headerMode: 'none',
  },
);

const MainProfileNavigator = createAppContainer(ProfileNavigator);

export default ProfileRootNavigator