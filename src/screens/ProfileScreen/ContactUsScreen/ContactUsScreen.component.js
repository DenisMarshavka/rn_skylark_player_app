import React, { useState, useCallback, useEffect, useRef } from 'react';
import { StyleSheet, View, TextInput, ScrollView, Alert, Linking, Keyboard}  from 'react-native';
import {connect} from 'react-redux';
import qs from 'qs';

import {COLORS} from '../../../constants/colors.constants';
import NavigationBlank from '../../../components/NaviagtionBlank/NavigationBlank';
import {toggleBottomBar} from "../../../store/appReducer";
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";

const supportMail = 'support@skylarkmusic.online';

const ContactUsScreen = ({ navigation, darkMode, toggleBottomBar, userName }) => {
    let [message, setMessage] = useState('');

    const refTextInput = useRef(null);

    useEffect(() => {
    toggleBottomBar(false);

    return () => toggleBottomBar(true);
    }, []);

    const changePhone = useCallback(
        text => setMessage(text),
        [setMessage],
    );

    const finishSendMail = () => {
        setTimeout(() => {
            showAlert(
                'Информация',
                `${userName}, спасибо большое за ваше обращение!\nМы обязательно его рассмотрим в ближайшеё время.\n\nС уважением: Администрация приложения "Skylark"😉`,
                [
                    {
                        text: 'Не за что',
                        onPress: () => {},
                        style: 'success',
                    },
                ],
                {
                    cancelable: false
                }
            );
        }, 1500);
    };

    const handleSenderToSupport = () => {
        const isNotEmpty        = !!message.trim(),
            minLength         = 15,
            containsMinLength = message.trim().length >= minLength;

        try {
            if (isNotEmpty && containsMinLength) {
                sendMail(supportMail);
            } else {
                showAlert(
                    'Информация',
                    !isNotEmpty ?
                                'Сообжение не может быть пустым!\nЗаполните пожалуйста и повторите попытку' :
                                `Минимальная длинна сообщения - ${minLength} символов!`,
                    [
                        {
                            text: 'Хорошо',
                            onPress: () => refTextInput.current.focus(),
                            style: 'success',
                        },
                    ],
                    {
                        cancelable: false
                    }
                );
            }
        } catch (e) {
          console.log('Error send message to Support Mail: ', e);
        }
    };

    const sendMail = async (to, options = {}) => {
        const { cc, bcc } = options;

        let url = `mailto:${to}`;

        const canOpen = await Linking.canOpenURL(url);

        // Create email link query
        const query = qs.stringify({
            subject: 'Message from the application "Skylark"',
            body: message,
            cc: cc,
            bcc: bcc
        });

        if (query.length) url += `?${query}`;
        // console.log('Generated message to send: ', url);

        if (!canOpen) throw new Error('Provided URL can not be handled');

        showAlert(
            'Подтверждение',
            `Ваше сообщение: "${message}",\n всё верно?`,
            [
                {
                    text: 'Да',
                    onPress: () => {
                        Keyboard.dismiss();
                        setMessage('');

                        finishSendMail();
                        return Linking.openURL(url);
                    },
                    style: 'success',
                },
                {
                    text: 'Нет',
                    onPress: () => refTextInput.current.focus(),
                    style: 'cancel',
                },
            ],
            {
                cancelable: false
            }
        );
    };

    const showAlert = (title = '', message = '', buttons = [], options = {}) => {
        try {
            if (title.trim() && message.trim() && buttons.length) {
                Alert.alert(
                    title,
                    message,
                    [...buttons],
                    {cancelable: false, ...options}
                );
            }
        } catch (e) {
            console.log('Error showed a Alert: ', e)
        }
    };

    return (
        <MainScreenContainer darkMode={darkMode}>
            <NavigationBlank
              title="Профиль"
              goBack={() => navigation.goBack()}
              buttonText="Отправить"
              enableScroll={false}
              onPressBtn={handleSenderToSupport}
              darkMode={darkMode}
            >
                <View style={styles.contentWrapper}>
                    <ScrollView style={styles.textInputWrapper}>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={changePhone}
                            value={message}
                            ref={refTextInput}
                            multiline
                            numberOfLines={20}
                            placeholder="Какие сложности возникли у вас при работе с приложением, чего не хватает, кого из любимых исполнителей Вы не нашли?"
                            placeholderTextColor={darkMode ? COLORS.grey : COLORS.grey}
                            textAlignVertical='top'
                        />
                    </ScrollView>
                </View>
            </NavigationBlank>
        </MainScreenContainer>
    );
};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,
    userName: state.authReducer.userName,
});

const mapDispatchToProps = {
  toggleBottomBar,
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsScreen);

const styles = StyleSheet.create({
  contentWrapper: {
    width: '100%',
    marginTop: 20,
  },
  textInputWrapper: {
    paddingHorizontal: 15,
  },
  textInput: {
    width: '100%',
    height: 345,
    borderColor: COLORS.grey,
    borderWidth: 1,
    color: '#fff',
    borderRadius: 8,
    fontSize: 16,
    paddingHorizontal: 23,
    paddingVertical: 15,
    marginBottom: '10%',
  },
});
