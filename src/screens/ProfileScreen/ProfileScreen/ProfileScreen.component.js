import React, { useState, useEffect } from 'react';
import { StyleSheet, ScrollView, SafeAreaView, Text, View, ImageBackground, Switch, ActivityIndicator, Modal, StatusBar, TouchableOpacity, Linking } from 'react-native';
import { palette } from '../../../state/palette';
import { COLORS } from 'constants/colors.constants';
import { MINI_PLAYER_HEIGHT } from '../../../constants/styles';
import { NavigationItem, Container, Divider } from '../../../components';
import { connect } from 'react-redux';
import { toggleTheme, toggleOrderNew, toggleTracksAutosave, toggleInfinityPlaying, toggleSubscribe, toggleActiveSubscription } from '../../../store/appReducer';
import { getUserData } from "../../../actions";
import { removeDownloadedTracks } from '../../../actions/download';
import { removeToken, logout, isSignInAction } from '../../../store/authReducer';
import { NavigationActions, StackActions } from 'react-navigation'
import { destroyPlayer } from '../../../utils/player';
import ConfirmModal from '../../../components/Modals/ConfirmModal';
import { MainScreenContainer } from "../../../components/MainScreenContainer/MainScreenContainer";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { miniPlayerStop } from '../../../actions/miniPlayer';
import RNModal from 'react-native-modal';
import * as RNIap from 'react-native-iap';
import { SUBSCRIPTION_SKU } from '../../../constants/api_config';


const Block = ({ children, title, theme, isLastChild }) => (
  <View style={{ ...styles(theme).block, paddingBottom: isLastChild ? 90 : 0 }}>
    <Text style={styles(theme).title}>{title}</Text>
    {children}
  </View>
);

const ProfileScreen = props => {
  let { navigation, darkMode, toggleTheme, getUserData, loading, userName, userEmail, userPhone, isSubscribed, isActiveSubscription } = props

  const [theme, setTheme] = useState(darkMode ? 'dark' : 'light');
  let [logoutModalVisible, setLogoutModalVisible] = useState(false)
  let [confirmModalVisible, setConfirmModalVisible] = useState(false);

  useEffect(() => {
    getUserData();
  }, []);

  useEffect(() => {
    darkMode ? setTheme('dark') : setTheme('light');
  }, [darkMode]);


  const handleChangeSetting = key => {
    switch (key) {
      case 'darkMode': toggleTheme(!darkMode);
        break;
      case 'infinityPlaying': props.toggleInfinityPlaying(!props.infinityPlaying);
        break;
      case 'tracksAutosave': props.toggleTracksAutosave(!props.tracksAutosave);
        break;
      case 'orderNew': props.toggleOrderNew(!props.orderNew);
        break;
    }
  };

  const onConfirmCleanHistory = async () => {
    if (props.track && props.track.localPath) {
      props.miniPlayerStop();
      await destroyPlayer();
    }
    props.removeDownloadedTracks();
    setConfirmModalVisible(false);
  };

  const cleanHistory = () => {
    setConfirmModalVisible(true);
  };

  const logout = async () => {
    await removeToken();
    await props.isSignInAction(false);
    await destroyPlayer();

    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Authorization' })
      ]
    });

    props.screenProps.rootNavigation.dispatch(resetAction);
    props.logout()
  };

  useEffect(() => {
    try {
      RNIap.initConnection()
      .then(conn => {
        RNIap.getSubscriptions([SUBSCRIPTION_SKU]).then(res => {
          console.log('getSubscriptions', res)
        })
      });
    } catch (e) {
      console.log(e)
    }
  }, [])

  const requestSubscription = async () => {
    try {
      let status = await RNIap.requestSubscription(SUBSCRIPTION_SKU, false);
      console.log('requestSubscription', status)
      return status && status.autoRenewingAndroid ? true : false
    } catch (err) { 
      console.log(err) 
      return false
    }
  }

  const subscribe = async () => {
    let status = await requestSubscription()
    
    if (status) {
      props.toggleSubscribe(true)
      props.toggleActiveSubscription(true)
    }
  }

  const unsubscribe = () => {
    Linking.openURL('https://play.google.com/store/account/subscriptions?package=com.skylark&sku=com.skylark.subscription')
  }

  return (
    <SafeAreaView style={[styles(theme).container, { backgroundColor: darkMode ? '#111' : '#F9F9F9', paddingTop: getStatusBarHeight(), paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT : 0 }]}>
      <StatusBar translucent backgroundColor='transparent' />

      <MainScreenContainer darkMode={darkMode}>
        <ScrollView>
          <ImageBackground
            imageStyle={{ resizeMode: 'stretch' }}
            source={require('../../../assets/profile-bg.png')}
            style={styles(theme).background}>

            <View style={styles(theme).backgroundContent}>
              {
                loading || (!userName || !userEmail) ?

                  <View style={styles.loader}>
                    <ActivityIndicator />
                  </View> :

                  <View style={styles(theme).userInfo}>
                    <Text style={styles(theme).name}>{userName}</Text>

                    <Text style={styles(theme).email}>{userEmail}</Text>
                  </View>
              }
            </View>
          </ImageBackground>

          <Container theme={theme}>

            <View style={styles(theme).card}>
              {/* <Text style={styles(theme).tariff}>Ваш тариф</Text> */}

              <Text style={styles(theme).subscriptionName}>Подписка</Text>

              <Text style={styles(theme).subscriptionItem}>
                • Прослушивание без ограничений
              </Text>
              <Text style={styles(theme).subscriptionItem}>
                • Скачивание любимых треков
              </Text>
              {/* {isSubscribed &&
                <Text style={styles(theme).subscriptionExpiration}>
                  Следущиее списание произойдет 10.09.2020
                </Text>
              } */}
              <TouchableOpacity onPress={isActiveSubscription ? unsubscribe : subscribe} style={styles(theme).subscribeBtn}>
              {/* <TouchableOpacity onPress={subscribe} style={styles(theme).subscribeBtn}> */}
                <Text style={{ color: 'white', textAlign: 'center' }}>{isActiveSubscription ? 'Отписаться' : 'Подписаться'}</Text>
              </TouchableOpacity>

            </View>

          </Container>

          <View style={styles(theme).container}>
            <Block title="Интерфейс" theme={theme}>
              <View style={styles(theme).wrapper}>
                <Text style={styles(theme).subTitle}>Темная тема</Text>

                <Switch
                  trackColor={{ true: palette.common.secondary, false: palette.common.disableSwitchButton }}
                  thumbColor={"#fff"}
                  ios_backgroundColor={palette.common.disableSwitchButton}
                  onValueChange={() => handleChangeSetting('darkMode')}
                  value={darkMode}
                />
              </View>
              <Divider />
            </Block>

            <Block title="Музыка" theme={theme}>
              <View style={styles(theme).wrapper}>
                <Text style={styles(theme).subTitle}>Музыка продолжается</Text>

                <Switch
                  trackColor={{ true: palette.common.secondary, false: palette.common.disableSwitchButton }}
                  thumbColor={"#fff"}
                  ios_backgroundColor={palette.common.disableSwitchButton}
                  onValueChange={() => handleChangeSetting('infinityPlaying')}
                  value={props.infinityPlaying}
                />
              </View>
              <Divider />
            </Block>

            <Block title="Приложение" theme={theme}>
              <View style={styles(theme).wrapper}>
                <Text style={styles(theme).subTitle}>Новые треки в начало</Text>

                <Switch
                  trackColor={{ true: palette.common.secondary, false: palette.common.disableSwitchButton }}
                  thumbColor={"#fff"}
                  ios_backgroundColor={palette.common.disableSwitchButton}
                  onValueChange={() => handleChangeSetting('orderNew')}
                  value={props.orderNew}
                />
              </View>

              <Divider />
            </Block>

            <Block title="Память" theme={theme}>
              <NavigationItem title="Очистить загрузки" theme={theme} onPress={() => cleanHistory()} />

              <Divider />
            </Block>

            <Block title="О приложении" theme={theme}>
              <NavigationItem
                onPress={() => navigation.navigate('ContactUsScreen')}
                title="Связаться с техподдержкой"
                theme={theme}
              />

              <Divider />
            </Block>

            <Block title="Номер телефона" theme={theme}>
              <NavigationItem
                extra={userPhone}
                title="Изменить"
                onPress={() => navigation.navigate('ChangePhoneScreen')}
                theme={theme}
                isPhone={true}
              />
              <Divider />
            </Block>

            <Block title="Выход" theme={theme} isLastChild={true}>
              <NavigationItem
                extra=" "
                title="Выйти из аккаунта"
                onPress={() => setLogoutModalVisible(true)}
                theme={theme}
              />
            </Block>

          </View>
        </ScrollView>

        <RNModal
          animationType="fade"
          // transparent={true}
          visible={logoutModalVisible}
          onRequestClose={() => setLogoutModalVisible(false)}
          onBackdropPress={() => setLogoutModalVisible(false)}
          style={{ margin: 0, backgroundColor: 'rgba(0,0,0,0.8)', alignItems: 'center', justifyContent: 'center' }}
        >
          <View style={styles(theme).modalWindow}>
            <Text style={styles(theme).modalTitle}>Вы уверены, что хотите выйти?</Text>
            <View style={styles(theme).modalBtnsWrapper}>
              <TouchableOpacity onPress={() => setLogoutModalVisible(false)}>
                <Text style={[styles(theme).modalBtn,]} >Нет</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={logout}>
                <Text style={styles(theme).modalBtn} >Да</Text>
              </TouchableOpacity>
            </View>
          </View>
        </RNModal>

        <ConfirmModal
          modalVisible={confirmModalVisible}
          closeModal={() => setConfirmModalVisible(false)}
          info={'Вы действительно хотите очистить историю?'}
          buttonPositive={'Очистить'}
          buttonNeutral={'Отменить'}
          confirm={onConfirmCleanHistory}
          darkMode={darkMode}
        />
      </MainScreenContainer>
    </SafeAreaView>
  );
};

const mapStateToProps = state => ({
  darkMode: state.appReducer.darkMode,
  isSubscribed: state.appReducer.isSubscribed,
  isActiveSubscription: state.appReducer.isActiveSubscription,
  infinityPlaying: state.appReducer.infinityPlaying,
  tracksAutosave: state.appReducer.tracksAutosave,
  orderNew: state.appReducer.orderNew,
  userName: state.authReducer.userName,
  userEmail: state.authReducer.userEmail,
  userPhone: state.authReducer.userPhone,
  loading: state.authReducer.loading,
  track: state.playerReducer.track,
  isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
  toggleTheme,
  toggleOrderNew,
  toggleTracksAutosave,
  toggleInfinityPlaying,
  toggleSubscribe,
  toggleActiveSubscription,
  getUserData,
  logout,
  isSignInAction,
  removeDownloadedTracks,
  miniPlayerStop,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);


const styles = theme => StyleSheet.create({
  loader: {
    flex: 1,
    height: 240,
    alignItems: 'center',
    justifyContent: 'center',
  },
  background: {
    width: '100%',
    height: 210,
    backgroundColor: palette[theme].container,
  },
  card: {
    position: 'relative',
    padding: 20,
    marginTop: -30,
    backgroundColor: palette.common.primary,
    borderRadius: 10,
  },
  tariff: {
    fontSize: 12,
    textAlign: 'center',
    color: '#fff',
    marginBottom: 5,
  },
  subscriptionName: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 10,
    textAlign: 'center',
  },
  subscriptionItem: {
    fontSize: 15,
    color: '#fff',
    marginBottom: 5,
    textAlign: 'center',
  },
  subscriptionExpiration: {
    fontSize: 12,
    marginTop: 10,
    textAlign: 'center',
    color: 'rgba(255, 255, 255, 0.55);',
  },
  backgroundContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  userInfo: {
    marginTop: 40,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 22,
    textAlign: 'center',
    color: palette[theme].text,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 24,
    color: palette[theme].text,
    paddingLeft: 15,
  },
  email: {
    fontSize: 16,
    textAlign: 'center',
    color: palette[theme].grey,
  },
  container: {
    flex: 1,
    position: 'relative',
  },
  block: {
    paddingTop: 30,
    backgroundColor: palette[theme].container,
  },
  section: {
    backgroundColor: COLORS.white,
    padding: 0,
  },
  subTitle: {
    fontWeight: '400',
    fontSize: 18,
    lineHeight: 24,
    color: palette[theme].text,
  },
  wrapper: {
    paddingHorizontal: 15,
    paddingVertical: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },


  modalWindow: {
    // flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 999,
    padding: 40,
    width: '80%',
    height: '30%',
    borderRadius: 20
  },
  modalTitle: {
    fontSize: 20,
    color: COLORS.black,
    textAlign: 'center'
  },
  modalBtnsWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
    width: '70%'
  },
  modalBtn: {
    color: COLORS.black,
    fontSize: 20,
    padding: 10,
    // width: 50,
    textAlign: 'center',
  },

  subscribeBtn: {
    borderColor: 'white',
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
    width: '60%',
    alignSelf: 'center',
    marginTop: 10
  }
});
