import React, { useState, useEffect, useRef, useCallback } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, Image, Platform, Share, Alert } from 'react-native';
import { COLORS } from '../../constants/colors.constants'
import { palette } from 'state/palette';
import { connect } from 'react-redux';
import Slider from '@react-native-community/slider';
import RBSheet from 'react-native-raw-bottom-sheet';
import Modal from 'react-native-modal';

import MenuIcon from 'icons/menu';
import PlusIcon from 'icons/plus';
import RandomIcon from 'icons/random';
import RepeatIcon from 'icons/repeat';
import PlayBackIcon from 'icons/playBack';
import RadioPlayIcon from 'icons/radioPlay';
import PlayForwardIcon from 'icons/playForward';
import TrackPlayer, {
	STATE_NONE, // 0
    STATE_READY, // 2
    STATE_PLAYING, // 3
    STATE_PAUSED, // 2
    STATE_STOPPED, // 1
    STATE_BUFFERING, // 6
    STATE_CONNECTING, // 8
} from 'react-native-track-player';

import MusicCover from '../../components/MusicCover/MusicCover';
import {checkSingersMusicItem, secondsToMMSS} from '../../utils/functions';
import { toggleBottomBar } from "../../store/appReducer";
import { setTrackListening } from '../../actions';
import {
	playerInit,
	playerSetRepeat,
	playerSetShuffle,
	playerSetShuffleQueue,
	playerReset,
} from '../../actions/player';
import {
    miniPlayerStart,
	miniPlayerStop,
} from '../../actions/miniPlayer';
import { removeDownloadedTrack } from "../../actions/download";

import { destroyPlayer, mapToPlayerData, mapTrackToPlayerData, shuffle as shuffleTracks } from '../../utils/player';
import { getSingersName } from '../../utils/functions';

const { height } = Dimensions.get('screen');
// const { width } = Dimensions.get('screen');
// import { defaultImage } from '../../constants/default_images';
import MusicMenu from '../../components/MusicItem/MusicMenu';
import AddPlaylist from '../../components/MusicItem/AddTrackToPlayListMenu';
import DeleteModal from '../../components/MusicItem/DeleteModal';
import DownloadModal from '../../components/Modals/DownloadModal';
import AddTrackSuccessModal from '../../screens/SearchMusicTabScreen/AddTrackSuccessModal/AddTrackSuccessModal';

import {
	addTrackToPlayList,
	createPlayList,
	getAddTrackToUserTrackListByTrackId,
	resetTrackInUserTrackListIsAlready,
	setTrackItemLike,
	getTrackLikedCheck,
} from "../../actions";
import {MainScreenContainer} from "../../components/MainScreenContainer/MainScreenContainer";
import SubscriptionModal from '../../components/SubscriptionModal/SubscriptionModal';

const PlayerScreen = props => {
	const {
		playLists,
		navigation,
		darkMode,
        infinityPlaying,
        highQuality,
		toggleBottomBar,
		createPlayList,
		addTrackToPlayList,
		getAddTrackToUserTrackListByTrackId,
		playlist,
        duration,
        playerInitScreen,
        isRepeat,
        isShuffle,
        shuffleQueue,
        playerInit,
        playerReset,
        playerSetRepeat,
        playerSetShuffle,
        playerSetShuffleQueue,

		likedTrackLoading,
		getTrackLikedCheck,
		trackLikedTrackStatus,
		isLikedTrackStatusLoading,
		likedTrackError,
		setTrackItemLike,

        miniPlayerStart,
	} = props;
	

	let [progress, setProgress] = useState(0)
	let [playerState, setPlayerState] = useState(null)
	const [visibleAddTrackModalSuccess, setVisibleAddTrackModalSuccess] = useState(false);

	let onPlaybackStateSubscribe = useRef(null);
    let onPlayerStopSubscribe = useRef(null);
    let onPlayerErrorSubscribe = useRef(null);

    const { setMusicData = () => {}, fromSingersScreen = false, onMoreMenuOpen = () => {}, currentTrack, onPlayListsMenuOpen, onTrackDelete, withoutDelete = true, isDownloadedTracks = false, isPlayListPage = false, isAllUserTracksList = false } = navigation.state.params;
	const track = props.track || currentTrack || navigation.state.params && navigation.state.params.track

	const musicData = track
	const approveToAddTrack = (!isPlayListPage && !isAllUserTracksList);

	console.log('track', track)

	// console.log('approveToAddTrack: ', approveToAddTrack);
	// console.log('isPlayListPage: ', isPlayListPage, 'isAllUserTracksList: ', isAllUserTracksList);

    // console.log('PlayerScreen withoutDelete: ', withoutDelete);
	// console.log('Player Screen navigation state: ', navigation.state, 'onPlayListsMenuOpen: ', navigation.state.params.onPlayListsMenuOpen, 'onTrackDelete: ', onTrackDelete);

	useEffect(() => {
		const { track, playlist } = navigation.state.params;

    	if (playlist) {
    		const trackData = track || playlist[0];
            playerInit({ track: trackData, playlist: playlist });
			props.setTrackListening(trackData.id);
            miniPlayerStart({ track: trackData });
		}

		if (currentTrack) {
    		const trackData = currentTrack;
            playerInit({ track: trackData });
			props.setTrackListening(trackData.id);
            miniPlayerStart({ track: trackData });
		}

        onPlaybackStateSubscribe.current = TrackPlayer.addEventListener('playback-state', ({ state }) => {
            setPlayerState(state)
        });

        onPlayerStopSubscribe.current = TrackPlayer.addEventListener('remote-stop', () => {
            navigation.goBack();
        });

        onPlayerErrorSubscribe.current = TrackPlayer.addEventListener('playback-error', () => {
            navigation.goBack();
        });

        return () => {
            playerReset();

            if (onPlaybackStateSubscribe.current) {
                onPlaybackStateSubscribe.current.remove();
            }

            if (onPlayerStopSubscribe.current) {
                onPlayerStopSubscribe.current.remove();
            }

            if (onPlayerErrorSubscribe.current) {
                onPlayerErrorSubscribe.current.remove();
            }
        };
    }, []);

    const onTrackLikedCheck  = () => {
		if (musicData && musicData.id) getTrackLikedCheck(+musicData.id);
	};

	const changeTime = async percents => {
		let time = duration * (percents / 100);
		console.log(time);

		setProgress(time);
		TrackPlayer.seekTo(time)
	};

	const start = async () => {
		if (playerState === STATE_STOPPED) { // 1
            await TrackPlayer.pause();
			TrackPlayer.seekTo(0).then(() => TrackPlayer.play());
		} if (
            playerState === STATE_READY 	||
            playerState === STATE_PAUSED 	||
            playerState === STATE_CONNECTING
		) {
            await TrackPlayer.play()
		} else {
			await TrackPlayer.pause()
		}

	};


	useEffect(() => {
		if (playerInitScreen) {
            setupPlayer();
        }
	}, [playerInitScreen]);

    const addTracksToPlayer = async () => {
		let playData = mapToPlayerData([track], { isDownloadedTracks, isHighQuality: highQuality});
		console.log('track data -----------------------------', playData)

		await TrackPlayer.add(playData); // set single track

		if (isShuffle) {
			await createShuffleOrder();
		} else {
            await initOrder();
		}
    };

	const setupPlayer = async () => {
		await TrackPlayer.setupPlayer({
			waitForBuffer: true,
		});
		TrackPlayer.updateOptions({

			stopWithApp: true,

			capabilities: [
				TrackPlayer.CAPABILITY_PLAY,
				TrackPlayer.CAPABILITY_PAUSE,
                TrackPlayer.CAPABILITY_STOP,
                TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
				TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
			],

		});

        await addTracksToPlayer();

		let position = await TrackPlayer.getPosition()
		let buffered = await TrackPlayer.getBufferedPosition()
		// let duration = await TrackPlayer.getDuration()
		console.log('duration', duration);

		setProgress(position)
	};

    const onSkipTo = async (type) => {
    	// type: 'next' or 'previous'
        const event = type;

		const currentTrack = await TrackPlayer.getCurrentTrack();

		if (isRepeat) {
			TrackPlayer.skip(currentTrack);
		} else {
			const queue = await TrackPlayer.getQueue();
			const firstTrackId = queue[0].id;
			const lastTrackId = queue[queue.length - 1].id;

			if (currentTrack === lastTrackId && event === 'next' && infinityPlaying) {
				await TrackPlayer.skip(firstTrackId);
			} else if (currentTrack === firstTrackId && event === 'previous' && infinityPlaying) {
				await TrackPlayer.skip(lastTrackId);
			} else {
				if (event === 'next') {
					await TrackPlayer.skipToNext().catch((err) => {});
				} else {
					await TrackPlayer.skipToPrevious().catch((err) => {});
                }
			}
		}
    };

	const setRepeat = async () => {
		if (!isRepeat) {
			await TrackPlayer.removeUpcomingTracks();
		} else {
			const source = isShuffle ? shuffleQueue : playlist;

            const currentTrackIndexInPlaylist = source.findIndex(item => item.id === track.id);
            let nextTracks = source.slice(currentTrackIndexInPlaylist);
            nextTracks.shift();
            const nextPlayData = mapToPlayerData(nextTracks, { isDownloadedTracks, isHighQuality: highQuality});
            await TrackPlayer.add(nextPlayData);
		}
        playerSetRepeat(!isRepeat);
	};

	const cleanQueue = async () => {
        const currentTrackId = await TrackPlayer.getCurrentTrack();

        const queue = await TrackPlayer.getQueue();
        const removeQueue = queue.filter(item => item !== currentTrackId).map(item => item.id);
        await TrackPlayer.remove(removeQueue);
	};

	const createShuffleOrder = async () => {
        if (playlist && playlist.length > 1) {
        	let clonePlaylist = [...playlist];

            const currentTrackId = await TrackPlayer.getCurrentTrack();

            const currentTrackIndexInPlaylist = clonePlaylist.findIndex(item => item.id === +currentTrackId);
            clonePlaylist.splice(currentTrackIndexInPlaylist, 1);

            const order = shuffleTracks(clonePlaylist);

            playerSetShuffleQueue({ shuffleQueue: [track, ...order] });

            if (!isRepeat) {
                const playData = mapToPlayerData(order, { isDownloadedTracks, isHighQuality: highQuality});
                await TrackPlayer.add(playData);
			}
        }
	};

    const initOrder = async () => {
        if (playlist && playlist.length > 1) {
            const currentTrackIndexInPlaylist = playlist.findIndex(item => item.id === track.id);

            const prevTracks = playlist.slice(0, currentTrackIndexInPlaylist);
            const prevPlayData = mapToPlayerData(prevTracks, { isDownloadedTracks, isHighQuality: highQuality});
            await TrackPlayer.add(prevPlayData, track.id.toString()); // set prev tracks before current track

            if (!isRepeat) {
                let nextTracks = playlist.slice(currentTrackIndexInPlaylist);
                nextTracks.shift();
                const nextPlayData = mapToPlayerData(nextTracks, { isDownloadedTracks, isHighQuality: highQuality});
                await TrackPlayer.add(nextPlayData); // set next tracks after
            }
        }
    };

    const setShuffle = async () => {
    	if (!isShuffle) {
    		// to shuffle
            await cleanQueue();
            await createShuffleOrder();
		} else {
            // reset order to initial
            await cleanQueue();
            await initOrder();
		}
        playerSetShuffle(!isShuffle);
    };

	useEffect(() => {
		toggleBottomBar(false);

		return () => {
			if (!fromSingersScreen) toggleBottomBar(true);
		};
	}, []);


	const musicMenuRef = useRef(null)
	const addPlaylistRef = useRef(null)

	let [deleteModalVisible, setDeleteModalVisible] = useState(false)
	// let [addPlaylistModalVisible, setAddPlaylistModalVisible] = useState(false)
    // const [addTrackToTrackListModalVisible, setAddTrackToTrackListModalVisible] = useState(false);
    const [downloadModalVisible, setDownloadModalVisible] = useState(false);

	const deleteMusicItem = async () => {
	    console.log('setMusicData: ', setMusicData, 'musicData: ', musicData, 'onTrackDelete: ', onTrackDelete);

	    if (!isDownloadedTracks) {
            if (musicData) setMusicData(musicData);
            if (onTrackDelete) onTrackDelete(musicData);
		} else {
            props.miniPlayerStop();
            await destroyPlayer();
            await props.removeDownloadedTrack(musicData);
            navigation.goBack();
		}


		setDeleteModalVisible(false);
	};

	// const createPlaylist = () => {
	// 	setAddPlaylistModalVisible(false)
	// };

	const onMenuPress = () => {
		onTrackLikedCheck();
		musicMenuRef.current.open();
	};

	// const createPlayListInModal = useCallback((playListName) => {
	// 	createPlayList(playListName);
	// 	setAddPlaylistModalVisible(false)
	// }, []);

	// const onAddTrackToPlayList = useCallback((playListId, songId) => {
	// 	// alert(`playListId: ${playListId}, songId: ${songId}`)
	// 	addTrackToPlayList(playListId, songId);
	// 	addPlaylistRef.current.close()
	// }, []);

	const handleTrackToPlaylistAdd = playListId => {
       
        if (musicData && musicData.id) {
            console.log('playListId', playListId)
            console.log('musicData add to playlist', musicData)

            addTrackToPlayList(playListId, musicData.id)
            getAddTrackToUserTrackListByTrackId(musicData.id)

            setVisibleAddTrackModalSuccess(true);
        } else {
            alert(`error add track, playListId ${playListId}, trackId ${musicData.id}`)
        }
    };

    const onAddTrackToUserTracksList = () => {
        // console.log('onAddTrackToUserTracksList onPlayListsMenuOpen: ', onPlayListsMenuOpen);
		if (onPlayListsMenuOpen) {
			onPlayListsMenuOpen(musicData);
		} else addPlaylistRef.current.open();
    };

	const handleTrackShare = async () => {
		try {
			const result = await Share.share({ message: musicData.audio + '?music_id=' + musicData.id});
		} catch (e) {
			console.log('Error a Track share of the screen "Player": ', e);
		}
	};

    const handleDownloadTrack = () => {
        musicMenuRef.current.close();

        if (!props.downloading) {
            setDownloadModalVisible(true);
        } else {
            Alert.alert(
                'Информация',
                'Можно скачать только один файл одновременно!',
                [
                    {
                        text: 'Хорошо',
                        style: 'success',
                    },
                ],
                { cancelable: true }
            );
        }
	};
	

	let [subscriptionModalIsVisible, setSubscriptionModalIsVisible] = useState(false)

	const subscribe = () => {
		setSubscriptionModalIsVisible(true)
	}

	return (
		<MainScreenContainer darkMode={darkMode} darkTintColor={'#242424'} lightTintColor={'#FCFCFC'}>
			<View style={[ styles.container, {backgroundColor: darkMode ? '#242424' : '#FCFCFC', paddingTop: height / 10} ]}>
				{track && (
					<View style={{flex: 1}}>
						<View style={{flex: 1, maxHeight: '80%', justifyContent: 'space-between'}}>
							<MusicCover image={track.photo} darkMode={darkMode}>
								<View style={{ marginTop: '5%' }}>
									<Text style={[styles.title, { color: darkMode ? COLORS.white : COLORS.black }]}>{track && track.singers ? getSingersName(track.singers) : 'Исполнитель'}</Text>
									<Text style={[styles.text, { color: darkMode ? COLORS.white : COLORS.black }]}>{track && track.name ? track.name : 'Название'}</Text>
								</View>
							</MusicCover>

							<View style={{maxHeight: 100}}>
								<View style={styles.actions}>
								{
									approveToAddTrack ?

										<TouchableOpacity
											style={styles.iconWrapper}
											onPress={() => {
												onAddTrackToUserTracksList(track.id);
											}}
										>
											<PlusIcon color={palette.common.primary} />
										</TouchableOpacity> :

										<View style={{width: 24, height: 24, backgroundColor: 'transparent'}} />
								}

								<View style={styles.playAction}>
									<TouchableOpacity
										onPress={() => onSkipTo('previous')}
									>
										<PlayBackIcon
											style={styles.icon}
											color={palette.common.primary}
										/>
									</TouchableOpacity>

									<TouchableOpacity
										onPress={(props.isSubscribed || props.isActiveSubscription) ? start : subscribe}
									>
										{
										    // !== 3
											playerState !== STATE_PLAYING  ?

												<RadioPlayIcon width={76} height={76} /> :

												<View style={{ position: 'relative' }}>
													<Image source={require('../../assets/circle.png')} style={{ width: 76, height: 76, resizeMode: 'contain' }} />
													<View style={[styles.stopBtn, { left: '36%' }]}></View>
													<View style={[styles.stopBtn, { left: '56%' }]}></View>
												</View>
										}
									</TouchableOpacity>

									<TouchableOpacity
										onPress={() => onSkipTo('next')}
									>
										<PlayForwardIcon
											style={styles.icon}
											color={palette.common.primary}
										/>
									</TouchableOpacity>
								</View>

								<TouchableOpacity style={styles.iconWrapper} onPress={onMenuPress}>
									<MenuIcon color={palette.common.primary} />
								</TouchableOpacity>
							</View>

								<MyPlayerBar
									progress={progress}
									duration={duration}
									darkMode={darkMode}
									changeTime={time => changeTime(time)}
								/>

								<View style={styles.bottomActions}>
									<TouchableOpacity onPress={() => setRepeat()}>
										<RepeatIcon color={isRepeat ? palette.common.primary : '#c2c2c2'} />
									</TouchableOpacity>

									<TouchableOpacity onPress={() => setShuffle()}>
										<RandomIcon color={isShuffle ? palette.common.primary : '#c2c2c2'} />
									</TouchableOpacity>
								</View>
							</View>
						</View>


						<MusicMenu
							ref={musicMenuRef}
							withoutAddToPlayList={!approveToAddTrack}
							musicData={track}
							darkMode={darkMode}
                            singerName={checkSingersMusicItem('', musicData)}

							likedTrackLoading={likedTrackLoading}
							likedTrackError={likedTrackError}
							trackLikedTrackStatus={trackLikedTrackStatus}
							setTrackItemLike={setTrackItemLike}

							onShareTrack={handleTrackShare}
							setDeleteModalVisible={() => {
								setDeleteModalVisible(true);
								musicMenuRef.current.close();
							}}
							openAddPlaylistSheet={() => {
								addPlaylistRef.current.open();
								musicMenuRef.current.close();
							}}
							openMusicText={() => {
								musicMenuRef.current.close();
								navigation.navigate('MusicTextScreen', {musicData: track})
							}}
							withoutDelete={withoutDelete}
							onDownload={handleDownloadTrack}
						/>

						<AddPlaylist
							trackId={track.id}
							// onAddTrackToPlayList={(playListId) => onAddTrackToPlayList(playListId, track.id)}
							ref={addPlaylistRef}
							playlists={playLists}
							createPlayList={createPlayList}
							darkMode={darkMode}
							handleTrackToPlaylistAdd={ (playlistId, trackId) => handleTrackToPlaylistAdd(playlistId, trackId) }
							setAddPlaylistModalVisible={() => {
								setAddPlaylistModalVisible(true)
								// addPlaylistRef.current.close()
							}}
						/>

						{/*<AddPlaylistModal*/}
						{/*    modalVisible={addPlaylistModalVisible}*/}
						{/*    closeModal={() => setAddPlaylistModalVisible(false)}*/}
						{/*    createPlaylist={createPlayListInModal}*/}
						{/*    darkMode={darkMode}*/}
						{/*/>*/}

						<DeleteModal
							modalVisible={deleteModalVisible}
							closeModal={() => setDeleteModalVisible(false)}
							onMusicItemDelete={deleteMusicItem}
							darkMode={darkMode}
						/>

						{/*<AddTrackSuccessModal*/}
						{/*    modalVisible={addTrackToTrackListModalVisible}*/}
						{/*    setModalVisible={setAddTrackToTrackListModalVisible}*/}
						{/*    darkMode={darkMode}*/}
						{/*    errorRequest={errorAddTrackToTrackList}*/}
						{/*    trackIsAlready={trackIsAlready}*/}
						{/*    loading={loadingAddTrackToTrackList}*/}
						{/*    resetTrackIsAlready={resetTrackInUserTrackListIsAlready}*/}
						{/*/>*/}

                        <DownloadModal
                            modalVisible={downloadModalVisible}
                            closeModal={() => setDownloadModalVisible(false)}
                            track={musicData}
							darkMode={darkMode}
							navigation={navigation}
                        />

						<AddTrackSuccessModal
							modalVisible={visibleAddTrackModalSuccess}
							setModalVisible={setVisibleAddTrackModalSuccess}
							darkMode={darkMode}
							loading={props.loadingAddTrackToTrackList}
							errorRequest={props.errorAddTrackToTrackList}
						/>

						<Modal
							animationIn={'fadeIn'}
							animationOut={'fadeOut'}
							animationInTiming={200}
							animationOutTiming={10}
							hideModalContentWhileAnimating={true}
							backdropOpacity={0.5}
							style={{ alignItems: 'center' }}
							isVisible={subscriptionModalIsVisible}
						>
							<SubscriptionModal 
								closeModal={() => setSubscriptionModalIsVisible(false)} 
								darkMode={darkMode}
								navigation={navigation}
								forDownloading={false}
							/>
						</Modal>

					</View>
				)}
			</View>
		</MainScreenContainer>
	);
};


const mapStateToProps = state => ({
	isSubscribed: state.appReducer.isSubscribed,
	isActiveSubscription: state.appReducer.isActiveSubscription,
    darkMode: state.appReducer.darkMode,
    infinityPlaying: state.appReducer.infinityPlaying,
    highQuality: state.appReducer.highQuality,
    playLists: state.playListReduce.playLists,
    isLoading: state.playListReduce.loading,
    isPlayListsEmpty: state.playListReduce.isPlayListsEmpty,
    loadingAddTrackToTrackList: state.trackListReducer.loadingAddTrackToTrackList,
    errorAddTrackToTrackList: state.trackListReducer.errorAddTrackToTrackList,
    trackIsAlready: state.trackListReducer.trackInTrackListIsAlready,
    track: state.playerReducer.track,
    playlist: state.playerReducer.playlist,
    duration: state.playerReducer.duration,
    playerInitScreen: state.playerReducer.initScreen,
    isRepeat: state.playerReducer.isRepeat,
    isShuffle: state.playerReducer.isShuffle,
	shuffleQueue: state.playerReducer.shuffleQueue,

	likedTrackLoading: state.appReducer.likedTrackLoading,
	trackLikedTrackStatus: state.appReducer.trackLikedTrackStatus,
	isLikedTrackStatusLoading: state.appReducer.isLikedTrackStatusLoading,
	likedTrackError: state.appReducer.likedTrackError,
    downloadPendingList: state.downloadReducer.pendingList,
    downloading: state.downloadReducer.downloading,
});

const mapDispatchToProps = {
	toggleBottomBar,
	createPlayList,
	addTrackToPlayList,
    resetTrackInUserTrackListIsAlready,
    getAddTrackToUserTrackListByTrackId,
    playerInit,
    playerSetRepeat,
    playerSetShuffle,
    playerSetShuffleQueue,
    playerReset,
    setTrackListening,

	getTrackLikedCheck,
	setTrackItemLike,

    miniPlayerStart,
    miniPlayerStop,

	removeDownloadedTrack,
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerScreen);
// export default PlayerScreen;

class MyPlayerBar extends TrackPlayer.ProgressComponent {
	render() {
		return (
			<View style={{ marginVertical: 15 }}>
				<View
					style={{
						flexDirection: 'row',
						justifyContent: 'space-between',
					}}>
					<Text style={styles.timeStamp}>{secondsToMMSS(this.state.position)}</Text>
					<Text style={styles.timeStamp}>{secondsToMMSS(this.props.duration)}</Text>
				</View>

				<Slider
					value={this.getProgress().toFixed(2) * 100}
					minimumValue={0}
					maximumValue={100}
					trackStyle={styles.track}
					thumbStyle={styles.thumb}
					thumbTintColor={palette.common.primary}
					minimumTrackTintColor={palette.common.primary}
					onValueChange={seconds => this.props.changeTime(seconds)}
					maximumTrackTintColor={this.props.darkMode ? 'white' : 'black'}
				/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 20,
	},
	img: {
		height: height - 100,
	},
	iconWrapper: {
		width: 24,
		alignItems: 'center',
	},
	icon: {
		marginHorizontal: 12,
	},
	timeStamp: {
		fontSize: 14,
		color: '#C2C2C2',
	},
	actions: {
		flexDirection: 'row',
		alignItems: 'center',
		marginTop: '5%',
		justifyContent: 'space-between',
		height: height * 0.1,
	},
	bottomActions: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		marginTop: '5%',
	},
	title: {
		fontWeight: '600',
		fontSize: 20,
		color: '#000',
		marginBottom: 8,
		textAlign: 'center',
	},
	playAction: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	text: {
		fontSize: 18,
		color: '#C2C2C2',
		textAlign: 'center',
	},
	track: {
		height: 2,
		borderRadius: 1,
		backgroundColor: '#FFF',
	},
	thumb: {
		width: 8,
		height: 8,
		backgroundColor: palette.common.primary,
	},


	stopBtn: {
		width: 6,
		height: '40%',
		backgroundColor: COLORS.purple,
		position: 'absolute',
		top: '30%',
		borderRadius: 10
	}
});
