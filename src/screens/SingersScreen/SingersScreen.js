import React, { useEffect } from 'react';
import { StyleSheet, SafeAreaView, View, ScrollView, StatusBar, Dimensions, FlatList, Text } from 'react-native';
import { connect } from 'react-redux';
import { COLORS } from '../../constants/colors.constants'
import CustomHeader from '../../components/CustomHeader/CustomHeader';
import { MusicCard } from '../../components/'
import MenuIcon from '../../icons/menu';
import { palette } from '../../state/palette';
import { getStatusBarHeight } from "react-native-status-bar-height";
import { MainScreenContainer } from "../../components/MainScreenContainer/MainScreenContainer";
import { toggleBottomBar } from "../../store/appReducer";
import { API_URL } from '../../constants/api_config';
import SearchBlock from '../../components/SearchBlock/SearchBlock';
import { instance } from '../../api/api';
import axios from 'axios';

const screenHeight = Dimensions.get('window').height;

class SingersScreen extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      data: [],
      isPopularSingers: this.props.navigation.state.params && this.props.navigation.state.params.isPopularSingers,
      searchText: '',
      isEmptySearchData: false,
      page: 1,
      nextPage: false
    }
  }

  getSingersData = async (page, perPage) => {
    try {
      const data = await fetch(API_URL + `singers`);
      const json = await data.json()

      if (json.status === 200){
        this.setState({ data: json.data.list.data })
      }
    }
    catch (error) { console.log(error) }
  }

  getPopularSingers = async (page = 1, perPage = 10) => {
    try {
      let data = await axios.get(`${API_URL}popular-singers?perPage=${perPage}&page=${page}`).then( res => res.data )

      if (data.status === 200){
        this.setState({ data: [...this.state.data, ...data.data.list] })
        
        if (data.data.next_page_url) {
            this.setState({ nextPage: true })
            this.setState({ page: +data.data.current_page + 1 })
        } else {
            this.setState({nextPage: false})
        }
      }
    } catch (error) { console.log(error) }
  }

  getNextPopularSingersPage = () => {
    if (this.state.nextPage) {
      this.getPopularSingers(this.state.page)
    }
  }

  async componentDidMount() {
    this.props.toggleBottomBar(false);

    if (this.state.isPopularSingers) {
      this.getPopularSingers()
    } else {
      await this.getSingersData()
    }
  }

  componentWillUnmount() {
    this.props.toggleBottomBar(true);
  }

  handleSearchValue = value => this.setState({ searchText: value });

  handleFetchSearch = async () => {
    try {
      let response = await instance.get(API_URL + `search-singers?name=${this.state.searchText}&page=&perPage=`)
      if (response.data.status === 200) {
        if (response.data.data.count > 0) {
          this.setState({ data: response.data.data.list.data, isEmptySearchData: false })
        } else {
          this.setState({ data: [] })
          this.setState({ isEmptySearchData: true })
        }
      }
    } catch (e) {
      console.log('error of popular singers searching')
    }
  }

  render() {
    let darkMode = this.props.darkMode;

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: darkMode ? '#000' : '#F9F9F9', marginTop: getStatusBarHeight() }}>
        <MainScreenContainer darkMode={darkMode}>
          <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'}
            backgroundColor={darkMode ? '#000' : '#F9F9F9'} />

          <View style={{ marginHorizontal: 15, paddingBottom: 0 }}>
            <CustomHeader
              title="Исполнители"
              icon={<MenuIcon color={palette[this.props.darkMode ? 'dark' : 'light'].text} />}
              onIconPress={() => {}}
              pressGoBack={() => this.props.navigation.navigate('SearchMusicTabScreen')}
              darkMode={this.props.darkMode}
            />

            {this.state.isPopularSingers &&
              <SearchBlock 
                value={this.state.searchText} 
                onSearchStart={this.handleFetchSearch} 
                onChange={this.handleSearchValue} 
                darkMode={darkMode} 
                style={{ paddingVertical: 10 }} 
              />
            }
          </View>

          {this.state.isEmptySearchData &&
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
              <Text style={{
                width: '100%',
                textAlign: 'center',
                color: !darkMode ? '#000' : '#fff'
              }}>К сожалению таких исполнителей не найдено :(</Text>
            </View>
          }
          <ScrollView>
              <FlatList
                data={this.state.data}
                showsHorizontalScrollIndicator={false}
                numColumns={2}
                renderItem={({ item, ...props }) => {
                  console.log('item', item.photo)
                  return (
                  <MusicCard
                    name={item.name}
                    image={item.photo}
                    resizeItem={true}
                    onPress={() => this.props.navigation.navigate('PerformerScreen', { id: item.id, singerName: item.name, photo: item.photo, fromSingersScreen: true })}
                    style={{ marginHorizontal: '2%' }}
                  />
                )}}
                keyExtractor={item => item.id.toString()}
                contentContainerStyle={styles.contentStyle}

                ListHeaderComponent={() => <View></View>}
                ItemSeparatorComponent={() => <View></View>}
                ListFooterComponent={() => <View></View>}
                onEndReached={this.state.isPopularSingers ? this.getNextPopularSingersPage : () => {}}
                onEndReachedThreshold={0.5}
              />
          </ScrollView>
        </MainScreenContainer>
      </SafeAreaView>
    );
  }
};

const mapStateToProps = state => ({
  darkMode: state.appReducer.darkMode,
});

const mapDispatchToProps = {
  toggleBottomBar,
};

export default connect(mapStateToProps, mapDispatchToProps)(SingersScreen);

const styles = StyleSheet.create({
  btnIcon: {
    marginHorizontal: 10,
  },
  btnText: {
    color: '#fff',
    fontSize: 13,
  },
  playButton: {
    width: '48%',
    paddingHorizontal: 25,
    paddingVertical: 6,
    borderRadius: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: palette.common.secondary,
  },
  createButton: {
    width: '48%',
    paddingHorizontal: 15,
    paddingVertical: 6,
    borderRadius: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: palette.common.primary,
  },
  actions: {
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  loader: {
    flex: 1,
    height: screenHeight / 1.5,
    alignItems: 'center',
    justifyContent: 'center'
  },

  contentStyle: {
    paddingTop: 10,
    paddingBottom: 50,
    alignItems: 'center'
  }
});
