import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';

import RadioScreen from './RadioScreen/RadioScreen.component';
import RadioStationsScreen from './RadioStationsScreen/RadioStationsScreen.component';
// import RadioPlayer from './../../components/RadioPlayer/RadioPlayer.component';
import RadioPlayerScreen from './RadioPlayerScreen/RadioPlayerScreen';

const RadioNavigator = createStackNavigator(
  {
    // RadioPlayer: {
    //     screen: () => <RadioPlayer />
    // },
    RadioScreen,
    RadioStationsScreen,
    RadioPlayerScreen,
  },
  {
    headerMode: 'none',
  },
);

export default RadioNavigator;
