import React, { useEffect, useState, useRef } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    ImageBackground,
    Image,
} from 'react-native';
import TrackPlayer, {
    STATE_NONE, // 0
    STATE_READY, // 2
    STATE_PLAYING, // 3
    STATE_PAUSED, // 2
    STATE_STOPPED, // 1
    STATE_BUFFERING, // 6
    STATE_CONNECTING, // 8
} from 'react-native-track-player';

import PlayBackIcon from 'icons/playBack';
import PlayForwardIcon from 'icons/playForward';
import RadioPlayIcon from 'icons/radioPlay';

import MusicCover from 'components/MusicCover/MusicCover';
import {connect} from "react-redux";
import { setRadioStationListening } from '../../../actions';
import {
    radioPlayerInit,
    radioPlayerReset,
} from '../../../actions/radioPlayer';
import {
    miniPlayerStart,
} from '../../../actions/miniPlayer';
import {toggleBottomBar} from '../../../store/appReducer';
import {mapToRadioPlayerData} from "../../../utils/player";
import {COLORS} from "../../../constants/colors.constants";
// import {getSingersName} from "../../../utils/functions";
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";

const { height } = Dimensions.get('screen');

const RadioPlayerScreen = props => {
    const {
        navigation,
        darkMode,
        track,
        playlist,
        initScreen,
        toggleBottomBar,
        radioPlayerInit,
        radioPlayerReset,
        miniPlayerStart,
    } = props;

    let [playerState, setPlayerState] = useState(null);

    let onPlaybackStateSubscribe = useRef(null);
    let onPlayerStopSubscribe = useRef(null);
    let onPlayerErrorSubscribe = useRef(null);

    useEffect(() => {
        toggleBottomBar(false);

        const { track, playlist } = navigation.state.params;
        if (playlist) {
            const trackData = track || playlist[0];
            radioPlayerInit({ track: trackData, playlist: playlist });
            props.setRadioStationListening(trackData.id);
            miniPlayerStart({ track: trackData });
        }

        onPlaybackStateSubscribe.current = TrackPlayer.addEventListener('playback-state', ({ state }) => {
            setPlayerState(state)
        });

        onPlayerStopSubscribe.current = TrackPlayer.addEventListener('remote-stop', () => {
            navigation.goBack();
        });

        onPlayerErrorSubscribe.current = TrackPlayer.addEventListener('playback-error', () => {
            navigation.goBack();
        });

        return () => {
            toggleBottomBar(true);

            radioPlayerReset();

            if (onPlaybackStateSubscribe.current) {
                onPlaybackStateSubscribe.current.remove();
            }

            if (onPlayerStopSubscribe.current) {
                onPlayerStopSubscribe.current.remove();
            }

            if (onPlayerErrorSubscribe.current) {
                onPlayerErrorSubscribe.current.remove();
            }
        };
    }, []);

    useEffect(() => {
        if (initScreen) {
            setupPlayer();
        }
    }, [initScreen]);

    const setupPlayer = async () => {
        await TrackPlayer.setupPlayer({
            waitForBuffer: true,
        });
        TrackPlayer.updateOptions({

            stopWithApp: true,

            capabilities: [
                TrackPlayer.CAPABILITY_PLAY,
                TrackPlayer.CAPABILITY_PAUSE,
                TrackPlayer.CAPABILITY_STOP,
                TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
                TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
            ],

        });

        await addTracksToPlayer();
    };

    const addTracksToPlayer = async () => {
        let playData = mapToRadioPlayerData([track]);
        await TrackPlayer.add(playData); // set single track

        await initOrder();
    };

    const initOrder = async () => {
        if (playlist && playlist.length > 1) {
            const currentTrackIndexInPlaylist = playlist.findIndex(item => item.id === track.id);

            const prevTracks = playlist.slice(0, currentTrackIndexInPlaylist);
            const prevPlayData = mapToRadioPlayerData(prevTracks);
            await TrackPlayer.add(prevPlayData, track.id.toString()); // set prev tracks before current track

            let nextTracks = playlist.slice(currentTrackIndexInPlaylist);
            nextTracks.shift();
            const nextPlayData = mapToRadioPlayerData(nextTracks);
            await TrackPlayer.add(nextPlayData); // set next tracks after
        }
    };

    const skipToPrevious = async () => {
        TrackPlayer.skipToPrevious().catch((err) => {});
    };

    const skipToNext = async () => {
        TrackPlayer.skipToNext().catch((err) => {});
    };

    const start = async () => {
        if (
            playerState === STATE_READY     ||
            playerState === STATE_PAUSED    ||
            playerState === STATE_STOPPED   ||
            playerState === STATE_CONNECTING
        ) {
            await TrackPlayer.play()
        } else {
            await TrackPlayer.pause()
        }
    };

    return (
        <MainScreenContainer darkMode={darkMode} darkTintColor={'#242424'} lightTintColor={'#FCFCFC'}>
            <View style={[styles.container]}>
                {track && (
                    <ImageBackground
                        style={styles.img}
                        resizeMode="cover"
                        source={{ uri: track.photo }}
                        blurRadius={3}
                    >
                        <MusicCover image={track.photo}>
                            <View style={{ marginTop: '5%' }}>
                                <Text style={styles.title}>{track && track.name ? track.name : 'Радио'}</Text>
                                <Text style={styles.text}>Прямой эфир</Text>
                            </View>
                        </MusicCover>

                        <View style={styles.actions}>
                            <TouchableOpacity
                                onPress={skipToPrevious}
                            >
                                <PlayBackIcon style={styles.icon} />
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={start}
                            >
                                {
                                    // !== 3
                                    playerState !== STATE_PLAYING ?

                                        <RadioPlayIcon width={76} height={76} /> :

                                        <View style={{ position: 'relative' }}>
                                            <Image source={require('../../../assets/circle.png')} style={{ width: 76, height: 76, resizeMode: 'contain' }} />
                                            <View style={[styles.stopBtn, { left: '36%' }]}></View>
                                            <View style={[styles.stopBtn, { left: '56%' }]}></View>
                                        </View>
                                }
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={skipToNext}
                            >
                                <PlayForwardIcon style={styles.icon} />
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                )}
            </View>
        </MainScreenContainer>
    );
};

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,
    track: state.radioPlayerReducer.track,
    playlist: state.radioPlayerReducer.playlist,
    initScreen: state.radioPlayerReducer.initScreen,
});

const mapDispatchToProps = {
    toggleBottomBar,
    radioPlayerInit,
    radioPlayerReset,
    setRadioStationListening,
    miniPlayerStart,
};

export default connect(mapStateToProps, mapDispatchToProps)(RadioPlayerScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333',
    },
    img: {
        paddingTop: height / 10,
        flex: 1,
        paddingHorizontal: 20,
        backgroundColor: 'rgba(51, 51, 51, 0.48)',
    },
    icon: {
        marginHorizontal: 12,
    },
    title: {
        fontWeight: '600',
        fontSize: 20,
        color: '#fff',
        marginBottom: 8,
        marginTop: 90,
        textAlign: 'center',
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
    },
    text: {
        fontSize: 18,
        color: '#fff',
        textAlign: 'center',
    },
    playButtonContainer: {
        backgroundColor: '#FFF',
        borderColor: 'rgba(93, 63, 106, 0.2)',
        borderWidth: 16,
        width: 128,
        height: 128,
        borderRadius: 64,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 32,
        shadowColor: '#5D3F6A',
        shadowRadius: 30,
        shadowOpacity: 0.5,
    },
    stopBtn: {
        width: 6,
        height: '40%',
        backgroundColor: COLORS.purple,
        position: 'absolute',
        top: '30%',
        borderRadius: 10
    }
});
