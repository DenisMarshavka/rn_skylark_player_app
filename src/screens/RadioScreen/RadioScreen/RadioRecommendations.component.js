import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions, ActivityIndicator
} from 'react-native';
import Ripple from 'react-native-material-ripple';
import RadioPlayIcon from 'icons/radioPlay';
import {COLORS} from '../../../constants/colors.constants';
import Gradient from "../../../assets/gradient.png";
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";


const RadioRecommendations = ({ loading = false, requestFailed, onPress, darkMode, list = [] }) => (
  <SafeAreaView>
    <View style={{flex: 1, width: '100%'}}>

    <View style={styles.block}>
      <Text style={[styles.blockTitle, {color: darkMode ? COLORS.white : COLORS.black }]}>Рекомендуемые</Text>
    </View>

    {
      loading ?

      <View style={styles.loader}>
        <ActivityIndicator />
      </View> : list.length && !requestFailed ?

      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <View style={styles.list}>
        {
          list.map((item, index) => {
            // console.log('Radio recommendations item: ', item);

            return (
                <TouchableOpacity
                    style={
                      index === list.length - 1
                          ? {...styles.radioBlock, marginRight: 0}
                          : styles.radioBlock
                    } key={index + item.name}>
                  <Ripple
                      onPress={() => onPress(item, list)}
                  >
                    <Image
                        style={styles.image}
                        source={ item.photo ? {uri: item.photo} : require('../../../assets/edward-cisneros-KoKAXLKJwhk-unsplash.png')}
                        resizeMode="cover"
                    />

                    <Image
                        style={[styles.image, styles.gradient]}
                        source={Gradient}
                    />

                    <View style={styles.wrapperContent}>
                      <View style={styles.content}>
                        <RadioPlayIcon style={styles.icon}/>

                        <Text numberOfLines={2} style={styles.text}>{item.name}</Text>
                      </View>
                    </View>
                  </Ripple>
                </TouchableOpacity>
            )})
          }
        </View>
      </ScrollView> : !requestFailed && !list.length ?

      <View style={styles.loader}>
        <Text style={{
          width: '100%',
          textAlign: 'center',
          color: !darkMode ? '#000' : '#fff',
        }}>Список радиостанций пуст</Text>
      </View> :

      <View style={styles.loader}>
        <Text style={{
          width: '100%',
          textAlign: 'center',
          color: !darkMode ? '#000' : '#fff'
        }}>Ошибка получения данных</Text>
      </View>
    }
    </View>
  </SafeAreaView>
);

export default RadioRecommendations;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    height: (Dimensions.get('window').height / 4.75) + 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  block: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 15,
  },
  radioBlock: {
    position: 'relative',
    borderRadius: 8,
    marginRight: 15,
    width: Dimensions.get('window').width / 3.25,
    height: Dimensions.get('window').height / 4.75,
    overflow: 'hidden',
  },
  // radioBlockEmpty: {
  //   position: 'relative',
  //   borderRadius: 8,
  //   overflow: 'hidden',
  // },
  gradient: {
    position: 'absolute',
    top: 0, left: 0, right: 0, bottom: 0,
    zIndex: 10
  },
  wrapperContent: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: '65%',
    zIndex: 15,
  },
  content: {
    position: 'relative',
    left: 10,
    bottom: 10,
    width: '90%',
    height: '100%',
    justifyContent: 'flex-end',
  },
  icon: {
    marginBottom: 5,
  },
  text: {
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
    // position: 'absolute',
    // left: 10,
    // bottom: 10,
    // zIndex: 20
  },
  image: {
    width: '100%',
    height: '100%',
  },
  list: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  blockTitle: {
    fontSize: 22,
    fontWeight: '600',
  },
});

