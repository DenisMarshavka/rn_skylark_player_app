import React from 'react';
import {
    StyleSheet,
    SafeAreaView,
    View,
    Text,
    Image,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    Dimensions,
} from 'react-native';
import Ripple from 'react-native-material-ripple';

import {COLORS} from '../../../constants/colors.constants';

const RadioPreviousListen = ({ loading = false, requestFailed, onPress, darkMode, previousListen}) => (
  <>
      {
          (previousListen && previousListen && previousListen.length) || requestFailed ?

          <SafeAreaView>
              <View style={styles.block}>
                  <Text style={[styles.blockTitle, {color: darkMode ? COLORS.white : COLORS.black}]}>Вы недавно слушали</Text>
              </View>

              {
                  loading ?

                  <View style={styles.loader}>
                      <ActivityIndicator/>
                  </View> : previousListen.length && !requestFailed ?

                  <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                      <View style={{...styles.list, paddingBottom: 45}}>
                          {
                              previousListen.map((item, index) => {
                                  return (
                                      <TouchableOpacity
                                          style={
                                              index === previousListen.length - 1
                                                  ? styles.radioBlockEmpty
                                                  : styles.radioBlock
                                          }
                                          key={index + item.radio.name}>
                                          <Ripple
                                              onPress={() => onPress(item.radio, previousListen)}
                                          >
                                              <Image
                                                  style={styles.image}
                                                  source={{uri: item.radio.photo}}
                                                  resizeMode="cover"
                                              />

                                              <View style={styles.radioCover}/>

                                              <Text style={styles.text}>{item.radio.name}</Text>
                                          </Ripple>
                                      </TouchableOpacity>
                                  );
                              })
                          }
                      </View>
                  </ScrollView> :

                  <View style={styles.loader}>
                      <Text style={{
                          width: '100%',
                          textAlign: 'center',
                          color: !darkMode ? '#000' : '#fff'
                      }}>Ошибка получения данных</Text>
                  </View>
              }
          </SafeAreaView> :

          null
      }
  </>
);

const styles = StyleSheet.create({
  block: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
  },
  radioBlock: {
    position: 'relative',
    borderRadius: 8,
    marginRight: 15,
    overflow: 'hidden',
  },
  radioBlockEmpty: {
    position: 'relative',
    borderRadius: 8,
    marginRight: 0,
    overflow: 'hidden',
  },
  text: {
    color: '#fff',
    fontSize: 11,
    fontWeight: 'bold',
    position: 'absolute',
    left: 10,
    bottom: 10,
  },
  image: {
    width: 130,
    height: 130,
    borderRadius: 8
  },
  list: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: 50,
  },
  blockTitle: {
    fontSize: 22,
    fontWeight: '600',
  },
  loader: {
    flex: 1,
    height: (Dimensions.get('window').height / 4.75) + 35,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 20,
  },
  radioCover: {
    width: 130,
    height: 130,
    borderRadius: 8,
    backgroundColor: 'rgba(0, 0, 0, 0.2)', // need gradient
    position: 'absolute',
  },
});

export default RadioPreviousListen;
