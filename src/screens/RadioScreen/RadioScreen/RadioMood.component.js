import React from 'react';
import {StyleSheet, SafeAreaView, View, Text, ActivityIndicator} from 'react-native';
import {COLORS} from '../../../constants/colors.constants';
import {MusicCard} from "../../../components";


const RadioMood = ({ requestPreviousListenFailed = false, navigation, loading = false, requestFailed = false, onPress, darkMode, moodsList = [], previousListen = [] }) => {
  console.log('requestPreviousListenFailed: ', requestPreviousListenFailed, 'previousListen && previousListen.length: ', previousListen && previousListen.length);

  return (
    <SafeAreaView>
      <View style={styles.block}>
        <Text style={[styles.blockTitle, {color: darkMode ? COLORS.white : COLORS.black}]}>Настроение</Text>
      </View>

      <View style={[ styles.playlist, {paddingBottom: ( requestPreviousListenFailed || (previousListen && previousListen.length) ) ? 0 : 35} ]}>
        {
          !loading && moodsList.length && !requestFailed ?

          moodsList.map((item, index) => {
            // console.log('Radio mood item data: ', item);

            return (
                <MusicCard
                    onPress={() => navigation.navigate('RadioStationsScreen', { isRadioStationsByMoodId: true, moodId: item.id })}
                    resizeItem={true}
                    key={item.name + index}
                    name={item.name}
                    image={item.image}
                    extra={item.extra}
                />
            )
          }) : !requestFailed && loading ?

          <View style={styles.loader}>
            <ActivityIndicator />
          </View> : !loading && !moodsList.length && !requestFailed ?

          <View style={styles.loader}>
            <Text style={{
              width: '100%',
              textAlign: 'center',
              color: !darkMode ? '#000' : '#fff',
            }}>Список настроений пуст</Text>
          </View> :

          <View style={styles.loader}>
            <Text style={{
              width: '100%',
              textAlign: 'center',
              color: !darkMode ? '#000' : '#fff',
            }}>Ошибка получения данных</Text>
          </View>
        }
      </View>
    </SafeAreaView>
  );
};

export default RadioMood;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    height: 240,
    alignItems: 'center',
    justifyContent: 'center',
  },
  block: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
  },
  playlist: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: 15,
  },
  blockTitle: {
    fontSize: 22,
    fontWeight: '600',
  },
});
