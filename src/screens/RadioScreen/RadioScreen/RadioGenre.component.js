import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity, ActivityIndicator,
} from 'react-native';
import {palette} from 'state/palette';
import ArrowRightIcon from 'icons/arrowRight';
import { COLORS } from '../../../constants/colors.constants';


const RadioGenre = ({ loading, onOpen, darkMode, genreList, requestFailed }) => (
    <SafeAreaView>
      <View style={styles.block}>
        <Text style={[styles.blockTitle, {color: darkMode ? COLORS.white : COLORS.black}]}>По жанрам</Text>
      </View>

      <View>
        {
          loading ?

              <View style={styles.loader}>
                <ActivityIndicator />
              </View> : !loading && genreList.length ?

              genreList.map((item, index) => (
                  <TouchableOpacity
                      onPress={() => onOpen(item.id)}
                      key={index + item.name}>
                    <View style={styles.wrapper}>
                      <Text style={styles.title}>{item.name}</Text>

                      <ArrowRightIcon />
                    </View>
                  </TouchableOpacity>
              )) : !requestFailed ?

              <View style={styles.empty}>
                <Text style={{
                  width: '100%',
                  textAlign: 'center',
                  color: !darkMode ? '#000' : '#fff',
                }}>Список треков пуст</Text>
              </View> :

              <View style={styles.loader}>
                <Text style={{
                  width: '100%',
                  textAlign: 'center',
                  color: !darkMode ? '#000' : '#fff'
                }}>Ошибка получения данных</Text>
              </View>
        }
      </View>
    </SafeAreaView>
);

export default RadioGenre;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    height: 110,
    alignItems: 'center',
    justifyContent: 'center',
  },
  block: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
  },
  title: {
    fontSize: 18,
    color: palette.common.primary,
    paddingVertical: 15,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 40,
  },
  blockTitle: {
    fontSize: 22,
    fontWeight: '600',
  },
});
