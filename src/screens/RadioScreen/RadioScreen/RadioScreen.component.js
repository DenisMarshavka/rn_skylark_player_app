import React, { useState, useCallback, useEffect } from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  ScrollView,
    RefreshControl,
    Dimensions
} from 'react-native';
import {connect} from 'react-redux';

import RadioAll from './RadioAll.component';
import RadioMood from './RadioMood.component';
import RadioGenre from './RadioGenre.component';
import RadioPreviousListen from './RadioPreviousListen.component';
import RadioRecommendations from './RadioRecommendations.component';
import {palette} from 'state/palette';
import {COLORS} from '../../../constants/colors.constants';
import { MINI_PLAYER_HEIGHT } from '../../../constants/styles';
import {
  getGenreList,
  getRadioMoods,
  getRadioRecommendationsStations,
  getRadioRecentListening,
  getRadioStations,
  getSearchRadio, resetSearchRadioStations
} from "../../../actions";
import {wait} from "../../../utils/promises";
import {MainScreenContainer} from "../../../components/MainScreenContainer/MainScreenContainer";
import {getStatusBarHeight} from "react-native-status-bar-height";


const RadioScreen = props => {
  const darkMode = props.darkMode;
  const [refreshing, setRefresh] = useState(false);
  const {
    getRadioRecommendationsStations,
    isLoadingRecommendations,
    errorRadioRecommendations,
    radioRecommendations,

    isLoadingGenreList,
    getGenreList,
    genreList,
    errorGenreList,

    getRadioMoods,
    isMoodsRadioLoading,
    moodsRadioList,
    moodsRadioError,

    getRadioStations,
    isLoadingStations,
    radioStations,
    errorRadioStations,

    getRadioRecentListening,
    loadingRecently,
    errorRecently,
    radioRecently,
  } = props;

  const onDataListsLoad = () => {
    getGenreList();
    getRadioRecommendationsStations();
    getRadioStations();
    getRadioMoods();
    getRadioRecentListening();
    // getPlayLists()
  };

  const onDataRefresh = useCallback(() => {
    setRefresh(true);

    onDataListsLoad();

    wait(500)
        .then(() => setRefresh(false));
  }, [refreshing]);

  useEffect(() => {
    onDataListsLoad();
  }, []);

  const redirectToPlayRadio = () => props.navigation.navigate('RadioStationsScreen', {stationsFromParams: radioStations, isAllRadioStations: true});

  const openRadioPlayer = (track, playlist) => {
    props.navigation.navigate('RadioPlayerScreen', { track: track, playlist: playlist});
  };

  const openPreviousListenRadioPlayer = (track, playlist) => {
    const data = playlist.map(item => item.radio);
    openRadioPlayer(track, data);
  };

  const handleRadioStationsByGenreOpen = genreId => props.navigation.navigate('RadioStationsScreen', { isRadioStationsByGenreId: true, genreId });

  console.log('previousListen: ', radioRecently);

  return (
    <MainScreenContainer darkMode={darkMode}>
      <SafeAreaView style={[{ backgroundColor: darkMode ? '#000' : '#F9F9F9', paddingTop: getStatusBarHeight() }]}>
        <ScrollView
          contentContainerStyle={{paddingBottom: props.isPlayerActive ? MINI_PLAYER_HEIGHT : 0}}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onDataRefresh}
              progressViewOffset={70}
              progressBackgroundColor={'transparent'}
              colors={['#fff', palette.common.secondary, palette.common.primary]}
              tintColor={palette.common.primary}
            />
          }>
            <View style={[styles.container, { backgroundColor: darkMode ? COLORS.black : COLORS.white }]}>
              {/*<SearchBlock value={searchValue} onChange={changeSearchValue} darkMode={darkMode} />*/}


              <RadioRecommendations loading={isLoadingRecommendations} requestFailed={errorRadioRecommendations} onPress={openRadioPlayer} list={radioRecommendations} darkMode={darkMode} />

              <RadioAll loading={isLoadingStations} list={radioStations} requestFailed={errorRadioStations} onPress={redirectToPlayRadio} onItemPress={openRadioPlayer} darkMode={darkMode} />

              <RadioGenre loading={isLoadingGenreList} requestFailed={errorGenreList} genreList={genreList} onOpen={handleRadioStationsByGenreOpen} navigation={props.navigation} darkMode={darkMode} />

              <RadioMood previousListen={radioRecently} requestPreviousListenFailed={errorRecently} navigation={props.navigation} darkMode={darkMode} loading={isMoodsRadioLoading} moodsList={moodsRadioList} requestFailed={moodsRadioError} />

              <RadioPreviousListen loading={loadingRecently} requestFailed={errorRecently} onPress={openPreviousListenRadioPlayer} previousListen={radioRecently} darkMode={darkMode} />
            </View>
        </ScrollView>
    </SafeAreaView>
  </MainScreenContainer>
  );
};

const mapStateToProps = state => ({
  darkMode: state.appReducer.darkMode,

  isLoadingRecommendations: state.radioReducer.loadingRecommendations,
  errorRadioRecommendations: state.radioReducer.errorRadioRecommendations,
  radioRecommendations: state.radioReducer.radioRecommendations,

  isLoadingStations: state.radioReducer.loadingStations,
  radioStations: state.radioReducer.radioStations,
  errorRadioStations: state.radioReducer.errorRadioStations,

  isLoadingGenreList: state.radioReducer.loadingGenreList,
  genreList: state.radioReducer.genreList,
  errorGenreList: state.radioReducer.errorGenreList,

  isMoodsRadioLoading: state.radioReducer.moodsRadioLoading,
  moodsRadioList: state.radioReducer.moodsRadioList,
  moodsRadioError: state.radioReducer.moodsRadioError,

  loadingRecently: state.radioReducer.loadingRecently,
  errorRecently: state.radioReducer.errorRecently,
  radioRecently: state.radioReducer.radioRecently,

  isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
  getRadioRecommendationsStations,
  getRadioStations,
  getGenreList,
  getRadioMoods,
  getRadioRecentListening,

  getSearchRadio,
  resetSearchRadioStations,
};

export default connect(mapStateToProps, mapDispatchToProps)(RadioScreen);

const styles = StyleSheet.create({
  searchInputWrapper: {
    flexGrow: 1,
    marginLeft: 12,
  },
  block: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
  },
  blockTitle: {
    fontSize: 22,
    fontWeight: '600',
  },
  btnText: {
    fontSize: 13,
    backgroundColor: palette.common.secondary,
    color: palette.dark.text,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  btnWrapper: {
    borderRadius: 17,
    overflow: 'hidden',
  },
  container: {
    paddingVertical: 15,
    paddingTop: 25,
    paddingHorizontal: 15
  },
  searchBlock: {
    paddingTop: 20,
    paddingBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
