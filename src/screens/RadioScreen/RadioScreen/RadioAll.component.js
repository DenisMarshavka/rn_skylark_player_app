import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity, ActivityIndicator, Dimensions,
} from 'react-native';
import {palette} from 'state/palette';
import {RadioCard} from '../../../components';
import { COLORS } from '../../../constants/colors.constants';


const RadioAllScreen = ({ list = [], onPress, onItemPress, darkMode, loading = false, requestFailed = true }) => {
    // console.log('RadioAllScreen props DATA - list: ', list, 'onPress: ', onPress, 'darkMode: ', darkMode, 'loading: ', loading, 'requestFailed; ', requestFailed);

    return (
        <SafeAreaView>
            <View style={styles.block}>
                <Text style={[styles.blockTitle, {color: darkMode ? COLORS.white : COLORS.black }]}>Все</Text>

                <TouchableOpacity style={styles.btnWrapper} onPress={onPress}>
                    <Text style={styles.btnText}>Все</Text>
                </TouchableOpacity>
            </View>

            <View style={{
                minHeight: 225,
            }}>
                {
                    !loading && list.length ?

                    list.slice(0, 3).map(item => (
                        <RadioCard onPress={() => onItemPress(item, list)} key={item.id + item.name} radio={item} darkMode={darkMode} />
                    )) : loading && !requestFailed ?

                    <View style={styles.loader}>
                        <ActivityIndicator />
                    </View> : !requestFailed && !list.length ?

                    <View style={styles.empty}>
                        <Text style={{
                            width: '80%',
                            textAlign: 'center',
                            color: !darkMode ? '#000' : '#fff',
                        }}>Список радиостанций пуст</Text>
                    </View> :

                    <View style={styles.loader}>
                        <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            color: !darkMode ? '#000' : '#fff'
                        }}>Ошибка получения данных</Text>
                    </View>
                }
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    height: (Dimensions.get('window').height / 4.75) + 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  block: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
  },
  blockTitle: {
    fontSize: 22,
    fontWeight: '600',
  },
  btnText: {
    fontSize: 13,
    backgroundColor: palette.common.secondary,
    color: palette.dark.text,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  btnWrapper: {
    borderRadius: 17,
    overflow: 'hidden',
  },
  empty: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default RadioAllScreen;
