import React, { useState, useEffect } from 'react';
import { StyleSheet, SafeAreaView, View, ScrollView, Text, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

import { TextInput, RadioCard } from '../../../components';
import CustomHeader from '../../../components/CustomHeader/CustomHeader';

// import SearchIcon from 'icons/search';
// import {searchItems} from '../data'
import { COLORS } from '../../../constants/colors.constants';
import { MINI_PLAYER_HEIGHT } from '../../../constants/styles';
// import RadioPlayer from "../../../components/RadioPlayer/RadioPlayer.component";
import {
  getRadioListByMoodId,
  getRadioStationsByGenreId,
  getSearchRadio,
  resetSearchRadioStations
} from "../../../actions";
// import MusicItem from "../../../components/MusicItem/MusicItem";
// import SvgPlusIcon from "../../../icons/plus";
import SearchBlock from "../../../components/SearchBlock/SearchBlock";
import { MainScreenContainer } from "../../../components/MainScreenContainer/MainScreenContainer";
import { getStatusBarHeight } from "react-native-status-bar-height";

const RadioStationsScreen = ({
  radioStations,
  darkMode,
  getSearchRadio,
  resetSearchRadioStations,
  searchRadioStations,
  radioStationsByMood,
  getRadioListByMoodId,
  errorRadioStations = false,
  requestFailed = false,
  navigation,
  searchStationsLoading = false,
  allStationsLoading = false,
  getRadioStationsByGenreId,
  genreRadioStationsList = [],
  isPlayerActive,
}) => {
  const perPage = 20;
  const [page, setPage] = useState(1);

  const { stationsFromParams = [], isRadioStationsByMoodId = false, moodId, isRadioStationsByGenreId = false, genreId } = navigation.state.params;
  const [wasSearchActivated, setWasSearchActivated] = useState(false);
  // const [currentRadio, setCurrentRadio] = useState(null);
  const [searchValue, setSearchValue] = useState('');

  let stations = searchRadioStations && wasSearchActivated ? searchRadioStations
    : radioStations && !moodId && !genreId ? radioStations
      : isRadioStationsByGenreId && genreId ? genreRadioStationsList
        : radioStationsByMood && moodId ? radioStationsByMood
          : stationsFromParams;

  const loading = searchStationsLoading || allStationsLoading;
  const requestError = requestFailed || errorRadioStations;

  useEffect(() => {
    if (isRadioStationsByMoodId && moodId) {
      getRadioListByMoodId(moodId);
    } else if (isRadioStationsByGenreId && genreId) getRadioStationsByGenreId(genreId);

    return () => {
      resetSearchRadioStations();
      setWasSearchActivated(false);
    }
  }, []);

  const openRadio = track => navigation.navigate('RadioPlayerScreen', { track: track, playlist: stations });


  const handleFetchSearchStart = () => {
    const searchRadioParams = {
      page,
      searchValue: searchValue.toLowerCase(),
      perPage,
    };

    if (moodId) searchRadioParams.moodId = moodId;

    getSearchRadio(searchRadioParams);
    setWasSearchActivated(true);
  };

  const generateContentStations = () => (
    <View style={{ marginBottom: 40, flex: 1, alignItems: 'center', paddingHorizontal: 15 }}>
      {
        stations.map((item, i) => {
          let currentItem = item.radio ? item.radio : item;

          console.log('Radio Stations Screen current Item data: ', currentItem);

          return (
            <RadioCard
              onPress={() => openRadio(currentItem)}
              key={currentItem.id + i + currentItem.name}
              radio={currentItem}
              darkMode={darkMode}
            />
          )
        })
      }
    </View>
  );

  const emptySearchRadioList = (
    <Text style={{
      width: '100%',
      textAlign: 'center',
      color: !darkMode ? '#000' : '#fff',
      marginTop: 35,
    }}>Введите название{"\n"} интересующей Вас радиостанции</Text>
  );

  return (
    <SafeAreaView style={[styles.container, { backgroundColor: darkMode ? COLORS.black : COLORS.white, paddingTop: getStatusBarHeight() + 10, paddingBottom: isPlayerActive ? MINI_PLAYER_HEIGHT : 0 }]}>
      <MainScreenContainer darkMode={darkMode}>
        <CustomHeader title="Радио" pressGoBack={() => navigation.goBack()} darkMode={darkMode} style={{ paddingHorizontal: 15 }} />

        <ScrollView>
          <SearchBlock value={searchValue} onSearchStart={handleFetchSearchStart} onChange={setSearchValue} darkMode={darkMode} style={{ paddingHorizontal: 15 }} />

          <View style={{}}>
            {
              loading ?

                <View style={styles.loading}>
                  <ActivityIndicator />
                </View> : (!searchValue.trim() && !stationsFromParams.length && !isRadioStationsByGenreId) && !wasSearchActivated && !isRadioStationsByMoodId && !requestError ?

                  emptySearchRadioList : !loading && !requestError && stations.length ?

                    generateContentStations() : !loading && !stations.length && !requestError && wasSearchActivated ?

                      <View>
                        <Text style={{
                          width: '100%',
                          textAlign: 'center',
                          color: !darkMode ? '#000' : '#fff',
                          marginTop: 35,
                          lineHeight: 20,
                        }}>Извините, по вашему запросу ничего{"\n"} не найдено :(</Text>
                      </View> : !loading && !stations.length && !requestError && !wasSearchActivated ?

                        <View>
                          <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            color: !darkMode ? '#000' : '#fff',
                            paddingTop: 35,
                          }}>Список радиостанций пуст</Text>
                        </View> :

                        <View>
                          <Text style={{
                            width: '100%',
                            textAlign: 'center',
                            color: !darkMode ? '#000' : '#fff',
                            paddingTop: 35,
                          }}>Ошибка получения данных</Text>
                        </View>
            }
          </View>

          {/*<RadioPlayer ref={playerRef} currentRadio={currentRadio} />*/}
        </ScrollView>
      </MainScreenContainer>
    </SafeAreaView>
  );
};

const mapStateToProps = state => ({
  darkMode: state.appReducer.darkMode,

  searchStationsLoading: state.radioReducer.loading,
  allStationsLoading: state.radioReducer.loadingStations,
  radioStations: state.radioReducer.radioStations,
  errorRadioStations: state.radioReducer.errorRadioStations,

  searchRadioStations: state.radioReducer.searchRadioStations,
  requestFailed: state.radioReducer.error,

  radioStationsByMood: state.radioReducer.radioStationsByMood,

  genreRadioStationsList: state.radioReducer.genreRadioStationsList,

  isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
  getSearchRadio,
  resetSearchRadioStations,

  getRadioListByMoodId,
  getRadioStationsByGenreId,
};

export default connect(mapStateToProps, mapDispatchToProps)(RadioStationsScreen);

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchInputWrapper: {
    flexGrow: 1,
    marginLeft: 12,
  },
  container: {
    paddingVertical: 15,
    // paddingHorizontal: 15,
    flex: 1,
  },
  searchBlock: {
    paddingTop: 8,
    paddingBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15
  },
});
