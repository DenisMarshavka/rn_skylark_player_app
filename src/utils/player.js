import {Platform} from "react-native";
import TrackPlayer from 'react-native-track-player';

import {defaultImage} from "../constants/default_images";
import { getSingersName } from './functions';

export const mapToPlayerData = (list, config = null) => {
    return list.map(item => {
        return mapTrackToPlayerData(item, config);
    });
};

export const getTrackPlayingPath = (item, config) => {
    let url = item.audio;

    if (config) {
        if (config.isDownloadedTracks) {
            url = 'file://' + item.localPath;
        } else if (config.isHighQuality) {
            url = item.hq_audio ? item.hq_audio : item.audio;
        }
    }

    return url;
};

export const mapTrackToPlayerData = (item, config) => {
    const url = getTrackPlayingPath(item, config);
    console.log(item)

    return {
        id: item.id ? '' + item.id : '' + new Date().getTime(),
        url: url ? url : '',
        title: item.name ? item.name : 'Track Title',
        artist: item.singers ? getSingersName(item.singers) : 'Track Artist',
        artwork: item.photo ? '' + item.photo : defaultImage,
    };
};

export const mapToRadioPlayerData = list => {
    return list.map(item => {
        return {
            id: item.id ? '' + item.id : '' + new Date().getTime(),
            type: 'default',
            url: item.url ? item.url : '',
            title: item.name ? item.name : 'Radio Title',
            artist: 'Radio',
            artwork: item.photo ? '' + item.photo : defaultImage,
        };
    });
};

export const durationToSeconds = value => {
    let duration = value ? value.split(':') : '00:00';
    let durationSeconds = +duration[0] * 60 + +duration[1];
    return durationSeconds;
};

export const shuffle = (data) => {
    if (data.length === 1) return data;

    let array = [...data];

    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }

    return array;
};

export const destroyPlayer = async () => {
    await TrackPlayer.destroy();
};

export const removeTrackFromPlayerQueue = async (id) => {
    return await TrackPlayer.remove(id.toString());
};