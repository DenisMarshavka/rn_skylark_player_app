export const wait = (timeOut = 1000) => {
    return new Promise(resolve => {
        setTimeout(resolve, timeOut);
    });
};
