import { Platform, PermissionsAndroid } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import AsyncStorage from "@react-native-community/async-storage";

import { DOWNLOAD_DIR, DOWNLOAD_TRACKS_DATA } from '../constants/storage';
import { getSingersName } from './functions';

export const requestPermissionsAndMakeDir = async () => {
    if (Platform.OS === 'android') {
        try {
            const writeGranted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            );
            if (writeGranted === PermissionsAndroid.RESULTS.GRANTED) {
                const dirStatus = await RNFetchBlob.fs.exists(DOWNLOAD_DIR);

                if (dirStatus) {
                    return true;
                } else {
                    const mkDirStatus = await RNFetchBlob.fs.mkdir(DOWNLOAD_DIR);
                    return true;
                }
            }
        } catch (err) {
            throw 'Произошла ошибка';
        }
    } else {
        return true;
    }
};

export const getFileNameViaPath = (path) => {
    return path.substring(path.lastIndexOf('/') + 1);
};

export const getFileExtensionViaPath = (path) => {
    return path.substring(path.lastIndexOf('.') + 1);
};

export const getFileNameViaTrack = (data) => {
    let singers = getSingersName(data.singers);
    singers = singers ? singers : 'Исполнитель';
    const trackName = data.name ? data.name : 'track';
    let extension = getFileExtensionViaPath(data.audio);
    extension = extension ? extension : 'mp3';
    return `${singers} - ${trackName}.${extension}`;
};

export const getTracksFromStorage = async () => {
    try {
        const data = await AsyncStorage.getItem(DOWNLOAD_TRACKS_DATA);
        return JSON.parse(data) || [];
    } catch (e) {
        console.log(e)
    }
};

export const setTracksToStorage = async (tracks) => {
    try {
        await AsyncStorage.setItem(DOWNLOAD_TRACKS_DATA, JSON.stringify(tracks));
    } catch (e) {
        console.log(e)
    }
};

export const isFileExist = async (path) => {
    try {
        return await RNFetchBlob.fs.exists(path);
    } catch (e) {
        console.log(e)
    }
};

export const addTrackToStorage = async (track) => {
    try {
        if (track && track.id) {
            let data = await getTracksFromStorage();

            let foundIndex = data.findIndex(item => item.id === track.id);
            if (foundIndex !== -1) {
                const oldTrack = data[foundIndex];
                const isExist = await isFileExist(oldTrack.localPath);
                if (isExist) {
                    await RNFetchBlob.fs.unlink(oldTrack.localPath);
                }

                data.splice(foundIndex, 1);
                data.unshift(track);
            } else {
                data.unshift(track);
            }

            await AsyncStorage.setItem(DOWNLOAD_TRACKS_DATA, JSON.stringify(data));
            return true;
        }
        return false;
    } catch (e) {
        console.log(e)
    }
};

export const removeTracksFromStorage = async () => {
    try {
        return await AsyncStorage.removeItem(DOWNLOAD_TRACKS_DATA);
    } catch (e) {
        console.log(e)
    }
};

const deleteFiles = async (data) => {
    let promises = data.map(item => {
        return new Promise((resolve, reject) => {
            RNFetchBlob.fs.exists(item.localPath)
                .then(() => {
                    return RNFetchBlob.fs.unlink(item.localPath);
                })
                .then(() => resolve())
                .catch((err) => reject(err));
        });
    });
    return Promise.all(promises);
};

export const removeFiles = async () => {
    try {
        const data = await getTracksFromStorage();

        await deleteFiles(data);

    } catch (e) {
        console.log(e)
    }
};

export const removeDownloadedTrack = async (track) => {
    try {
        let data = await getTracksFromStorage();

        let foundIndex = data.findIndex(item => item.id === track.id);
        if (foundIndex !== -1) {
            data.splice(foundIndex, 1);
        }
        await AsyncStorage.setItem(DOWNLOAD_TRACKS_DATA, JSON.stringify(data));

        const isExist = await isFileExist(track.localPath);

        if (isExist) {
            await RNFetchBlob.fs.unlink(track.localPath);
        }

        return true;
    } catch (e) {
        console.log(e);
        return false;
    }
};