export const secondsToMMSS = seconds => {
    let min = Math.floor(seconds / 60)
    let sec = Math.floor(seconds % 60)

    let mDisplay = min < 10 ? `0${min}` : `${min}`
    let sDisplay = sec < 10 ? `0${sec}` : `${sec}`

    return `${mDisplay}:${sDisplay}`
};

export const getSingersName = singers => {
    const names = singers.map((item) => item.name);
    return names.join(' & ');
};

export const checkSingersMusicItem = (singerName = '', musicData = {}) => {
    let singersMusicData = singerName ? singerName : musicData && musicData.performer ? musicData.performer : '';

    if (!singersMusicData.trim()) {
        if (musicData && musicData.singers && musicData.singers.length) {
            for (let singerIndex = 0; singerIndex < musicData.singers.length; singerIndex++) {
                if (musicData.singers[singerIndex] && musicData.singers[singerIndex].name) {
                    singersMusicData += (singerIndex === 0 || singerIndex === musicData.singers.length - 1) ? musicData.singers[singerIndex].name : ' & ' + musicData.singers[singerIndex].name;
                }
            }
        }
    }
    // console.log('singers Music Item Data: ', singersMusicData);

    return singersMusicData
};
