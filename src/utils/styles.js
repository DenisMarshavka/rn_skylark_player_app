import {StyleSheet} from 'react-native';

export const mainStyles = StyleSheet.create({
    safeAreaViewContainer: {
        flex: 1,
        height: '100%',
        width: "100%",
        maxWidth: 650,
        margin: 0,
        justifyContent: 'flex-start',
        marginLeft: 'auto',
        marginRight: 'auto',
    }
});
