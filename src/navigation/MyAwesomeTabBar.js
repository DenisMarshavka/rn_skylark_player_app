import React from 'react';
import { Animated, View, Keyboard } from 'react-native';
import { connect } from "react-redux";
import { BottomTabBar } from "react-navigation-tabs";
import {Dimensions} from 'react-native';

import { palette } from "../state/palette";
import MiniPlayer from '../components/MiniPlayer/MiniPlayer';
import { miniPlayerStop } from "../actions/miniPlayer";

const TAB_BAR_OFFSET = 250;

class MyAwesomeTabBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            offset: new Animated.Value(0),
            isKeyboardVisible: false,
        };
    }

    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this.setState({isKeyboardVisible: true}));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.setState({isKeyboardVisible: false}));
    }

    UNSAFE_componentWillReceiveProps(props) {
        const wasVisible = props.showBottomBar;
        const isVisible = this.props.showBottomBar;

        if (wasVisible && !isVisible) {
            Animated.timing(this.state.offset, { toValue: 0, duration: 0, useNativeDriver: false }).start();
        } else if (isVisible && !wasVisible) {
            Animated.timing(this.state.offset, { toValue: TAB_BAR_OFFSET, duration: 0, useNativeDriver: false }).start();
        }
    }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
        this.props.miniPlayerStop();
    }

    render() {
        return (
            <>
                {this.props.isPlayerActive && (
                    <View
                        style={{
                            position: 'absolute',
                            bottom: this.props.showBottomBar ? 0 : -TAB_BAR_OFFSET,
                            padding: 0,
                            width: '100%',
                            paddingBottom: 54,
                            translateY: this.state.isKeyboardVisible ? 54 : 0,
                            backgroundColor: this.props.darkMode ? "#232323" : "#fff",
                            borderTopWidth: 1,
                            borderColor: this.props.darkMode ? "#232323" : "rgba(0, 0, 0, 0.1)",
                        }}
                    >
                        <MiniPlayer navigation={this.props.navigation} />
                    </View>
                )}

                {this.props.showBottomBar && <BottomTabBar
                    { ...this.props }
                    activeTintColor={!this.props.darkMode ? palette.common.primary : palette.common.secondary}
                    inactiveTintColor={palette.common.titleGrey}
                    style={{
                        borderTopLeftRadius: 18,
                        borderTopRightRadius: 18,
                        borderTopWidth: 0,
                        backgroundColor: this.props.darkMode ? "#232323" : "#fff",
                        translateY: this.state.offset,
                        position: 'absolute',
                        bottom: 0,
                        padding: 0,
                        height: 54,
                        zIndex: 8,
                        shadowColor: this.props.darkMode ? "#fff" : 'grey',
                        // shadowOffset: { width: 42, height: 42 },
                        // shadowOpacity: 0.1,
                        // shadowRadius: 20,
                        // elevation: 3,

                        // shadowColor: 'grey',
                        shadowOffset: { width: 2, height: 2 },
                        shadowOpacity: 0.1,
                        shadowRadius: 10,
                        elevation: 3,
                    }}
                />}
            </>
        );
    }
}

const mapStateToProps = state => ({
    darkMode: state.appReducer.darkMode,
    showBottomBar: state.appReducer.showBottomBar,
    isPlayerActive: state.miniPlayerReducer.isActive,
});

const mapDispatchToProps = {
    miniPlayerStop,
};

export default connect(mapStateToProps, mapDispatchToProps)(MyAwesomeTabBar)
