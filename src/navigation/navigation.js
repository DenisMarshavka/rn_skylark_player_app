import React from 'react';
import { createAppContainer } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

// Import Authorization Navigator
import AuthorizationNavigator from '../screens/AuthorizationScreen/Authorization.navigator';

// Import Search Navigator
import SearchMusicScreen from "./../screens/SearchScreen/SearchMusicScreen/SearchMusicScreen.component";

// Import Radio Navigator
import RadioNavigator from '../screens/RadioScreen/Radio.navigator';

// Import Profile Navigator
import ProfileNavigator from '../screens/ProfileScreen/Profile.navigator';

// Import SearchMusic Navigator
import SearchMusicTabNavigator from '../screens/SearchMusicTabScreen/SearchMusicTabScreen.navigator';
// import TracksScreen from '../screens/TracksScreen/TracksScreen';

//Icons
import MusicTabIcon from "../icons/bottomNav/MusicTabIcon";
import ProfileTabIcon from "../icons/bottomNav/ProfileTabIcon";
import RadioTabIcon from "../icons/bottomNav/RadioTabIcon";
import SearchTabIcon from "../icons/bottomNav/SearchTabIcon";
import MyAwesomeTabBar from "./MyAwesomeTabBar";


const GlobalBottomNavigator = createBottomTabNavigator(
    {
        SearchMusicTabNavigator : {
            screen : SearchMusicTabNavigator,
            navigationOptions: {
                title: 'Музыка',
                tabBarIcon: ({ focused }) => <MusicTabIcon focused={focused} />,
            }
        },
        SearchNavigator : {
            screen : SearchMusicScreen,
            navigationOptions: {
                title: 'Поиск',
                tabBarIcon: ({ focused }) => <SearchTabIcon focused={focused} />,
            }
        },
        RadioNavigator: {
            screen : RadioNavigator,
            navigationOptions: {
                title: 'Радио',
                tabBarIcon: ({ focused }) => <RadioTabIcon focused={focused}  />,
            }
        },
        ProfileNavigator : {
            screen : ProfileNavigator,
            navigationOptions: {
                title: 'Профиль',
                tabBarIcon: ({ focused }) => <ProfileTabIcon focused={focused} />,
            }
        },
    },
    {
        initialRouteName: 'SearchMusicTabNavigator',
        tabBarComponent: MyAwesomeTabBar,
    }
);

const RootNavigator = createStackNavigator(
    {
        Authorization: AuthorizationNavigator,
        Profile: GlobalBottomNavigator
    },
    {
        initialRouteName: 'Authorization',
        headerMode: 'none'
    }
  );

const AppNavigator = createAppContainer(RootNavigator);

export default AppNavigator;
