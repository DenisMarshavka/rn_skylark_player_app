import TrackPlayer from 'react-native-track-player';

import store from '../store/store'
import { setTrackListening, setRadioStationListening } from '../actions';
import { playerSetTrackAction } from '../actions/player';
import { radioPlayerSetTrackAction } from '../actions/radioPlayer';
import {
    miniPlayerStop,
    miniPlayerSetTrackAction,
} from '../actions/miniPlayer';

let didAddEventListeners = false;

module.exports = async function() {
    if (didAddEventListeners) {
        return;
    } else {
        didAddEventListeners = true;
    }

    let isSubscribed = store.getState().appReducer.isSubscribed
    let isActiveSubscription = store.getState().appReducer.isActiveSubscription

    if (isSubscribed || isActiveSubscription){
        // Forward remote events to the player
        TrackPlayer.addEventListener('remote-play', data => {
            TrackPlayer.play();
        });
    }

    TrackPlayer.addEventListener('remote-pause', data => {
        TrackPlayer.pause();
    });

    TrackPlayer.addEventListener('remote-stop', data => {
        // TrackPlayer.stop();
        TrackPlayer.destroy();
    });

    TrackPlayer.addEventListener('remote-next', async data => {
        try {
            await onRemoteSkip('remote-next');
        } catch (_) {}
    });

    TrackPlayer.addEventListener('remote-previous', async data => {
        try {
            await onRemoteSkip('remote-previous');
        } catch (_) {}
    });


    // Playback updates
    TrackPlayer.addEventListener('playback-track-changed', async data => {
        await onTrackChanged(data);
    });

    TrackPlayer.addEventListener('playback-queue-ended', async data => {
        await onQueueEnded(data);
    });

    TrackPlayer.addEventListener('playback-error', async data => {
        store.dispatch(miniPlayerStop());
        let appReducer = store.getState().appReducer;
        alert(appReducer.isRadioPlayer ? 'Радио недоступно' : 'Произошла ошибка');
    });
};

const onTrackChanged = async (data) => {
    let appReducer = store.getState().appReducer;
    let playerReducer = store.getState().playerReducer;
    let radioPlayerReducer = store.getState().radioPlayerReducer;

    const track = await TrackPlayer.getTrack(data.track);
    const nextTrack = await TrackPlayer.getTrack(data.nextTrack);
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (track && nextTrack && nextTrack.id) {
        let trackData = null;
        if (!appReducer.isRadioPlayer) {
            const trackState = playerReducer.track;
            trackData = playerReducer.playlist.find(item => item.id === +nextTrack.id);

            if (trackState.id !== trackData.id) {
                store.dispatch(setTrackListening(trackData.id));
            }

            store.dispatch(playerSetTrackAction({ track: trackData }));
        } else {
            const trackState = radioPlayerReducer.track;
            trackData = radioPlayerReducer.playlist.find(item => item.id === +nextTrack.id);

            if (trackState.id !== trackData.id) {
                store.dispatch(setRadioStationListening(trackData.id));
            }

            store.dispatch(radioPlayerSetTrackAction({ track: trackData }));
        }

        store.dispatch(miniPlayerSetTrackAction({ track: trackData }));
    }
};

const onQueueEnded = async (data) => {
    let appReducer = store.getState().appReducer;
    let playerReducer = store.getState().playerReducer;

    if(!appReducer.isRadioPlayer) {
        if (playerReducer.isRepeat && data.track) {
            await TrackPlayer.skip(data.track);
        } else if (appReducer.infinityPlaying && data.track) {
            const queue = await TrackPlayer.getQueue();
            const trackId = queue[0].id;
            await TrackPlayer.skip(trackId);
        }
    } else {

    }

    // draft for not blink notification player issue
    // let playerReducer = store.getState().playerReducer;
    // if(playerReducer.isRepeat && data.track) {
    //     return new Promise((resolve, reject) => {
    //         return resolve(true);
    //     })
    //         .then(() => {
    //             return TrackPlayer.getCurrentTrack();
    //         })
    //         .then((id) => {
    //             return TrackPlayer.getTrack(id);
    //         })
    //         .then(trackData => {
    //             return TrackPlayer.remove([trackData.id]).then(() => trackData);
    //         })
    //         .then(trackData => {
    //             return TrackPlayer.add(trackData).then(() => trackData);
    //         })
    //         .then((trackData) => TrackPlayer.pause().then(() => trackData))
    //         .then(trackData => {
    //             return TrackPlayer.skip(trackData.id);
    //         })
    //         .then(() => {
    //             return TrackPlayer.play();
    //         });
    // }
};

const onRemoteSkip = async (event) => {
    let appReducer = store.getState().appReducer;
    let playerReducer = store.getState().playerReducer;

    if (!appReducer.isRadioPlayer) {
        const currentTrack = await TrackPlayer.getCurrentTrack();

        if (playerReducer.isRepeat) {
            TrackPlayer.skip(currentTrack);
        } else {
            const queue = await TrackPlayer.getQueue();
            const firstTrackId = queue[0].id;
            const lastTrackId = queue[queue.length - 1].id;

            if (currentTrack === lastTrackId && event === 'remote-next' && appReducer.infinityPlaying) {
                await TrackPlayer.skip(firstTrackId);
            } else if (currentTrack === firstTrackId && event === 'remote-previous' && appReducer.infinityPlaying) {
                await TrackPlayer.skip(lastTrackId);
            } else {
                if (event === 'remote-next') {
                    await TrackPlayer.skipToNext();
                } else {
                    await TrackPlayer.skipToPrevious();
                }
            }
        }
    } else {
        if (event === 'remote-next') {
            await TrackPlayer.skipToNext();
        } else {
            await TrackPlayer.skipToPrevious();
        }
    }
};