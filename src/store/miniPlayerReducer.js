import { types } from "../actions/miniPlayer";

const initialState = {
    track: null,
    isActive: false,
    loading: false,
    error: null
};

export const miniPlayerReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.MINI_PLAYER_START:
            return {
                ...state,
                track: action.payload.track,
                isActive: true,
            };
        case types.MINI_PLAYER_SET_TRACK:
            return {
                ...state,
                track: action.payload.track,
            };
        case types.MINI_PLAYER_STOP:
            return {
                ...state,
                track: null,
                isActive: false,
            };
        default:
            return state;
    }
};
