import { types } from "../actions/player";

import { durationToSeconds } from '../utils/player';

const initialState = {
    track: null,
    duration: 0,
    playlist: [],
    shuffleQueue: [],
    initScreen: false,
    isRepeat: false,
    isShuffle: false,
    loading: false,
    error: null
};

export const playerReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.PLAYER_INIT:
            return {
                ...state,
                track: action.payload.track,
                playlist: action.payload.playlist,
                duration: durationToSeconds(action.payload.track.duration),
                initScreen: true,
            };
        case types.PLAYER_SET_TRACK:
            return {
                ...state,
                track: action.payload.track,
                duration: durationToSeconds(action.payload.track.duration),
            };
        case types.PLAYER_SET_REPEAT:
            return {
                ...state,
                isRepeat: action.payload.isRepeat,
            };
        case types.PLAYER_SET_SHUFFLE:
            return {
                ...state,
                isShuffle: action.payload.isShuffle,
            };
        case types.PLAYER_SET_SHUFFLE_QUEUE:
            return {
                ...state,
                shuffleQueue: action.payload.shuffleQueue,
            };
        case types.PLAYER_REMOVE_TRACK_FROM_PLAYLIST:
            return {
                ...state,
                playlist: state.playlist.filter(item => +item.id !== +action.payload),
                shuffleQueue: state.shuffleQueue.filter(item => +item.id !== +action.payload),
            };
        case types.PLAYER_RESET:
            return {
                ...state,
                initScreen: false,
            };
        default:
            return state;
    }
};
