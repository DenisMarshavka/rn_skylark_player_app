import AsyncStorage from "@react-native-community/async-storage";
import { authAPI } from '../api/api'
import {SET_USER_DATA, START_GET_USER_DATA} from "../constants/ActionsTypes";
import { toggleSubscribe } from "./appReducer";

let initialState = {
    loading: false,
    isSignIn: false,
    errorMessage: '',
    errorLoginMessage: '',
    userName: null,
    userEmail: null,
    userPhone: null
};

export const authReducer = (state = initialState, action) => {
	switch (action.type) {
        case SIGN_IN:
            return { ...state, isSignIn: action.payload };

        case ERROR:
            return { ...state, errorMessage: action.payload, loading: false, };

        case ERROR_LOGIN_MESSAGE:
            return { ...state, errorLoginMessage: action.payload, loading: false, };

        case START_GET_USER_DATA:
            return {
                ...state,
                loading: true,
            };

        case SET_USER_DATA:
            return {
                ...state,
                loading: false,
                userName: action.payload.name,
                userEmail: action.payload.email,
                userPhone: action.payload.phone,
            };

		default: return state;
	}
};

const SIGN_IN = 'authReducer/SIGN_IN';
const ERROR = 'authReducer/ERROR';
const ERROR_LOGIN_MESSAGE = 'authReducer/ERROR_LOGIN_MESSAGE';
const USER_LOGOUT = 'USER_LOGOUT';

export const logout = () => ({
    type: USER_LOGOUT,
})

export const isSignInAction = bool => ({
    type: SIGN_IN,
    payload: bool
});

export const setErrorMessage = text => ({
    type: ERROR,
    payload: text
});

export const setErrorLoginMessage = text => ({
    type: ERROR_LOGIN_MESSAGE,
    payload: text
});

export const isUserSignIn = () => async dispatch => {
    try {
        let token = await getToken();
        console.log(`user is sign in ${token}`);
        if (!!token) {
            dispatch(isSignInAction(true));
            console.log('user is sign in')
        } else {
            dispatch(isSignInAction(false));
            console.log('user no sign in')
        }
    } catch (e) {
        console.log('Error User sign in: ', e)
    }
};

export const registration = (name, phone, email, password) => async dispatch => {
    try {
        let data = await authAPI.registration(name, phone, email, password);
        console.log('Registration data', data);

        if (data.message === 'Success') {
            console.log('sign up success');
            return true
        }

        dispatch(setErrorMessage(data.message));
        return false
    } catch (e) {
        console.log(e)
        alert(e)
    }
};

export const login = (phone, password, isEmail) => async dispatch => {
    try {
        dispatch(setErrorLoginMessage(''));
        let data = await authAPI.login(phone, password, isEmail);
        console.log('login data', data);

        if (data.message === 'Success') {
            await AsyncStorage.setItem('token', data.data.token.toString())
            await AsyncStorage.setItem('user_id', data.data.user_id.toString())
            await AsyncStorage.setItem('isSubscribed', data.data.subscribe.toString())
            console.log('login success', data.data.token.toString())

            dispatch(isSignInAction(true))
            if (data.data.subscribe === 1){
                dispatch(toggleSubscribe(true))
            } else {
                dispatch(toggleSubscribe(false))
            }

            return true
        } else {
            dispatch(isSignInAction(false))
            if (data.message === 'Wrong Credentials') {
                dispatch(setErrorLoginMessage('Вы ввели неверные данные!'));
            } else {
                dispatch(setErrorLoginMessage(data.message));
            }
        }
        return false
    } catch (e) {
        console.log(e)
        alert(e)
    }
};

export const getToken = async () => {
    try {
        return await AsyncStorage.getItem('token');
    } catch (error) {
        console.log('Error generate Token: ', error);
    }
};

export const getUserId = async () => {
    try {
        return await AsyncStorage.getItem('user_id');
    } catch (error) {
        console.log('Error getting user_id ', error);
    }
};

export const removeToken = async () => {
    try {
        await AsyncStorage.removeItem('token')
        await AsyncStorage.removeItem('user_id')
    } catch (error) {
        console.log('Error remove Token: ', error)
    }
};
