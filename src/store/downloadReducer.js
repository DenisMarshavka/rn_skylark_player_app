import { types } from "../actions/download";

const initialState = {
    playlist: [],
    pendingList: [],
    loading: false,
    error: null,
    isDownloaded: false,
    downloading: false,
    downloadError: null,
    percent: 0,
};

export const downloadReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_DOWNLOADED_TRACKS_STARTED:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case types.DOWNLOAD_SET_DOWNLOADED_TRACKS:
            return {
                ...state,
                loading: false,
                playlist: [...action.payload],
                error: null,
            };
        case types.GET_DOWNLOADED_TRACKS_FAILED:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
            };
        case types.DOWNLOAD_ADD_TRACK_PENDING: {
            const id = action.payload;
            return {
                ...state,
                pendingList: state.pendingList.includes(id) ? state.pendingList : [ ...state.pendingList, id ],
            };
        }
        case types.DOWNLOAD_REMOVE_TRACK_PENDING: {
            const id = action.payload;
            return {
                ...state,
                pendingList: state.pendingList.includes(id) ? state.pendingList.filter(i => i !== id) : state.pendingList,
            };
        }
        case types.DOWNLOAD_FETCH_TRACK_START:
            return {
                ...state,
                isDownloaded: false,
                downloading: true,
                percent: 0,
                downloadError: null,
            };
        case types.DOWNLOAD_FETCH_TRACK_PROGRESS:
            return {
                ...state,
                percent: action.payload,
            };
        case types.DOWNLOAD_FETCH_TRACK_SUCCESS:
            return {
                ...state,
                isDownloaded: true,
                downloading: false,
                downloadError: null,
            };
        case types.DOWNLOAD_FETCH_TRACK_ERROR:
            return {
                ...state,
                isDownloaded: false,
                downloading: false,
                percent: 0,
                downloadError: true,
            };
        case types.DOWNLOAD_FETCH_TRACK_RESET:
            return {
                ...state,
                isDownloaded: false,
                downloading: false,
                percent: 0,
                downloadError: null,
            };
        default:
            return state;
    }
};
