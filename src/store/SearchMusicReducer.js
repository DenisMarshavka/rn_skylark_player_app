import {
    CLEAN_SEARCH_MUSIC_LISTS_DATA,
    ERROR_MOODS_LIST_REQUEST,
    ERROR_SINGERS_LIST_REQUEST,
    ERROR_TRACK_LIST_TOP_REQUEST,
    // GET_DATA_FAILED,
    GET_DATA_MOODS_START,
    GET_DATA_START_TOP_TACK_LIST,
    GET_MONTH_POPULAR_TRACKS_DATA_FAILED,
    GET_MONTH_POPULAR_TRACKS_DATA_START,
    GET_MONTH_POPULAR_TRACKS_DATA_SUCCESS,
    GET_MOODS_SUCCESS,
    GET_SINGERS_SUCCESS,
    GET_START_SINGERS_TACK_LIST_DATA,
    GET_TRACK_LIST_TOP_SUCCESS,
    GET_WEEK_POPULAR_TRACKS_DATA_FAILED,
    GET_WEEK_POPULAR_TRACKS_DATA_START,
    GET_WEEK_POPULAR_TRACKS_DATA_SUCCESS,
    GET_POPULAR_SINGERS_SUCCESS,
    GET_START_POPULAR_SINGERS_TACK_LIST_DATA,
    ERROR_POPULAR_SINGERS_LIST_REQUEST
} from "../constants/ActionsTypes";

const initialState = {
    // loading: false,
    // error: null,
    topTrackListLoading: false,
    trackListTop: [],
    errorTopTrackList: null,

    singersTackListLoading: false,
    singersList: [],
    errorSingersList: null,

    isPopularSingersTackListLoading: false,
    popularSingersList: [],
    errorPopularSingersListRequest: null,

    moodsListLoading: false,
    moodsList: [],
    errorMoodsList: null,

    weekListLoading: false,
    weekList: [],
    errorWeekList: null,

    monthListLoading: false,
    monthList: [],
    errorMonthList: null,
};

export default function searchMusicReducer(state = initialState, action) {
    switch (action.type) {
        case GET_WEEK_POPULAR_TRACKS_DATA_START:
            return {
                ...state,
                weekListLoading: true,
                errorWeekList: null,
            };

        case GET_WEEK_POPULAR_TRACKS_DATA_SUCCESS:
            return {
                ...state,
                weekListLoading: false,
                weekList: [...action.payload],
            };

        case GET_WEEK_POPULAR_TRACKS_DATA_FAILED:
            return {
                ...state,
                weekListLoading: false,
                weekList: action.payload.error,
            };

        case GET_MONTH_POPULAR_TRACKS_DATA_START:
            return {
                ...state,
                monthListLoading: true,
                errorWeekList: null,
            };

        case GET_MONTH_POPULAR_TRACKS_DATA_SUCCESS:
            return {
                ...state,
                monthListLoading: false,
                monthList: [...action.payload],
            };

        case GET_MONTH_POPULAR_TRACKS_DATA_FAILED:
            return {
                ...state,
                monthListLoading: false,
                monthList: action.payload.error,
            };




        case GET_DATA_START_TOP_TACK_LIST:
            return {
                ...state,
                topTrackListLoading: true,
            };

        case GET_TRACK_LIST_TOP_SUCCESS:
            return {
                ...state,
                topTrackListLoading: false,
                errorTopTrackList: null,
                trackListTop: [...action.payload]
            };

        case ERROR_TRACK_LIST_TOP_REQUEST:
            return {
                ...state,
                topTrackListLoading: false,
                errorTopTrackList: action.payload.error,
            };

        case GET_START_SINGERS_TACK_LIST_DATA:
            return {
                ...state,
                singersTackListLoading: true,
            };

        case GET_SINGERS_SUCCESS:
            return {
                ...state,
                singersTackListLoading: false,
                errorSingersList: null,
                singersList: [...action.payload]
            };

        case ERROR_SINGERS_LIST_REQUEST:
            return {
                ...state,
                singersTackListLoading: false,
                errorSingersList: action.payload.error,
            };

        case GET_START_POPULAR_SINGERS_TACK_LIST_DATA:
            return {
                ...state,
                isPopularSingersTackListLoading: true,
            };

        case GET_POPULAR_SINGERS_SUCCESS:
            return {
                ...state,
                isPopularSingersTackListLoading: false,
                errorPopularSingersListRequest: null,
                popularSingersList: [...action.payload]
            };

        case ERROR_SINGERS_LIST_REQUEST:
            return {
                ...state,
                isPopularSingersTackListLoading: false,
                errorPopularSingersListRequest: action.payload.error,
            };

        case GET_DATA_MOODS_START:
            return {
                ...state,
                moodsListLoading: true,
            };

        case GET_MOODS_SUCCESS:
            return {
                ...state,
                moodsListLoading: false,
                errorMoodsList: null,
                moodsList: [...action.payload],
            };

        case ERROR_MOODS_LIST_REQUEST:
            return {
                ...state,
                moodsListLoading: false,
                errorMoodsList: action.payload.error,
            };

        // case GET_DATA_FAILED:
        //     return {
        //         ...state,
        //         loading: false,
        //         error: action.payload.error
        //     };

        default:
            return state;
    }
}
