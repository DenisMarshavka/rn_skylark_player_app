import {
    CLEAR_SINGER_DATA,
    GET_SINGER_ALBUMS_DATA_START,
    GET_SINGER_ALBUMS_FAILED,
    GET_SINGER_ALBUMS_SUCCESS,
    GET_SINGER_DATA_ERROR,
    GET_SINGER_DATA_STARTED,
    GET_SINGER_TRACK_LIST_DATA_FAILED,
    GET_SINGER_TRACK_LIST_DATA_START,
    GET_SINGER_TRACK_LIST_SUCCESS,
    GET_TRACK_BY_ALBUM_ID_FAILED,
    GET_TRACK_BY_ALBUM_ID_START,
    GET_TRACK_BY_ALBUM_ID_SUCCESS,
} from "../constants/ActionsTypes";

const initialState = {
    loading: true,
    error: null,

    singerTrackList: [],

    loadingSingerAlbums: false,
    singerAlbums: [],
    errorSingerAlbums: null,

    singerId: null,
    loadingSingerPopularTracks: false,
    singerPopularTracks: [],
    errorSingerPopularTracks: null,

    albumTracksLoading: false,
    albumTracksError: null,
    albumTracks: [],
};

export default function singerReducer(state = initialState, action) {
    switch (action.type) {
        case GET_SINGER_DATA_STARTED:
            return {
                ...state,
                loading: true
            };

        case GET_SINGER_TRACK_LIST_DATA_START:
            return {
                ...state,
                loadingSingerPopularTracks: true,
                errorSingerPopularTracks: null,
                singerPopularTracks: [],
            };

        case GET_SINGER_TRACK_LIST_SUCCESS:
            return {
                ...state,
                loadingSingerPopularTracks: false,
                errorSingerPopularTracks: null,
                singerId: action.payload.singerId,
                singerPopularTracks: [...action.payload.singerTracks],
            };

        case GET_SINGER_TRACK_LIST_DATA_FAILED:
            return {
                ...state,
                loadingSingerPopularTracks: false,
                singerPopularTracks: [],
                errorSingerPopularTracks: action.payload.error
            };

        case GET_SINGER_ALBUMS_DATA_START:
            return {
                ...state,
                loadingSingerAlbums: true,
                errorSingerAlbums: null,
            };

        case GET_SINGER_ALBUMS_SUCCESS:
            return {
                ...state,
                loadingSingerAlbums: false,
                errorSingerAlbums: null,
                singerAlbums: [...action.payload]
            };

        case GET_SINGER_ALBUMS_FAILED:
            return {
                ...state,
                loadingSingerAlbums: false,
                errorSingerAlbums: action.payload.error,
                singerAlbums: []
            };

        case CLEAR_SINGER_DATA:
            return {
                ...state,
                loading: true,
                error: null,
                singerTrackList: [],
                loadingSingerAlbums: false,
                singerAlbums: [],
                errorSingerAlbums: null,
                singerId: null,
                loadingSingerPopularTracks: false,
                singerPopularTracks: [],
                errorSingerPopularTracks: null,
            };

        case GET_SINGER_DATA_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload.error
            };

        case GET_TRACK_BY_ALBUM_ID_START:
            return {
                ...state,
                albumTracksLoading: true,
                albumTracksError: null,
            };

        case GET_TRACK_BY_ALBUM_ID_SUCCESS:
            return {
                ...state,
                albumTracksLoading: false,
                albumTracks: [...action.payload],
            };

        case GET_TRACK_BY_ALBUM_ID_FAILED:
            return {
                ...state,
                albumTracksLoading: false,
                albumTracksError: action.payload.error,
            };

        default:
            return state;
    }
}
