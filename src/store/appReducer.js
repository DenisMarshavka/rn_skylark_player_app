import {
    CHECK_LIKED_TRACK_FAILED,
    CHECK_LIKED_TRACK_START,
    CHECK_LIKED_TRACK_SUCCESS, GET_RECENT_LISTENING_FAILED, GET_RECENT_LISTENING_START, GET_RECENT_LISTENING_SUCCESS,
    TOGGLE_BOTTOM_BAR
} from "../constants/ActionsTypes";
import AsyncStorage from "@react-native-community/async-storage";

let initialState = {
    darkMode: false,
    isSubscribed: false,
    isActiveSubscription: false,
    infinityPlaying: true,
    tracksAutosave: false,
    orderNew: false,
    showBottomBar: true,
    isRadioPlayer: false,

    likedTrackLoading: false,
    trackLikedTrackStatus: false,
    likedTrackError: null,

    recentListeningLoading: false,
    recentListeningData: [],
    recentListeningError: null,
};

export const appReducer = (state = initialState, action) => {
	switch (action.type) {
        case DARK_MODE:
            return { ...state, darkMode: action.payload }

        case SUBSCRIBE:
            return { ...state, isSubscribed: action.payload }

        case INFINITY_PLAYING:
            return { ...state, infinityPlaying: action.payload }

        case TRACKS_AUTOSAVE:
            return { ...state, tracksAutosave: action.payload }

        case ORDER_NEW:
            return { ...state, orderNew: action.payload }

        case TOGGLE_BOTTOM_BAR:
            return {
                ...state,
                showBottomBar: action.payload
            };

        case APP_PLAYER_TYPE:
            return {
                ...state,
                isRadioPlayer: action.payload
            };

        case CHECK_LIKED_TRACK_START:
            return {
                ...state,
                likedTrackLoading: true,
                likedTrackError: null,
            };

        case CHECK_LIKED_TRACK_SUCCESS:
            return {
                ...state,
                likedTrackLoading: false,
                trackLikedTrackStatus: action.payload,
                likedTrackError: null,
            };

        case CHECK_LIKED_TRACK_FAILED:
            return {
                ...state,
                likedTrackLoading: false,
                likedTrackError: action.payload.error,
            };


        case GET_RECENT_LISTENING_START:
            return {
                ...state,
                recentListeningLoading: true,
                recentListeningError: null,
            };

        case GET_RECENT_LISTENING_SUCCESS:
            return {
                ...state,
                recentListeningLoading: false,
                recentListeningData: action.payload,
            };

        case GET_RECENT_LISTENING_FAILED:
            return {
                ...state,
                recentListeningLoading: false,
                recentListeningData: [],
                recentListeningError: action.payload.error,
            };

        case ACTIVE_SUBSCRIPTION: 
            return {
                ...state,
                isActiveSubscription: action.payload
            }

		default: return state;
	}
};

const DARK_MODE = 'appReducer/DARK_MODE';
const SUBSCRIBE = 'appReducer/SUBSCRIBE';
const ACTIVE_SUBSCRIPTION = 'appReducer/ACTIVE_SUBSCRIPTION';
const INFINITY_PLAYING = 'appReducer/INFINITY_PLAYING';
const TRACKS_AUTOSAVE = 'appReducer/TRACKS_AUTOSAVE';
const ORDER_NEW = 'appReducer/ORDER_NEW';
const APP_PLAYER_TYPE = 'appReducer/APP_PLAYER_TYPE';

export const setDarkMode = bool => ({
    type: DARK_MODE,
    payload: bool
});

export const setSubscribe = bool => ({
    type: SUBSCRIBE,
    payload: bool
});

export const setActiveSubscription = bool => ({
    type: ACTIVE_SUBSCRIPTION,
    payload: bool
});

export const setInfinityPlaying = bool => ({
    type: INFINITY_PLAYING,
    payload: bool
});

export const setTracksAutosave = bool => ({
    type: TRACKS_AUTOSAVE,
    payload: bool
});

export const setOrderNew = bool => ({
    type: ORDER_NEW,
    payload: bool
});

export const toggleBottomBar = status => ({
    type: TOGGLE_BOTTOM_BAR,
    payload: status
});

export const togglePlayerType = status => ({
    type: APP_PLAYER_TYPE,
    payload: status
});


export const toggleTheme = (bool) => async dispatch => {
    try {
        dispatch(setDarkMode(bool))
        await AsyncStorage.setItem('darkMode', bool ? '1' : '0')
        return true
    } catch (e) {
        console.log(e)
    }
}

export const toggleSubscribe = (bool) => async dispatch => {
    try {
        dispatch(setSubscribe(bool))
        await AsyncStorage.setItem('isSubscribed', bool ? '1' : '0')
        return true
    } catch (e) {
        console.log(e)
    }
}

export const toggleActiveSubscription = (bool) => async dispatch => {
    try {
        dispatch(setActiveSubscription(bool))
        await AsyncStorage.setItem('isActiveSubscription', bool ? '1' : '0')
        return true
    } catch (e) {
        console.log(e)
    }
}

export const toggleInfinityPlaying = (bool) => async dispatch => {
    try {
        dispatch(setInfinityPlaying(bool))
        await AsyncStorage.setItem('infinityPlaying', bool ? '1' : '0')
        return true
    } catch (e) {
        console.log(e)
    }
}

export const toggleTracksAutosave = (bool) => async dispatch => {
    try {
        dispatch(setTracksAutosave(bool))
        await AsyncStorage.setItem('tracksAutosave', bool ? '1' : '0')
        return true
    } catch (e) {
        console.log(e)
    }
}

export const toggleOrderNew = (bool) => async dispatch => {
    try {
        dispatch(setOrderNew(bool))
        await AsyncStorage.setItem('orderNew', bool ? '1' : '0')
        return true
    } catch (e) {
        console.log(e)
    }
}


export const getSettings = () => async dispatch => {
    try {
        let theme = await AsyncStorage.getItem('darkMode');
        let isSubscribed = await AsyncStorage.getItem('isSubscribed');
        let isActiveSubscription = await AsyncStorage.getItem('isActiveSubscription');
        let infinityPlaying = await AsyncStorage.getItem('infinityPlaying');
        let tracksAutosave = await AsyncStorage.getItem('tracksAutosave');
        let orderNew = await AsyncStorage.getItem('orderNew');

        theme === '1'           ? dispatch(setDarkMode(true))           : dispatch(setDarkMode(false))
        isSubscribed === '1'    ? dispatch(setSubscribe(true))          : dispatch(setSubscribe(false))
        isActiveSubscription === '1' ? dispatch(setActiveSubscription(true)) : dispatch(setActiveSubscription(false))
        infinityPlaying === '1' ? dispatch(setInfinityPlaying(true))    : dispatch(setInfinityPlaying(false))
        tracksAutosave === '1'  ? dispatch(setTracksAutosave(true))     : dispatch(setTracksAutosave(false))
        orderNew === '1'        ? dispatch(setOrderNew(true))           : dispatch(setOrderNew(false))

        return theme
    } catch (error) {
        console.log(error);
    }
}
