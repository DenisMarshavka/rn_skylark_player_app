import { types } from "../actions/radioPlayer";

const initialState = {
    track: null,
    playlist: [],
    initScreen: false,
    loading: false,
    error: null
};

export const radioPlayerReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.RADIO_PLAYER_INIT:
            return {
                ...state,
                track: action.payload.track,
                playlist: action.payload.playlist,
                initScreen: true,
            };
        case types.RADIO_PLAYER_SET_TRACK:
            return {
                ...state,
                track: action.payload.track,
            };
        case types.RADIO_PLAYER_RESET:
            return {
                ...state,
                initScreen: false,
            };
        default:
            return state;
    }
};
