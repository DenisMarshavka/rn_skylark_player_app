import {
    ADD_TRACK_TO_USER_TRACK_LIST_FAILED,
    ADD_TRACK_TO_USER_TRACK_LIST_START,
    ADD_TRACK_TO_USER_TRACK_LIST_SUCCESS,
    DELETE_TRACK_BY_ID_SUCCESS,
    GET_TRACK_LIST_BY_USER_TOKEN_EMPTY,
    GET_TRACK_LIST_ERROR,
    TRACK_LIST_CLEAN,
    GET_TRACK_LIST_STARTED,
    GET_TRACK_LIST_SUCCESS,
    RESET_TRACK_TO_USER_TRACK_LIST_IS_ALREADY,
    TRACK_TO_USER_TRACK_LIST_IS_ALREADY,
    GET_LIST_OF_LIKED_TRACKS
} from "../constants/ActionsTypes";

const initialState = {
    loading: false,
    trackList: [],
    trackListIsEmpty: false,
    error: null,
    loadingAddTrackToTrackList: false,
    errorAddTrackToTrackList: null,
    trackInTrackListIsAlready: false,
    trackListLikes: [],
    listOfLikedTracks: []
};

export default function trackListReducer(state = initialState, action) {
    switch (action.type) {
        case GET_TRACK_LIST_STARTED:
            return {
                ...state,
                loading: true,
                trackListIsEmpty: false,
                error: null,
            };

        case GET_TRACK_LIST_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                trackList: [...action.payload],
                trackListIsEmpty: false,
            };

        case GET_TRACK_LIST_BY_USER_TOKEN_EMPTY:
            return {
                ...state,
                loading: false,
                trackListIsEmpty: true,
            };

        case GET_TRACK_LIST_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                trackListIsEmpty: false,
            };

        case TRACK_LIST_CLEAN:
            return {
                ...state,
                ...initialState,
            };

        case DELETE_TRACK_BY_ID_SUCCESS:
            return {
                ...state,
                trackList: state.trackList.filter(track => +track.id !== +action.payload),
            };

        case ADD_TRACK_TO_USER_TRACK_LIST_START:
            return {
                ...state,
                loadingAddTrackToTrackList: true,
                errorAddTrackToTrackList: null,
            };

        case TRACK_TO_USER_TRACK_LIST_IS_ALREADY:
            return {
                ...state,
                loadingAddTrackToTrackList: false,
                trackInTrackListIsAlready: true,
                errorAddTrackToTrackList: null,
            };

        case RESET_TRACK_TO_USER_TRACK_LIST_IS_ALREADY:
            return {
                ...state,
                trackInTrackListIsAlready: false,
                errorAddTrackToTrackList: null,
            };

        case ADD_TRACK_TO_USER_TRACK_LIST_SUCCESS:
            return {
                ...state,
                loadingAddTrackToTrackList: false,
                errorAddTrackToTrackList: null,
                trackInTrackListIsAlready: false,
            };

        case ADD_TRACK_TO_USER_TRACK_LIST_FAILED:
            return {
                ...state,
                loadingAddTrackToTrackList: false,
                errorAddTrackToTrackList: action.payload.error,
                trackInTrackListIsAlready: false,
            };

        case GET_LIST_OF_LIKED_TRACKS:
            return {
                ...state,
                listOfLikedTracks: action.payload
            };

        default:
            return state;
    }
}
