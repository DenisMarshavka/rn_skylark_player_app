import {
    ADD_TRACK_TO_PLAY_LIST_FAILED,
    ADD_TRACK_TO_PLAY_LIST_START,
    ADD_TRACK_TO_PLAY_LIST_SUCCESS,
    CREATE_PLAY_LIST_SUCCESS,
    DELETE_PLAY_LIST_SUCCESS,
    DELETE_PLAY_LIST_TRACK_SUCCESS,
    GET_PLAY_LIST_TRACKS_SUCCESS,
    GET_PLAY_LISTS_DATA_ERROR,
    GET_PLAY_LISTS_DATA_STARTED,
    GET_PLAY_LISTS_SUCCESS,
    GET_START_FETCH_PLAY_LIST_TRACKS_DATA,
    PLAY_LIST_TRACKS_EMPTY,
    PLAY_LISTS_EMPTY,
    RESET_PLAY_LIST_TRACKS_DATA,
} from "../constants/ActionsTypes";

const initialState = {
    loading: false,
    loadingAddTrack: false,
    playLists: [],
    playListTracks: [],
    error: null,
    errorAddTrack: null,
    isPlayListTracksEmpty: false,
    isPlayListsEmpty: false
};

export default function playListReduce(state = initialState, action) {
    switch (action.type) {
        case GET_START_FETCH_PLAY_LIST_TRACKS_DATA:
            return {
                ...state,
                loading: true,
            };

        case GET_PLAY_LISTS_DATA_STARTED:
            return {
                ...state,
                loading: true,
                isPlayListsEmpty: false,
                error: null,
            };

        case CREATE_PLAY_LIST_SUCCESS:
            return {
                ...state,
                loading: false,
                isPlayListsEmpty: false,
                error: null,
                // playLists: [...action.payload]
            };

        case GET_PLAY_LISTS_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                isPlayListsEmpty: false,
                playLists: [...action.payload]
            };

        case DELETE_PLAY_LIST_SUCCESS:
            return {
                ...state,
                playLists: state.playLists.filter(i => i.id !== action.payload)
            };

        case ADD_TRACK_TO_PLAY_LIST_START:
            return {
                ...state,
                loadingAddTrack: true,
                errorAddTrack: null,
            };

        case ADD_TRACK_TO_PLAY_LIST_SUCCESS:
            return {
                ...state,
                loadingAddTrack: false,
                playListTracks: [...state.playListTracks, action.payload]
            };

        case ADD_TRACK_TO_PLAY_LIST_FAILED:
            return {
                ...state,
                loadingAddTrack: false,
                errorAddTrack: action.payload.error,
            };

        case GET_PLAY_LIST_TRACKS_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                isPlayListTracksEmpty: false,
                playListTracks: [...action.payload]
            };

        case DELETE_PLAY_LIST_TRACK_SUCCESS:
            return {
                ...state,
                playListTracks: state.playListTracks.filter(i => i.id !== action.payload)
            };

        case PLAY_LIST_TRACKS_EMPTY:
            return {
                ...state,
                loading: false,
                isPlayListTracksEmpty: true,
            };

        case PLAY_LISTS_EMPTY:
            return {
                ...state,
                loading: false,
                isPlayListsEmpty: true,
            };

        case GET_PLAY_LISTS_DATA_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                isPlayListsEmpty: false,
            };

        case RESET_PLAY_LIST_TRACKS_DATA:
            return {
                ...state,
                loading: false,
                playListTracks: [],
                isPlayListsEmpty: false,
                error: null,
            };

        default:
            return state;
    }
}
