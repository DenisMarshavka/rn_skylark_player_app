import {
    GET_RADIO_DATA_ERROR,
    GET_RADIO_DATA_STARTED,
    GET_RADIO_GENRE_REQUEST_FAILED,
    GET_RADIO_GENRE_START,
    GET_RADIO_GENRE_SUCCESS,
    GET_RADIO_LIST_BY_MOOD_SUCCESS,
    GET_RADIO_MOODS_DATA_SUCCESS,
    GET_RADIO_MOODS_REQUEST_FAILED,
    GET_RADIO_RECOMMENDATIONS_STATIONS_DATA_STARTED,
    GET_RADIO_RECOMMENDATIONS_STATIONS_REQUEST_FAILED,
    GET_RADIO_RECOMMENDATIONS_STATIONS_SUCCESS,
    SET_RADIO_STATIONS_LISTENING_STARTED,
    SET_RADIO_STATIONS_LISTENING_SUCCESS,
    SET_RADIO_STATIONS_LISTENING_REQUEST_FAILED,
    GET_RADIO_STATIONS_DATA_STARTED,
    GET_RADIO_STATIONS_ERROR,
    GET_RADIO_STATIONS_REQUEST_FAILED,
    GET_RADIO_STATIONS_STARTED,
    GET_RADIO_STATIONS_SUCCESS,
    GET_SEARCH_RADIO_STATIONS_FAILED,
    GET_SEARCH_RADIO_STATIONS_START,
    GET_SEARCH_RADIO_STATIONS_SUCCESS,
    GET_START_RADIO_MOODS_REQUEST,
    RESET_SEARCH_RADIO_STATIONS,
    GET_RADIO_RECENTLY_STATIONS_DATA_STARTED,
    GET_RADIO_RECENTLY_STATIONS_SUCCESS,
    GET_RADIO_RECENTLY_STATIONS_REQUEST_FAILED, GET_RADIO_STATIONS_BY_GENRE_ID_SUCCESS,
} from "../constants/ActionsTypes";

const initialState = {
    loading: false,
    error: null,

    loadingRecommendations: false,
    errorRadioRecommendations: null,
    radioRecommendations: [],

    loadingRecently: false,
    errorRecently: null,
    radioRecently: [],

    loadingStations: false,
    radioStations: [],
    errorRadioStations: null,

    loadingGenreList: false,
    errorGenreList: null,
    genreList: [],

    moodsRadioLoading: false,
    moodsRadioList: [],
    moodsRadioError: null,

    searchRadioStations: [],
    radioStationsByMood: [],

    genreRadioStationsList: [],
};

export default function radioReducer(state = initialState, action) {
    switch (action.type) {
        case GET_RADIO_DATA_STARTED:
            return {
                ...state,
                loading: true,
                error: null,
            };


        case GET_RADIO_DATA_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload.error
            };

        case GET_RADIO_RECOMMENDATIONS_STATIONS_DATA_STARTED:
            return {
                ...state,
                loadingRecommendations: true,
                radioRecommendations: [],
            };

        case GET_RADIO_RECOMMENDATIONS_STATIONS_SUCCESS:
            return {
                ...state,
                loadingRecommendations: false,
                errorRadioRecommendations: null,
                radioRecommendations: [...action.payload],
            };

        case GET_RADIO_RECOMMENDATIONS_STATIONS_REQUEST_FAILED:
            return {
                ...state,
                loadingRecommendations: false,
                errorRadioRecommendations: action.payload.error,
                radioRecommendations: [],
            };


        case GET_RADIO_STATIONS_BY_GENRE_ID_SUCCESS:
            return {
                ...state,
                loadingStations: false,
                genreRadioStationsList: [...action.payload],
            };


        case SET_RADIO_STATIONS_LISTENING_STARTED:
            return {
                ...state,
            };

        case SET_RADIO_STATIONS_LISTENING_SUCCESS:
            return {
                ...state,
            };

        case SET_RADIO_STATIONS_LISTENING_REQUEST_FAILED:
            return {
                ...state,
            };

        case GET_RADIO_STATIONS_DATA_STARTED:
            return {
                ...state,
                loadingStations: true,
                // radioStations: [],
                errorRadioStations: null,
            };

        case GET_RADIO_STATIONS_SUCCESS:
            return {
                ...state,
                loadingStations: false,
                errorRadioStations: null,
                radioStations: [...action.payload]
            };

        case GET_RADIO_STATIONS_REQUEST_FAILED:
            return {
                ...state,
                loadingStations: false,
                errorRadioStations: action.payload.error,
            };

        case GET_RADIO_GENRE_START:
            return {
                ...state,
                loadingGenreList: true,
            };

        case GET_RADIO_GENRE_SUCCESS:
            return {
                ...state,
                loadingGenreList: false,
                errorGenreList: null,
                genreList: [...action.payload]
            };

        case GET_RADIO_GENRE_REQUEST_FAILED:
            return {
                ...state,
                loadingGenreList: false,
                errorGenreList: action.payload.error,
            };

        case GET_START_RADIO_MOODS_REQUEST:
            return {
                ...state,
                moodsRadioLoading: true,
            };

        case GET_RADIO_MOODS_DATA_SUCCESS:
            return {
                ...state,
                moodsRadioLoading: false,
                moodsRadioList: [...action.payload],
                moodsRadioError: null,
            };

        case GET_RADIO_MOODS_REQUEST_FAILED:
            return {
                ...state,
                moodsRadioLoading: false,
                moodsRadioError: action.payload.error,
            };

        case GET_SEARCH_RADIO_STATIONS_START:
            return {
                ...state,
                loading: true,
            };

        case GET_SEARCH_RADIO_STATIONS_SUCCESS:
            return {
                ...state,
                loading: false,
                searchRadioStations: [...action.payload],
            };

        case RESET_SEARCH_RADIO_STATIONS:
            return {
                ...state,
                loading: false,
                searchRadioStations: [],
                radioStationsByMood: [],
                error: null,
            };

        case GET_SEARCH_RADIO_STATIONS_FAILED:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
            };

        case GET_RADIO_LIST_BY_MOOD_SUCCESS:
            return {
                ...state,
                loading: false,
                radioStationsByMood: [...action.payload],
            };

        case GET_RADIO_RECENTLY_STATIONS_DATA_STARTED:
            return {
                ...state,
                loadingRecently: true,
                radioRecently: [],
            };

        case GET_RADIO_RECENTLY_STATIONS_SUCCESS:
            return {
                ...state,
                loadingRecently: false,
                errorRecently: null,
                radioRecently: [...action.payload],
            };

        case GET_RADIO_RECENTLY_STATIONS_REQUEST_FAILED:
            return {
                ...state,
                loadingRecently: false,
                errorRecently: action.payload.error,
                radioRecently: [],
            };

        default:
            return state;
    }
}
