import {
    GET_DATA_SEARCH_SCREEN_FAILED,
    START_FETCH_DATA_SEARCH_SCREEN_TRACK_LIST,
    GET_TRACK_LIST_SEARCH_SCREEN_SUCCESS, GET_RESET_SEARCH_SCREEN_DATA
} from "../constants/ActionsTypes";

const initialState = {
    loading: false,
    trackListSearchMusicPageId: 0,
    trackList: [],
    error: null
};

export const SearchMusicScreenReducer = (state = initialState, action) => {
    switch (action.type) {
        case START_FETCH_DATA_SEARCH_SCREEN_TRACK_LIST:
            return {
                ...state,
                loading: true,
            };

        case GET_TRACK_LIST_SEARCH_SCREEN_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                trackList: [...action.payload]
            };

        case GET_DATA_SEARCH_SCREEN_FAILED:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        case GET_RESET_SEARCH_SCREEN_DATA:
            return {
              ...state,
              trackList: [],
            };

        default: return state;
    }
};
