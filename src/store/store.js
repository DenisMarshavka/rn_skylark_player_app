import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { appReducer } from './appReducer'
import trackListReducer from "./TrackListReducer";
import searchMusicReducer from "./SearchMusicReducer";
import { authReducer } from './authReducer'
import singerReducer from "./SingerReducer";
import radioReducer from "./RadioReducer";
import playListReduce from "./PlayListReducer";
import {SearchMusicScreenReducer} from "./SearchMusicScreenReducer";
import { playerReducer } from "./playerReducer";
import { radioPlayerReducer } from "./radioPlayerReducer";
import { downloadReducer } from "./downloadReducer";
import { miniPlayerReducer } from "./miniPlayerReducer";

const mainAppReducer = combineReducers({
    appReducer,
    trackListReducer,
    searchMusicReducer,
    singerReducer,
    radioReducer,
    playListReduce,
    authReducer,
    SearchMusicScreenReducer,
    playerReducer,
    radioPlayerReducer,
    downloadReducer,
    miniPlayerReducer,
});

const rootReducer = (state, action) => {
    if (action.type === 'USER_LOGOUT') {
      state = undefined
    }
    return mainAppReducer(state, action)
};

let store = createStore(rootReducer, {}, compose(
    applyMiddleware( thunkMiddleware),

));

export default store;
