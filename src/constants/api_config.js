export const API_URL = 'https://skylarkmusic.ru/api/'

export const SHARE_URL = 'https://skylarkmusic.ru/'

export const SUBSCRIPTION_SKU = 'com.skylark.subscription'