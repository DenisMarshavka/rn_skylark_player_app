
export const COLORS = {
    white: '#fff',
    black: '#000',
    yellow: '#FFC42E',
    purple: '#7B3DFF',
    grey: '#A4A4A4',
    darkgrey: '#6A6A6A',
    lightgrey: '#EAEAEA',

    opacityBlack: 'rgba(255, 255, 255, 0.1)'
};
