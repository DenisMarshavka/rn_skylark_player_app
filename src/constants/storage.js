import { Platform } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

const dir = Platform.OS === 'android' ? RNFetchBlob.fs.dirs.MusicDir : RNFetchBlob.fs.dirs.DocumentDir;

export const PLAYER_CONFIG_REPEAT = 'PLAYER_CONFIG_REPEAT';
export const PLAYER_CONFIG_SHUFFLE = 'PLAYER_CONFIG_SHUFFLE';

export const DOWNLOAD_TRACKS_DATA = 'DOWNLOAD_TRACKS_DATA';
export const DOWNLOAD_TRACKS_SESSION_KEY = 'skylark-tracks';

export const DOWNLOAD_DIR = dir + '/skylark/';
